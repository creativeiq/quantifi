/****** Object:  Table [dbo].[quantifi_ImageLinks]    Script Date: 02/11/2010 09:07:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quantifi_ImageLinks](
[ItemID] [int] IDENTITY(0,1) NOT NULL,
[ModuleID] [int] NOT NULL,
[Title] [nvarchar](255) NULL,
[Url] [nvarchar](255) NULL,
[ImageUrl] [nvarchar](255) NULL,
[Target] [nvarchar](20) NOT NULL CONSTRAINT [DF_Quantifi_Links_Target]  DEFAULT ('_blank'),
[ViewOrder] [int] NULL,
[Description] [ntext] NULL,
[CreatedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[ItemGuid] [uniqueidentifier] NULL,
[ModuleGuid] [uniqueidentifier] NULL,
[UserGuid] [uniqueidentifier] NULL,
[PubDate] [datetime] NULL,
[Location] [nvarchar](255) NOT NULL,
CONSTRAINT [PK_Quantifi_Links] PRIMARY KEY NONCLUSTERED
(
[ItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[quantifi_ImageLinks]  WITH NOCHECK ADD  CONSTRAINT [FK_Quantifi_Links_Modules] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[mp_Modules] ([ModuleID])
ON DELETE CASCADE
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[quantifi_ImageLinks] CHECK CONSTRAINT [FK_Quantifi_Links_Modules]

/*** Create Stored Procedures ***/

/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_Delete]    Script Date: 02/11/2010 09:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[quantifi_ImageLinks_Delete]


@ItemID int


AS

DELETE FROM
quantifi_ImageLinks

WHERE
ItemID = @ItemID

/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_DeleteByModule]    Script Date: 02/11/2010 09:10:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[quantifi_ImageLinks_DeleteByModule]


@ModuleID int


AS

DELETE FROM [dbo].quantifi_ImageLinks

WHERE ModuleID = @ModuleID


/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_DeleteBySite]    Script Date: 02/11/2010 09:11:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[quantifi_ImageLinks_DeleteBySite]


@SiteID int


AS

DELETE FROM [dbo].quantifi_ImageLinks

WHERE ModuleID IN (SELECT ModuleID FROM mp_Modules WHERE SiteID = @SiteID)



/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_GetCount]    Script Date: 02/11/2010 09:11:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[quantifi_ImageLinks_GetCount]

@ModuleID int

AS

SELECT COUNT(*)
FROM [dbo].[quantifi_ImageLinks]
WHERE ModuleID = @ModuleID


/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_Insert]    Script Date: 02/11/2010 09:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[quantifi_ImageLinks_Insert]

/*
Author:   			Joe Audette
Created: 			12/24/2004
Last Modified: 		2008-01-27

*/


@ItemGuid	uniqueidentifier,
@ModuleGuid	uniqueidentifier,
@ModuleID int,
@Title nvarchar(255),
@Url nvarchar(255),
@ImageUrl nvarchar(255),
@Location nvarchar(255),
@PubDate datetime,
@ViewOrder int,
@Description ntext,
@CreatedDate datetime,
@CreatedBy int,
@Target nvarchar(20),
@UserGuid	uniqueidentifier


AS

INSERT INTO 	[dbo].[quantifi_ImageLinks]
(
ItemGuid,
ModuleGuid,
[ModuleID],
[Title],
[Url],
[ImageUrl],
[Location],
[PubDate],
[ViewOrder],
[Description],
[CreatedDate],
[CreatedBy],
Target,
UserGuid
)

VALUES
(
@ItemGuid,
@ModuleGuid,
@ModuleID,
@Title,
@Url,
@ImageUrl,
@Location,
@PubDate,
@ViewOrder,
@Description,
@CreatedDate,
@CreatedBy,
@Target,
@UserGuid

)
SELECT @@IDENTITY
  

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[quantifi_ImageLinks_Select]


@ModuleID int,
@FromDate datetime

AS

SELECT	*

FROM
quantifi_ImageLinks

WHERE
ModuleID = @ModuleID



ORDER BY
PubDate desc, ViewOrder, Title



/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_SelectByPage]    Script Date: 02/11/2010 09:12:41 ******/
SET ANSI_NULLS OFF




/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_SelectByPage]    Script Date: 02/11/2010 09:12:41 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



CREATE PROCEDURE [dbo].[quantifi_ImageLinks_SelectByPage]

@SiteID		int,
@PageID		int

AS
SELECT  	u.*,
m.ModuleTitle,
m.ViewRoles,
md.FeatureName

FROM		quantifi_ImageLinks u
JOIN		mp_Modules m
ON		u.ModuleID = m.ModuleID

JOIN		mp_ModuleDefinitions md
ON		m.ModuleDefID = md.ModuleDefID

JOIN		mp_PageModules pm
ON			pm.ModuleID = m.ModuleID

JOIN		mp_Pages p
ON		p.PageID = pm.PageID

WHERE	p.SiteID = @SiteID
AND pm.PageID = @PageID


/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_SelectOne]    Script Date: 02/11/2010 09:13:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[quantifi_ImageLinks_SelectOne]


@ItemID int

AS

SELECT
*

FROM
quantifi_ImageLinks

WHERE
ItemID = @ItemID


/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_SelectPage]    Script Date: 02/11/2010 09:13:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[quantifi_ImageLinks_SelectPage]


@ModuleID int,
@PageNumber int,
@PageSize int

AS

DECLARE @PageLowerBound int
DECLARE @PageUpperBound int


SET @PageLowerBound = (@PageSize * @PageNumber) - @PageSize
SET @PageUpperBound = @PageLowerBound + @PageSize + 1


CREATE TABLE #PageIndex
(
IndexID int IDENTITY (1, 1) NOT NULL,
ItemID Int
)

BEGIN

INSERT INTO #PageIndex (
ItemID
)

SELECT
[ItemID]

FROM
[dbo].[quantifi_ImageLinks]

WHERE
ModuleID = @ModuleID

ORDER BY
ViewOrder, Title

END


SELECT
t1.*

FROM
[dbo].[quantifi_ImageLinks] t1

JOIN			#PageIndex t2
ON
t1.[ItemID] = t2.[ItemID]

WHERE
t2.IndexID > @PageLowerBound
AND t2.IndexID < @PageUpperBound
		
ORDER BY t2.IndexID

DROP TABLE #PageIndex




/****** Object:  StoredProcedure [dbo].[quantifi_ImageLinks_Update]    Script Date: 02/11/2010 09:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[quantifi_ImageLinks_Update]

 
@ItemID int, 
@ModuleID int, 
@Title nvarchar(255), 
@Url nvarchar(255),
@ImageUrl nvarchar(255),
@Location nvarchar(255),
@PubDate datetime,  
@ViewOrder int, 
@Description ntext, 
@CreatedDate datetime, 
@CreatedBy int ,
@Target nvarchar(20)


AS

UPDATE 		[dbo].[quantifi_ImageLinks] 

SET
			[ModuleID] = @ModuleID,
			[Title] = @Title,
			[Url] = @Url,
			[ImageUrl] = @ImageUrl,
			[Target] = @Target,
			[Location]= @Location,
			[PubDate] = @PubDate,
			[ViewOrder] = @ViewOrder,
			[Description] = @Description,
			[CreatedDate] = @CreatedDate,
			[CreatedBy] = @CreatedBy
			
WHERE
			[ItemID] = @ItemID


