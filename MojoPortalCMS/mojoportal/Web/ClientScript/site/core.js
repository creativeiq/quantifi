$(document).ready(function() {

	var scrollbarWidth = $.scrollbarWidth();

	// Lightbox galleries
	if ($('.gallery.lightbox').length) {

		$('.gallery.lightbox').each(function() {
			$(this).find('a').tosrus({
				caption: {
					add:true
				},
			});
		});
	}

	// Responsive videos
	if ($('.editor, .video-player').length) {
		$('.editor, .video-player').fitVids();
	}

	// Sticky header
	if ($('.fixed-header').length) {

		var $header = $(".fixed-header");
		var $placeholder = $('<div class="fixed-header-placeholder" style="height:0px;"></div>').insertBefore($header);
		
		$header.affix({
			target: window,
			offset: {
				top: function() {
					return 87;
				}
			}
		}).on('affix.bs.affix', function() {
			$placeholder.height($header.height());
		}).on('affix-top.bs.affix', function() {
			$placeholder.height(0);
		});

	}

	// Sticky sidenav
	if ($('.fixed-page-nav').length) {

		var $document = $(document),
			$sidenav = $(".fixed-page-nav"),
			$bottom = $('.sidenav-bottom'),
			$top = $('.sidenav-top');
		
		$sidenav.affix({
			target: window,
			offset: {
				top: function() {
					return $top.offset().top - 32;
				},
				bottom: function() {
					return $document.height() - $bottom.offset().top - $sidenav.height() + 32;
				}
			}
		}).on('affix-bottom.bs.affix', function() {
			$sidenav.prependTo($bottom);
		}).on('affix-top.bs.affix', function() {
			$sidenav.prependTo($top);
		});

		$('body').scrollspy({
			offset: 36,
			target: '.fixed-page-nav'
		}).on('activate.bs.scrollspy', function (e) {
			$sidenav.find('li').removeClass('passed').slice(0, $(e.target).index()).addClass('passed');
		});

		$sidenav.on('click', 'a', function(e) {
			var selector = $(e.currentTarget).attr('href');
			var offset = $(selector).offset();
			scrollPageTo(offset.top - 35, 1000);
			return false;
		});

		$(window).on('resize', checkSidenavSizes);
		checkSidenavSizes();

		function checkSidenavSizes() {

			var maxw = 0;

			$sidenav.find('a').each(function() {
				var elw = $(this).css('width', '').width();
				if (elw > maxw) {
					maxw = elw;
				}
			}).width(maxw);
			
		}

	}

	// Equal height groups
	heightGroups = new EqualHeightGroups();
	
	// Parallax effects
	if ($('html').hasClass('backgroundsize') && $('[data-stellar-background-ratio], [data-stellar-ratio]').length) {

		var stellarActive = false;
		$(window).on('resize', checkWindowSize);

		function checkWindowSize() {
			var ww = $(window).width() + scrollbarWidth;
			if (ww < 768) {
				if (stellarActive) {
					stellarActive = false;
					$.stellar('destroy');
				}
			} else {
				if (!stellarActive) {
					stellarActive = true;
					$.stellar({
						horizontalScrolling: false,
						responsive: true
					});
				}
			}
		}

		checkWindowSize();
	}

	// 'Back to top' links
	if ($('.back-to-top').length) {
		$(document).on('click', '.back-to-top', function() {
			scrollPageTo(0);
			return false;
		});
	}

	// 'Scroll to' links
	if ($('[data-scroll-to]').length) {
		$(document).on('click', '[data-scroll-to]', function(e) {
			var selector = $(e.currentTarget).attr('data-scroll-to');
			var offset = $(selector).offset();
			var offsetAdjust = parseInt($(e.currentTarget).attr('data-scroll-offset'));
			if (isNaN(offsetAdjust) || typeof(offsetAdjust) == 'undefined') {
				offsetAdjust = -35;
			}

			scrollPageTo(offset.top + offsetAdjust, 1000);
			return false;
		});
	}

	setTimeout(function() {
		$('.site-header .nav, .site-header li, .site-header .nav ul, .AspNet-Menu-Horizontal').attr('style', '');
	}, 10);

	// Main site dropdowns (for mobile)
	if ($('.site-header .subnav, .site-header .nav ul.dynamic').length) {

		$('.site-header .nav > li').each(function(i, el) {

			var $el = $(el),
				$subnav = $el.find('> .subnav, > ul.dynamic');

			if ($subnav.length) {
				$el.addClass('has-submenu');
			}

			$el.find('> a > span').append('<i class="fa fa-caret-down"></i><i class="fa fa-caret-right"></i>');

		});

		$('.site-header .nav').on('click', '.has-submenu > a', function(e) {
			console.log($(window).width() + scrollbarWidth);
			if ($(window).width() + scrollbarWidth <= 767) {
				var $li = $(e.currentTarget).parent();
				$li.toggleClass('expanded');
				return false;
			}

		});
	}

	// Flip tiles
	if ($('.tile').length) {

		$('.tile').each(function() {
			$(this).append('<div class="hit"></div>');
		})
		.on('mouseenter', function(e) {

			var $el = $(e.target);

			if ($el.prop('tagName').toLowerCase() == 'a'  || $el.parents('a').length) {
				return;
			}

			var $tile = $el.closest('.tile');
			$tile.addClass('flip');

		}).on('mouseleave', function(e) {

			var $el = $(e.target);

			if ($el.prop('tagName').toLowerCase() == 'a'  || $el.parents('a').length) {
				return;
			}

			var $tile = $el.closest('.tile');
			$tile.removeClass('flip');

		});

	}

	// Item toggles
	if ($('[data-toggle-height]').length) {

		$(document).on('click', '[data-toggle-height]', function(e) {

			var $el = $(e.currentTarget),
				$parent = $el.parent(),
				$target = $parent.find($el.attr('data-toggle-height')),
				timeoutId = $target.data('timeoutId');

			clearTimeout(timeoutId);

			if (!$parent.hasClass('open')) {

				var newHeight = getTargetHeight();
				timeoutId = setTimeout(function() {

					$target.css('height', newHeight);
					timeoutId = setTimeout(function() {
						$target.css('height', '');
					}, 300);
					$parent.toggleClass('open');

				}, 1);
				
			} else {

				$target.css('height', getTargetHeight());
				timeoutId = setTimeout(function() {
					$target.css('height', '');
				}, 1);
				$parent.toggleClass('open');

			}

			$target.data('timeoutId', timeoutId);

			return false;

			function getTargetHeight() {
				$target.css('height', 'auto');
				var h = $target.height();
				$target.css('height', '');
				return h;
			}

		});

	}

});

function scrollPageTo(offset, duration)
{
	offset = offset || 0;
	if (typeof(duration) == 'undefined' || duration == null) duration = 1500;

	if (duration > 0) {
		$('html, body').stop(true, false).animate({ scrollTop:offset }, { easing:'easeInOutCubic', duration:duration });
	} else {
		$('html, body').scrollTop(offset);
	}
}

// ------------------------------------------------------------------------------------------------------
// CONTACT MAP
/*
ContactMap = function(data)
{
	var markerData = [
		{ "x":402, "y":172, "labelX":80, "labelY":-100, "text":"London" },
		{ "x":0, "y":0, "labelX":0, "labelY":0, "text":"Frankfurt" },
		{ "x":0, "y":0, "labelX":0, "labelY":0, "text":"Paris" },
		{ "x":0, "y":0, "labelX":0, "labelY":0, "text":"New York" },
		{ "x":0, "y":0, "labelX":0, "labelY":0, "text":"Summit, NJ" },
		{ "x":0, "y":0, "labelX":0, "labelY":0, "text":"Sydney" }
	];

	var svg = d3.select(".contact-map")
		.append("svg")
		.attr("width", "100%")
		.attr("viewBox", "0 0 830 460");

	var projection = d3.geo.kavrayskiy7()
		.center([35, 22])
		.scale(150),

	    color = d3.scale.category20(),
	    graticule = d3.geo.graticule();

	var path = d3.geo.path()
	    .projection(projection);

	d3.json("/assets/json/world-110m2.json", function(error, world) {

		var countries = topojson.feature(world, world.objects.countries).features,
		neighbors = topojson.neighbors(world.objects.countries.geometries);

		svg.selectAll(".country")
			.data(countries)
			.enter().insert("path", ".graticule")
			.attr("class", "country")
			.attr("d", path);

		var markersGroup = svg.append("g");
		var markers = markersGroup.selectAll('g')
			.data(markerData)
			.enter()
			.append("g")
			.attr("class", "marker")
			.attr("transform", function(d) { return "translate("+d.x+","+d.y+")"; });

		var lines = markers
			.append("line")
			.attr("x1", 0)
			.attr("y1", 0)
			.attr("x2", function(d) { return d.labelX; })
			.attr("y2", function(d) { return d.labelY; });

		var circles = markers
			.append("circle")
			.attr("r", 3.5);

	});
}
*/

// ------------------------------------------------------------------------------------------------------
// BOX SLIDES

BoxSlidesDefaults = {

};

BoxSlides = function(opts)
{
	this.opts = $.extend(true, {}, EqualHeightGroups.defaults, opts || {});
	this.wrap = wrap || $('body');
}

// ------------------------------------------------------------------------------------------------------
// COMPANY TIMELINE

CompanyTimeline = function(events)
{
	var svg = d3.select(".company-timeline .graphic")
		.append("svg")
		.attr("width", "1213")
		.attr("height", "460")
		.attr("viewBox", "0 0 1213 460");

	var hits = d3.select(".company-timeline .graphic")
		.append("svg")
		.attr("width", "1213")
		.attr("height", "460")
		.attr("class", "hits")
		.attr("viewBox", "0 0 1213 460");

	var $wrap = $('.company-timeline .graphic');
	var $svgs = $('.company-timeline svg');
	var w = 1213;
	var h = 460;
	var types = ["#93c530", "#009aa6", "#eaab00", "#b70834", "#0075b0"];
	var vSpacing = 40;
	var vRange = 10;
	var hRange = 20;
	var lines = [];
	var crossovers = [];
	var numSegments = 2;

	for (var i = 0; i < events.length; i++) {
		var event = events[i],
			index = event.type-1;
		if (typeof(lines[index]) == 'undefined' || lines[index]) {
			lines[index] = { "color":types[index] };
		}
	}

	var centerY = 360;
	var startY = centerY - Math.round(((lines.length - 1) * vSpacing) / 2);

	svg.selectAll('circle')
		.data(crossovers)
		.enter()
		.append('circle')
		.attr('r', '6')
		.attr('fill', '#ff0000')
		.attr('cx', function(d) { return d.x; })
		.attr('cy', function(d) { return d.y; });

	for (var i = 0; i < lines.length; i++) {
		
		var cPoints = [];
		var cPointObjects = [];

		for (var j = 0; j < numSegments+1; j++) {

			var cX = Math.round(j * (w / numSegments));
			var reverse = (j % 2) === 1;
			var lineIndex = reverse ? ((lines.length-1) - i) : i;
			var cY = (startY + (lineIndex * vSpacing));

			// Randomise position of point
			if (j > 0 && j < numSegments) {
				//cX += Math.round((hRange * 2) * Math.random()) - hRange;
			}

			cPoints.push((j === 0 ? "M" : "L")+cX + "," + cY);
			cPointObjects.push({ x:cX, y:cY });
			
		}

		lines[i].points = cPointObjects;

		var cLine = svg
			.append("path")
			.attr("d", cPoints.join(" "))
			.attr("stroke", lines[i].color)
			.attr("stroke-width", "2.5")
			.attr("fill", "none");
	}

	var spacing = Math.round((w - 90) / (events.length-1));
	var v = Math.round(spacing * .2);
	var circles = [];
	var circleData = [];

	for (var i = 0; i < events.length; i++) {

		var cLine = Math.floor((i / events.length) * numSegments);
		var p = lines[events[i].type-1].points[cLine];
		var n = lines[events[i].type-1].points[cLine+1];
		var cX = ((i * spacing) + 45);

		if (i > 0 && i < events.length-1) {
			 cX += Math.round((v * 2) * Math.random()) - v;
		}

		var cY = p.y + (((cX - p.x) / (n.x - p.x)) * (n.y - p.y));
		var color = lines[events[i].type-1].color;
		var rX = w - cX - 30;
		var r = 8 + Math.round(Math.random() * 8);
		var boxLeft = rX - 215;
		if (boxLeft > -15) { boxLeft = -15; }
		if (boxLeft < -200) { boxLeft = -200; }
		
		var circle = svg.append('circle')
			.attr('r', r)
			.attr('cx', cX)
			.attr('cy', cY)
			.attr('fill', color);

		circles.push(circle);
		circleData.push({ r:r });

		hits.append('rect')
			.attr('x', cX - 25)
			.attr('y', centerY - 100)
			.attr('width', 50)
			.attr('height', 170)
			.attr('fill', '#ffffff')
			.attr("fill-opacity", '0');

		var $label = $('<div class="label'+(i == events.length-1 ? ' last' : '')+(i % 2 === 0 ? ' odd' : ' even')+'" style="left:'+cX+'px;bottom:'+(h - cY)+'px;">' +
			'<div class="box" style="background-color:'+color+';left:'+boxLeft+'px;"><p>'+events[i].text+'</p><a href="'+events[i].href+'">Click to read more</a></div>' +
			'<div class="line" style="background-color:'+color+';"></div>' +
		'</div>').appendTo($wrap);
	}

	var $labels = $('.company-timeline .graphic .label');

	hits
		.selectAll('rect')
		.data(circleData)
		.on("mouseover", function(d, i) {

			$labels.filter('.active').removeClass('active');

			$labels.eq(i).addClass('active');

			circles[i]
			.transition()
			.ease('cubic-out')
			.duration(220)
			.attr("r", d.r + 6);

		})
		.on("mouseout", function(d, i) {

			$labels.removeClass('active');

			circles[i]
			.transition()
			.ease('cubic-out')
			.duration(220)
			.attr("r", d.r);
			
		})
		.on("click", function(d, i) {
			window.location.href = events[i].href;
		});

	var $canvas = $('.company-timeline .graphic');
	var pW = 0;
	$(window).on('resize', onWindowResize);
	var labelPositions = [];

	$labels.each(function(i, el) {
		labelPositions[i] = {
			x: parseInt($(el).css('left').replace('px', '')),
			y: parseInt($(el).css('bottom').replace('px', ''))
		};
	});

	onWindowResize();

	function onWindowResize() {
		
		var cw = $canvas.width();
		$svgs.attr('width', cw);
		$svgs.attr('height', Math.round((h / w) * cw));
		if (cw == pW) { return; }
		var scale = cw / 1213;

		$labels.each(function(i, el) {
			$(el).css({
				left: Math.round(labelPositions[i].x * scale),
				bottom: Math.round(labelPositions[i].y * scale)
			})
		});

		pW = cw;
	}

	var $text = $('.company-timeline .text-version'),
		$textInner = $text.find('ul'),
		toggleTimeout = null;

	$('.company-timeline').on('click', '.toggle', function() {

		if ($text.hasClass('open')) {
			$textInner.css('height', getTextHeight());
			$text.removeClass('open');
			clearTimeout(toggleTimeout);
			toggleTimeout = setTimeout(function() {
				$textInner.css('height', '');
			}, 1);

		} else {
			var openHeight = getTextHeight();
			clearTimeout(toggleTimeout);
			toggleTimeout = setTimeout(function() {
				$text.addClass('open');
				$textInner.css('height', openHeight);
				toggleTimeout = setTimeout(function() {
					$textInner.css('height', '');
				}, 510);
			}, 1);
		}
		
		return false;
	});

	function getTextHeight() {
		var cHeight = $textInner.css('height');
		$textInner.css('height', 'auto');
		var result = $textInner.height();
		$textInner.css('height', cHeight);
		return result;
	}

	// Select the first one

	$labels.eq(0).addClass('active');

	circles[0]
	.transition()
	.ease('cubic-out')
	.duration(220)
	.attr("r", circleData[0].r + 6);

}


// ------------------------------------------------------------------------------------------------------
// EXPLORE INTERACTIVE GRAPHIC

ExploreGraphic = function(data, topLeftText, topRightText)
{
	var svgContainer = d3.select("#explore .canvas")
		.append("svg")
		.attr("width", "100%")
		.attr("viewBox", "0 0 830 525");

	var hitContainer = d3.select("#explore .canvas")
		.append("svg")
		.classed("hits", true)
		.attr("width", "100%")
		.attr("viewBox", "0 0 830 525");

	var defs = svgContainer.append("defs");

	var filter = defs.append("filter")
	    .attr("id", "drop-shadow");

	filter.append("feGaussianBlur")
	    .attr("in", "SourceAlpha")
	    .attr("stdDeviation", 5);

	var feOffset = filter.append("feOffset")
	    .attr("dx", 0)
	    .attr("dy", 0)
	    .attr("result", "offsetblur");

	var feFlood = filter.append("feFlood")
	    .attr("flood-color", 'rgba(0,0,0,.3)');

	var feComposite = filter.append('feComposite')
		.attr('in2', 'offsetblur')
		.attr('operator', 'in');

	var feMerge = filter.append("feMerge");

	feMerge.append("feMergeNode");
	feMerge.append("feMergeNode")
	    .attr("in", "SourceGraphic");

	var leftText = svgContainer
		.append('text')
		.attr("font-family", "Avenir Roman")
		.attr("font-size", "18px")
		.attr("fill", "#616365")
		.attr("text-anchor", "middle")
		.attr("transform", "translate(74,120) rotate(-14)");
	var leftTextLines = topLeftText.split("\n");

	for (var i = 0; i < leftTextLines.length; i++) {
		var tspan = leftText.append('tspan')
		.text(leftTextLines[i])
		.attr('x', 0).attr('dy', '24');
		if (i == 0) {
			tspan.attr("font-size", "25px");
		}
	}

	var rightText = svgContainer
		.append('text')
		.attr("font-family", "Avenir Roman")
		.attr("font-size", "18px")
		.attr("fill", "#616365")
		.attr("text-anchor", "middle")
		.attr("transform", "translate(740,120) rotate(14)");
	var rightTextLines = topRightText.split("\n");

	for (var i = 0; i < rightTextLines.length; i++) {
		var tspan = rightText.append('tspan')
		.text(rightTextLines[i])
		.attr('x', 0).attr('dy', '24');
		if (i == 0) {
			tspan.attr("font-size", "25px");
		}
	}
	
	var groupsOrdered = [];
	var groups = svgContainer
		.selectAll("g")
		.data(data)
		.enter()
		.append('g')
		.attr("transform", function(d) { return "translate("+d.x+","+d.y+") rotate("+d.rotation+") scale(1)"; })
		.attr("cx", function(d) { return d.x })
		.attr("cy", function (d) { return d.y; })
		.each(function(d, i) {
			if (d.target) {
				d3.select(this).classed('hover', true);
			}
		});

	groups
		.filter('.hover')
		.each(function(d, i) {
			groupsOrdered[i] = this;
		});

	var radiusOffset = 0;

	function drawPolygon(d) {
		var points = [],
		    part = 60,
		    rot = d.rotation,
		    r = d.radius + radiusOffset;

		for (var i = 0; i < 6; i++)
		{
			var a = (i * part),
			    x = r * Math.cos(a * Math.PI / 180),
			    y = r * Math.sin(a * Math.PI / 180);

			points.push(x + "," + y);
		}

		return points.join(" ");
	}
	
	var polygons = groups.append("polygon")
		.attr("points", drawPolygon)
		.attr("stroke", function (d) { return d.stroke; })
		.attr("stroke-opacity", function(d) { return d.opacity; })
		//.attr("fill-opacity", function(d) { return d.opacity; })
		.style("fill", function(d) { return d.bg; })
		.each(function(d) {
			if (d.target) {
				d3.select(this).classed('interactive', true);
			}
		});

	groups.each(function(d) {

		var el = d3.select(this);

		if (d.image) {
			el.append('image')
			.attr('xlink:href', d.image.href)
			.attr('transform', 'translate('+d.image.x+','+d.image.y+') rotate('+(d.image.rotation || 0)+','+(d.image.width/2)+','+(d.image.height/2)+')')
			.attr('width', d.image.width)
			.attr('height', d.image.height);
		}

	});

	var text = groups.append("text")
		.attr("font-family", "Avenir Medium")
		.attr("font-size", "18px")
		.attr("text-anchor", "middle")
		.attr("transform", function(d) { return "rotate("+(d.textRotation || 0)+")"; })
		.attr("fill", function(d) { return d.textColor; })
		.each(function (d) {

			if (d.text) {

				var el = d3.select(this);
				var lines = (d.text || '').split('\n');

				for (var i = 0; i < lines.length; i++) {
					var tspan = el.append('tspan').text(lines[i]);
					if (i > 0)
						tspan.attr('x', 0).attr('dy', '24');
				}

				el.attr("y", -((lines.length * 24) / 2) + 16);

			}

		});

	radiusOffset = 5;
	var hits = hitContainer
		.selectAll("g")
		.data(data)
		.enter()
		.append('g')
		.attr("transform", function(d) { return "translate("+d.x+","+d.y+") rotate("+d.rotation+") scale(1)"; })
		.attr("cx", function(d) { return d.x })
		.attr("cy", function (d) { return d.y; })
		.append("polygon")
		.attr("points", drawPolygon)
		.attr("fill-opacity", '0')
		.style("fill", '#ffffff')
		.each(function(d) {
			if (d.target) {
				d3.select(this).classed('hover', true);
			}
		});
	radiusOffset = 0;

	hits
		.filter('.hover')
		.on("mouseover", function(d, i) {

			var group = groupsOrdered[i];
			group.parentNode.appendChild(group);

			d3.select(group)
			.classed('active', true)
			.selectAll('polygon')
			.style("filter", "url(#drop-shadow)")
			.transition()
			.ease('cubic-out')
			.duration(220)
			.attr("transform", function(d) {
				var scale = (parseFloat(d.radius) + 10) / parseFloat(d.radius);
				return "scale("+scale+")";
			});

			$(d.target).addClass('active');

			clearTimeout(pulseInterval);

		})
		.on("mouseout", function(d, i) {

			var group = groupsOrdered[i];

			d3.select(group)
			.classed('active', false)
			.selectAll('polygon')
			.style("filter", null)
			.transition()
			.ease('cubic-out')
			.duration(300)
			.attr("transform", function(d) {
				return "scale(1)";
			});

			$(d.target).removeClass('active');

		});

	var $boxes = $('.home-section.explore .box');
	var $canvas = $('.home-section.explore .canvas');
	var pW = 0;
	$(window).on('resize', onWindowResize);
	var boxPositions = [];

	$boxes.each(function(i, el) {
		boxPositions[i] = {
			x: parseInt($(el).css('left').replace('px', '')),
			y: parseInt($(el).css('top').replace('px', ''))
		};
	});

	onWindowResize();

	function onWindowResize() {
		
		var w = $canvas.width();
		if (w == pW) { return; }
		var scale = w / 913;

		$boxes.each(function(i, el) {
			$(el).css({
				left: Math.round(boxPositions[i].x * scale),
				top: Math.round(boxPositions[i].y * scale)
			})
		});

		pW = w;
	}

	var pulseTimeout = null;
	var pulseInterval = setInterval(pulse, 5000);

	function pulse() {

		clearTimeout(pulseTimeout);

		groups.each(function(d) {
			d3.select(this)
			.selectAll('polygon.interactive')
			.transition()
			.ease('cubic-out')
			.duration(250)
			.attr("transform", "scale(1.04)");
		});

		pulseTimeout = setTimeout(function() {

			groups.each(function(d) {
				d3.select(this)
				.selectAll('polygon.interactive')
				.transition()
				.ease('cubic-out')
				.duration(350)
				.attr("transform", "scale(1)");
			});

		}, 260);

	}

}

// ------------------------------------------------------------------------------------------------------
// EQUAL HEIGHT GROUPS

EqualHeightGroups = function(wrap, opts)
{
	this.opts = $.extend({}, EqualHeightGroups.defaults, opts || {});
	this.wrap = wrap || $('body');
	this.initted = false;
	
	if (!this.wrap.find('[data-height-group]').length) return;

	var groups = {};

	this.wrap.find('[data-height-group]').each(function() {

		var groupId = $(this).attr('data-height-group');
		if (groups.hasOwnProperty(groupId)) return;

		groups[groupId] = $('[data-height-group="'+groupId+'"]');

	});

	this.groups = groups;

	$(window).on('load resize', $.proxy(this.onResize, this));
	//this.onResize();

}

EqualHeightGroups.defaults = {};

EqualHeightGroups.prototype.processGroup = function(groupId)
{
	var group = this.groups[groupId].height('auto'),
		lines = {};

	group.each(function(i, el) {
		var cPos = $(el).offset().top+'';
		if (!lines.hasOwnProperty(cPos)) lines[cPos] = [];
		lines[cPos].push(el);
	});

	for (var p in lines) {
		var line = $(lines[p]).map(function() { return $(this).toArray(); } ),
			master = line.filter('[data-height-group-master="1"]');

		if (line.length == 1) {
			line.css('height', 'auto');
			continue;
		}

		if (master.length) {
			line.css('height', master.outerHeight());
			continue;
		}

		var maxHeight = Math.max.apply(null, line.map(function() {
			var height = $(this).outerHeight();
			return height;
		}).get());
		line.css('height', maxHeight);
	}

	if (!this.initted) {
		group.trigger('equalHeightsInit');
	}
}

EqualHeightGroups.prototype.onResize = function()
{
	if (this.throttleResize == true) {
		this.resizeAttempts++;
		return;
	}

	for (var p in this.groups) {
		this.processGroup(p);
	}

	this.initted = true;

	this.throttleResize = true;
	this.resizeAttempts = 0;
	clearTimeout(this.resizeTimeout || null);
	setTimeout($.proxy(function() {
		this.throttleResize = false;
		if (this.resizeAttempts > 0) {
			this.onResize();
		}
	}, this), 500);
}