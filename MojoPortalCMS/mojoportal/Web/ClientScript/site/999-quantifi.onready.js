﻿
/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />
/// <reference path="010-jquery.utils.js" />
/// <reference path="001-quantifi.core.js" />


$("#topnav").ready(function () {

  var topNavUL = $("#topnav").find("ul.AspNet-Menu");
  topNavUL.find("li:first").addClass("first");
  topNavUL.find("li:last").addClass("last");

});



$(document).ready(function () {

  $("#wrapfooter").hover(
    function () { $(this).find("span.userSignIn").addClass("showSignIn"); },
    function () {
      $(this).find("span.userSignIn").removeClass("showSignIn");
    });

  $(".mojo-accordion-nh").accordion({ collapsible: true, autoHeight: false });
  $(".mojo-accordion-nh-closed").accordion({ active: -1, collapsible: true, autoHeight: false });

  function HideMenuToolbar() {
    $("#toolbar").fadeOut();
    $("#toolbarbut").fadeIn("slow");
  }

  function ShowMenuToolbar() {
    $("#toolbar").fadeIn();
    $("#toolbarbut").fadeOut("slow");
  }

  $("span.downarr a").click(function () {
    HideMenuToolbar();
    Set_Cookie('openstate', 'closed');
  });

  $("span.showbar a").click(function () {
    ShowMenuToolbar();
    Set_Cookie('openstate', 'open');
  });

  $("span.downarr a, span.showbar a").click(function () { return false; });

  var openState = Get_Cookie('openstate');
  if (openState != null) {
    if (openState == 'closed') {
      HideMenuToolbar();
    }
    if (openState == 'open') {
      ShowMenuToolbar();
    }
  }


  $('body.q-default-skin #divRight').jScroll({ top: 20, speed: 2000 });

  $("body.q-default-skin #divRight").ready(function () {
    var firstChild = $("body.q-default-skin #divRight").children().first();

    if (firstChild.length && firstChild.has("h2.moduletitle").length == 0) {
      firstChild.prepend('<div class="topColumnSpacer"></div>');
    }
  });

  // Add event tracking to all external links
  try {
    $('a:external').click(function (event) {
      event.preventDefault();
      Q.T.recordOutboundLink(this, "External-Link");
    });
  } catch (error) {
    Q.logError(error);
  }

});






 