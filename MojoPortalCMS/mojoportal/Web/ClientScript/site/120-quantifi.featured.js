﻿/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />
/// <reference path="001-quantifi.core.js" />
/// <reference path="050-quantifi.tracking.js" />
/// <reference path="100-quantifi.controls.js" />

Q.Controls.FeaturedRotator = function (parentId, changeSpeed, animationSpeed) {
  var fadingOut = null;
  var fadingIn = null;

  var wrapper = $('#' + parentId);
  var header = wrapper.find(".header");


  if (changeSpeed === undefined)
    changeSpeed = 4000;
  if (animationSpeed === undefined)
    animationSpeed = 1000;


  var featured = $('.hp-featured');
  var settings = {
    activeIndex: 0,
    maxIndex: 0,
    maxAutoCycles: 20,
    autoCycles: 0,
    cycleTime: 6000,
    baseOpacity: '0.5',
    slideIntervalId: null
  };

  featured.find(".mask").css('opacity', '.75');
  var features = featured.find(".feature-item");

  features.each(function (index, value) {
    if (index != settings.activeIndex) {
      $(value).hide();
    }
    settings.maxIndex = index;
  });


  function togglePause(btn) {
    if (settings.slideIntervalId == null) {
      $(btn).removeClass('play');
      setTimer();
    } else {
      $(btn).addClass('play');
      clearInterval(settings.slideIntervalId);
      settings.slideIntervalId = null;
    }
  }

  function setTimer(reset) {
    if (reset != null) {
      settings.autoCycles = 0;
      if (settings.slideIntervalId != null)
        clearInterval(settings.slideIntervalId);
    }

    settings.slideIntervalId = setInterval(function () {
      showNext();
      if (settings.maxAutoCycles == settings.autoCycles) {
        clearInterval(settings.slideIntervalId);
        settings.slideIntervalId = null;
      }
    }, settings.cycleTime);
  }

  function showNext(resetTimer) {
    if (resetTimer) {
      if (settings.slideIntervalId != null)
        clearInterval(settings.slideIntervalId);
    }
    var nextIndex = settings.activeIndex + 1;
    if (nextIndex > settings.maxIndex) {
      nextIndex = 0;
    }
    showFeature(nextIndex);
  }

  function showPrev(reset) {
    if (reset) {
      resetAnimation();
    }
    var nextIndex = settings.activeIndex - 1;
    if (nextIndex < 0) {
      nextIndex = settings.maxIndex;
    }
    showFeature(nextIndex);
  }

  function resetAnimation() {
    if (settings.slideIntervalId != null)
      clearInterval(settings.slideIntervalId);

    if (fadingOut != null) {
      $(fadingOut).stop(true, true);
    }

    if (fadingIn != null) {
      $(fadingIn).stop(true, true);
    }
  }

  function showFeature(toIndex) {
    var current = $(features[settings.activeIndex]);
    var next = $(features[toIndex]);

    fadingOut = current.fadeOut(700, function () {
      fadingOut = null;
    });
    fadingIn = next.fadeIn(700, function () {
      settings.activeIndex = toIndex;
      settings.autoCycles++;
      fadingIn = null;
    });


  }


  if (settings.maxIndex < 1) {
    $(header).find('.mediabtn').hide();
  } else {
    $(header).find('.mediabtn')
      .css('opacity', settings.baseOpacity)
      .hover(function () { $(this).css('opacity', '1'); },
        function () {
          $(this).css('opacity', settings.baseOpacity);
        });
    header.find('.prev').click(function () { showPrev(true); });
    header.find('.pause').click(function () { togglePause(); });
    header.find('.next').click(function () { showNext(true); });

    setTimer();
  }

  wrapper.find(".track-link").click(function (event) {
    event.preventDefault();

    var linkUrl = this.href;
    var title = $(this).attr("title");
    var val = settings.autoCycles;
    var id = $(this).parents("div.feature-item").first().attr("itemid");


    _gaq.push(['_trackEvent', 'Featured-Rotator', "click", id + "-" + title, settings.autoCycles]);


    if ($(this).attr("target") == "_blank") {
      setTimeout(function () { window.open(linkUrl, "_blank"); }, 100);
    } else {
      setTimeout('document.location = "' + linkUrl + '"', 100);
    }

  });

}


//function QFeaturedSetup(parentId, changeSpeed, animationSpeed) {
//  var fadingOut = null;
//  var fadingIn = null;

//  var wrapper = $('#' + parentId);
//  var header = wrapper.find(".header");


//  if (changeSpeed === undefined)
//    changeSpeed = 4000;
//  if (animationSpeed === undefined)
//    animationSpeed = 1000;


//  var featured = $('.hp-featured');
//  var settings = {
//    activeIndex: 0,
//    maxIndex: 0,
//    maxAutoCycles: 20,
//    autoCycles: 0,
//    cycleTime: 6000,
//    baseOpacity: '0.5',
//    slideIntervalId: null
//  };

//  featured.find(".mask").css('opacity', '.75');
//  var features = featured.find(".feature-item");

//  features.each(function(index, value) {
//    if (index != settings.activeIndex) {
//      $(value).hide();
//    }
//    settings.maxIndex = index;
//  });


//  function togglePause(btn) {
//    if (settings.slideIntervalId == null) {
//      $(btn).removeClass('play');
//      setTimer();
//    } else {
//      $(btn).addClass('play');
//      clearInterval(settings.slideIntervalId);
//      settings.slideIntervalId = null;
//    }
//  }

//  function setTimer(reset) {
//    if (reset != null) {
//      settings.autoCycles = 0;
//      if (settings.slideIntervalId != null)
//        clearInterval(settings.slideIntervalId);
//    }

//    settings.slideIntervalId = setInterval(function() {
//      showNext();
//      if (settings.maxAutoCycles == settings.autoCycles) {
//        clearInterval(settings.slideIntervalId);
//        settings.slideIntervalId = null;
//      }
//    }, settings.cycleTime);
//  }

//  function showNext(resetTimer) {
//    if (resetTimer) {
//      if (settings.slideIntervalId != null)
//        clearInterval(settings.slideIntervalId);
//    }
//    var nextIndex = settings.activeIndex + 1;
//    if (nextIndex > settings.maxIndex) {
//      nextIndex = 0;
//    }
//    showFeature(nextIndex);
//  }

//  function showPrev(reset) {
//    if (reset) {
//      resetAnimation();
//    }
//    var nextIndex = settings.activeIndex - 1;
//    if (nextIndex < 0) {
//      nextIndex = settings.maxIndex;
//    }
//    showFeature(nextIndex);
//  }

//  function resetAnimation() {
//    if (settings.slideIntervalId != null)
//      clearInterval(settings.slideIntervalId);

//    if (fadingOut != null) {
//      $(fadingOut).stop(true, true);
//    }

//    if (fadingIn != null) {
//      $(fadingIn).stop(true, true);
//    }
//  }

//  function showFeature(toIndex) {
//    var current = $(features[settings.activeIndex]);
//    var next = $(features[toIndex]);

//    fadingOut = current.fadeOut(700, function() {
//      fadingOut = null;
//    });
//    fadingIn = next.fadeIn(700, function() {
//      settings.activeIndex = toIndex;
//      settings.autoCycles++;
//      fadingIn = null;
//    });


//  }


//  if (settings.maxIndex < 1) {
//    $(header).find('.mediabtn').hide();
//  } else {
//    $(header).find('.mediabtn')
//      .css('opacity', settings.baseOpacity)
//      .hover(function() { $(this).css('opacity', '1'); },
//        function() {
//          $(this).css('opacity', settings.baseOpacity);
//        });
//    header.find('.prev').click(function() { showPrev(true); });
//    header.find('.pause').click(function() { togglePause(); });
//    header.find('.next').click(function() { showNext(true); });


//    setTimer();
//  }

//}