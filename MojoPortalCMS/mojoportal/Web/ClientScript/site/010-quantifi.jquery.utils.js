


// Creating custom :external href selector
$.expr[':'].external = function (obj) {
  return obj.href
            && !obj.href.match(/^mailto\:/)
            && obj.href != ''
            && !obj.href.match(/^#/)
            && !obj.href.match(/^javascript/)
            && (obj.hostname != location.hostname);
};

// Creating custom [:qdownloads] quantifi downloads link href selector
$.expr[':'].qdownloads = function (obj) {
  return obj.href
    && obj.href != ''
      && (obj.hostname == location.hostname)
        && obj.href.match( /qdownloads\.aspx/i );
};




///*global jQuery */
///*!
//* FitText.js 1.1
//*
//* Copyright 2011, Dave Rupert http://daverupert.com
//* Released under the WTFPL license
//* http://sam.zoy.org/wtfpl/
//*
//* Date: Thu May 05 14:23:00 2011 -0600
//*/

//(function ($) {

//  $.fn.fitText = function (kompressor, options) {

//    // Setup options
//    var compressor = kompressor || 1,
//        settings = $.extend({
//          'minFontSize': Number.NEGATIVE_INFINITY,
//          'maxFontSize': Number.POSITIVE_INFINITY
//        }, options);

//    return this.each(function () {

//      // Store the object
//      var $this = $(this);

//      // Resizer() resizes items based on the object width divided by the compressor * 10
//      var resizer = function () {
//        $this.css('font-size', Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
//      };

//      // Call once to set.
//      resizer();

//      // Call on resize. Opera debounces their resize by default.
//      $(window).on('resize orientationchange', resizer);

//    });

//  };

//})(jQuery);

//https://github.com/davatron5000/FitText.js
(function ($) {

  // Public: jFitText Plugin
  $.fn.jFitText = function (options) {

    var opts = $.extend({}, $.fn.jFitText.defaults, options);

    return this.each(function () {
      var $element = $(this);
      var width = $element.width();
      var html = '<span style="white-space:nowrap">';
      var line = $element.wrapInner(html).children()[0];
      var n = opts.maxFontSize;

      if (opts.useCurrentFontSizeAsMax)
        n = Number($element.css('font-size').replace(/[^\d]/g, ''));
      else
        $element.css('font-size', n);

      while ($(line).width() > width) {
        if (n <= opts.minFontSize)
          break;

        $element.css('font-size', --n);
      }

      $element.text($(line).text());


    });


  };

  // Public: Default values
  $.fn.jFitText.defaults = {
    useCurrentFontSizeAsMax: true,
    maxFontSize: 100,
    minFontSize: 10
  };

})(jQuery)
