﻿/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />


if (typeof Quantifi === "undefined" || !Quantifi) {
  var Quantifi = { };
  var Q = Quantifi;


  Q.Errors = new Array();
  Q.logError = function(msg) {
    Q.Errors.unshift(msg);
  };
  Q.Info = new Array();
  Q.logInfo = function(msg) {
    Q.Info.unshift(msg);
  };

  Q.util = { };
  Q.util.callMyValidators = function() {
    Q.util.callMyValidators(this);
  };
  Q.util.callMyValidators = function(elem) {
    if (elem == null)
      return;
    var id = elem.id;
    if (id == null || id.length == 0)
      return;

    if (window.Page_Validators == null || window.Page_Validators.length == 0)
      return;

    for (var i = 0; i < window.Page_Validators.length; i++) {
      if (window.Page_Validators[i].controltovalidate === id) {
        window.ValidatorValidate(window.Page_Validators[i]);
      }
    }
  };


  Q.setCookie = function(name, value, days) {
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
  };

  Q.getCookie = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  };

  Q.deleteCookie = function(name) {
    Q.setCookie(name, "", -1);
  };


  Q.Tracking = { };


  Q.Tracking.recordOutboundLink = function(link, category, action) {
    /// <summary>
    /// Google Analytics Link Tracking Via Event 
    /// http://www.google.com/support/analytics/bin/answer.py?hl=en&answer=55527
    /// </summary>

    if (action === undefined) {
      action = "External-Link";
      try {
        action = link.hostname;
      } catch(ex) {
        Q.logError(ex);
      }
    }

    if (_gaq != undefined || _gaq != null) {
      _gaq.push(['_trackEvent', category, action, link.href]);
    }

    setTimeout(function() {

      window.open(link.href, "_blank");

    }, 100);
  };


  Q.Tracking.recordLink = function(link, category, action) {
    _gaq.push(['_trackEvent', category, action, link.href]);
    setTimeout('document.location = "' + link.href + '"', 100);
  };


}