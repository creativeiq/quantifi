﻿/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />
/// <reference path="001-quantifi.core.js" />
/// <reference path="010-quantifi.jquery.utils.js" />
 



Q.Tracking = {};
Q.T = Q.Tracking;
/// <summary>
/// Google Analytics Link Tracking Via Event 
/// http://www.google.com/support/analytics/bin/answer.py?hl=en&answer=55527
/// </summary>

Q.Tracking.recordOutboundLink = function(link, category, action) {
  if (action === undefined) {
    try {
      action = link.hostname;
    } catch(ex) {
      action = "External-Link";
    }
  }

  if (_gaq != undefined || _gaq != null) {
    _gaq.push(['_trackEvent', category, action, link.href]);
  }

  setTimeout(function() { window.open(link.href, "_blank"); }, 100);
};


Q.Tracking.recordLink = function (link, category, action) {
  _gaq.push(['_trackEvent', category, action, link.href]);
  setTimeout('document.location = "' + link.href + '"', 100);
};


