﻿/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />
/// <reference path="001-quantifi.core.js" />



Q.NewsFeed = function (parentId, changeSpeed, animationSpeed) {

  if (changeSpeed === undefined)
    changeSpeed = 4000;
  if (animationSpeed === undefined)
    animationSpeed = 1000;


  var wrapper = $('#' + parentId);
  var header = wrapper.find(".header");
  var container = wrapper.find(".news-container");
  var newsitems = container.find(".news-item");


  var newsIntervalId = null;
  var maxRotation = newsitems.length * 5;
  var numRotation = 0;


  function moveUp(interaction) {
    if (interaction) {
      try {
        clearInterval(newsIntervalId);
      }
      catch (ex) { }
    }


    newsitems = container.find(".news-item");
    var firstChild = newsitems.first();


    firstChild.animate({ height: ['toggle', 'swing'], opacity: 'toggle' },
                                  animationSpeed,
                                  function () {
                                    firstChild.detach().appendTo(container).show();
                                  });


    if (interaction) {
      startAutoFeed();
    }
  }

  

  function moveDown(interaction) {
    if (interaction) {
      try {
        clearInterval(newsIntervalId);
      }
      catch (ex) { }
    }
    newsitems = container.find(".news-item");
    var lastChild = $(newsitems[newsitems.length - 1]);

    lastChild.hide().detach().prependTo(container).animate({ height: ['toggle', 'swing'], opacity: 'toggle' }, 1000);

    if (interaction) {
      startAutoFeed();
    }
  }




  function startAutoFeed() {

    newsIntervalId = setInterval(function () {
      if (numRotation >= maxRotation) {
        clearInterval(newsIntervalId);
        return;
      }
      moveUp(false);
      numRotation++;
    },
        changeSpeed);


  }



  function init() {
    wrapper.find(".mask").css("opacity", "0.75");
    // wrap item in div for correct padding
    newsitems.wrapInner("<div class='pad' />");

    if (newsitems.length > 2) {
      header.find('.mediabtn').css('opacity', '.50')
          .hover(function () { $(this).css('opacity', '1'); },
            function () { $(this).css('opacity', '.50'); });

      header.find(".btnUP").click(function () { moveUp(true); });
      header.find(".btnDOWN").click(function () { moveDown(true); });
      startAutoFeed();
    } else {
      header.find('.mediabtn').hide();
    }


    wrapper.find(".track-link").click(function (event) {
      event.preventDefault();

      var linkUrl = this.href;
      var title = $(this).attr("title");
 
      var id = $(this).parents("div.news-item").first().attr("itemid");


      _gaq.push(['_trackEvent', 'News-Feed', "click", id + "-" + title, numRotation]);


      if ($(this).attr("target") == "_blank") {
        setTimeout(function () { window.open(linkUrl, "_blank"); }, 100);
      } else {
        setTimeout('document.location = "' + linkUrl + '"', 100);
      }

    });
  }


  init();
}