﻿/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />
/// <reference path="001-quantifi.core.js" />
/// <reference path="100-quantifi.controls.js" />
/// <reference path="010-jquery.cookies.js" />


/* TAGS */ //function QTagsManager(parentJQueryId) {
Q.C.TagsManager = function (parentJQueryId) {
  var my = this;
  var isInited = false;

  my.hideTags = function (speed) {
    my.tags.fadeOut(speed);
    my.tagToggles.attr("title", "Show Tags");
  };
  my.showTags = function (speed) {
    my.tags.fadeIn(speed);
    my.tagToggles.attr("title", "Hide Tags");
  };

  my.init = function() {
    if (isInited === true) {
      return;
    }
    isInited = true;

    my.parent = $(parentJQueryId);

    my.tagToggles = my.parent.find(".tags .tags-toggle");
    if (my.tagToggles.length == 0) {
      return;
    }

    my.tags = my.parent.find(".tags .taglink");
    if (my.tags.length == 0) {
      return;
    }

    if ($.cookie('vt') != '0') {
      my.showTags(300);
    } else {
      my.hideTags(0);
    }

    my.tagToggles.click(function() {
      var isVisible = !(my.tags.first().css('display') == 'none');

      if (isVisible) {
        $.cookie('vt', '0');
        my.hideTags(500);
      } else {
        $.cookie('vt', '1');
        my.showTags(500);
      }
    });
    
  };

  my.init();
}


 


