﻿/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />
/// <reference path="001-quantifi.core.js" />




Quantifi.Page = {};
Quantifi.Page.ResizePage = function (useAnimate) {
  Quantifi.Page.WindowWidth = $(window).width();
  Quantifi.Page.BodyWidth = $(Quantifi.Page.WrapperId).width();

  if (Quantifi.Page.MaxWidth > Quantifi.Page.WindowWidth) {
    if (Quantifi.Page.MinWidth < Quantifi.Page.WindowWidth) {
      if (useAnimate) {
        $(Quantifi.Page.WrapperId).animate({ width: Quantifi.Page.WindowWidth }, { queue: false, duration: 100 });
      } else {
        $(Quantifi.Page.WrapperId).width(Quantifi.Page.WindowWidth);
      }
    } else {
      if (useAnimate) {
        $(Quantifi.Page.WrapperId).animate({ width: Quantifi.Page.MinWidth }, { queue: false, duration: 100 });
      } else {
        $(Quantifi.Page.WrapperId).width(Quantifi.Page.MinWidth);
      }
    }
  } else if (Quantifi.Page.MaxWidth < Quantifi.Page.WindowWidth && Quantifi.Page.BodyWidth != Quantifi.Page.MaxWidth) {
    if (useAnimate) {
      $(Quantifi.Page.WrapperId).animate({ width: Quantifi.Page.MaxWidth }, { queue: false, duration: 100 });
    } else {
      $(Quantifi.Page.WrapperId).width(Quantifi.Page.MaxWidth);
    }
  }
};

if (Q.Controls === "undefined" || !Quantifi.Controls) {
  Q.Controls = {};
  Q.C = Q.Controls;
}



if (Q.C.WatermarkTextBox === "undefined" || !Q.C.WatermarkTextBox) {
  Q.C.WatermarkTextBox = function (txtboxId, wmtxt, wmCss, noWmCss) {

    var textbox = $(txtboxId);
    if (textbox.length == 0) {
      Q.logError(txtboxId + " was not found");
      return;
    }

    if (textbox.val() == "" || textbox.val() == wmtxt) {
      /*Add Water Mark*/
      if (wmCss != null && wmCss.length > 0)
        textbox.addClass(wmCss);

      textbox.val(wmtxt);
    } else {
      if (noWmCss != null && noWmCss.length > 0)
        textbox.addClass(noWmCss);
    }


    /*  Events  */
    textbox.focusin(function () {
      if (textbox.val() == wmtxt) {
        textbox.val("");

        if (wmCss != null && wmCss.length > 0)
          textbox.removeClass(wmCss);
        if (noWmCss != null && noWmCss.length > 0)
          textbox.addClass(noWmCss);
      }
    });

    textbox.blur(function () {
      if (textbox.val() == "") {
        textbox.val(wmtxt);

        if (noWmCss != null && noWmCss.length > 0)
          textbox.removeClass(noWmCss);
        if (wmCss != null && wmCss.length > 0)
          textbox.addClass(wmCss);
      }
    });
  };
}






