﻿
/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />
/// <reference path="combined/quantifi.combined.min.js" />


if (typeof Q.admin === "undefined" || !Q.admin) {
  Q.admin = { };
}


Q.admin.checkLinks = function (id, baseUrl) {

  if (baseUrl === undefined)
    baseUrl = "";

  var container = $("#" + id);
  var hostCheck = "^" + location.protocol + "//" + location.host;
  var patt = new RegExp(hostCheck, "i");


  container.find("a").each(function (index, obj) {
    var url = obj.href;

    if (!url || url.length == '' || url.match(/^mailto\:/) || url.match(/^#/) || url.match(/^javascript/))
      return;

    var serviceUrl = url;
    if (!url.match(patt)) {
      serviceUrl = baseUrl + "/Quantifi/Controls/Utils/UrlCheckHandler.ashx?check=" + encodeURIComponent(url);
    }

    $.ajax(serviceUrl, {
      cache: false,
      context: obj,
      type: "HEAD",
      beforeSend: function () {
        $(this).removeClass("link-not-found").removeClass("good-link").addClass("checking-link");
        if ($(this).find("span.link-status").length == 0) {
          $(this).append("<span class='link-status' style='display:none;' title='Checking link, please wait'></span>");
        }
        $(this).find("span.link-status").show();
      },
      complete: function (jqXHR, textStatus) {
        $(this).removeClass("checking-link");
      },
      success: function (html) {
        $(this).addClass("good-link");
        $(this).find("span.link-status").fadeOut(4000);

        setTimeout(function () {
          $(this).removeClass("good-link");
        }, 4500);

      },
      error: function (jqXHR, textStatus, errorThrown) {
        $(this).addClass("link-not-found").find("span.link-status").attr("title", "Warning! Looks like the link is not working?");
      }
    });

  });
};

