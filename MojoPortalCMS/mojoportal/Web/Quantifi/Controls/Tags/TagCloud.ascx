﻿<%@ Control Language="C#" 
AutoEventWireup="true" CodeBehind="TagCloud.ascx.cs"  ViewStateMode="Disabled"
Inherits="Quantifi.UI.Controls.Tags.TagCloud" %>

    
<portal:ModulePanel ID="pnlContainer" runat="server" CssClass="panelwrapper" >
	<portal:ModuleTitleControl ID="mojoTitle" runat="server" />
	<asp:Panel ID="pnlWrapper" runat="server" CssClass="TagCloudWrapper">
		<asp:Panel ID="pnlTagcloudflash" runat="server" ClientIDMode="Predictable">
			<a href="http://www.adobe.com/go/getflashplayer" target="_blank">
				<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
					border="0" alt="Get Adobe Flash player" />
			</a>
		</asp:Panel>    
 

		<script type="text/javascript">

		  (function () {

		    var flashvars = {
		      tcolor: "0x<%=this.TextColor1 %>",
		      tcolor2: "0x<%=this.TextColor2 %>",
		      hicolor: "0x<%=this.HighlightColor %>",
		      mode: "tags",
		      distr: "true",
		      tspeed: "100",
		      tagcloud: "<%=this.TagsXmlData %>"
		    };
		    var params = {
		      menu: "false",
		      wmode: "transparent"
		    };
		    var attributes = {
		      id: "<%=this.pnlTagcloudflash.ClientID %>",
		      name: "tagcloud"
		    };

		    swfobject.embedSWF("<%= SiteRoot %>/Quantifi/Controls/Tags/tagcloud.swf", "<%=this.pnlTagcloudflash.ClientID %>", "<%= this.CloudWidth %>", "<%= this.CloudHeight %>", "9.0.0", "expressInstall.swf", flashvars, params, attributes);

		    if (jQuery.browser.msie && parseInt(jQuery.browser.version) < 7) {
		      $("#<%=this.pnlWrapper.ClientID %>").css("padding", "3px");
		    }


		  })();
		</script>
	</asp:Panel>      
</portal:ModulePanel>

 