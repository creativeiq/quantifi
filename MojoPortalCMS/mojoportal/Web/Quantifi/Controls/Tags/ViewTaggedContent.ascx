﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewTaggedContent.ascx.cs"
  Inherits="Quantifi.UI.Controls.Tags.ViewTaggedContent" %>

<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>


<portal:ModulePanel ID="pnlContainer" runat="server" CssClass="panelwrapper tagsview">
  <h2 class=" moduletitle">
    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    <asp:Panel ID="pnlAdminWrapper" runat="server" CssClass="adminWrapper" Visible="false">
      <div class="adminContainer">
        <asp:HyperLink ID="adminEditTagsLink" runat="server" CssClass="ModuleEditLink" ToolTip="Edit All Tags">Edit Tags</asp:HyperLink>
        <asp:HyperLink ID="adminLink" runat="server" CssClass="ModuleEditLink" ToolTip="Edit Settings For this Content Instance">Settings</asp:HyperLink>
      </div>
    </asp:Panel>
  </h2>
  <asp:Panel ID="pnlWrapper" runat="server">
    <asp:Repeater ID="rptItems" runat="server" ViewStateMode="Disabled">
      <HeaderTemplate>
        <div class="taggedItemList blogRoll">
      </HeaderTemplate>
      <ItemTemplate>
        <div class="item blogitem ">
          <h3 class="blogtitle">
            <asp:HyperLink ID="lnkTitle" runat="server" EnableViewState="false" NavigateUrl='<%# FormatBlogTitleUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID")))  %>'
              Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'>
            </asp:HyperLink>
          </h3>
          <div class="blogdate">
            <span class="bdate">
              <%# FormatBlogDate(Convert.ToDateTime(Eval("StartDate"))) %>
            </span>
          </div>
          <div class="blogtext">
            <%# FormatBlogEntry(DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Excerpt").ToString(), DataBinder.Eval(Container.DataItem, "ItemUrl").ToString(), Convert.ToInt32(Eval("ItemID")), Convert.ToInt32(Eval("ModuleID")))%>
          </div>
          <div class="clear">
          </div>
          <div>
            <quantifi:ExRepeater ID="rptTags" runat="server" ViewStateMode="Disabled" DataSource='<%#DataBinder.Eval(Container.DataItem, "Tags") %>'>
              <EmptyTemplate>
                <!-- No Tags -->
              </EmptyTemplate>
              <HeaderTemplate>
                <div class="tags tags-hidden">
                  <span class="tags-toggle" title="Show Tags">Tags:</span>
              </HeaderTemplate>
              <ItemTemplate>
                <a class="taglink " href='<%= SiteRoot %>/tags.aspx?tag=<%# Server.UrlEncode( Eval("Text").ToString()) %>&amp;tid=<%# Server.UrlEncode( Eval("Id").ToString()) %>'
                  title='<%# string.Format("View {0} tagged content", Server.HtmlEncode( Eval("Text").ToString()))%>'>
                  <%# Server.HtmlEncode(Eval("Text").ToString())%></a>
              </ItemTemplate>
              <FooterTemplate>
                </div>
              </FooterTemplate>
            </quantifi:ExRepeater>
          </div>
        </div>
      </ItemTemplate>
      <FooterTemplate>
        </div>
 
      </FooterTemplate>
    </asp:Repeater>
    <asp:Panel ID="pnlNoData" runat="server" Visible="false" ViewStateMode="Disabled">
      <asp:Repeater ID="rptAllTags" runat="server" ViewStateMode="Disabled">
        <HeaderTemplate>
          <div class="alltagscontainer">
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
          <li><a href='?tag=<%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem, "Text").ToString()) %>&tid=<%# DataBinder.Eval(Container.DataItem, "Id") %>'>
            <%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem, "Text").ToString())%></a>
          </li>
        </ItemTemplate>
        <FooterTemplate>
          </ul></div>
        </FooterTemplate>
      </asp:Repeater>
    </asp:Panel>
  </asp:Panel>
  <portal:mojoCutePager ID="pgr" runat="server" CssClass="" />
</portal:ModulePanel>
