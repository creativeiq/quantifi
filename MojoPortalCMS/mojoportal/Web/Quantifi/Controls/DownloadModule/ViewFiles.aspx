﻿<%@ Page Title="View Download Files" Language="C#" 
  MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="ViewFiles.aspx.cs" 
  Inherits="Quantifi.UI.Controls.DownloadModule.ViewFiles" %>

<%@ Import Namespace="Quantifi.Business.DownloadModule" %>


<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <div class="breadcrumbs">
    <asp:HyperLink runat="server" NavigateUrl="~/Admin/AdminMenu.aspx" CssClass="unselectedcrumb"
      Text="Administration Menu" />&nbsp;&gt;
    <asp:HyperLink runat="server" CssClass="selectedcrumb" Text="Download Files" />
  </div>         
  <h2 class="moduletitle" style="padding-top:10px;">
    DownloadFiles &nbsp;&nbsp;&nbsp;&nbsp; <a title="Add New File" class="link-add" href="/Quantifi/Controls/DownloadModule/edit.aspx">
      Add New Download File</a> &nbsp;&nbsp;&nbsp;&nbsp; <a title="Manage Email Restrictions"
        href="/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx" class="link-manage">
        Manage Email Restrictions</a>
  </h2>
  <div class="tblGrid">
    <asp:DataGrid ID="dgDownloadFiles" runat="server"
      AutoGenerateColumns="false" GridLines="None"
      EnableTheming="false" CellPadding="0" CellSpacing="0" >
      <AlternatingItemStyle CssClass="altRow" />
      <HeaderStyle Font-Bold="true" CssClass="headerRow" />
      <ItemStyle CssClass="itemRow" />
      <Columns>
        <asp:HyperLinkColumn HeaderText="" DataNavigateUrlField="Id" DataNavigateUrlFormatString="edit.aspx?fileId={0}"
          DataTextField="Id" DataTextFormatString="View" />
        <asp:TemplateColumn>
          <HeaderTemplate>
            Name
          </HeaderTemplate>
          <ItemTemplate>
            <img src='<%# GetFileTypeImageUrl((DownloadFile)Container.DataItem)%>' alt="file type icon"
              title="<%# System.IO.Path.GetFileName((DataBinder.Eval(Container.DataItem, "OriginalFileName")).ToString())%>" />
            &nbsp;
            <%# (DataBinder.Eval(Container.DataItem, "DisplayName"))%>
          </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn>
          <HeaderTemplate>
            File Size
          </HeaderTemplate>
          <ItemStyle HorizontalAlign="Right" Wrap="false" />
          <ItemTemplate>
            <%# GetFileSize((DownloadFile)Container.DataItem) %>
          </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn>
          <HeaderTemplate>
            Is Active</HeaderTemplate>
          <ItemStyle HorizontalAlign="Center" />
          <ItemTemplate>
            <asp:CheckBox runat="server" Checked="<%# ((DownloadFile)Container.DataItem).isActive %>"
              Enabled="false" />
          </ItemTemplate>
        </asp:TemplateColumn>
        <asp:BoundColumn DataField="DownloadCount" HeaderText="Count" ItemStyle-HorizontalAlign="Right" />
        <asp:TemplateColumn>
          <HeaderTemplate>
            Url
          </HeaderTemplate>
          <ItemTemplate>
            <a href="<%# GetFileDownloadUrl((DownloadFile)Container.DataItem) %>" style="font-size: 11px;">
              Link</a>
          </ItemTemplate>
        </asp:TemplateColumn>
      </Columns>
    </asp:DataGrid>
  </div>
  <div style="padding: 5px 2px;">
    <mp:CutePager ID="pager" runat="server" />
  </div>
</asp:Content>
