﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateDownloadFileControl.ascx.cs"
	Inherits="Quantifi.UI.Controls.DownloadModule.Controls.CreateDownloadFileControl" %>
<style type="text/css">
		.style1
		{
				width: 90px;
		}
</style>
<fieldset>
	<legend>Create New Download Tracked File</legend>

		<table style="width: 500px">
			<tr>
				<th align="left" class="style1" >
					<asp:Label runat="server" AssociatedControlID="textboxName">Name</asp:Label>
				</th>
				<td>
					<asp:TextBox ID="textboxName" runat="server" Width="350px"></asp:TextBox>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="textboxName" ValidationGroup="createDF"
						ErrorMessage="Please enter a friendly name for the file">*</asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th align="left" class="style1" >
					<asp:Label runat="server" AssociatedControlID="textboxDesc">Description</asp:Label>
				</th>
				<td>
					<asp:TextBox ID="textboxDesc" runat="server" Width="350px"></asp:TextBox>
					<asp:RequiredFieldValidator runat="server" 
						ControlToValidate="textboxName" 
						ValidationGroup="createDF"
						ErrorMessage="Please enter a friendly description for the file">*</asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th colspan="2"  align="left" >
					<asp:Label runat="server" AssociatedControlID="fileUpload">File</asp:Label>
				</th>
			</tr>
			<tr>
				<td colspan="2">
					<asp:FileUpload ID="fileUpload" runat="server" Width="221px" />
					<asp:CustomValidator ID="customValidator_FileUpload" runat="server" 
						CssClass="validator"  
						EnableClientScript="true"
						ValidationGroup="createDF" 
						ErrorMessage="Please select a file to upload">*</asp:CustomValidator>

					<script language="javascript" type="text/javascript">

							function <%= CustomValidatorJsFunctionName %>(Source, args) {
								var controlId = '<%= fileUpload.ClientID %>';

								var fileUploadData = document.getElementById(controlId);
								var FileUploadPath = fileUploadData.value;
 
								if (FileUploadPath == '') {
									// There is no file selected 
									args.IsValid = false;
								}
								else {
									args.IsValid = true;
								}
							}
					</script>
				</td>
			</tr>
			<tr>
				<td class="style1">
					<asp:Button ID="btnCreate" runat="server" 
						Text="Create" 
						ToolTip="Create" 
						ValidationGroup="createDF"
						CausesValidation="true" />
				</td>
				<td class="button" align="right">
					<a href="ViewFiles.aspx">Cancel</a>
				</td>
			</tr>
		</table>

	<asp:Panel ID="pnlError" runat="server" CssClass="errorPanel">
		<h2>
			<asp:Label ID="lblError" runat="server"></asp:Label></h2>
		<div>
			<asp:Literal ID="litErrorBody" runat="server"></asp:Literal></div>
	</asp:Panel>
</fieldset>
