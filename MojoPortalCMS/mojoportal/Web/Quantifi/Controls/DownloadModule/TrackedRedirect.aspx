﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrackedRedirect.aspx.cs"
  Inherits="Quantifi.UI.Controls.DownloadModule.TrackedRedirect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Redirecting...</title>
  <style type="text/css">
    html, body
    {
      font-family: arial,sans-serif;
      font-size: 12px;
      margin: 0;
      padding: 0;
    }
    
    body.pagebody
    {
      color: #585858;
    }
    
    .pagebody
    {
      background-color: #BDBDBD;
    }
    
    div#wrapwebsite
    {
      background-color: White;
      margin: 16px auto 0px auto;
      height: auto !important;
      position: relative;
      width: 400px;
    }
    
    #wrapheader
    {
      padding: 18px 36px 10px 36px;
      background-color: White;
    }
    
    #mainContent
    {
      padding: 0px 36px 10px 36px;
    }
    
    #footer
    {
      background-color: #BDBDBD;
      background-image: url("/Data/Sites/1/skins/QC_HP_v4_3PackLow/images/footer_imageBar_9_1280.jpg");
      background-position: right top;
      background-repeat: no-repeat;
    }
    
    #footer .content
    {
      padding-top: 14px;
      text-align: center;
    }
    
    
    h1, h2, h3, h4, h5
    {
      margin: 0;
      padding: 8px 0;
      font-size: 16px;
    }
  </style>
</head>
<body class="pagebody">
  <form id="form1" runat="server">
  <portal:AnalyticsAsyncTopScript ID="AnalyticsAsyncTop" runat="server" TrackPageLoadTime="false"   />
  <div id="wrapwebsite">
    <div id="wrapheader">
      <a id="qlogolink" class="toplogo" href="/">
        <img id="qlogo" width="214" border="0" height="50" alt="Quantifi Logo" src="/Quantifi/Images/quantify_logo_214_50.png" />
      </a>
    </div>
    <div id="mainContent">
      <h2>
        Thank you for your interest in Quantifi</h2>
      <p>
        Your download will start shortly.&nbsp;
      </p>
      <p>
        Problems with the download? please use this
        <asp:LinkButton ID="btnDirectLink" runat="server" EnableTheming="false" Font-Underline="false">Direct Link</asp:LinkButton>
      </p>
    </div>
    <div id="footer">
      <div class="content">
        &copy;&nbsp;2002&nbsp;-&nbsp;<%= DateTime.Now.Year.ToString() %>&nbsp;Quantifi
      </div>
    </div>
  </div>
  <portal:AnalyticsAsyncBottomScript ID="AnalyticsAsyncBottom" runat="server"  />
  </form>
</body>
</html>
