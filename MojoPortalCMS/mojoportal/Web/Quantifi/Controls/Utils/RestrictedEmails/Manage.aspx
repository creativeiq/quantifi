﻿<%@ Page Title="Manage - Restricted Emails" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="Quantifi.UI.Controls.Utils.RestrictedEmails.Manage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <asp:Panel ID="pnlWrapper" runat="server">
    <asp:Panel ID="pnlView" runat="server">
      <div class="breadcrumbs" style="padding-bottom: 16px;">
        <asp:HyperLink runat="server" NavigateUrl="~/Admin/AdminMenu.aspx" CssClass="unselectedcrumb"
          Text="Administration Menu" />&nbsp;&gt;
        <asp:HyperLink runat="server" CssClass="selectedcrumb" Text="Manage Restricted Email Domains"
          NavigateUrl="~/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx" />
      </div>
      <h2 class="moduletitle">
        <span style="padding-right: 20px;">Restricted Email Domains</span> <a class="link-add"
          href="Edit.aspx">Create</a> &nbsp;
        <asp:HyperLink ID="linkTestEmail" runat="server" ToolTip="Test email against all the current rules"
          CssClass="link-accept" NavigateUrl="~/Quantifi/Controls/Utils/RestrictedEmails/QuickTest.aspx">
          Test An Email
        </asp:HyperLink>
        &nbsp;
        <asp:LinkButton ID="btnExport" runat="server" EnableTheming="false" CssClass="link-xls"
          ToolTip="Export all">Export</asp:LinkButton>
      </h2>
      <div class="tblGrid">
        <div style="padding-bottom: 8px;">
          <asp:TextBox runat="server" ID="txtSearch"></asp:TextBox>
          <asp:LinkButton runat="server" CssClass="link-search" EnableTheming="False" ID="btnSearch">Search
          </asp:LinkButton>
          <asp:LinkButton CssClass="link-clear-search" runat="server" ID="btnClearSearch" EnableTheming="False">Clear</asp:LinkButton>
        </div>
        <table border="0" cellpadding="0" cellspacing="0">
          <tr class="headerRow">
            <th align="left">
              <asp:HyperLink ID="linkSortName" runat="server" CssClass="link-sort-up">Name</asp:HyperLink>
            </th>
            <th>
              RegEx
            </th>
            <th>
              <asp:HyperLink ID="linkSortIsCompetitor" runat="server" CssClass="link-sort-up">IsCompetitor</asp:HyperLink>
            </th>
            <th>
              <asp:HyperLink ID="linkSortIsActive" runat="server" CssClass="link-sort-up">IsActive</asp:HyperLink>
            </th>
            <th align="center">
              <asp:HyperLink ID="linkSortCount" runat="server" CssClass="link-sort-up">Count</asp:HyperLink>
            </th>
            <th>
            </th>
          </tr>
          <asp:Repeater ID="rptItems" runat="server">
            <ItemTemplate>
              <tr class="itemRow">
                <td>
                  <%# Eval("Name") %>
                </td>
                <td>
                  <%# HttpUtility.HtmlEncode(Eval("RegExMatch")) %>
                </td>
                <td align="center">
                  <input type="checkbox" disabled="disabled" <%#  ((bool)Eval("IsCompetitor"))? "checked='true'" : string.Empty %> />
                </td>
                <td align="center">
                  <input type="checkbox" disabled="disabled" <%#  ((bool)Eval("IsActive"))?"checked='true'" : string.Empty %> />
                </td>
                <td align="right">
                  <%# Eval("Count")%>
                </td>
                <td>
                  <a class="link-edit" title="Edit Restriction" href="Edit.aspx?Id=<%# Eval("Id") %>">
                    Edit</a>
                </td>
              </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
              <tr class="altRow">
                <td>
                  <%# Eval("Name") %>
                </td>
                <td>
                  <%# HttpUtility.HtmlEncode(Eval("RegExMatch")) %>
                </td>
                <td align="center">
                  <input type="checkbox" disabled="disabled" <%#  ((bool)Eval("IsCompetitor"))? "checked='true'" : string.Empty %> />
                </td>
                <td align="center">
                  <input type="checkbox" disabled="disabled" <%#  ((bool)Eval("IsActive"))?"checked='true'" : string.Empty %> />
                </td>
                <td align="right">
                  <%# Eval("Count")%>
                </td>
                <td>
                  <a class="link-edit" title="Edit Restriction" href="Edit.aspx?Id=<%# Eval("Id") %>">
                    Edit</a>
                </td>
              </tr>
            </AlternatingItemTemplate>
          </asp:Repeater>
        </table>
      </div>
      <div class="pager" style="padding: 10px;">
        <mp:CutePager ID="pgr" runat="server" />
      </div>
    </asp:Panel>
  </asp:Panel>
  <script type="text/javascript">
    $(document).ready(function () {
      $("#<%= this.linkTestEmail.ClientID %>").colorbox({ title: "Test Emails", iframe: true, width: 400, height: 350 });
    });
  
  </script>
</asp:Content>
