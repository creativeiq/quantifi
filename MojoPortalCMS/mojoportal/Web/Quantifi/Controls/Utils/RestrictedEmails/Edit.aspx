﻿<%@ Page Title="Create - Email Restriction" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.Utils.RestrictedEmails.Edit" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <style type="text/css">
    .regex-help
    {
      font-size: 11px;
    }
  </style>
  <asp:Panel ID="pnlWrapper" runat="server">
    <div style="padding-bottom: 15px;">
      <asp:HyperLink ID="backLink" runat="server" NavigateUrl="~/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx"
        CssClass="link-manage"> Manage Restrictions</asp:HyperLink>
    </div>
    <portal:HeadingControl ID="headerControl" runat="server" HeadingTag="h2" Text="Create New Email Restriction" />
    <div style="padding: 15px;">
      <div class="tblGrid">
        <table class="editEmailRestriction" style="min-width: 450px; margin-top: 10px;" cellpadding="0"
          cellspacing="0">
          <tr class="headerRow">
            <th colspan="2">
              Email Restriction
            </th>
          </tr>
          <tr class="itemRow">
            <td style="width: 100px;">
              <asp:Label runat="server">Name:</asp:Label>
            </td>
            <td>
              <asp:TextBox ID="txtName" runat="server" MaxLength="256" Width="90%"></asp:TextBox>
              <span class="help-me" tip="Name Help"></span>
            </td>
          </tr>
          <tr class="itemRow">
            <td valign="top">
              <asp:Label runat="server">RegEx:</asp:Label>
            </td>
            <td>
              <asp:Panel ID="pnlRegEx" runat="server">
                <asp:TextBox ID="txtRegEx" runat="server" MaxLength="500" Width="90%"></asp:TextBox>
                <span class="help-me" tip="Reg Ex can be very hard"></span>
              </asp:Panel>
              <div class="regex-help">
                <p>
                  Check is based off the first character after "@", and ignore case
                  <br />
                  testuser@testdom.com, will pass testdom.com to be checked
                  <br />
                  check from start ^(gmail\.com) or  ^(gmail\.)
                  <br />
                  check from end (\.ac\.uk)$  or (\.edu)$
                  <br />
                  <a href="http://regexlib.com/CheatSheet.aspx" target="_blank">RegEx Cheatsheet</a>
                </p>
              </div>
            </td>
          </tr>
          <tr class="itemRow">
            <td colspan="2">
              <asp:CheckBox ID="chkboxIsCompetitor" runat="server" Text="Is a Competitor" />
              <span class="help-me" tip="Is a Competitor"></span>
            </td>
          </tr>
          <tr class="itemRow">
            <td colspan="2">
              <asp:CheckBox ID="chkboxIsActive" runat="server" Text="Is Active" Checked="true" />
              <span class="help-me" tip="Is Active"></span>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              &nbsp;
              <br />
            </td>
          </tr>
          <tr>
            <td align="left" style="padding-left: 5px;">
              <asp:LinkButton ID="btnSave" runat="server" Text="Save" CssClass="link-save" EnableTheming="false"></asp:LinkButton>
              <%--          
              <asp:Button ID="btnSave" runat="server" Text="Save" />--%>
            </td>
            <td align="right" style="padding-right: 5px;">
              <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CssClass="link-trash"
                EnableTheming="false"></asp:LinkButton>
              <%--            <asp:Button ID="btnDelete" runat="server" Text="Delete" />--%>
            </td>
          </tr>
        </table>
      </div>
      <br />
    </div>
  </asp:Panel>
  <script type="text/javascript">

    $(document).ready(function () {
      $(".editEmailRestriction span.help-me").qtip({
        content: {
          attr: 'tip',
          title: { text: 'Help Tips:' }
        },
        style: {
          classes: 'ui-tooltip-tipped ui-tooltip-shadow'
        },
        position: {
          my: 'left top',
          at: 'right center'
        }
      });
    });
  </script>
</asp:Content>
