﻿<%@ Page Language="C#" MasterPageFile="~/App_MasterPages/layout.Master" AutoEventWireup="false"
	EnableViewState="false" CodeBehind="ViewList.aspx.cs" Inherits="Quantifi.UI.Blogs.ViewList" %>

<%@ Register TagPrefix="blog" TagName="PostList" Src="~/Quantifi/Controls/BlogsModule/Controls/PostList.ascx" %>
<%@ Register TagPrefix="blog" TagName="SideBarNav" Src="~/Quantifi/Controls/BlogsModule/Controls/BlogSideBarNav.ascx" %>



<asp:Content ContentPlaceHolderID="leftContent" ID="MPLeftPane" runat="server">
	
</asp:Content> 
<asp:Content ContentPlaceHolderID="mainContent" ID="MPContent" runat="server">
	<portal:mojoPanel ID="mp1" runat="server" ArtisteerCssClass="art-Post" RenderArtisteerBlockContentDivs="true">
		<mp:CornerRounderTop ID="ctop1" runat="server" />
		<asp:Panel ID="pnl1" runat="server" CssClass="art-Post-inner panelwrapper ">
			<portal:ModuleTitleControl ID="moduleTitle" runat="server" />
			<portal:mojoPanel ID="MojoPanel1" runat="server" ArtisteerCssClass="art-PostContent">
				<div class="modulecontent">
					<blog:PostList ID="postList" runat="server" />
				</div>
			</portal:mojoPanel>
			<div class="cleared">
			</div>
		</asp:Panel>
		<mp:CornerRounderBottom ID="cbottom1" runat="server" />
	</portal:mojoPanel>


</asp:Content>
<asp:Content ContentPlaceHolderID="rightContent" ID="MPRightPane" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="pageEditContent" ID="MPPageEdit" runat="server" />
