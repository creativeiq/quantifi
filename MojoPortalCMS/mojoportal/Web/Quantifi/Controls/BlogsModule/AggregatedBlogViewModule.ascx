﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AggregatedBlogViewModule.ascx.cs"
  ViewStateMode="Disabled" EnableViewState="false" Inherits="Quantifi.UI.Controls.BlogsModule.AggregatedBlogViewModule" %>

<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>
<asp:Panel ID="pnlWrapper" runat="server" CssClass="art-Post-inner panelwrapper blogmodule">
  <portal:ModuleTitleControl ID="Title1" runat="server" />
  <asp:Panel ID="divblog" runat="server" CssClass="blogRoll " SkinID="plain">
    <quantifi:ExRepeater ID="rptAggregatedPosts" runat="server" EnableViewState="False">
      <EmptyTemplate>
      </EmptyTemplate>
      <HeaderTemplate>
        <div class="blogArchive">
      </HeaderTemplate>
      <ItemTemplate>
        <div class="blogitem">
          <h3 class="blogtitle">
            <asp:HyperLink ID="lnkTitle" runat="server" EnableViewState="false" NavigateUrl='<%# FormatBlogTitleUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID")))  %>'
              SkinID="BlogTitle" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'>
            </asp:HyperLink>
            <asp:HyperLink ID="editLink" runat="server" CssClass="editlink" EnableViewState="false"
              NavigateUrl='<%# this.SiteRoot + "/Quantifi/Controls/BlogsModule/EditPost.aspx?pageid=" +  PageId.ToString() + "&amp;ItemID=" + DataBinder.Eval(Container.DataItem,"ItemID") + "&amp;mid=" + DataBinder.Eval(Container.DataItem,"ModuleId")  %>'
              Text="Edit" ToolTip="Edit Post" Visible="<%# IsEditable %>" />
          </h3>
          <div class="blogdate">
            <span class="bdate">
              <%# FormatBlogDate(Convert.ToDateTime(Eval("StartDate"))) %>
            </span>
          </div>
          <div class="blogtext">
            <%# FormatBlogEntry(DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Excerpt").ToString(), DataBinder.Eval(Container.DataItem, "ItemUrl").ToString(), Convert.ToInt32(Eval("ItemID")))%>
          </div>
          <quantifi:ExRepeater ID="rptTags" runat="server" ViewStateMode="Disabled" DataSource='<%#DataBinder.Eval(Container.DataItem, "Tags") %>' >
            <EmptyTemplate>
              <!-- No Tags -->
            </EmptyTemplate>
            <HeaderTemplate>
              <div class="tags tags-hidden">
                <span class="tags-toggle pngFix">Tags:</span>
            </HeaderTemplate>
            <ItemTemplate>
      
              <a class="taglink " href='<%= SiteRoot %>/tags.aspx?tag=<%# Server.UrlEncode( Eval("Text").ToString()) %>&amp;tid=<%# Server.UrlEncode( Eval("Id").ToString()) %>'
                title='<%# string.Format("View {0} tagged content", Server.HtmlEncode( Eval("Text").ToString()))%>'>
                <%# Server.HtmlEncode(Eval("Text").ToString())%></a>
            </ItemTemplate>
            <FooterTemplate>
              </div>
            </FooterTemplate>
          </quantifi:ExRepeater>
        </div>
      </ItemTemplate>
      <FooterTemplate>
        </div>
 
      </FooterTemplate>
    </quantifi:ExRepeater>
    <div class="pager">
      <portal:mojoCutePager ID="pgr" runat="server" />
    </div>
    <div class="blogcopyright">
      <asp:Label ID="lblCopyright" runat="server" />
    </div>
  </asp:Panel>
</asp:Panel>
  <script type="text/javascript">
    $(document).ready(function () {

      Q.C.TagsManager(".blogRoll");

    });
  </script>
