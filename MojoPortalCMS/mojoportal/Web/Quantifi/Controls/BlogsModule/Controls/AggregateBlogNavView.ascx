﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AggregateBlogNavView.ascx.cs"
  Inherits="Quantifi.UI.Controls.BlogsModule.Controls.AggregateBlogNavView" %>
                                                                                   
<h3 class="AggregateBlogNavView ui-accordion-header ui-helper-reset ui-state-default <%=this.SelectedCss%>">
  <span class="ui-icon ui-icon-triangle-1-e"></span>
  <asp:HyperLink ID="link" runat="server" NavigateUrl="~/default.aspx">In the News</asp:HyperLink>
</h3>

