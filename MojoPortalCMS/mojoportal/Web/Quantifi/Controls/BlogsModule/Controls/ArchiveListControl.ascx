﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArchiveListControl.ascx.cs"
	Inherits="Quantifi.UI.Blogs.BlogArchiveList" %>
<h3>
	<mp:SiteLabel ID="Sitelabel5" runat="server" ConfigKey="BlogArchivesLabel" resourceFile="QuantifiBlogResources"
		EnableViewState="false" UseLabelTag="false"></mp:SiteLabel>
</h3>
<asp:Repeater ID="dlArchive" runat="server" EnableViewState="False" SkinID="plain">
	<HeaderTemplate>
		<ul class="blognav">
	</HeaderTemplate>
	<ItemTemplate>
		<li>
			<asp:HyperLink runat="server" EnableViewState="false" Text='<%# DateTimeHelper.FormatArchiveLinkText(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"Month")),Convert.ToInt32(DataBinder.Eval(Container.DataItem,"Year")),Convert.ToInt32(DataBinder.Eval(Container.DataItem,"Count"))) %>'
				NavigateUrl='<%# this.SiteRoot + "/Quantifi/Controls/BlogsModule/ViewArchive.aspx?month=" + DataBinder.Eval(Container.DataItem,"Month") + "&amp;year=" + DataBinder.Eval(Container.DataItem,"Year").ToString() + "&amp;mid=" + ModuleId.ToString() + "&amp;pageid=" + PageId.ToString() %>'>
			</asp:HyperLink></li>
	</ItemTemplate>
	<FooterTemplate>
		</ul>
	</FooterTemplate>
</asp:Repeater>

