﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogSideBarControl.ascx.cs" Inherits="Quantifi.UI.Blogs.Controls.BlogSideBarControl" %>


<%@ Register TagPrefix="blog" TagName="TagList" Src="~/Quantifi/Controls/BlogsModule/Controls/CategoryListControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="Archives" Src="~/Quantifi/Controls/BlogsModule/Controls/ArchiveListControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="FeedLinks" Src="~/Quantifi/Controls/BlogsModule/Controls/FeedLinksControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="StatsControl" Src="~/Quantifi/Controls/BlogsModule/Controls/StatsControl.ascx" %>

<%@ Register TagPrefix="blog" TagName="CategoryArchive" Src="~/Quantifi/Controls/BlogsModule/Controls/CategoryArchiveControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="BlogSideBarNav" Src="~/Quantifi/Controls/BlogsModule/Controls/BlogSideBarNav.ascx" %>

	
					

<asp:Panel ID="Panel1" runat="server" SkinID="plain" CssClass="blog_Calendar">
					 <blog:CategoryArchive ID="CategoryArchive" runat="server"  			 />
</asp:Panel>

<asp:Panel ID="pnlCalendar" runat="server" SkinID="plain" CssClass="blog_Calendar">
	<asp:Calendar ID="calBlogNav" runat="server" EnableViewState="false" CaptionAlign="Top"
		CssClass="aspcalendarmain" DayHeaderStyle-CssClass="aspcalendardayheader" DayNameFormat="FirstLetter"
		DayStyle-CssClass="aspcalendarday" FirstDayOfWeek="sunday" NextMonthText="+"
		NextPrevFormat="CustomText" NextPrevStyle-CssClass="aspcalendarnextprevious"
		OtherMonthDayStyle-CssClass="aspcalendarothermonth" PrevMonthText="-" SelectedDayStyle-CssClass="aspcalendarselectedday"
		SelectorStyle-CssClass="aspcalendarselector" ShowDayHeader="true" ShowGridLines="false"
		ShowNextPrevMonth="true" ShowTitle="true" TitleFormat="MonthYear" TitleStyle-CssClass="aspcalendartitle"
		TodayDayStyle-CssClass="aspcalendartoday" WeekendDayStyle-CssClass="aspcalendarweekendday">
	</asp:Calendar>
</asp:Panel>

<asp:Panel ID="pnlFeeds" runat="server" SkinID="plain" CssClass="blog_Feeds">
	<blog:FeedLinks ID="Feeds" runat="server" />
</asp:Panel>

<asp:Panel ID="pnlStatistics" runat="server" SkinID="plain" CssClass="blog_Statistics">
	<blog:StatsControl ID="stats" runat="server" />
</asp:Panel>

<asp:Panel ID="pnlCategories" runat="server" SkinID="plain" CssClass="blog_Categories">
	<blog:TagList ID="tags" runat="server"  />
</asp:Panel>

<asp:Panel ID="pnlArchives" runat="server" SkinID="plain" CssClass="blog_Archives">
	<blog:Archives ID="archive" runat="server" />
	<br class="clear" />
</asp:Panel>