﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryArchiveControl.ascx.cs"
	Inherits="Quantifi.UI.Controls.BlogsModule.Controls.CategoryArchiveControl" %>
<div class="AccordianLeftNavPanel">
	<asp:Repeater ID="rptCategories" runat="server">
		<HeaderTemplate>
			<div class=" QLightTheme  accordion ">
		</HeaderTemplate>
		<ItemTemplate>
			<h3>
				<%#GetHeaderLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "CategoryId")), DataBinder.Eval(Container.DataItem, "Category").ToString(),"")%>
			</h3>
			<asp:Repeater ID="rptYears" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem, "Years") %>'>
				<HeaderTemplate>
					<div class="AspNet-TreeView" style="overflow: hidden">
						<ul>
				</HeaderTemplate>
				<ItemTemplate>
					<li class="AspNet-TreeView-Root AspNet-TreeView-Leaf <%# GetCssClassForCategoryAndYear(Container.DataItem) %>">
						<a class="blogCatYearLink" href="<%# GetLinkForCategoryAndYear(Container.DataItem) %>">
							<%# DataBinder.Eval(Container.DataItem, "Year").ToString()%></a></li>
				</ItemTemplate>
				<FooterTemplate>
					</ul></div>
				</FooterTemplate>
			</asp:Repeater>
		</ItemTemplate>
		<FooterTemplate>
			</div>
		</FooterTemplate>
	</asp:Repeater>
</div>
<script type="text/javascript">
	$(function () {
		$(".accordion").accordion({
			active: <%=this.ActiveCategoryIndex.ToString() %>,
			autoHeight: false,
			collapsible: true
		});


		$(".AccordianLeftNavPanel").find(".blogCatYearLink").hover(function(){ $(this).addClass("Menu-Hover");}, function(){$(this).removeClass("Menu-Hover");}		);


		$(".AccordianLeftNavPanel").find("h3 a").click(function(){
			window.location.href = this.href; return false;
		});
	});
</script>
