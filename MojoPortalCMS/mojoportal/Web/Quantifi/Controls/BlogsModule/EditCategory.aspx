<%@ Page Language="c#" CodeBehind="EditCategory.aspx.cs" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="false" Inherits="Quantifi.UI.Blogs.BlogCategoryEdit" %>

<asp:Content ContentPlaceHolderID="leftContent" ID="MPLeftPane" runat="server" />
<asp:Content ContentPlaceHolderID="mainContent" ID="MPContent" runat="server">
	<portal:mojoPanel ID="mp1" runat="server" ArtisteerCssClass="art-Post" RenderArtisteerBlockContentDivs="true">
		<mp:CornerRounderTop ID="ctop1" runat="server" />
		<asp:Panel ID="pnlBlog" runat="server" DefaultButton="btnAddCategory" CssClass="art-Post-inner panelwrapper blogcategoryedit">
			<h2 class="moduletitle">
				<mp:SiteLabel ID="lbl1" runat="server" ConfigKey="BlogCategoriesLabel" ResourceFile="QuantifiBlogResources"
					UseLabelTag="false"></mp:SiteLabel>
			</h2>
			<portal:mojoPanel ID="MojoPanel1" runat="server" ArtisteerCssClass="art-PostContent">
				<div class="modulecontent">
					<div class="settingrow">
						<asp:Label ID="lblError" runat="server" CssClass="txterror"></asp:Label>
					</div>
					<div class="settingrow">
						<asp:Button runat="server" ID="btnAddCategory"></asp:Button>
						<asp:TextBox ID="txtNewCategoryName" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
					</div>
					<br /><br />
					<div class="settingrow">
						<portal:mojoDataList ID="dlCategories" DataKeyField="CategoryID" runat="server">
							<ItemTemplate>
								<asp:ImageButton runat="server" ID="btnEdit" ImageUrl='<%# ImageSiteRoot + "/Data/SiteImages/" + EditContentImage %>'
									CommandName="edit" ToolTip="<%# Resources.QuantifiBlogResources.EditImageAltText%>" AlternateText="Edit Category" />
								&nbsp;
								<asp:ImageButton runat="server" ID="btnDelete" ImageUrl='<%# ImageSiteRoot + "/Data/SiteImages/" + DeleteLinkImage %>'
									CommandName="delete" ToolTip="<%# Resources.QuantifiBlogResources.BlogEditDeleteButton%>"
									AlternateText="<%# Resources.QuantifiBlogResources.BlogEditDeleteButton%>" />
								&nbsp;&nbsp;
								<%# DataBinder.Eval(Container.DataItem, "Category") %>
								&nbsp;&nbsp;
								<span style="font-size:smaller;"><%# DataBinder.Eval(Container.DataItem, "CategoryID") %></span>
							</ItemTemplate>
							<EditItemTemplate>
								<div>
									<asp:TextBox ID="CategoryName" runat="server" MaxLength="50" Columns="50" CssClass="widetextbox"
										Text='<%# DataBinder.Eval(Container.DataItem, "Category") %>' />&nbsp;
									<asp:Button Text="<%# Resources.QuantifiBlogResources.BlogEditUpdateButton%>" ToolTip="<%# Resources.QuantifiBlogResources.BlogEditUpdateButton%>"
										CommandName="apply" runat="server" ID="Button1" />
								</div>
							</EditItemTemplate>
						</portal:mojoDataList>
					</div>
				</div>
			</portal:mojoPanel>
			<div class="cleared">
			</div>
		</asp:Panel>
		<mp:CornerRounderBottom ID="cbottom1" runat="server" />
	</portal:mojoPanel>
</asp:Content>
<asp:Content ContentPlaceHolderID="rightContent" ID="MPRightPane" runat="server" />
<asp:Content ContentPlaceHolderID="pageEditContent" ID="MPPageEdit" runat="server" />
