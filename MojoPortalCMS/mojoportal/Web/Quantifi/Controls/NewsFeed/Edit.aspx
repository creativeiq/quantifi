﻿<%@ Page Title="Edit News Feed Item" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.NewsFeed.Edit" %>

<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <style type="text/css">
    .hdrRow
    {
      border-bottom: 1px dotted #4F7997;
      margin: 12px 0;
      min-height: 28px;
      position: relative;
    }
    .hdrRowLable
    {
      font-size: 22px;
      color: #7AB800;
      font-weight: 400;
      padding-right: 30px;
    }
  </style>
  <div>
    <asp:Panel ID="pnlWrapper" runat="server">
      <div>
        <asp:HyperLink ID="linkManage" runat="server" CssClass="link-manage">Manage</asp:HyperLink></div>
      <div class="hdrRow " style="">
        <asp:Label ID="lblTitle" runat="server" CssClass="hdrRowLable">EDIT - NEW</asp:Label>
        <asp:LinkButton ID="btnSave" runat="server" ValidationGroup="nfi-val-grp" CausesValidation="True"
          EnableTheming="False" CssClass="link-save">Save</asp:LinkButton>
        <asp:LinkButton ID="btnDelete" runat="server" EnableTheming="False" CssClass="link-trash">Delete</asp:LinkButton>
      </div>
      <div class="settingrow">
        <label class="settinglabel">
          Title</label>
        <asp:TextBox ID="txtTitle" runat="server" CssClass="widetextbox forminput"></asp:TextBox>
        <quantifi:ExRequiredFieldValidator ControlToValidate="txtTitle" runat="server" ValidationGroup="nfi-val-grp"
          Display="Dynamic" EnableClientScript="true">                           
            <span  style="padding:2px; vertical-align: bottom;"><img src="<%= SiteUtils.GetNavigationSiteRoot() %>/Data/SiteImages/warning.png"  alt="* Required" title="Required"  /></span>
            <span class="val-e"></span>
        </quantifi:ExRequiredFieldValidator>
      </div>
      <div class="settingrow">
        <label class="settinglabel">
          Link</label>
        <asp:TextBox ID="txtLinkUrl" runat="server" CssClass="widetextbox forminput"></asp:TextBox>
        <quantifi:ExRequiredFieldValidator ControlToValidate="txtLinkUrl" runat="server"
          ValidationGroup="nfi-val-grp" Display="Dynamic" EnableClientScript="true">
            <span  style="padding:2px; vertical-align: bottom;"><img src="<%= SiteUtils.GetNavigationSiteRoot() %>/Data/SiteImages/warning.png"  alt="* Required" title="Required"  /></span>
        </quantifi:ExRequiredFieldValidator>
      </div>
      <div class=" settingRow">
        <label class="settinglabel">
          Pub Date</label>
        <mp:DatePickerControl ID="dpPubDate" runat="server" ShowTime="True" CssClass="forminput">
        </mp:DatePickerControl>
      </div>
      <div class="settingrow">
        <label class="settinglabel">
          Link Target</label>
        <asp:DropDownList ID="ddlLinkTarget" runat="server" CssClass="forminput">
          <asp:ListItem Selected="True" Text="_self" Value="0"></asp:ListItem>
          <asp:ListItem Text="_blank" Value="1"></asp:ListItem>
        </asp:DropDownList>
      </div>
      <div class="settingrow">
        <label class="settinglabel">
          IsActive</label>
        <asp:CheckBox ID="checkboxIsActive" runat="server" Checked="True" CssClass="forminput" />
      </div>
      <div class="settingrow">
        <div style="height: 300px; width: 550px;">
          <mpe:EditorControl ID="edContent" runat="server">
          </mpe:EditorControl>
        </div>
      </div>
      <div class="settingrow">
      </div>
    </asp:Panel>
  </div>
</asp:Content>
