﻿<%@ Page Title="View all Main Content" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="View.aspx.cs" Inherits="Quantifi.UI.Controls.MainContentModule.View" %>

<%@ Import Namespace="Quantifi.Shared" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <asp:Panel ID="pnlContentWrapper" runat="server" BackColor="White">
    <portal:HeadingControl ID="headline" runat="server" Text="Main Content Items" HeadingTag="h2" />
    <br />
    <a href="Edit.aspx?pageId=0" style="text-decoration: none;">
      <img src="/Data/SiteImages/add.png" alt="Create" />&nbsp;Create</a>
    <br />
    <div class="tblGrid" style="padding: 5px;">
      <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" EnableTheming="false"
        GridLines="None">
        <HeaderStyle CssClass="headerRow" />
        <RowStyle CssClass="itemRow" />
        <AlternatingRowStyle CssClass="altRow" />
        <EmptyDataTemplate>
          <div>
            No Data
          </div>
        </EmptyDataTemplate>
        <Columns>
          <asp:TemplateField>
            <HeaderTemplate>
              IsActive</HeaderTemplate>
            <ItemStyle HorizontalAlign="Center" CssClass="" />
            <ItemTemplate>
              <asp:CheckBox runat="server" Enabled="false" Checked='<%# ((Quantifi.Business.MainContentModule.MainContent)Container.DataItem).isActive %>' />
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField>
            <ItemTemplate>
              <a title="View" href='Edit.aspx?pageId=<%# Eval("PageId") %>&id=<%# Eval("Id") %>'
                class="link-edit">Edit</a>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField>
            <ItemTemplate>
              <asp:ImageButton ID="priorityUp" runat="server" ToolTip="Move Up in Priority" ImageUrl="/Data/SiteImages/up.gif"
                CommandName="Up" CommandArgument='<%# Eval("Id") %>' />
            </ItemTemplate>
          </asp:TemplateField>
          <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Priority"></asp:BoundField>
          <asp:TemplateField>
            <ItemStyle Width="26px" HorizontalAlign="Left" CssClass="" />
            <ItemTemplate>
              <asp:ImageButton ID="priorityDown" runat="server" ToolTip="Move Down in Priority"
                ImageUrl="/Data/SiteImages/dn.gif" CommandName="Down" CommandArgument='<%# Eval("Id") %>' />
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField>
            <HeaderTemplate>
              Preview</HeaderTemplate>
            <ItemStyle CssClass="" HorizontalAlign="Center" />
            <ItemTemplate>
              <a class="previewWindow" href='/home.aspx?previewMC=<%# Eval("Id")%>' title="Preview">
                <img src="/Data/SiteImages/search.gif" alt="Preview" /></a>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:BoundField HeaderText="Main Image Alt Text" DataField="MainImageAltText"></asp:BoundField>
          <asp:TemplateField>
            <HeaderTemplate>
              Link</HeaderTemplate>
            <ItemStyle CssClass="" />
            <ItemTemplate>
              <a class="previewWindow" href='<%# Eval("MainImageLink")%>' title='Preview Link '>
                <%#   Eval("MainImageLink").ToString().TrimToLength()  %></a>
            </ItemTemplate>
          </asp:TemplateField>
        </Columns>
      </asp:GridView>
    </div>
  </asp:Panel>
  <script type="text/javascript">
    $(document).ready(function () {
      $(".previewImage").colorbox();
      $(".previewWindow").colorbox({ width: "90%", height: "90%", iframe: true });
    });
  </script>
</asp:Content>
