﻿<%@ Page Title="Edit Main Content" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.MainContentModule.Edit" %>

<%@ Register Src="~/Quantifi/Dialog/FileSelectorControl.ascx" TagName="FileSelectorControl"
  TagPrefix="quantifi" %>
<%@ Register Src="~/Quantifi/Controls/ColorPicker/ColorPickerControl.ascx" TagName="ColorPicker"
  TagPrefix="quantifi" %>


<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <asp:Panel ID="pnlWrapper" runat="server" BackColor="White">
    <div class="settingsPanels" style="margin: 15px 0px;">
      <portal:HeadingControl ID="headline" runat="server" Text="Add New Main Content" HeadingTag="h2" />
      <div class="padFive">
        <asp:Button ID="btnSave" runat="server" Text="Save" CausesValidation="true" ValidationGroup="mc-val-grp" />
        <span style="padding: 0 15px;">&nbsp;</span>
        <asp:Button ID="btnDelete" runat="server" Text="Delete" />
        <span style="padding: 0 15px;">&nbsp;</span>
        <asp:HyperLink ID="linkCancel1" runat="server" Font-Overline="false" Font-Size="Small"
          NavigateUrl="~/Quantifi/Controls/MainContentModule/View.aspx"><img src="/Data/SiteImages/CancelChanges.gif" alt="Cancel" />&nbsp;Back</asp:HyperLink></div>
      <br />
      <div class="settingContainer">
        <div class="settingRow">
          <div class="lbl">
            Priority</div>
          <asp:TextBox ID="txtPriority" runat="server" Columns="10" />
        </div>
        <div class="settingRow" style="clear: left; height: 30px;">
          <div style="float: left; width: 200px;">
            <asp:CheckBox ID="checkboxIsActive" runat="server" Text="IsActive" /></div>
          <div style="float: left;">
            <asp:PlaceHolder ID="phPreview" runat="server">
              <asp:HyperLink ID="linkPreview" ToolTip="Preview" runat="server" Font-Underline="false"><img src="/Data/SiteImages/search.gif" alt="Preview" />&nbsp;Preview</asp:HyperLink>
              <script type="text/javascript">
                $("#<%= this.linkPreview.ClientID %>").colorbox({ width: "95%", height: "95%", iframe: true });
              </script>
            </asp:PlaceHolder>
          </div>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Campaign Name (some special charactors will be removed)</div>
          <asp:TextBox ID="txtCampaign" runat="server" CssClass="verywidetextbox" MaxLength="50"></asp:TextBox>
          <quantifi:ExRequiredFieldValidator runat="server" ControlToValidate="txtCampaign"
            ValidationGroup="mc-val-grp" Display="Dynamic" EnableClientScript="true">
                 <span style="padding:2px;"><img src="/Data/SiteImages/warning.png" alt="* Required" title="Required" /></span>
          </quantifi:ExRequiredFieldValidator>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Background Image</div>
          <quantifi:FileSelectorControl ID="fileSelector_BackgroundImg" runat="server" FileType="image"
            ValidationGroup="mc-val-grp" Display="Dynamic">
            <ValidatorContentTemplate>
              <span style="padding: 2px;">
                <img src="/Data/SiteImages/warning.png" alt="* Required" title="Required" /></span>
            </ValidatorContentTemplate>
          </quantifi:FileSelectorControl>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Main Image</div>
          <quantifi:FileSelectorControl ID="fileSelector_MainImg" runat="server" FileType="image"
            ValidationGroup="mc-val-grp" Display="Dynamic">
            <ValidatorContentTemplate>
              <span style="padding: 2px;">
                <img src="/Data/SiteImages/warning.png" alt="* Required" title="Required" /></span>
            </ValidatorContentTemplate>
          </quantifi:FileSelectorControl>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Main Image Alt Text</div>
          <asp:TextBox ID="txtMainImageAltText" runat="server" CssClass="verywidetextbox"></asp:TextBox>
          <quantifi:ExRequiredFieldValidator runat="server" ControlToValidate="txtMainImageAltText"
            ValidationGroup="mc-val-grp" Display="Dynamic" EnableClientScript="true">
            <span  style="padding:2px;"><img src="/Data/SiteImages/warning.png"  alt="* Required" title="Required"  /></span>
          </quantifi:ExRequiredFieldValidator>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Main Link <span style="padding-left: 50px;"><a id="testMainLink" href="#" style="color: #04B404;">
              TEST LINK</a></span></div>
          <asp:TextBox ID="txtMainLink" runat="server" CssClass="verywidetextbox"></asp:TextBox>&nbsp;
          <quantifi:ExRequiredFieldValidator runat="server" ControlToValidate="txtMainLink"
            ValidationGroup="mc-val-grp" Display="Dynamic" EnableClientScript="true">
            <span  style="padding:2px;"><img src="/Data/SiteImages/warning.png"  alt="* Required" title="Required"  /></span>
          </quantifi:ExRequiredFieldValidator>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Quantifi Logo Image</div>
          <quantifi:FileSelectorControl ID="fileSelector_LogoImg" runat="server" FileType="image"
            ValidationGroup="mc-val-grp" Display="Dynamic">
            <ValidatorContentTemplate>
              <span style="padding: 2px;">
                <img src="/Data/SiteImages/warning.png" alt="* Required" title="Required" /></span>
            </ValidatorContentTemplate>
          </quantifi:FileSelectorControl>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Request A Demo Image</div>
          <quantifi:FileSelectorControl ID="fileSelector_DemoImg" runat="server" FileType="image"
            ValidationGroup="mc-val-grp" Display="Dynamic">
            <ValidatorContentTemplate>
              <span style="padding: 2px;">
                <img src="/Data/SiteImages/warning.png" alt="* Required" title="Required" /></span>
            </ValidatorContentTemplate>
          </quantifi:FileSelectorControl>
        </div>
        <div class="settingRow">
          <div class="lbl">
            TopNav CssClass</div>
          <asp:DropDownList ID="ddlTopNavCssClass" runat="server">
            <asp:ListItem Text="-- Please Select --" Value="normal"></asp:ListItem>
            <asp:ListItem Text="White" Value="whiteText"></asp:ListItem>
            <asp:ListItem Text="Blue" Value="blueText"></asp:ListItem>
            <asp:ListItem Text="Green" Value="greenText"></asp:ListItem>
            <asp:ListItem Text="Green Text, Transparent Blue Background" Value="blueBackGreenText"></asp:ListItem>
            <asp:ListItem Text="Orange" Value="orangeText"></asp:ListItem>
          </asp:DropDownList>
        </div>
        <div class="settingRow">
          <div class="lbl">
            TopSubNav CssClass</div>
          <asp:DropDownList ID="ddlTopSubNavCssClass" runat="server">
            <asp:ListItem Text="-- Please Select --" Value="normalSub"></asp:ListItem>
            <asp:ListItem Text="White" Value="subWhiteText"></asp:ListItem>
            <asp:ListItem Text="Blue" Value="subBlueText"></asp:ListItem>
            <asp:ListItem Text="Green" Value="subGreenText"></asp:ListItem>          
            <asp:ListItem Text="Orange" Value="subOrangeText"></asp:ListItem>
          </asp:DropDownList>
        </div>
        <div class="settingRow">
          <div class="lbl">
            Features Header Color</div>
          <quantifi:ColorPicker ID="colorFeatureHeaderColor" runat="server" />
        </div>
        <div class="settingRow">
          <div class="lbl">
            Features Mask Color</div>
          <quantifi:ColorPicker ID="colorFeatureMaskColor" runat="server" />
        </div>
        <div class="settingRow">
          &nbsp;
        </div>
      </div>
    </div>
  </asp:Panel>
  <script type="text/javascript">


    function updateTopNavCss() {
      var selectedCss = $("#<%= ddlTopNavCssClass.ClientID %> option:selected").val();
      if (selectedCss != null && selectedCss.length > 0) {
        $("#wrapwebsite").attr("class", "").addClass(selectedCss);
      }
    }
    function updateTopSubNavCss() {
      var selectedCss = $("#<%= ddlTopSubNavCssClass.ClientID %> option:selected").val();
      if (selectedCss != null && selectedCss.length > 0) {
        $("#wrapheader").attr("class", "").addClass(selectedCss);
      }
    }

    $(document).ready(function () {

      $("#testMainLink").colorbox({ href: function () { return $("#<%= txtMainLink.ClientID %>").val(); }, width: "90%", height: "90%", iframe: true });

      $(".popopen").colorbox({ width: "90%", height: "90%", iframe: true });

      $("#<%= txtPriority.ClientID %>").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8) {
          // let it happen, don't do anything
        }
        else {
          // Ensure that it is a number and stop the keypress
          if (event.keyCode < 48 || event.keyCode > 57) {
            event.preventDefault();
          }
        }
      });


      updateTopNavCss();
      updateTopSubNavCss();

      $("#<%= ddlTopNavCssClass.ClientID %>").change(function () {
        updateTopNavCss();
      });

      $("#<%= ddlTopSubNavCssClass.ClientID %>").change(function () {
        updateTopSubNavCss();
      });

    });

    

  </script>
</asp:Content>
