﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master" AutoEventWireup="true" CodeBehind="ViewTag.aspx.cs" Inherits="Quantifi.UI.Controls.ContentArchiveModule.ViewTag" %>
<asp:Content ID="Content1" ContentPlaceHolderID="leftContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">

	<asp:Panel ID="pnlModuleDesc" runat="server" CssClass="moduleDesc"></asp:Panel>
	<asp:Repeater ID="rptItems" runat="server">
		<HeaderTemplate>
			<ul class="list">
		</HeaderTemplate>
		<ItemTemplate>
			<li class="listItem">
				<div class="listItemContent">
					<h3><%#  (DataBinder.Eval(Container.DataItem, "Title")).ToString() %></h3>
					<h4><%#  (DataBinder.Eval(Container.DataItem, "SubTitle")).ToString() %></h4>
					<div class="date" >
						<%#  ((DateTime)DataBinder.Eval(Container.DataItem, "StartDate")).ToString("D")%>
					</div>
				</div>
			</li>
		</ItemTemplate>
		<AlternatingItemTemplate>
			<li class="listItem altlistItem">
				<div class="listItemContent">
					<h3><%#  (DataBinder.Eval(Container.DataItem, "Title")).ToString() %></h3>
					<h4><%#  (DataBinder.Eval(Container.DataItem, "SubTitle")).ToString() %></h4>
					<div class="date" >
						<%#  ((DateTime)DataBinder.Eval(Container.DataItem, "StartDate")).ToString("D")%>
					</div>
				</div>
			</li>
		</AlternatingItemTemplate>
		<FooterTemplate>
			</ul>
		</FooterTemplate>
	</asp:Repeater>
	<div class="blogpager">
		<portal:mojoCutePager ID="pgr" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageEditContent" runat="server">
</asp:Content>
