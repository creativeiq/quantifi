﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewListControl.ascx.cs"
	Inherits="Quantifi.UI.Controls.ContentArchiveModule.ViewListControl" %>
<%@ Register Src="~/Quantifi/Controls/Tags/ItemTagsView.ascx" TagName="ItemTagsView"
	TagPrefix="quantifi" %>
<asp:Panel ID="pnlContentWrapper" runat="server" CssClass="ContentArchiveModule">
	<portal:ModuleTitleControl ID="Title1" runat="server" />
	<asp:Panel ID="pnlModuleDesc" runat="server" CssClass="moduleDesc">
	</asp:Panel>
	<asp:Repeater ID="rptItems" runat="server">
		<HeaderTemplate>
			<div class="list">
		</HeaderTemplate>
		<ItemTemplate>
			<div class="listItem">
				<h3>
					<%#  (DataBinder.Eval(Container.DataItem, "Title")).ToString() %>
					<%# GetAdminLink( (int)(DataBinder.Eval(Container.DataItem, "Id")))%></h3>
				<h4>
					<%#  (DataBinder.Eval(Container.DataItem, "SubTitle")).ToString() %></h4>
				<div class="desc">
					<%#  (DataBinder.Eval(Container.DataItem, "Description")).ToString()%>
				</div>
				<div class="link">
					<span>
						<%#  (DataBinder.Eval(Container.DataItem, "LinkDescription")).ToString() + "&nbsp;"%></span><a
							href="<%# (DataBinder.Eval(Container.DataItem, "LinkUrl")).ToString()%>" <%#  GetLinkTarget(Container.DataItem) %>><%#  (DataBinder.Eval(Container.DataItem, "LinkText")).ToString()%></a>
				</div>
				<div class="clear">
				</div>
		<%--		<quantifi:ItemTagsView runat="server" TaggedObject="<%# ((Quantifi.Business.TagsModule.iTaggable)Container.DataItem)  %>" />
		--%>	</div>
		</ItemTemplate>
		<AlternatingItemTemplate>
			<div class="listItem altlistItem">
				<h3>
					<%#  (DataBinder.Eval(Container.DataItem, "Title")).ToString() %><%# GetAdminLink( (int)(DataBinder.Eval(Container.DataItem, "Id")))%></h3>
				<h4>
					<%#  (DataBinder.Eval(Container.DataItem, "SubTitle")).ToString() %></h4>
				<div class="desc">
					<%#  (DataBinder.Eval(Container.DataItem, "Description")).ToString()%></div>
				<div class="link">
					<span>
						<%#  (DataBinder.Eval(Container.DataItem, "LinkDescription")).ToString() + "&nbsp;"%></span><a
							href="<%# (DataBinder.Eval(Container.DataItem, "LinkUrl")).ToString()%>" <%#  GetLinkTarget(Container.DataItem) %>><%#  (DataBinder.Eval(Container.DataItem, "LinkText")).ToString()%></a>
				</div>
				<div class="clear">
				</div>
		<%--		<quantifi:ItemTagsView runat="server" TaggedObject="<%# ((Quantifi.Business.TagsModule.iTaggable)Container.DataItem)  %>" />
		--%>	</div>
		</AlternatingItemTemplate>
		<FooterTemplate>
			</div>
		</FooterTemplate>
	</asp:Repeater>
	<asp:Panel ID="pnlViewArchive" runat="server" CssClass="archiveLink">
		<span class="archiveLinkWrapper">
			<asp:HyperLink ID="linkViewArchive" runat="server"></asp:HyperLink></span>
	</asp:Panel>
</asp:Panel>
