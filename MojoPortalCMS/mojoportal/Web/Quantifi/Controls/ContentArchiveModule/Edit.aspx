﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.ContentArchiveModule.Edit" %>

<%@ Register Src="~/Quantifi/Controls/Tags/Controls/TagSelectorControl.ascx" TagName="TagSelectorControl"
	TagPrefix="quantifi" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
	<asp:Panel ID="pnlError" runat="server" Visible="false" EnableViewState="false">
		<div class="ErrorContainer" style="margin: 5px; padding: 5px; color: #FFF; border: solid 1px #AA0000;
			background-color: #AA0000">
			<span>
				<img src="/Data/SiteImages/warning-yellow.png" alt="Warning" /></span>
			<asp:Literal ID="litErrorMsg" runat="server" EnableViewState="false">An Error Occured</asp:Literal>
		</div>
		<script type="text/javascript">
			$(document).ready(function () {
				$(".ErrorContainer").animate({
					backgroundColor: "#FFF",
					color: "#000000"
				}, 5000);

			});
		
		
		</script>
	</asp:Panel>
	<div style="height: 900px;">
		<fieldset>
			<legend>
				<asp:Literal ID="litLegend" runat="server">Edit</asp:Literal>
			</legend>
			<div>
				<div class="editRow">
					<asp:Label runat="server" CssClass="label">Title</asp:Label>
					<asp:TextBox ID="txtTitle" runat="server" CssClass="longtxtbox"></asp:TextBox>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="txtTitle" EnableClientScript="true"
						ErrorMessage="Title is required">* Required</asp:RequiredFieldValidator>
				</div>
				<div class="editRow">
					<asp:Label runat="server" CssClass="label">SubTitle</asp:Label>
					<asp:TextBox ID="txtSubTitle" runat="server" CssClass="longtxtbox"></asp:TextBox>
				</div>
				<div class="htmlEditorRow">
					<asp:Label runat="server" CssClass="label">Description</asp:Label>
					<div>
						<mpe:EditorControl ID="edContent" runat="server">
						</mpe:EditorControl>
					</div>
				</div>
				<div class="editRow">
					<asp:Label runat="server" CssClass="label">Link Description</asp:Label>
					<asp:TextBox ID="txtLinkDesc" runat="server" CssClass="longtxtbox"></asp:TextBox>
				</div>
				<div class="editRow">
					<asp:Label runat="server" CssClass="label">Link Text</asp:Label>
					<asp:TextBox ID="txtLinkText" runat="server" CssClass="longtxtbox"></asp:TextBox>
				</div>
				<div class="editRow">
					<asp:Label runat="server" CssClass="label">Link URL</asp:Label>
					<asp:TextBox ID="txtLinkUrl" runat="server" CssClass="longtxtbox"></asp:TextBox>
				</div>
				<div class="editRow">
					<div style="float: left; width: 400px;">
						<div class="editRow">
							<asp:CheckBox ID="checkboxIsExternalLink" runat="server" Text="Open Link In New Window"
								Checked="true" Font-Bold="true" />
						</div>
						<div class="editRow">
							<asp:Label CssClass="label" runat="server">Start Date</asp:Label>
							<div>
								<mp:DatePickerControl ID="dpBeginDate" runat="server" ShowTime="True" CssClass="forminput">
								</mp:DatePickerControl>
							</div>
						</div>
					</div>
					<div style="clear: left;">
					</div>
				</div>
				<div class="editRow">
					<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="saveButton" />
					<asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="deleteButton" />
					<asp:HyperLink ID="linkCancel" runat="server" CssClass="cancelButton">Cancel</asp:HyperLink>
				</div>
			</div>
		</fieldset>
	</div>
	<asp:HiddenField ID="hdnPrevUrl" runat="server" Value="" />
</asp:Content>
