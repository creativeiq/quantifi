﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="Archive.aspx.cs" Inherits="Quantifi.UI.Controls.ContentArchiveModule.Archive" %>

<%@ Register Src="~/Quantifi/Controls/Tags/ItemTagsView.ascx" TagName="ItemTagsView"
	TagPrefix="quantifi" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
	<asp:Panel ID="pnlContentWrapper" runat="server" CssClass="ContentArchiveModule ArchiveView">
		<h2 class="art-PostHeader moduletitle">
			<asp:Literal ID="litTitle" runat="server">Archive</asp:Literal></h2>
		<asp:Panel ID="pnlModuleDesc" runat="server" CssClass="moduleDesc">
		</asp:Panel>
		<asp:Repeater ID="rptItems" runat="server">
			<HeaderTemplate>
				<div id="archivelist" class="archivelist">
			</HeaderTemplate>
			<ItemTemplate>
				<div class="listItem">
					<h3>
						<%#  (DataBinder.Eval(Container.DataItem, "Title")).ToString() %>
						<%# GetAdminLink( (int)(DataBinder.Eval(Container.DataItem, "Id")))%></h3>
					<div>
						<div class="date">
							<%#  GetLocalDateTime((DateTime)(DataBinder.Eval(Container.DataItem, "StartDate"))) %>
						</div>
						<div class="content" style="display: none; padding-bottom: 5px;">
							<h4>
								<%#  (DataBinder.Eval(Container.DataItem, "SubTitle")).ToString() %></h4>
							<%#  (DataBinder.Eval(Container.DataItem, "Description")).ToString()%>
							<div class="link">
								<span>
									<%# GetLinkDescription((DataBinder.Eval(Container.DataItem, "LinkDescription")).ToString()) %></span><a
										href="<%# (DataBinder.Eval(Container.DataItem, "LinkUrl")).ToString()%>" <%#  GetLinkTarget(Container.DataItem) %>><%#  (DataBinder.Eval(Container.DataItem, "LinkText")).ToString()%></a>
							</div>
							<quantifi:ItemTagsView runat="server" TaggedObject="<%# ((Quantifi.Business.TagsModule.iTaggable)Container.DataItem)  %>" Visible="false" />
						</div>
					</div>
				</div>
			</ItemTemplate>
			<FooterTemplate>
				</div>
			</FooterTemplate>
		</asp:Repeater>
		<div class="pager">
			<portal:mojoCutePager ID="pgr" runat="server" />
		</div>
		<script type="text/javascript">
			$("#archivelist").ready(function () {
				$(this).find("h3").click(function () {
					if ($(this).hasClass("open")) {
						$(this).removeClass("open").next().find(".content").slideUp(400);
					}
					else {
						$(this).addClass("open").next().find(".content").slideDown(400);
					}
				});
			});
		</script>
	</asp:Panel>
</asp:Content>
