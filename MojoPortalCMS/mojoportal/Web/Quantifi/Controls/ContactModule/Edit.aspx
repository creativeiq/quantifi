﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.ContactModule.Edit" %>


<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
	<mp:CornerRounderTop ID="ctop1" runat="server" />
	<asp:Panel ID="pnlContent" runat="server" DefaultButton="btnUpdate" CssClass="panelwrapper editpage">
		<div class="modulecontent editContactUs">
			<fieldset>
				<legend id="legendTitle" runat="server"> Edit Contact Us Content </legend>
				<div>
					<div>
						<h3>Top Description HTML</h3>
						<p class="info">Content To be shown above the form</p>
						<mpe:EditorControl ID="edTopDesc" runat="server"  ></mpe:EditorControl>
					</div>
					<br /><br />
					<div>
						<h3>Thank You HTML</h3>
						<p class="info">Content To be shown on successful submission of the form</p>
						<mpe:EditorControl ID="edBottomDesc" runat="server"  ></mpe:EditorControl>
					</div>
					<br /><br />

					<div class="bottomButtons">
						<asp:Button ID="btnUpdate" runat="server" Text="Save"  /> 
						<span style="margin-left:25px;">
							<asp:LinkButton ID="btnCancel" runat="server" Text="Cancel"  />
						</span>

					</div>
				</div>
			</fieldset>
		</div>
		<asp:HiddenField ID="hdnReturnUrl" runat="server" />
	</asp:Panel>
	<mp:CornerRounderBottom ID="cbottom1" runat="server" />



	<portal:SessionKeepAliveControl ID="ka1" runat="server" />

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="leftContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageEditContent" runat="server">
</asp:Content>
