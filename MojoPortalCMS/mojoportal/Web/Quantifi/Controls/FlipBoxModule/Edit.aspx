﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.FlipBoxModule.Edit" %>

<%@ Register Src="~/Quantifi/Dialog/FileSelectorControl.ascx" TagName="FileSelectorControl"
	TagPrefix="quantifi" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
	<asp:Panel ID="pnlError" runat="server" Visible="false" EnableViewState="false">
		<div class="ErrorContainer" style="margin: 5px; padding: 5px; color: #FFF; border: solid 1px #AA0000;
			background-color: #AA0000">
			<span>
				<img src="/Data/SiteImages/warning-yellow.png" alt="Warning" /></span>
			<asp:Literal ID="litErrorMsg" runat="server" EnableViewState="false">An Error Occured</asp:Literal>
		</div>
		<script type="text/javascript">
			$(document).ready(function () {
				$(".ErrorContainer").animate({
					backgroundColor: "#FFF",
					color: "#000000"
				}, 5000);

			});
		</script>
	</asp:Panel>
	<div>
		<fieldset>
			<legend>
				<asp:Literal ID="litLegend" runat="server">Add</asp:Literal>
			</legend>
			<div>
				<div class="editRow">
					<asp:Label runat="server" CssClass="label">Image Url</asp:Label></div>
				<div class="editRow">
					<quantifi:FileSelectorControl ID="ImageSelector_ImageUrl" runat="server" FileType="image" />
				</div>
				<div class="editRow">
					<asp:Label runat="server" AssociatedControlID="txtImgAlt" CssClass="label">Image Alt Text</asp:Label>
					<asp:TextBox ID="txtImgAlt" runat="server" CssClass="longtxtbox"></asp:TextBox>
				</div>
				<div class="editRow">
					<asp:Label runat="server" CssClass="label">Description</asp:Label></div>
				<div class="editRow">
					<mpe:EditorControl ID="editorDescription" runat="server"/>
				</div>
				<div class="editRow">
					<asp:Label runat="server" AssociatedControlID="txtLinkText" CssClass="label">Link Text</asp:Label>
					<asp:TextBox ID="txtLinkText" runat="server" CssClass="longtxtbox"></asp:TextBox>
				</div>
				<div class="editRow">
					<asp:Label runat="server" AssociatedControlID="txtLinkUrl" CssClass="label">Link Url</asp:Label>
					<asp:TextBox ID="txtLinkUrl" runat="server" CssClass="longtxtbox"></asp:TextBox>
				</div>
				<div class="editRow">
					<asp:CheckBox ID="checkboxIsExternalLink" runat="server" Text="is External Link" Font-Bold="true" />
				</div>

				<div class="editRow">
					<asp:Label  runat="server" AssociatedControlID="txtLinkText" CssClass="label">Priority</asp:Label>
					<asp:TextBox ID="txtPriority" runat="server" CssClass=""></asp:TextBox>
					<ajaxToolkit:FilteredTextBoxExtender ID="Filteredtextboxextender1" runat="server" FilterType="Numbers"   TargetControlID="txtPriority">
          </ajaxToolkit:FilteredTextBoxExtender>
 
				</div>

				<div class="editRow">
					<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="saveButton" />
					<asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="deleteButton" />
					<asp:HyperLink ID="linkCancel" runat="server" CssClass="cancelButton">Cancel</asp:HyperLink>
				</div>
			</div>
		</fieldset>
	</div>
</asp:Content>
