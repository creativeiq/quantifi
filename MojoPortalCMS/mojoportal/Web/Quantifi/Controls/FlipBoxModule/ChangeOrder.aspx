﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="ChangeOrder.aspx.cs" Inherits="Quantifi.UI.Controls.FlipBoxModule.ChangeOrder" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
	<asp:Panel ID="pnlError" runat="server" Visible="false" EnableViewState="false">
		<div class="ErrorContainer" style="margin: 5px; padding: 5px; color: #FFF; border: solid 1px #AA0000;
			background-color: #AA0000">
			<span>
				<img src="/Data/SiteImages/warning-yellow.png" alt="Warning" /></span>
			<asp:Literal ID="litErrorMsg" runat="server" EnableViewState="false">An Error Occured</asp:Literal>
		</div>
		<script type="text/javascript">
			$(document).ready(function () {
				$(".ErrorContainer").animate({
					backgroundColor: "#FFF",
					color: "#000000"
				}, 5000);

			});
		</script>
	</asp:Panel>
	<div>




	</div>
</asp:Content>
