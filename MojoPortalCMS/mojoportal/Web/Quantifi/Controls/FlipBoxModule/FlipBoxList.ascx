﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlipBoxList.ascx.cs"
	Inherits="Quantifi.UI.Controls.FlipBoxModule.FlipBoxList" %>


<portal:ModulePanel ID="pnlContainer" runat="server">
	<asp:Panel ID="pnlContent" runat="server" CssClass="FlipBoxList">
		<portal:ModuleTitleControl ID="Title1" runat="server" />
		<div class="flipboxcontainer">
			<asp:Repeater ID="rptItems" runat="server">
				<HeaderTemplate>
					<div class="sponsorListHolder">
				</HeaderTemplate>
				<ItemTemplate>
					<div title="Click to flip" class="sponsor">
						<%# GetAdminLink( (int)(DataBinder.Eval(Container.DataItem, "Id")))%>
						<div class="sponsorFlip"  >
							<div class="sponsorImg" style="width:100%; height:100%;  background: url('<%#  (DataBinder.Eval(Container.DataItem, "ImageUrl")).ToString() %>') no-repeat center center transparent " >
									 &nbsp;
<%--								<img alt="<%#  (DataBinder.Eval(Container.DataItem, "ImageAltText")).ToString() %>"
									src="<%#  (DataBinder.Eval(Container.DataItem, "ImageUrl")).ToString() %>" />--%>
							</div>
						</div>
						<div class="sponsorData">
							<div class="sponsorText">
								<div class="sponsorDescription">
									<%#  (DataBinder.Eval(Container.DataItem, "Description")).ToString() %>
								</div>
								<div class="sponsorURL">
									 <%# GetLink(Container.DataItem) %>
								</div>
							</div>
						</div>
					</div>
				</ItemTemplate>
				<FooterTemplate>
						<div class="clear">
						</div>
					</div>
				</FooterTemplate>
			</asp:Repeater>
		</div>
	</asp:Panel>

	<asp:Literal ID="litScripts" runat="server" Visible="false" ViewStateMode="Disabled">
	<script type="text/javascript">
		$(document).ready(function () {
			$('.sponsorFlip').bind("click", function () {
				var elem = $(this);
				if (elem.data('flipped')) {
					elem.revertFlip();
					elem.data('flipped', false)
				}
				else {
					elem.flip({
						direction: 'lr',
						speed: 350,
						color: "#F9F9F9",
						onBefore: function () {
							elem.html(elem.siblings('.sponsorData').html());
						}
					});
					elem.data('flipped', true);
				}
			});
		});
	</script>
	</asp:Literal>
</portal:ModulePanel>
