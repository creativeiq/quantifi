<%@ Control Language="C#" Inherits="Quantifi.UI.Controls.LinksModule.LinksModule"
	CodeBehind="Links.ascx.cs" AutoEventWireup="false" %>


<portal:ModulePanel ID="pnlContainer" runat="server" CssClass="panelwrapper">
	<mp:CornerRounderTop ID="ctop1" runat="server" EnableViewState="false" />
	<portal:ModuleTitleControl ID="Title1" runat="server" EnableViewState="false" />
	<asp:Panel ID="pnlWrapper" runat="server" CssClass="panelwrapper linksmodule">

		<asp:Panel ID="pnl" runat="server" >
			<div runat="server" id="mask" class="mask" />
			<div class="content">
				<%= GetHeaderHtml("linksheader") %>
				<div class="imagelinks">
					<asp:Repeater ID="rptLinks" runat="server">
						<headertemplate>
							<ul class="linkitems">
						</headertemplate>

						<itemtemplate>
							<li class="linkitem">
								<%# GetImageLink(DataBinder.Eval(Container.DataItem, "Title").ToString(), DataBinder.Eval(Container.DataItem, "Url").ToString(), DataBinder.Eval(Container.DataItem, "ImageUrl").ToString(), DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Target").ToString(),"imgThumb")%>
								<%# GetTitleLink(DataBinder.Eval(Container.DataItem, "Title").ToString(), DataBinder.Eval(Container.DataItem, "Url").ToString(), DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Target").ToString(),"title")%>
								<%# GetDateHtml(DataBinder.Eval(Container.DataItem, "PubDate").ToString(),"date") %>
								<%# GetLocationHtml(DataBinder.Eval(Container.DataItem, "Location").ToString(),"loc") %>
								
								<asp:HyperLink ID="editLink2" runat="server" CssClass="editlink" EnableViewState="false"
									Text="<%# Resources.QuantifiLinkResources.LinksEditLink %>" ToolTip="<%# Resources.QuantifiLinkResources.LinksEditLink %>"
									NavigateUrl='<%# this.SiteRoot + "/Quantifi/Controls/LinksModule/EditLink.aspx?ItemID=" + DataBinder.Eval(Container.DataItem,"ItemID") + "&amp;mid=" + ModuleId + "&amp;pageid=" + PageId + "&amp;curUrl=" + Server.UrlEncode( SiteUtils.GetCurrentPageUrl()) %>'
									Visible="<%# IsEditable %>" ImageUrl="<%# LinkImage %>"></asp:HyperLink>
									
								<%# GetDescriptionHtml(DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Url").ToString(), DataBinder.Eval(Container.DataItem, "Target").ToString(), "desc")%>

								<div class="end"></div>
							</li>
						</itemtemplate>

						<alternatingitemtemplate>
							<li class="linkaltitem">
								<%# GetImageLink(DataBinder.Eval(Container.DataItem, "Title").ToString(), DataBinder.Eval(Container.DataItem, "Url").ToString(), DataBinder.Eval(Container.DataItem, "ImageUrl").ToString(), DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Target").ToString(),"imgThumb")%>
								<%# GetTitleLink(DataBinder.Eval(Container.DataItem, "Title").ToString(), DataBinder.Eval(Container.DataItem, "Url").ToString(), DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Target").ToString(),"title")%>
								<%# GetDateHtml(DataBinder.Eval(Container.DataItem, "PubDate").ToString(),"date") %>
								<%# GetLocationHtml(DataBinder.Eval(Container.DataItem, "Location").ToString(),"loc") %>
								<asp:HyperLink ID="editLink3" runat="server" CssClass="editlink" EnableViewState="false"
									Text="<%# Resources.QuantifiLinkResources.LinksEditLink %>" ToolTip="<%# Resources.QuantifiLinkResources.LinksEditLink %>"
									NavigateUrl='<%# this.SiteRoot + "/Quantifi/Controls/LinksModule/EditLink.aspx?ItemID=" + DataBinder.Eval(Container.DataItem,"ItemID") + "&amp;mid=" + ModuleId + "&amp;pageid=" + PageId + "&amp;curUrl=" + Server.UrlEncode( SiteUtils.GetCurrentPageUrl()) %>'
									Visible="<%# IsEditable%>" ImageUrl="<%# LinkImage %>"></asp:HyperLink>
								<%# GetDescriptionHtml(DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Url").ToString(), DataBinder.Eval(Container.DataItem, "Target").ToString(), "desc")%>
							
	
							</li>
						</alternatingitemtemplate>
						<footertemplate>
							</ul>
						</footertemplate>
					</asp:Repeater>

					<asp:Repeater ID="rptDescription" runat="server" Visible="false">
						<itemtemplate>
							<div class="linkdesc">
								<asp:HyperLink ID="editLink2" runat="server" EnableViewState="false" Text="<%# Resources.QuantifiLinkResources.LinksEditLink %>"
									ToolTip="<%# Resources.QuantifiLinkResources.LinksEditLink %>" NavigateUrl='<%# this.SiteRoot + "/LinkModule/EditLink.aspx?ItemID=" + DataBinder.Eval(Container.DataItem,"ItemID") + "&amp;mid=" + ModuleId + "&amp;pageid=" + PageId %>'
									Visible="<%# IsEditable%>" ImageUrl="<%# LinkImage %>"></asp:HyperLink>
								<asp:Literal ID="lblDescription2" runat="server" Visible="<%# UseDescription%>" Text='<%# DataBinder.Eval(Container.DataItem,"Description").ToString()%>'
									EnableViewState="false" />
							</div>
						</itemtemplate>
						<alternatingitemtemplate>
							<div class="linkdesc linkdescalt">
								<asp:HyperLink ID="editLink3" runat="server" EnableViewState="false" Text='<%# Resources.QuantifiLinkResources.EditLinksAddLinkLabel %>'
									NavigateUrl='<%# this.SiteRoot + "/LinkModule/EditLink.aspx?ItemID=" + DataBinder.Eval(Container.DataItem,"ItemID") + "&amp;mid=" + ModuleId + "&amp;pageid=" + PageId %>'
									Visible="<%# IsEditable%>" ImageUrl="<%# LinkImage %>"></asp:HyperLink>
								<asp:ImageButton ID="btnDelete" runat="server" ImageUrl="<%# DeleteLinkImage %>"
									Visible="<%# ShowDeleteIcon%>" CommandName="delete" ToolTip='<%# Resources.QuantifiLinkResources.LinksDeleteLink %>'
									CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ItemId", "{0}") %>'>
								</asp:ImageButton>
								<asp:Literal ID="lblDescription2" runat="server" Visible="<%# UseDescription%>" Text='<%# DataBinder.Eval(Container.DataItem,"Description").ToString()%>'
									EnableViewState="false" />
							</div>
						</alternatingitemtemplate>
					</asp:Repeater>
					<portal:mojoCutePager ID="pgr" runat="server" Visible="false" />
				</div>
				<%= GetFooterHtml("linksfooter")%>
			</div>
		</asp:Panel>
	</asp:Panel>
	<mp:CornerRounderBottom ID="cbottom1" runat="server" EnableViewState="false" />
</portal:ModulePanel>
