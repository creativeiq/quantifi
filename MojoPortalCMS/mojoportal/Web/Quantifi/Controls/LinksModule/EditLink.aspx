<%@ Page Language="c#" CodeBehind="EditLink.aspx.cs" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="false" Inherits="Quantifi.UI.Controls.LinksModule.EditLinks" %>

<%@ Register Src="~/Quantifi/Dialog/FileSelectorControl.ascx" TagPrefix="Quantifi"
  TagName="FileSelector" %>
<asp:Content ContentPlaceHolderID="leftContent" ID="MPLeftPane" runat="server" />
<asp:Content ContentPlaceHolderID="mainContent" ID="MPContent" runat="server">
  <mp:CornerRounderTop ID="ctop1" runat="server" />
  <asp:Panel ID="pnlWrapper" runat="server" CssClass="panelwrapper linksmodule">
    <asp:Panel ID="pnlEdit" runat="server" CssClass="modulecontent" DefaultButton="updateButton">
      <fieldset class="linksedit">
        <legend>
          <mp:SiteLabel ID="lblLinkDetails" runat="server" ConfigKey="EditLinksDetailsLabel"
            ResourceFile="QuantifiLinkResources" UseLabelTag="false"></mp:SiteLabel>
        </legend>
        <%-- Description --%>
        <div class="settingrow">
          <mp:SiteLabel ID="lblDescription" runat="server" ForControl="fckDescription" ConfigKey="EditLinksDescriptionLabel"
            ResourceFile="QuantifiLinkResources" CssClass="settinglabel"></mp:SiteLabel>
        </div>
        <div class="settingrow">
          <mpe:EditorControl ID="edDescription" runat="server">
          </mpe:EditorControl>
        </div>
        <%-- Title --%>
        <div class="settingrow">
          <mp:SiteLabel ID="lblTitle" runat="server" ForControl="txtTitle" ConfigKey="EditLinksTitleLabel"
            ResourceFile="QuantifiLinkResources" CssClass="settinglabel"></mp:SiteLabel>
          <asp:TextBox ID="txtTitle" runat="server" MaxLength="255" Columns="65" CssClass="forminput verywidetextbox"></asp:TextBox>
        </div>
        <%-- Url --%>
        <div class="settingrow">
          <mp:SiteLabel ID="lblUrlLabel" runat="server" ForControl="txtUrl" ConfigKey="EditLinksUrlLabel"
            ResourceFile="QuantifiLinkResources" CssClass="settinglabel"></mp:SiteLabel>
          <div class="forminput">
            <asp:DropDownList ID="ddProtocol" runat="server" EnableTheming="false">
              <asp:ListItem Text="http://" Value="http://"></asp:ListItem>
              <asp:ListItem Text="https://" Value="https://"></asp:ListItem>
              <asp:ListItem Text="~/" Value="~/"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtUrl" runat="server" MaxLength="255" Columns="65" CssClass="verywidetextbox "></asp:TextBox>
          </div>
        </div>
        <%-- View Order	--%>
        <div class="settingrow">
          <mp:SiteLabel ID="lblViewOrder" runat="server" ConfigKey="EditLinksViewOrderLabel"
            ResourceFile="QuantifiLinkResources" ForControl="txtViewOrder" CssClass="settinglabel">
          </mp:SiteLabel>
          <asp:TextBox ID="txtViewOrder" runat="server" MaxLength="10" Text="500" CssClass="forminput smalltextbox"></asp:TextBox>
        </div>
        <%-- Target	--%>
        <div class="settingrow">
          <mp:SiteLabel ID="SiteLabel1" runat="server" ForControl="chkUseNewWindow" ConfigKey="LinksUseNewWindowLabel"
            ResourceFile="QuantifiLinkResources" CssClass="settinglabel"></mp:SiteLabel>
          <asp:CheckBox ID="chkUseNewWindow" runat="server" CssClass="forminput" />
        </div>
        <%-- Location	--%>
        <div class="settingrow">
          <mp:SiteLabel ID="lblLocation" runat="server" ConfigKey="EditLinksLocation" ResourceFile="QuantifiLinkResources"
            ForControl="txtViewOrder" CssClass="settinglabel"></mp:SiteLabel>
          <asp:TextBox ID="txtLocation" runat="server" MaxLength="255" Columns="65" CssClass="forminput verywidetextbox "></asp:TextBox>
        </div>
        <%-- Pub Start Date	--%>
        <div class="settingrow">
          <mp:SiteLabel ID="lblStartDate" runat="server" ForControl="dpBeginDate" ConfigKey="EditLinksPubStart"
            ResourceFile="QuantifiLinkResources" CssClass="settinglabel"></mp:SiteLabel>
          <mp:DatePickerControl ID="dpPubBegin" runat="server" ShowTime="True" CssClass="forminput">
          </mp:DatePickerControl>
        </div>
        <%-- Image Picker	--%>
        <asp:Panel ID="panelImagePicker" runat="server" CssClass="settingrow">
          <mp:SiteLabel ID="lblImageUrl" runat="server" ForControl="txtImageUrl" ConfigKey="LinksImageUrlLabel"
            ResourceFile="QuantifiLinkResources" CssClass="settinglabel"></mp:SiteLabel>
          <Quantifi:FileSelector ID="FileSelectorControl" runat="server" FileType="image" CssClass="forminput" />
        </asp:Panel>
        <div class="settingrow">
          <asp:Label ID="lblMessage" runat="server" CssClass="txterror" ForeColor="red"></asp:Label>
          <asp:ValidationSummary ID="vSummary" runat="server" CssClass="txterror" ValidationGroup="imglinks" />
          <%--					<asp:ValidationSu ID="lblMessage" runat="server" CssClass="txterror" ForeColor="red"></asp:Label>
					<asp:RequiredFieldValidator ID="reqTitle" runat="servmmary ID="vSummary" runat="server" CssClass="txterror"></asp:ValidationSummary>
					<asp:Label CssClass="txterror" ControlToValidate="txtTitle" ErrorMessage="" Display="None"></asp:RequiredFieldValidator>--%>
          <asp:RequiredFieldValidator ID="reqTitle" runat="server" CssClass="txterror" ControlToValidate="txtTitle"
            SetFocusOnError="true" ValidationGroup="imglinks" ErrorMessage="" Display="none"></asp:RequiredFieldValidator>
          <asp:RequiredFieldValidator ID="reqUrl" runat="server" CssClass="txterror" ControlToValidate="txtUrl"
            SetFocusOnError="true" ValidationGroup="imglinks" ErrorMessage="" Display="none"></asp:RequiredFieldValidator>
          <asp:RequiredFieldValidator ID="reqViewOrder" runat="server" CssClass="txterror"
            SetFocusOnError="true" ValidationGroup="imglinks" ControlToValidate="txtViewOrder"
            ErrorMessage="" Display="none"></asp:RequiredFieldValidator>
          <asp:CompareValidator ID="VerifyViewOrder" runat="server" CssClass="txterror" ControlToValidate="txtViewOrder"
            SetFocusOnError="true" ValidationGroup="imglinks" ErrorMessage="Must be an number"
            Display="None" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
        </div>
        <%-- Button Rows --%>
        <div class="settingrow ">
          <div class="forminput">
            <asp:Button ID="updateButton" runat="server" Text="Update" CausesValidation="true"
              ValidationGroup="imglinks"></asp:Button>&nbsp;
            <asp:Button ID="deleteButton" runat="server" Text="Delete this item" CausesValidation="False"
              ValidationGroup="imglinks"></asp:Button>&nbsp;
            <asp:HyperLink ID="lnkCancel" runat="server" CssClass="cancellink" />
          </div>
        </div>
      </fieldset>
    </asp:Panel>
    <asp:HiddenField ID="hdnReturnUrl" runat="server" />
  </asp:Panel>
  <mp:CornerRounderBottom ID="cbottom1" runat="server" />
  <portal:SessionKeepAliveControl ID="ka1" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="rightContent" ID="MPRightPane" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="pageEditContent" ID="MPPageEdit" runat="server">
</asp:Content>
