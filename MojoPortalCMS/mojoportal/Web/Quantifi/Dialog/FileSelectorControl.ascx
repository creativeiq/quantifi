﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileSelectorControl.ascx.cs"
  Inherits="Quantifi.UI.Dialog.FileSelectorControl" %>
<asp:Panel ID="wrapper" runat="server" CssClass="fsc ">
  <div class="selectorContainer">
    <asp:TextBox ID="txtUrl" runat="server" CssClass="verywidetextbox"></asp:TextBox>
    <asp:RequiredFieldValidator ID="reqValidator" runat="server" ControlToValidate="txtUrl"
      EnableClientScript="true"></asp:RequiredFieldValidator>
    <asp:HyperLink ID="linkSelect" runat="server" ToolTip="Select File" NavigateUrl="javascript:void(0);"><img border="0" alt="Select File" 
     src="<%= SiteRoot %>/Data/SiteImages/folder_edit.png"/></asp:HyperLink>
    <a href="javascript:void(0);" title="Preview Selected File" class="toggleImgWrapper"><img alt="Preview" src="<%= SiteRoot %>/Data/SiteImages/folder_explore.png" /></a>
  </div>
  <div class="imgWrapper" style="display: none;">
    <div class="imgInfo">
    </div>
    <div class="imgContainer">
      <asp:Image ImageUrl="~/Quantifi/Images/no-image.gif" ID="imgPreview" runat="server" /></div>
  </div>
</asp:Panel>
<script type="text/javascript">

  if (Q == "undefined" || !Q)
    var Q = {};

  if (Q.FSC == "undefined" || !Q.FSC) {
    Q.FSC = {};
    Q.FSC.siteRoot = "<%= SiteRoot %>";
    Q.FSC.errorImage = "<%= SiteRoot %>/Quantifi/Images/error_32.png";
    Q.FSC.emptyImage = "<%= SiteRoot %>/Quantifi/Images/no-image.png";
    Q.FSC.INFO = new Array();
    Q.FSC.previewUpdate = function (id, url) {
      var img = document.getElementById(id);
      if (img == null)
        return;
      if (url.length > 0) {
        img.src = url;
      } else {
        img.src = Q.FSC.emptyImage;
      }
    };

    Q.FSC.checkImage = function (img) {
      if (img.src.length == 0) {
        img.src = Q.FSC.emptyImage;
      } else if (!Q.FSC.isImageOk(img) && img.src != Q.FSC.errorImage) {
        img.src = Q.FSC.errorImage;
      }
    };
    Q.FSC.isImageOk = function (img) {
      // During the onload event, IE correctly identifies any images that
      // weren’t downloaded as not complete. Others should too. Gecko-based
      // browsers act like NS4 in that they report this incorrectly.
      if (!img.complete) {
        return false;
      }

      // However, they do have two very useful properties: naturalWidth and
      // naturalHeight. These give the true size of the image. If it failed
      // to load, either of these should be zero.
      if (typeof img.naturalWidth != 'undefined' && img.naturalWidth == 0) {
        return false;
      }

      // No other way of checking: assume it’s ok.
      return true;
    };
  }

  // Setup Click Events
  $(document).ready(function () {

    var wrapper = $("#<%=wrapper.ClientID %>");
    wrapper.find(".toggleImgWrapper").click(function () {
      wrapper.find(".imgWrapper").toggle("fast");
    });

    $("#<%=imgPreview.ClientID %>").load(function () {
      try { Q.FSC.checkImage(this); }
      catch (e) { alert(e); }
    }).error(function () {
      try {
        Q.FSC.INFO.unshift(this.id + " could not load : " + this.src);
        $(this).attr('src', Q.FSC.errorImage);
      }
      catch (ex) { alert(ex); }
    });


    var txtbox = $("#<%=txtUrl.ClientID %>");

    txtbox.change(function (e) {
      Q.FSC.previewUpdate("<%=imgPreview.ClientID %>", this.value);
    });
    txtbox.focus(function (e) {
      Q.FSC.previewUpdate("<%=imgPreview.ClientID %>", this.value);
    });
    txtbox.keyup(function (e) {
      Q.FSC.previewUpdate("<%=imgPreview.ClientID %>", this.value);
    });
    txtbox.blur(function (e) {
      Q.FSC.previewUpdate("<%=imgPreview.ClientID %>", this.value);
    });

    $("#<%=linkSelect.ClientID %>").click(function (e) {
      var filetype = "<%=FileType.ToString() %>";

      var url = Q.FSC.siteRoot 
			      + "/Quantifi/Dialog/FileDialog.aspx?type="
			      + filetype
			      + "&cid=<%=txtUrl.ClientID %>";


      try {
        myFileDialog = window.open(url, "FileSelector", "width=965,height=750");
        myFileDialog.moveTo(100, 100);
      }
      catch (err) {
        txt = "There was an error on this page.\n\n";
        txt += "filetype: " + filetype + "\n\n";
        txt += "url: " + url + "\n\n";
        txt += "Error description: " + err.description + "\n\n";
        txt += "Click OK to continue.\n\n";
        alert(txt);
      }

    });

  });
</script>
