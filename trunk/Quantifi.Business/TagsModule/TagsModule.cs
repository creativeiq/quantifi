﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Quantifi.Business.Utils;
using log4net;
using Quantifi.Data.TagsModule;
using System.Globalization;
using System.Web;
using System.Web.Caching;
using Quantifi.Shared;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;

namespace Quantifi.Business.TagsModule
{
  public class ContentTag
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(ContentTag));

    #region Properties

    private const string CacheDependecyFileName = "Module-Quantifi-Tags-";


    public int Id { get; set; }
    public string Text { get; set; }
    public int TaggedCount { get; set; }

    #endregion

    #region ctors

    public ContentTag() { Id = -1; }
    public ContentTag(int id, string text, int taggedCount)
    {
      Id = id;
      Text = text;
      TaggedCount = taggedCount;
    }
    private ContentTag(IDataReader reader)
    {
      Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
      Text = reader["Text"].ToString();
      TaggedCount = Convert.ToInt32(reader["TaggedCount"], CultureInfo.InvariantCulture);
    }

    #endregion

    public void Save()
    {
      if (this.Id < 0)
        Insert();
      else
        Update();
    }

    private void Insert()
    {
      this.Id = ContentTagsDB.InsertTag(this.Text);

      TouchCacheDependecy();
    }

    private void Update()
    {
      ContentTagsDB.UpdateTag(this.Id, this.Text);
      TouchCacheDependecy();
    }

    public void Delete()
    {
      ContentTagsDB.DeleleTag(this.Id);
      TouchCacheDependecy();
    }


    public static ContentTag TagCheck(string text)
    {
      if (string.IsNullOrWhiteSpace(text))
        return null;


      ContentTag tag = null;


      using (IDataReader reader = ContentTagsDB.GetTagsByText(text))
      {
        if (reader.Read())
          tag = new ContentTag(reader);
      }


      return tag;



    }

    public static ContentTag GetTag(int id)
    {
      ContentTag tag = null;


      using (IDataReader reader = ContentTagsDB.GetTag(id))
      {
        if (reader.Read())
          tag = new ContentTag(reader);
      }


      return tag;

    }

    public static List<ContentTag> GetAllTags()
    {


      List<ContentTag> tags = new List<ContentTag>();


      using (IDataReader reader = ContentTagsDB.GetAll())
      {
        while (reader.Read())
          tags.Add(new ContentTag(reader));
      }


      return tags;

    }

    public static List<ContentTag> GetAllTagsByTaggedCount()
    {

      List<ContentTag> tags = new List<ContentTag>();


      using (IDataReader reader = ContentTagsDB.GetByTaggedCount())
      {
        while (reader.Read())
          tags.Add(new ContentTag(reader));
      }


      return tags;

    }

    public static List<ContentTag> GetTopTagsByDate(DateTime date, int count)
    {
      List<ContentTag> tags = new List<ContentTag>();


      using (IDataReader reader = ContentTagsDB.GetTopTagsByDate(date, count))
      {
        while (reader.Read())
          tags.Add(new ContentTag(reader));
      }


      return tags;

    }

    public static List<ContentTag> GetAllTagsLike(string searchText)
    {
      List<ContentTag> tags = new List<ContentTag>();

      var searchTerm = searchText;
      if (!searchText.EndsWith("%"))
        searchTerm += "%";

      using (IDataReader reader = ContentTagsDB.GetTagsByText(searchTerm))
      {
        while (reader.Read())
          tags.Add(new ContentTag(reader));
      }


      return tags;

    }

    #region ContentItem


    public static List<ContentTag> GetContentItemTags(int contentItemId)
    {
      List<ContentTag> tags = new List<ContentTag>();

      if (contentItemId > 0)
      {
        using (IDataReader reader = ContentTagsDB.GetContentItemTags(contentItemId))
        {
          while (reader.Read())
            tags.Add(new ContentTag(reader));
        }
      }

      return tags;
    }

    public static void AddContentItemTags(int contentItemId, string tagText)
    {
      ContentTag tag = ContentTag.TagCheck(tagText);
      if (tag == null)
      {
        tag = new ContentTag() { Text = tagText };
        tag.Save();
      }

      ContentTagsDB.AddTagForContentItem(contentItemId, tag.Id);

    }

    public static void AddContentItemTags(int contentItemId, int tagId)
    {
      ContentTagsDB.AddTagForContentItem(contentItemId, tagId);
      TouchCacheDependecy();
    }

    public static void DeleteAllContentItemTags(int contentItemId)
    {
      ContentTagsDB.DeleteAllTagsForContentItem(contentItemId);
      TouchCacheDependecy();
    }


    #endregion

    #region BlogPost

    public static List<ContentTag> GetBlogPostTags(int blogId)
    {
      List<ContentTag> tags = new List<ContentTag>();

      if (blogId > 0)
      {
        using (IDataReader reader = ContentTagsDB.GetBlogPostTags(blogId))
        {
          while (reader.Read())
            tags.Add(new ContentTag(reader));
        }
      }

      return tags;
    }

    public static bool AddBlogPostTags(int blogId, string tagText)
    {
      ContentTag tag = ContentTag.TagCheck(tagText);
      if (tag == null)
      {
        tag = new ContentTag() { Text = tagText };
        tag.Save();
      }

      return ContentTagsDB.AddTagForBlogPost(blogId, tag.Id);

    }

    public static bool AddBlogPostTags(int blogId, int tagId)
    {

      var result = ContentTagsDB.AddTagForBlogPost(blogId, tagId);
      TouchCacheDependecy();
      return result;

    }

    public static bool DeleteAllBlogPostTags(int blogId)
    {
      var result = ContentTagsDB.DeleteAllTagsForBlogPost(blogId);
      TouchCacheDependecy();
      return result;

    }


    #endregion

    public static string GetCacheDependecyFileName()
    {
      return CacheDependecyFileName;
    }

    public static void TouchCacheDependecy()
    {
      var filePath = CacheUtils.GetCacheDependencyFilePath(ContentTag.GetCacheDependecyFileName());

      CacheHelper.EnsureCacheFile(filePath);

      CacheHelper.TouchCacheFile(filePath);

    }


  }



  public static class ContentTagCacheUtil
  {
    private static object CacheLock = new object();
    private static string Global_CacheKey = "ContentTag.All.Dependency";


    public static void AddToCache(string key, object value)
    {
      AddToCache(key, value, new List<string>(0));
    }

    public static void AddToCache(string key, object value, List<string> additionalCacheKeys)
    {
      if (HttpContext.Current.Cache[Global_CacheKey] == null)
      {

        HttpContext.Current.Cache.Insert(Global_CacheKey, new object(),
                                         null, System.Web.Caching.Cache.NoAbsoluteExpiration,
                                         System.Web.Caching.Cache.NoSlidingExpiration,
                                         System.Web.Caching.CacheItemPriority.High, null);

      }

      var cacheKeys = new List<string> { Global_CacheKey };
      if (!additionalCacheKeys.IsNullOrEmpty())
        cacheKeys.AddRange(additionalCacheKeys);

      var cacheDependency = new CacheDependency(null, cacheKeys.ToArray(), DateTime.Now);

      HttpContext.Current.Cache.Insert(key, value, cacheDependency);
    }

    public static void ClearCache()
    {
      HttpContext.Current.Cache.Remove(Global_CacheKey);
    }

    public static List<ContentTag> GetAllTagsByText()
    {
      var cachekey = "ContentTagDB.GetAllTagsByText";

      // try to pull from cache here
      var items = HttpContext.Current.Cache[cachekey] as List<ContentTag>;
      if (items != null)
        return items;


      items = ContentTag.GetAllTags().Where(t => t.TaggedCount > 0).ToList();

      var filePath = CacheUtils.GetCacheDependencyFilePath(ContentTag.GetCacheDependecyFileName());

      AddToCache(cachekey, items);
      return items;

    }

    public static List<ContentTag> GetAllTagsByTaggedCount()
    {
      var cachekey = "ContentTagDB.GetAllTagsByTaggedCount";

      // try to pull from cache here
      var items = HttpContext.Current.Cache[cachekey] as List<ContentTag>;
      if (items != null)
        return items;


      items = ContentTag.GetAllTagsByTaggedCount().Where(t => t.TaggedCount > 0).ToList(); ;

      var filePath = CacheUtils.GetCacheDependencyFilePath(ContentTag.GetCacheDependecyFileName());

      AddToCache(cachekey, items);
      return items;

    }

    public static List<ContentTag> GetBlogPostTags(int blogId)
    {
      var cachekey = "ContentTagDB.GetBlogPostTags[" + blogId.ToString() + "]";

      // try to pull from cache here
      var items = HttpContext.Current.Cache[cachekey] as List<ContentTag>;
      if (items != null)
        return items;


      items = ContentTag.GetBlogPostTags(blogId);

      var filePath = CacheUtils.GetCacheDependencyFilePath(ContentTag.GetCacheDependecyFileName());

      AddToCache(cachekey, items);
      return items;
    }

    public static List<ContentTag> GetContentItemTags(int itemId)
    {
      var cachekey = "ContentTagDB.GetContentItemTags[" + itemId.ToString() + "]";

      // try to pull from cache here
      var items = HttpContext.Current.Cache[cachekey] as List<ContentTag>;
      if (items != null)
        return items;


      items = ContentTag.GetContentItemTags(itemId);



      AddToCache(cachekey, items);
      return items;
    }

    public static List<ContentTag> GetTopTagsByDate(int days, int count)
    {
      return GetTopTagsByDate(DateTime.UtcNow.AddDays(-(days)).Date, count);
    }

    public static List<ContentTag> GetTopTagsByDate(DateTime date, int count)
    {
      var cachekey = "ContentTagDB.GetTopTagsByDate[" + date.ToShortDateString() + "]Count[" + count.ToString() + "]";

      // try to pull from cache here
      var items = HttpContext.Current.Cache[cachekey] as List<ContentTag>;
      if (items != null)
        return items;

      lock (CacheLock)
      {
        // cache was empty before we got the lock, check again inside the lock
        items = HttpContext.Current.Cache[cachekey] as List<ContentTag>;
        if (items != null)
          return items;


        // cache is still empty, so retreive the value here
        items = ContentTag.GetTopTagsByDate(date, count);


        AddToCache(cachekey, items);
      }

      // return the cached value here
      return items;

    }

    public static List<iTaggable> GetTaggedItems(int mid, int tagId, int pageNum, int pageSize, out int totalPages)
    {
      totalPages = 1;

      var items = GetTaggedItems(tagId);


      int totalRows = (items.IsNullOrEmpty()) ? 0 : items.Count();

      if (pageSize > 0) totalPages = totalRows / pageSize;

      if (totalRows <= pageSize)
      {
        totalPages = 1;
      }
      else
      {
        int remainder;
        Math.DivRem(totalRows, pageSize, out remainder);
        if (remainder > 0)
        {
          totalPages += 1;
        }
      }

      if (pageNum > totalPages)
        return items.Skip((totalPages - 1) * pageSize).Take(pageSize).ToList();

      return items.Skip((pageNum - 1) * pageSize).Take(pageSize).ToList();
    }

    public static List<iTaggable> GetTaggedItems(int tagId)
    {
      var cachekey = "ContentTagDB.GetTaggedItems[" + tagId.ToString() + "]";

      // try to pull from cache here
      var items = HttpContext.Current.Cache[cachekey] as List<iTaggable>;
      if (items != null)
        return items;

      lock (CacheLock)
      {
        // cache was empty before we got the lock, check again inside the lock
        items = HttpContext.Current.Cache[cachekey] as List<iTaggable>;
        if (items != null)
          return items;


        // cache is still empty, so retreive the value here
        items = GetTaggedBlogPosts(tagId).ToList();


        var moduleIds = items.Select(m => m.ModuleId).Distinct();

        // store the value in the cache here
        List<string> filenames = new List<string>();

        var filePath = CacheUtils.GetCacheDependencyFilePath(ContentTag.GetCacheDependecyFileName());

        CacheHelper.EnsureCacheFile(filePath);

        filenames.Add(filePath);


        foreach (int moduleId in moduleIds)
        {
          var path = CacheUtils.GetCacheDependencyFilePath(moduleId);

          CacheHelper.EnsureCacheFile(path);

          filenames.Add(path);
        }

        CacheDependency dep = new CacheDependency(filenames.ToArray(), DateTime.Now);
        HttpContext.Current.Cache.Insert(cachekey, items, dep);
      }

      // return the cached value here
      return items;
    }


    private static IEnumerable<iTaggable> GetTaggedContentItems(int tagId)
    {
      //if (Quantifi.UI.QuantifiSettings.)

      var items = Quantifi.Business.ContentArchiveModule.ContentItem.GetByTag(tagId);

      if (items.IsNullOrEmpty())
        return new List<Quantifi.Business.ContentArchiveModule.ContentItem>();

      return items.Cast<iTaggable>();
    }

    private static IEnumerable<iTaggable> GetTaggedBlogPosts(int tagId)
    {
      var items = Quantifi.Business.Blog.GetByTag(tagId);

      if (items.IsNullOrEmpty())
        return new List<Quantifi.Business.Blog>();

      return items.Cast<iTaggable>();
    }

    private static List<iTaggable> AggregateTags(IEnumerable<iTaggable> contentItems, IEnumerable<iTaggable> blogPosts)
    {
      if (contentItems.IsNullOrEmpty() && blogPosts.IsNullOrEmpty())
        return new List<iTaggable>();

      if (contentItems.IsNullOrEmpty())
        return blogPosts.ToList();

      if (blogPosts.IsNullOrEmpty())
        return contentItems.ToList();

      var aggItems = Enumerable.Concat(contentItems, blogPosts).OrderByDescending(i => i.GetStartDate).ToList();

      return aggItems;

    }


  }

}
