﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quantifi.Business.TagsModule
{

	public interface iTaggable
	{
		int ItemId { get; }
		int ModuleId { get; }
		string GetUrl { get; }
		string GetTitle { get; }
		string GetDescription { get; }
		DateTime GetStartDate { get; }
		List<ContentTag> GetTags { get; }
	}


}
