﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quantifi.Data;
using System.Data;
using System.Globalization;

namespace Quantifi.Business
{
  public class Contact
  {
    public int Id { get; private set; }
    public int ModuleId { get; set; }

    public string TopContent { get; set; }
    public string ThankYouContent { get; set; }


    public DateTime PubStart { get; set; }

    public Contact() { }

    private Contact(IDataReader reader)
    {
      Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
      ModuleId = Convert.ToInt32(reader["ModuleId"], CultureInfo.InvariantCulture);
      TopContent = reader["TopContent"].ToString();
      ThankYouContent = reader["ThankYouContent"].ToString();
    }

    public void Save()
    {
      if (this.Id > 0)
        Update();
      else
        Insert();

    }

    private void Update()
    {

      ContactDB.Update(Id, ModuleId, TopContent, ThankYouContent);


    }

    private void Insert()
    {

      var id = ContactDB.Insert(ModuleId, TopContent, ThankYouContent);

      this.Id = id;

    }

    public void Delete()
    {
      DeleteByModule(this.ModuleId);
    }

    public static void DeleteByModule(int moduleId)
    {
      ContactDB.DeleteByModule(moduleId);
    }

    public static Contact Get(int id)
    {
      Contact c = null;
      using (IDataReader reader = ContactDB.Get(id))
      {
        if (reader.Read())
          c = new Contact(reader);

      }
      return c;
    }


  }
}
