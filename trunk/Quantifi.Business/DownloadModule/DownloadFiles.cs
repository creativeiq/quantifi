﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quantifi.Data;
using System.Globalization;
using System.Configuration;
using System.Web;
using System.Data;
using Quantifi.Business.Countries;

namespace Quantifi.Business.DownloadModule
{
  public class DownloadFile
  {
    #region CONST

    public const string QS_DownFileId = "fileId";
    public const string QS_DownFileGuid = "guid";
    public const string QS_GlobalDownFileGuid = "fileguid";
    public const string QS_Encoded = "encfile";
    public const string QS_LinkDate = "time";
    public const string QS_FormActivityId = "formActivityId";
    public const string QS_DownFileTrackingEnabled = "track";
    public const string QS_NoRedirect = "nrdrct";

    public const string DownFileFolder = "QDownloads";
    public const string DownFileServerFileExt = ".config";

    #endregion

    #region Properties

    public int Id { get; set; }
    public Guid Guid { get; set; }

    public string DisplayName { get; set; }
    public string DisplayDescription { get; set; }
    public string OriginalFileName { get; set; }
    public string ServerFileName { get; set; }
    public bool isActive { get; set; }
    public int DownloadCount { get; set; }
    public int SizeInKB { get; set; }
    public DateTime LastUpdatedTime { get; set; }
    public string LastUpdatedUser { get; set; }
    public DateTime CreatedTime { get; set; }
    public string CreatedUser { get; set; }


    public bool RequireForm { get; set; }


    public bool EmailUserActivity { get; set; }

    public bool EmailFilesToUser { get; set; }
    public bool EmailFilesToRestrictedEmails { get; set; }

    public string AdminEmailAddress { get; set; }


    #endregion

    public DownloadFile() { Id = -1; }
    public DownloadFile(IDataReader reader)
		{
			Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
			Guid = new Guid(reader["Guid"].ToString());


			DisplayName = reader["DisplayName"].ToString();
			DisplayDescription = reader["DisplayDescription"].ToString();

			OriginalFileName = reader["OriginalFileName"].ToString();
			ServerFileName = reader["ServerFileName"].ToString();

			isActive = Convert.ToBoolean(reader["isActive"], CultureInfo.CurrentCulture);
			DownloadCount = Convert.ToInt32(reader["DownloadCount"], CultureInfo.InvariantCulture);
			SizeInKB = Convert.ToInt32(reader["SizeInKB"], CultureInfo.InvariantCulture);

			LastUpdatedTime = Convert.ToDateTime(reader["LastUpdatedTime"], CultureInfo.CurrentCulture);
			LastUpdatedUser = reader["LastUpdatedUser"].ToString();


			CreatedTime = Convert.ToDateTime(reader["CreatedTime"], CultureInfo.CurrentCulture);
			CreatedUser = reader["CreatedUser"].ToString();


 

			RequireForm = Convert.ToBoolean(reader["RequireForm"], CultureInfo.CurrentCulture);
			EmailUserActivity = Convert.ToBoolean(reader["EmailUserActivity"], CultureInfo.CurrentCulture);
 

			EmailFilesToUser = Convert.ToBoolean(reader["EmailFilesToUser"], CultureInfo.CurrentCulture);
      EmailFilesToRestrictedEmails = Convert.ToBoolean(reader["EmailFilesToRestrictedEmails"], CultureInfo.CurrentCulture);


			AdminEmailAddress = reader["AdminEmailAddress"].ToString();
		}



    /// <summary>
    /// Save
    /// </summary>
    public void Save()
    {
      if (this.Id < 1)
      {
        this.Id = Insert(this);
      }
      else
      {
        Update(this);
      }
    }

    /// <summary>
    /// Delete
    /// </summary>
    public void Delete()
    {
      if (this.Id > 0)
      {
        DownloadFileDB.Delete(this.Id);
        this.Id = -1;
      }
    }


    /// <summary>
    /// Insert
    /// </summary>
    /// <param name="df"></param>
    /// <returns></returns>
    private static int Insert(DownloadFile df)
    {
      var id = DownloadFileDB.Insert(df.Guid, df.DisplayName, df.DisplayDescription, df.OriginalFileName, df.ServerFileName,
        df.isActive, df.SizeInKB, df.LastUpdatedUser, df.RequireForm,
        df.EmailUserActivity, df.EmailFilesToUser, df.EmailFilesToRestrictedEmails, df.AdminEmailAddress);

      return id;
    }

    /// <summary>
    /// Update
    /// </summary>
    /// <param name="df"></param>
    private static void Update(DownloadFile df)
    {
      DownloadFileDB.Update(df.Id, df.DisplayName, df.DisplayDescription, df.OriginalFileName, df.ServerFileName,
        df.isActive, df.SizeInKB, df.LastUpdatedUser,
        df.RequireForm, df.EmailUserActivity, df.EmailFilesToUser, df.EmailFilesToRestrictedEmails, df.AdminEmailAddress);
    }

    /// <summary>
    /// Get by fileId
    /// </summary>
    /// <param name="fileId"></param>
    /// <returns></returns>
    public static DownloadFile Get(int fileId)
    {
      DownloadFile df = null;

      using (var reader = DownloadFileDB.GetDownloadFile(fileId))
      {
        if (reader.Read())
        {
          df = new DownloadFile(reader);

        }
      }


      return df;
    }

    /// <summary>
    /// Get by Guid
    /// </summary>
    /// <param name="guid"></param>
    /// <returns></returns>
    public static DownloadFile Get(Guid guid)
    {
      DownloadFile df = null;

      using (var reader = DownloadFileDB.GetDownloadFile(guid))
      {
        if (reader.Read())
        {
          df = new DownloadFile(reader);

        }
      }


      return df;
    }

    /// <summary>
    /// Get All DownloadFiles
    /// </summary>
    /// <returns></returns>
    public static List<DownloadFile> GetDownloadFiles()
    {
      var list = new List<DownloadFile>();

      using (var reader = DownloadFileDB.GetDownloadFiles())
      {
        while (reader.Read())
        {
          var df = new DownloadFile(reader);

          list.Add(df);
        }
      }
      return list;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageNum"></param>
    /// <returns></returns>
    public static List<DownloadFile> GetPage(int pageSize, int pageNum, out int totalpages)
    {
    


      var list = new List<DownloadFile>();

      using (var reader = DownloadFileDB.GetPage(pageSize, pageNum, out totalpages))
      {
        while (reader.Read())
        {
          var df = new DownloadFile(reader);

          list.Add(df);

        }
      }

      return list;

    }

  }

  public class DownloadFileActivity
  {

    #region Properties

    public long Id { get; set; }
    public int FileId { get; set; }

    public string UserHostAddress { get; set; }
    public string UserHostName { get; set; }
    public string Referrer { get; set; }
    public DateTime TimeStamp { get; set; }

    #endregion

    public DownloadFileActivity() { }

    public DownloadFileActivity(int fileId)
    {
      FileId = fileId;

      if (HttpContext.Current != null)
      {
        Referrer = (HttpContext.Current.Request.UrlReferrer == null) ? string.Empty : HttpContext.Current.Request.UrlReferrer.ToString();
        UserHostName = HttpContext.Current.Request.UserHostName;
        UserHostAddress = HttpContext.Current.Request.UserHostAddress;
        TimeStamp = DateTime.UtcNow;
      }

    }

    public DownloadFileActivity(IDataReader reader)
    {
      Id = Convert.ToInt64(reader["Id"], CultureInfo.InvariantCulture);
      FileId = Convert.ToInt32(reader["FileId"], CultureInfo.InvariantCulture);
      UserHostAddress = reader["UserHostAddress"].ToString();
      UserHostName = reader["UserHostName"].ToString();
      Referrer = reader["Referrer"].ToString();
      TimeStamp = Convert.ToDateTime(reader["TimeStamp"], CultureInfo.CurrentCulture);
    }

    /// <summary>
    /// Save
    /// </summary>
    public void Save()
    {
      if (this.Id < 1)
        this.Id = Insert(this);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dfa"></param>
    /// <returns></returns>
    private static long Insert(DownloadFileActivity dfa)
    {
      var id = DownloadFileActivityDB.Insert(dfa.FileId, dfa.UserHostAddress, dfa.UserHostName, dfa.Referrer, dfa.TimeStamp);

      return id;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <returns></returns>
    public static int GetCount(int fileId)
    {
      return DownloadFileActivityDB.GetCountByFile(fileId);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <returns></returns>
    public static List<DownloadFileActivity> GetActivity(int fileId)
    {
      var list = new List<DownloadFileActivity>();

      using (var reader = DownloadFileActivityDB.GetActivity(fileId))
      {
        while (reader.Read())
        {
          var dfa = new DownloadFileActivity(reader);

          list.Add(dfa);

        }
      }



      return list;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <param name="startDate"></param>
    /// <param name="endDate"></param>
    /// <returns></returns>
    public static List<DownloadFileActivity> GetActivityByDate(int fileId, DateTime startDate, DateTime endDate)
    {
      var list = new List<DownloadFileActivity>();

      using (var reader = DownloadFileActivityDB.GetActivityByDate(fileId, startDate, endDate))
      {
        while (reader.Read())
        {
          var dfa = new DownloadFileActivity(reader);

          list.Add(dfa);

        }
      }



      return list;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <param name="pageSize"></param>
    /// <param name="pageNum"></param>
    /// <returns></returns>
    public static List<DownloadFileActivity> GetActivityByPage(int fileId, int pageSize, int pageNum, out int totalPages)
    {
      var list = new List<DownloadFileActivity>();

      using (var reader = DownloadFileActivityDB.GetActivityByPage(fileId, pageSize, pageNum, out totalPages))
      {
        while (reader.Read())
        {
          list.Add(new DownloadFileActivity(reader));
        }
      }

      return list;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <returns></returns>
    public static List<DownloadFileActivity> GetAll(int fileId)
    {

      var items = new List<DownloadFileActivity>();

      using (IDataReader reader = DownloadFileActivityDB.GetAllByFile(fileId))
      {
        while (reader.Read())
          items.Add(new DownloadFileActivity(reader));

      }

      return items;

    }
  }


  public class DownloadFileFormActivity
  {

    #region Properties

    public int Id { get; set; }
    public int FileId { get; set; }
    public long ActivityId { get; set; }

    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string JobTitle { get; set; }
    public string CompanyName { get; set; }


    public string Email { get; set; }
    public string Phone { get; set; }

    public int CountryId { get; set; }
    public Country Country { get; set; }

    public bool MarketingOptIn { get; set; }

    public DownloadFileActivity Activity { get; set; }

    #endregion

    public DownloadFileFormActivity() { Id = -1; }
    public DownloadFileFormActivity(int fileId)
    {
      Id = -1;
      Activity = new DownloadFileActivity(fileId);
    }
    public DownloadFileFormActivity(IDataReader reader)
    {
      Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
      FileId = Convert.ToInt32(reader["FileId"], CultureInfo.InvariantCulture);
      ActivityId = Convert.ToInt64(reader["ActivityId"], CultureInfo.InvariantCulture);

      FirstName = reader["FirstName"].ToString();
      LastName = reader["LastName"].ToString();
      JobTitle = reader["JobTitle"].ToString();
      CompanyName = reader["CompanyName"].ToString();
      Email = reader["Email"].ToString();
      Phone = reader["Phone"].ToString();

      MarketingOptIn = Convert.ToBoolean(reader["MarketingOptIn"], CultureInfo.InvariantCulture);


      Activity = new DownloadFileActivity();
      Activity.Id = ActivityId;
      Activity.FileId = FileId;
      Activity.UserHostAddress = reader["UserHostAddress"].ToString();
      Activity.UserHostName = reader["UserHostName"].ToString();
      Activity.Referrer = reader["Referrer"].ToString();
      Activity.TimeStamp = Convert.ToDateTime(reader["TimeStamp"], CultureInfo.CurrentCulture);



      CountryId = Convert.ToInt32(reader["CountryId"], CultureInfo.InvariantCulture);
      if (CountryId > 0 && !Convert.IsDBNull(reader["CountryName"]) && !Convert.IsDBNull(reader["CountryIsoTwoCharCode"]))
      {
        Country = new Country()
        {
          Id = CountryId,
          Name = reader["CountryName"].ToString(),
          IsoTwoCharCode = reader["CountryIsoTwoCharCode"].ToString()
        };

      }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public bool Save()
    {
      if (this.Id < 1)
      {
        return Insert();
      }
      else
      {
        return Update();
      }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private bool Insert()
    {
      if (this.Activity.Id < 1)
      {
        this.Activity.Save();
      }

      this.Id = DownloadFileFormActivityDB.Insert(this.Activity.Id, FileId, FirstName, LastName, JobTitle, CompanyName, Email, Phone, CountryId, MarketingOptIn);

      return (this.Id > 0);
    }

    /// <summary>
    /// Not mplemented No reason to update data
    /// </summary>
    /// <returns></returns>
    private bool Update()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <returns></returns>
    public static int GetCount(int fileId)
    {
      return DownloadFileFormActivityDB.GetCountByFile(fileId);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static DownloadFileFormActivity Get(int fileId, int id)
    {
      DownloadFileFormActivity item = null;

      using (IDataReader reader = DownloadFileFormActivityDB.Get(fileId))
      {
        if (reader.Read())
          item = new DownloadFileFormActivity(reader);
      }

      return item;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <param name="pageNumber"></param>
    /// <param name="pageSize"></param>
    /// <param name="totalPages"></param>
    /// <returns></returns>
    public static List<DownloadFileFormActivity> GetPage(int fileId, int pageNumber, int pageSize, out int totalPages)
    {

      var items = new List<DownloadFileFormActivity>();

      using (IDataReader reader = DownloadFileFormActivityDB.GetPage(fileId, pageNumber, pageSize, out totalPages))
      {
        while (reader.Read())
          items.Add(new DownloadFileFormActivity(reader));

      }

      return items;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileId"></param>
    /// <returns></returns>
    public static List<DownloadFileFormActivity> GetAll(int fileId)
    {

      var items = new List<DownloadFileFormActivity>();

      using (IDataReader reader = DownloadFileFormActivityDB.GetAllByFile(fileId))
      {
        while (reader.Read())
          items.Add(new DownloadFileFormActivity(reader));

      }

      return items;

    }

  }

}
