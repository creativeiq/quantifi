﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using mojoPortal.Business.WebHelpers;
using System.Web.Caching;
using mojoPortal.Business;
using System.Data;
using System.Globalization;
using Quantifi.Data.BannerModule;


namespace Quantifi.Business.BannerModule
{
	public class Banner
	{
		#region Properties

		public int Id { get; set; }
		public int PageId { get; set; }
		public bool isActive { get; set; }
		public string ImageUrl { get; set; }


		public string LastUpdatedUser { get; set; }
		public DateTime LastUpdatedDate { get; set; }

		#endregion

		public Banner() { this.Id = -1; }

		private Banner(IDataReader reader)
		{
			Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
			PageId = Convert.ToInt32(reader["PageId"], CultureInfo.InvariantCulture);

			isActive = Convert.ToBoolean(reader["isActive"], CultureInfo.InvariantCulture);
			ImageUrl = reader["ImageUrl"].ToString();


			//LastUpdatedUser = reader["LastUpdatedUser"].ToString();
			//LastUpdatedDate = Convert.ToDateTime(reader["LastUpdatedDate"], CultureInfo.InvariantCulture);
		}

		public bool isSaved { get{return (this.Id > 0);}}

		public void Save()
		{
			if (this.isSaved)
			{
				this.Update();
			}
			else
			{
				this.Insert();
			}

			BannerCacheUtil.ClearFromCache(this.PageId);
		}

		public void Delete()
		{
			if (this.isSaved)
				BannerDB.Delete(this.Id);

			BannerCacheUtil.ClearFromCache(this.PageId);
		}

		private void Insert()
		{
			this.Id = BannerDB.Insert(this.PageId, this.isActive, this.ImageUrl);

		}

		private void Update()
		{
			BannerDB.Update(this.Id, this.PageId, this.isActive, this.ImageUrl);
		}

		public static Banner GetByPageId(int pageId, bool isActive = true)
		{
			// get from DB
			Banner b = null;
			using (IDataReader reader = BannerDB.GetByPage(pageId, isActive))
			{
				if (reader.Read())
					b = new Banner(reader);

			}
			return b;


		}

		public static Banner GetParentByPageId(int pageId, bool isActive)
		{
			var siteSettings = CacheHelper.GetCurrentSiteSettings();

			var pageSettings = new PageSettings(siteSettings.SiteId, pageId);
			if (pageSettings == null)
				return null;


			return Banner.GetByPageId(pageSettings.ParentId, isActive);


		}

	}

	public static class BannerCacheUtil
	{
		public static string GetCacheKeyByPageId(int pageId)
		{
			return "Quantifi_Banner_Page_" + pageId.ToString();
		}

		public static void ClearFromCache(int pageId)
		{

			HttpContext.Current.Cache.Remove(GetCacheKeyByPageId(pageId));

		}

		public static Banner GetByPageId(int pageId)
		{
			string key = GetCacheKeyByPageId(pageId);

			Banner banner = HttpContext.Current.Cache[key] as Banner;

			if (banner != null)
				return banner;

			banner = Banner.GetByPageId(pageId);

			if (banner != null)
			{
				HttpContext.Current.Cache.Insert(key, banner);
				return banner;
			}
			else
			{
				var menupages = CacheHelper.GetMenuPages();

				var pageSettings = menupages.SingleOrDefault(p => p.PageId == pageId);
				

				//If there is no page, or the page has no parent, 
				// Add an empty banner to cache so we dont lookup the banner in the db again.
				if (pageSettings == null || pageSettings.ParentId < 1)
				{
					var emptyBanner = new Banner();
					HttpContext.Current.Cache.Insert(key, emptyBanner);
					return emptyBanner;
				}
				
				Banner parentBanner = GetByPageId(pageSettings.ParentId);


				string parentKey = GetCacheKeyByPageId(pageSettings.ParentId);
	
				// Try cached item to parent key, just in case the parent banner is saved
				CacheDependency dep = new CacheDependency(null, new string[] { parentKey });
				HttpContext.Current.Cache.Insert(key, parentBanner, dep);

				return parentBanner;
			} 
		}

	}
}
