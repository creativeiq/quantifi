﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using Quantifi.Data.FlipBoxModule;
using System.Web;
using mojoPortal.Business.WebHelpers;
using System.Web.Caching;
using Quantifi.Business.Utils;

namespace Quantifi.Business.FlipBoxModule
{
  public class FlipBox
  {


    #region Properties

    public int Id { get; set; }
    public int ModuleId { get; set; }
    public int Priority { get; set; }
    public string ImageUrl { get; set; }
    public string ImageAltText { get; set; }
    public string Description { get; set; }
    public string LinkText { get; set; }
    public string LinkUrl { get; set; }
    public bool isExternalLink { get; set; }

    #endregion

    public FlipBox() { Id = -1; ModuleId = -1; }

    private FlipBox(IDataReader reader)
    {
      Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
      ModuleId = Convert.ToInt32(reader["ModuleId"], CultureInfo.InvariantCulture);
      Priority = Convert.ToInt32(reader["Priority"], CultureInfo.InvariantCulture);

      ImageUrl = reader["ImageUrl"].ToString();
      ImageAltText = reader["ImageAltText"].ToString();
      Description = reader["Description"].ToString();
      LinkText = reader["LinkText"].ToString();
      LinkUrl = reader["LinkUrl"].ToString();
      isExternalLink = Convert.ToBoolean(reader["isExternalLink"], CultureInfo.InvariantCulture);



    }

    public void Save()
    {
      if (this.Id < 0)
      {
        this.Id = FlipBox.Insert(this.ModuleId, this.Priority, this.ImageUrl, this.ImageAltText, this.Description, this.LinkText, this.LinkUrl, this.isExternalLink);
      }
      else
      {
        FlipBox.Update(this.Id, this.ModuleId, this.Priority, this.ImageUrl, this.ImageAltText, this.Description, this.LinkText, this.LinkUrl, this.isExternalLink);
      }

      if (this.ModuleId > 0)
      {


        FlipBoxCacheUtil.ClearCache(this.ModuleId);

      }

    }

    public void Delete()
    {
      FlipBox.Delete(this.Id);
    }


    private static int Insert(int moduleId, int priority, string imageUrl, string imageAltText, string description, string linkText, string linkUrl, bool isExtLink)
    {
      return FlipBoxDB.Insert(moduleId, priority, imageUrl, imageAltText, description, linkText, linkUrl, isExtLink);
    }

    private static bool Update(int id, int moduleId, int priority, string imageUrl, string imageAltText, string description, string linkText, string linkUrl, bool isExtLink)
    {
      return FlipBoxDB.Update(id, moduleId, priority, imageUrl, imageAltText, description, linkText, linkUrl, isExtLink);
    }

    private string CacheDependencyKey()
    {
      return FlipBox.CacheDependencyKey(this.ModuleId);
    }

    public static string CacheDependencyKey(int moduleId)
    {
      return "Module-" + moduleId.ToString();
    }

    public static void Delete(int itemId)
    {
      FlipBoxDB.Delete(itemId);
    }

    public static void DeleteByModule(int moduleId)
    {
      FlipBoxDB.DeleteByModule(moduleId);
    }

    public static FlipBox Get(int id)
    {
      FlipBox item = null;
      using (IDataReader reader = FlipBoxDB.Get(id))
      {
        if (reader.Read())
          item = new FlipBox(reader);

      }
      return item;


    }

    public static List<FlipBox> GetByModule(int moduleId)
    {
      List<FlipBox> items = new List<FlipBox>();
      using (IDataReader reader = FlipBoxDB.GetByModule(moduleId))
      {
        while (reader.Read())
          items.Add(new FlipBox(reader));

      }
      return items;
    }

    public static int GetNextPriority(int moduleId)
    {

      int priority = 1;

      var boxes = FlipBox.GetByModule(moduleId);
      if (boxes == null || !boxes.Any())
        return priority;

      var last = boxes.OrderBy(i => i.Priority).Last();
      if (last != null)
        priority = last.Priority + 1;

      return priority;

    }

  }

  public static class FlipBoxCacheUtil
  {
    private static string GetCacheKey(int moduleId)
    {
      return string.Format("Quantifi.FlipBox.GetByModule[{0}]", moduleId);
    }


    public static List<FlipBox> GetByModule(int moduleId)
    {

      var cachekey = GetCacheKey(moduleId);


      var data = HttpContext.Current.Cache[cachekey] as List<FlipBox>;
      if (data != null)
      {
        return data;
      }

      data = FlipBox.GetByModule(moduleId);


      //var fileDependencyPath = CacheUtils.GetCacheDependencyFilePath(FlipBox.CacheDependencyKey(moduleId));

      //CacheDependency dep = new CacheDependency(fileDependencyPath, DateTime.Now);
      HttpContext.Current.Cache.Insert(cachekey, data);

      return data;

    }


    public static void ClearCache(int moduleId)
    {
      var cachekey = GetCacheKey(moduleId);
      HttpContext.Current.Cache.Remove(cachekey);
    }


  }

}
