﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Globalization;

namespace Quantifi.Business.Countries
{
	public class Country
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string IsoTwoCharCode { get; set; }

		public Country() { Id = -1; }

		public Country(int id, string name,  string isoTwoCharCode) {
			Id = id;
			Name = name;
			IsoTwoCharCode = isoTwoCharCode;
		}



		private static object CacheLock = new object();
		public static List<Country> GetAll() 
		{
			var cachekey = "Country.GetAll";

			// try to pull from cache here
			var items = HttpContext.Current.Cache[cachekey] as List<Country>;
			if (items != null)
				return items;

			lock (CacheLock)
			{
				// cache was empty before we got the lock, check again inside the lock
				items = HttpContext.Current.Cache[cachekey] as List<Country>;
				if (items != null)
					return items;


				// cache is still empty, so retreive the value here
				items = GetAllContries();


				HttpContext.Current.Cache.Insert(cachekey, items,  null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			// return the cached value here
			return items;


		}


		private static List<Country> GetAllContries()
		{
			var countries = new List<Country>();

			using (var reader = Quantifi.Data.Countries.CountriesDB.GetAll())
			{
				while (reader.Read())
				{


					countries.Add(new Country()
					{
						Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture),
						Name = reader["Name"].ToString(),
						IsoTwoCharCode = reader["IsoTwoCharCode"].ToString()
					});

				}
			}
			return countries;

		}
	}
}
