﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Web;
using Quantifi.Shared;
using mojoPortal.Business.WebHelpers;
using System.Web.Caching;
using mojoPortal.Business;
using Quantifi.Data.MainContentModule;
using Quantifi.Business.Utils;

namespace Quantifi.Business.MainContentModule
{

  public class MainContent
  {

    #region Properties

    public int Id { get; set; }
    public int PageId { get; set; }
    public int Priority { get; set; }

    public bool isActive { get; set; }

    public string CampaignName { get; set; }

    public string BackgroundImageUrl { get; set; }
    public string MainImageUrl { get; set; }

    public string MainImageAltText { get; set; }
    public string MainImageLink { get; set; }

    public string HeaderTextColor { get; set; }
    public string MaskColor { get; set; }

    public string RequestADemoImageUrl { get; set; }
    public string QuantifiLogoImageUrl { get; set; }

    public string TopNavColorCss { get; set; }
    public string TopSubNavColorCss { get; set; }


    public DateTime CreatedDate { get; set; }

    #endregion


    public MainContent() { Id = -1; }

    private MainContent(IDataReader reader)
    {
      Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
      PageId = Convert.ToInt32(reader["PageId"], CultureInfo.InvariantCulture);
      Priority = Convert.ToInt32(reader["Priority"], CultureInfo.InvariantCulture);
      
      isActive = Convert.ToBoolean(reader["isActive"], CultureInfo.InvariantCulture);


      CampaignName = reader["CampaignName"].ToString();

      BackgroundImageUrl = reader["BackgroundImageUrl"].ToString();
      MainImageUrl = reader["MainImageUrl"].ToString();
      MainImageAltText = reader["MainImageAltText"].ToString();
      MainImageLink = reader["MainImageLink"].ToString();


      HeaderTextColor = reader["HeaderTextColor"].ToString();
      MaskColor = reader["MaskColor"].ToString();

      RequestADemoImageUrl = reader["RequestADemoImageUrl"].ToString();
      QuantifiLogoImageUrl = reader["QuantifiLogoImageUrl"].ToString();

      TopNavColorCss = reader["TopNavColorCss"].ToString();
      TopSubNavColorCss = reader["TopSubNavColorCss"].ToString();

      CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
    }

    private bool isSaved { get { return (this.Id > 0); } }

    public void Save()
    {
      if (this.isSaved)
      {
        this.Update();
      }
      else
      {
        this.Insert();
      }

      MainContentCacheUtil.ClearFromCache(this.PageId);
    }

    public void Delete()
    {
      if (this.isSaved)
        MainContentDB.Delete(this.Id);

      MainContentCacheUtil.ClearFromCache(this.PageId);
    }

    private void Insert()
    {
      this.Id = MainContentDB.Insert(this.PageId, this.Priority, this.isActive, this.CampaignName,
                                      this.BackgroundImageUrl, this.MainImageUrl,
                                      this.MainImageAltText, this.MainImageLink,
                                      this.HeaderTextColor, this.MaskColor,
                                      this.RequestADemoImageUrl, this.QuantifiLogoImageUrl,
                                      this.TopNavColorCss, this.TopSubNavColorCss);

    }

    private void Update()
    {
      MainContentDB.Update(this.Id, this.PageId, this.Priority, this.isActive, this.CampaignName,
                                      this.BackgroundImageUrl, this.MainImageUrl,
                                      this.MainImageAltText, this.MainImageLink,
                                      this.HeaderTextColor, this.MaskColor,
                                      this.RequestADemoImageUrl, this.QuantifiLogoImageUrl,
                                      this.TopNavColorCss, this.TopSubNavColorCss);
    }



    public void MovePriorityUp()
    {
      MoveContentPriorityUp(this.Id, this.Priority);


    }

    public void MovePriorityDown()
    {
      MoveContentPriorityDown(this.Id, this.Priority);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageId"></param>
    /// <param name="isActive"></param>
    /// <returns></returns>
    public static List<MainContent> GetByPageId(int pageId)
    {
      // get from DB
      var items = new List<MainContent>();
      using (IDataReader reader = MainContentDB.GetByPage(pageId))
      {
        while (reader.Read())
          items.Add(new MainContent(reader));

      }
      return items;


    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageId"></param>
    /// <param name="isActive"></param>
    /// <returns></returns>
    public static List<MainContent> GetParentByPageId(int pageId)
    {
      var siteSettings = CacheHelper.GetCurrentSiteSettings();

      var pageSettings = new PageSettings(siteSettings.SiteId, pageId);
      if (pageSettings == null)
        return null;


      return MainContent.GetByPageId(pageSettings.ParentId);


    }

    public static List<MainContent> GetPage(int pageId, int pageNum, int pageSize, out int totalPages)
    {

      totalPages = 1;
      int totalRows = MainContentDB.GetCount(pageId); ;

      if (pageSize > 0) totalPages = totalRows / pageSize;

      if (totalRows <= pageSize)
      {
        totalPages = 1;
      }
      else
      {
        int remainder;
        Math.DivRem(totalRows, pageSize, out remainder);
        if (remainder > 0)
        {
          totalPages += 1;
        }
      }

      var items = new List<MainContent>();

      if (pageNum > totalPages)
        return items;

      using (IDataReader reader = MainContentDB.GetPage(pageId, pageNum, pageSize))
      {
        while (reader.Read())
        {
          items.Add(new MainContent(reader));
        }
      }
      return items;

    }

    public static int GetNextPriority(int pageId, int priority)
    {
      return MainContentDB.GetNextPriority(pageId, priority);

    }

    public static int GetPreviousPriority(int pageId, int priority)
    {
      return MainContentDB.GetPreviousPriority(pageId, priority);

    }


    public static int GetHighestPriority(int pageId)
    {
      return MainContentDB.GetHighestPriority(pageId);
    }

    private static object _MoveContentPriorityObjLock = new object();
    public static void MoveContentPriorityUp(int itemId, int currentPriority)
    {
      lock (_MoveContentPriorityObjLock)
      {

        var item = Get(itemId);
        if (item == null)
        {
          return;
        }

        if (currentPriority != item.Priority)
        {
          // looks like it was already updated.
          return;
        }

        int nextPriority = GetNextPriority(item.PageId, currentPriority);

        var itemsToMoveDown = GetByPriority(item.PageId, nextPriority);
        
        // if we are at the top. do nothing
        if (itemsToMoveDown.IsNullOrEmpty())
          return;

        foreach (var mc in itemsToMoveDown)
        {
          mc.Priority = currentPriority;
          mc.Save();
        }

        item.Priority = nextPriority;
        item.Save();
      }
    }
    
    public static void MoveContentPriorityDown(int itemId, int currentPriority)
    {
      lock (_MoveContentPriorityObjLock)
      {
        var item = Get(itemId);
        if (item == null)
        {
          return;
        }
        if (currentPriority != item.Priority)
        {
          // looks like it was already updated.
          return;
        }

        int prevPriority = GetPreviousPriority(item.PageId, currentPriority);

        var itemsToMoveUp = GetByPriority(item.PageId, prevPriority);

        // if we are at the bottom. do nothing
        if (itemsToMoveUp.IsNullOrEmpty())
          return;

        foreach (var mc in itemsToMoveUp)
        {
          mc.Priority = currentPriority;
          mc.Save();
        }

        item.Priority = prevPriority;
        item.Save();


      }
    }

    public static List<MainContent> GetByPriority(int pageId, int priority)
    {
      var items = new List<MainContent>();
      using (IDataReader reader = MainContentDB.GetByPriority(pageId, priority))
      {
        while (reader.Read())
        {
          items.Add(new MainContent(reader));
        }
      }

      return items;
    }

    public static MainContent Get(int itemId)
    {

      using (IDataReader reader = MainContentDB.Get(itemId))
      {
        if (reader.Read())
        {
          return new MainContent(reader);
        }
      }

      return null;

    }

  }


  public static class MainContentCacheUtil
  {

    //public static List<MainContent> GetItemsByPage(int pageId)
    //{
    //  List<MainContent> items = new List<MainContent>();



    //  items = MainContent.GetByPageId(pageId);


    //  var filename = CacheUtils.GetCacheDependencyFilePath(GetCacheKeyByPageId(pageId));
    //  CacheHelper.EnsureCacheFile(filename);
    //  CacheDependency dep = new CacheDependency(filename, DateTime.Now);

    //  HttpContext.Current.Cache.Insert(key, mainContent, dep);


    //  return items;
    //}

    public static string GetCacheKeyByPageId(int pageId)
    {
      return "Quantifi_MainContent_Page_" + pageId.ToString();
    }

    public static void ClearFromCache(int pageId)
    {
      var filename = CacheUtils.GetCacheDependencyFilePath(GetCacheKeyByPageId(pageId));
      CacheHelper.TouchCacheFile(filename);

    }

    public static List<MainContent> GetByPageId(int pageId)
    {
      string key = GetCacheKeyByPageId(pageId);

      List<MainContent> mainContent = HttpContext.Current.Cache[key] as List<MainContent>;

      if (mainContent != null)
        return mainContent;

      mainContent = MainContent.GetByPageId(pageId);


      var filename = CacheUtils.GetCacheDependencyFilePath(GetCacheKeyByPageId(pageId));
      CacheHelper.EnsureCacheFile(filename);
      CacheDependency dep = new CacheDependency(filename, DateTime.Now);

      HttpContext.Current.Cache.Insert(key, mainContent, dep);
      
      return mainContent;





      //else
      //{
      //  var menupages = CacheHelper.GetMenuPages();

      //  var pageSettings = menupages.SingleOrDefault(p => p.PageId == pageId);


      //  //If there is no page, or the page has no parent, 
      //  // Add an empty banner to cache so we dont lookup the banner in the db again.
      //  if (pageSettings == null || pageSettings.ParentId < 1)
      //  {
      //    var emptymainContent = new MainContent();
      //    HttpContext.Current.Cache.Insert(key, emptymainContent);
      //    return emptymainContent;
      //  }

      //  MainContent parentMainContent = GetByPageId(pageSettings.ParentId);


      //  string parentKey = GetCacheKeyByPageId(pageSettings.ParentId);

      //  // Try cached item to parent key, just in case the parent banner is saved
      //  CacheDependency dep = new CacheDependency(null, new string[] { parentKey });
      //  HttpContext.Current.Cache.Insert(key, parentMainContent, dep);

      //  return parentMainContent;
      //}
    }

  }

}
