﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Web.Script.Serialization;
using Quantifi.Data.ContentArchiveModule;
using System.Web;
using System.Web.Caching;
using Quantifi.Shared;
using log4net;
using Quantifi.Business.TagsModule;
using Quantifi.Business.Utils;
using mojoPortal.Business.WebHelpers;
using Quantifi.Data.TagsModule;

namespace Quantifi.Business.ContentArchiveModule
{
	public class ContentItem : iTaggable
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(ContentItem));

		#region Properties

		public int Id { get; set; }
		public int ModuleId { get; set; }

		public DateTime StartDate { get; set; }

		public string Title { get; set; }
		public string SubTitle { get; set; }
		public string Description { get; set; }
		public string LinkDescription { get; set; }
		public string LinkText { get; set; }
		public string LinkUrl { get; set; }

		public bool isExternalLink { get; set; }


		/// <summary>
		/// Comma seperated Text
		/// </summary>
		public string Tags { get; set; }


		#endregion

		#region ctors

		public ContentItem() { Id = -1; Tags = string.Empty; }

		public ContentItem(int id, int moduleId, DateTime startDate,
			string title, string subTitle, string desc,
			string linkDesc, string linkText, string linkUrl,
			bool isExtLink, string tags)
		{
			Id = id;
			ModuleId = moduleId;
			StartDate = startDate;

			Title = title;
			SubTitle = subTitle;
			Description = desc;
			LinkDescription = linkDesc;
			LinkText = linkText;
			LinkUrl = linkUrl;
			isExternalLink = isExtLink;
			Tags = tags;

		}


		private ContentItem(IDataReader reader)
		{
			Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);
			ModuleId = Convert.ToInt32(reader["ModuleId"], CultureInfo.InvariantCulture);
			StartDate = Convert.ToDateTime(reader["StartDate"], CultureInfo.InvariantCulture);

			Title = reader["Title"].ToString();
			SubTitle = reader["SubTitle"].ToString();
			Description = reader["Description"].ToString();
			LinkDescription = reader["LinkDescription"].ToString();
			LinkText = reader["LinkText"].ToString();
			LinkUrl = reader["LinkUrl"].ToString();

			isExternalLink = Convert.ToBoolean(reader["isLinkExternal"], CultureInfo.InvariantCulture);
			Tags = reader["Tags"].ToString();
		}

		#endregion

		#region Methods

		public static ContentItem Load(int id)
		{
			ContentItem ci = null;

			if (id > 0)
			{
				using (IDataReader reader = ContentArchiveDB.Get(id))
				{
					if (reader.Read())
						ci = new ContentItem(reader);
				}
			}

			return ci;
		}

		public void Save()
		{
			if (this.Id < 0)
				Insert();
			else
				Update();

		}

		private void Insert()
		{
			// save item 
			this.Id = ContentArchiveDB.Insert(this.ModuleId, this.StartDate, this.Title, this.SubTitle, this.Description, this.LinkDescription, this.LinkText, this.LinkUrl, this.isExternalLink, this.Tags);



			// save tags
			//UpdateTags();

		}

		private void Update()
		{
			// save item 
			ContentArchiveDB.Update(this.Id, this.ModuleId, this.StartDate, this.Title, this.SubTitle, this.Description, this.LinkDescription, this.LinkText, this.LinkUrl, this.isExternalLink, this.Tags);


			// save tags
			//UpdateTags();

		}

		private void UpdateTags()
		{
			if (this.Id < 0)
				return;

			//var tags = this.Tags.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
			//                      .Select(s => s.Trim())
			//                      .ToList()
			//                      .Distinct();

			//ContentItem.DeleteAllContentItemTags(this.Id);

			//foreach (string s in tags)
			//{
			//  ContentItem.AddContentItemTags(this.Id, s);
			//}

		}

		public void Delete()
		{
			//ContentItem.DeleteAllContentItemTags(this.Id);

			ContentArchiveDB.Delete(this.Id, this.ModuleId);
		}


		#endregion

		#region iTaggable Members

		public int ItemId { get { return this.Id; } }

		public string GetUrl
		{
			get {
				var pages = Quantifi.Business.Utils.CacheUtils.GetPagesByModule(this.ModuleId);

				if (pages == null || !pages.Any())
					return string.Empty;

				var url = pages.First().Url + "?itemId=" + this.Id.ToString();

				return url;
			}
		}

		public string GetTitle
		{
			get { return this.Title; }
		}

		public string GetDescription
		{
			get {
				if (string.IsNullOrWhiteSpace(this.SubTitle))
					return this.Description;

				return "<h4>" + this.SubTitle + "</h4>" + this.Description;
			
			}
		}

		public DateTime GetStartDate
		{
			get { return this.StartDate; }
		}

		public List<ContentTag> GetTags
		{
			get { return ContentTagCacheUtil.GetContentItemTags(this.Id); }
		}

		#endregion

		#region Static Methods

		public static List<ContentItem> GetPage(int moduleId, int pageNum, int pageSize, out int totalCount)
		{
			List<ContentItem> items = new List<ContentItem>();
			using (IDataReader reader = ContentArchiveDB.GetPage(moduleId, DateTime.UtcNow, pageNum, pageSize, out totalCount))
			{
				while (reader.Read())
					items.Add(new ContentItem(reader));

			}
			return items;

		}

		//public static List<ContentItem> GetPageByTag(string tagText, int pageNum, int pageSize, out int totalCount)
		//{
		//  var tag = Tag.TagCheck(tagText);

		//  if (tag == null)
		//  {
		//    log.Warn("No Tag found for Text[" + tagText + "]");
		//    totalCount = 0;
		//    return new List<ContentItem>();
		//  }

		//  return GetPageByTag(tag.Id, pageNum, pageSize, out  totalCount);

		//}

		//public static List<ContentItem> GetPageByTag(int tagId, int pageNum, int pageSize, out int totalCount)
		//{
		//  List<ContentItem> items = new List<ContentItem>();

		//  using (IDataReader reader = ContentArchiveDB.GetPageByTag(tagId, DateTime.UtcNow, pageNum, pageSize, out totalCount))
		//  {
		//    while (reader.Read())
		//      items.Add(new ContentItem(reader));

		//  }
		//  return items;


		//}

		public static List<ContentItem> GetLatest(int moduleId, int count)
		{
			List<ContentItem> items = new List<ContentItem>();
			using (IDataReader reader = ContentArchiveDB.GetLatest(moduleId, count, DateTime.UtcNow))
			{
				while (reader.Read())
					items.Add(new ContentItem(reader));

			}
			return items;

		}

		public static List<ContentItem> GetByTag(int tagId)
		{
			List<ContentItem> items = new List<ContentItem>();

			using (IDataReader reader = ContentArchiveDB.GetByTag(tagId, DateTime.UtcNow))
			{
				while (reader.Read())
					items.Add(new ContentItem(reader));

			}
			return items;

		}

		public static void DeleteByModule(int moduleId)
		{


		}


		public static bool AddContentItemTags(int itemId, string tagText)
		{
			var tag = ContentTag.TagCheck(tagText);

			if (tag == null)
			{
				tag = new ContentTag() { Text = tagText };
				tag.Save();
			}

			return ContentTagsDB.AddTagForContentItem(itemId, tag.Id);
		}

		public static bool DeleteAllContentItemTags(int itemId)
		{
			return ContentTagsDB.DeleteAllTagsForContentItem(itemId);
		}

 
		#endregion

	}

 
	public static class ContentArchiveCacheUtil
	{
		private static object CacheLock = new object();
    private static string Global_CacheKey = "ContentArchive.All.Dependency";

    public static void ClearCache()
    {
      HttpContext.Current.Cache.Remove(Global_CacheKey);
    }

    public static void AddToCache(string key, object value)
    {
      AddToCache(key, value, new List<string>(0));
    }

    public static void AddToCache(string key, object value, List<string> additionalCacheKeys)
    {
      if (HttpContext.Current.Cache[Global_CacheKey] == null)
      {

        HttpContext.Current.Cache.Insert(Global_CacheKey, new object(),
                                         null, System.Web.Caching.Cache.NoAbsoluteExpiration,
                                         System.Web.Caching.Cache.NoSlidingExpiration,
                                         System.Web.Caching.CacheItemPriority.High, null);

      }

      var cacheKeys = new List<string> { Global_CacheKey };
      if (!additionalCacheKeys.IsNullOrEmpty())
        cacheKeys.AddRange(additionalCacheKeys);

      var cacheDependency = new CacheDependency(null, cacheKeys.ToArray(), DateTime.Now);

      HttpContext.Current.Cache.Insert(key, value, cacheDependency);
    }


		public static List<ContentItem> GetLatestItemsByModule(int mid, int topCount)
		{
			var cachekey = "Quantifi_ContentArchive_GetLatestItemsByModule[" + mid.ToString() + "]ByTopCount[" + topCount.ToString() + "]";


			var data = HttpContext.Current.Cache[cachekey] as List<ContentItem>;
			if (data != null)
			{
				return data;
			}

			data = ContentItem.GetLatest(mid, topCount);


      //var fileDependencyPath = CacheUtils.GetCacheDependencyFilePath(mid);

      //CacheHelper.EnsureCacheFile(fileDependencyPath);

      //CacheDependency dep = new CacheDependency(fileDependencyPath, DateTime.Now);
      //HttpContext.Current.Cache.Insert(cachekey, data, dep);


      AddToCache(cachekey, data);

			return data;

		}
 
		public static List<ContentItem> GetPageByModuleId(int moduleId, int pageNum, int pageSize, out int totalCount)
		{
			var cachekey = "Quantifi_ContentArchive_GetPageByModuleId[" + moduleId.ToString() + "]ByPageNum[" + pageNum.ToString() + "]ByPageSize[" + pageSize.ToString() + "]";

			var dataWrapper = HttpContext.Current.Cache[cachekey] as CachedPageInfo;
			if (dataWrapper != null)
			{
				totalCount = dataWrapper.TotalCount;
				return dataWrapper.Items;
			}

			dataWrapper = new CachedPageInfo() { Id = moduleId, PageNum = pageNum, PageSize = pageSize };
			dataWrapper.Items = ContentItem.GetPage(moduleId, pageNum, pageSize, out totalCount);
			dataWrapper.TotalCount = totalCount;


      //var fileDependencyPath = CacheUtils.GetCacheDependencyFilePath(moduleId);

      //CacheHelper.EnsureCacheFile(fileDependencyPath);

      //CacheDependency dep = new CacheDependency(fileDependencyPath, DateTime.Now);
      //HttpContext.Current.Cache.Insert(cachekey, dataWrapper, dep);

		  AddToCache(cachekey, dataWrapper);

			return dataWrapper.Items;

		}

		internal class CachedPageInfo
		{
			// TagId or ModuleId
			public int Id { get; set; }
			public int PageNum { get; set; }
			public int PageSize { get; set; }
			public int TotalCount { get; set; }
			public List<ContentItem> Items { get; set; }
		}

	}

}
