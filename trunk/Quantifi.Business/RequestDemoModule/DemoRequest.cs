﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quantifi.Data;
using System.Globalization;

namespace Quantifi.Business
{
  public class DemoRequest
  {
    #region Properties
    public int Id { get; set; }
    public string FullName { get; set; }
    public string Title { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public string CompanyName { get; set; }
    public string ContactNotes { get; set; }
    public string Questions { get; set; }

    public string UrlReferrer { get; set; }
    public DateTime TimeStamp { get; set; }
    public string ClientIP { get; set; } 
    #endregion

    public void Save()
    {
      if (this.Id < 1)
        this.Id = Insert(this);

      // Update not really needed to be supported? 
    }




    private static int Insert(DemoRequest request)
    {
      var id = DemoRequestDB.Insert(request.FullName, request.Title,
                                    request.Phone, request.Email,
                                    request.CompanyName, request.ContactNotes,
                                    request.Questions, request.ClientIP,
                                    request.UrlReferrer, request.TimeStamp);



      return id;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static List<DemoRequest> GetDemoRequests()
    {
      List<DemoRequest> list = new List<DemoRequest>();

      using (var reader = DemoRequestDB.GetDemoRequests())
      {
        while (reader.Read())
        {
          var demoRequest = new DemoRequest();


          demoRequest.Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);

          demoRequest.FullName = reader["FullName"].ToString();
          demoRequest.Title = reader["Title"].ToString();
          demoRequest.Phone = reader["Phone"].ToString();
          demoRequest.Email = reader["Email"].ToString();
          demoRequest.CompanyName = reader["CompanyName"].ToString();
          demoRequest.ContactNotes = reader["ContactNotes"].ToString();
          demoRequest.Questions = reader["Questions"].ToString();
          demoRequest.ClientIP = reader["ClientIP"].ToString();
          demoRequest.UrlReferrer = reader["UrlReferrer"].ToString();

          demoRequest.TimeStamp = Convert.ToDateTime(reader["TimeStamp"], CultureInfo.CurrentCulture);

          list.Add(demoRequest);

        }
      }




      return list;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageNum"></param>
    /// <param name="pageSize"></param>
    /// <returns></returns>
    public static List<DemoRequest> GetDemoRequests(int pageNum, int pageSize)
    {

      List<DemoRequest> list = new List<DemoRequest>();

      using (var reader = DemoRequestDB.GetDemoRequests(pageNum, pageSize))
      {
        while (reader.Read())
        {

          var demoRequest = new DemoRequest();


          demoRequest.Id = Convert.ToInt32(reader["Id"], CultureInfo.InvariantCulture);

          demoRequest.FullName = reader["FullName"].ToString();
          demoRequest.Title = reader["Title"].ToString();
          demoRequest.Phone = reader["Phone"].ToString();
          demoRequest.Email = reader["Email"].ToString();
          demoRequest.CompanyName = reader["CompanyName"].ToString();
          demoRequest.ContactNotes = reader["ContactNotes"].ToString();
          demoRequest.Questions = reader["Questions"].ToString();
          demoRequest.ClientIP = reader["ClientIP"].ToString();
          demoRequest.UrlReferrer = reader["UrlReferrer"].ToString();

          demoRequest.TimeStamp = Convert.ToDateTime(reader["TimeStamp"], CultureInfo.CurrentCulture);

          list.Add(demoRequest);


        }
      }
      return list;
    }


  }
}
