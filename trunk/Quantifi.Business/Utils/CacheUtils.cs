﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using mojoPortal.Business;
using System.Globalization;
using System.Web.Caching;
using System.Data;

namespace Quantifi.Business.Utils
{
  public static class CacheUtils
  {

    public static string GetCacheDependencyFilePath(int moduleId)
    {
      return GetCacheDependencyFilePath("Module-" + moduleId.ToString());
    }

    public static string GetCacheDependencyFilePath(string cacheDependencyKey)
    {
      if (HttpContext.Current == null)
        return string.Empty;

      SiteSettings siteSettings = mojoPortal.Business.WebHelpers.CacheHelper.GetCurrentSiteSettings();
        
      if (siteSettings == null)
        return string.Empty;

      string pathToCacheDependencyFile = HttpContext.Current.Server.MapPath(
                                                      "~/Data/Sites/"
                                                      + siteSettings.SiteId.ToString(CultureInfo.InvariantCulture)
                                                      + "/systemfiles/"
                                                      + cacheDependencyKey
                                                      + "cachedependecy.config");

      return pathToCacheDependencyFile;
    }

    public static List<PageSettings> GetPagesByModule(int mid)
    {
      var cacheKey = "GetPagesByModule[" + mid.ToString() + "]";

      List<PageSettings> pages = HttpContext.Current.Cache[cacheKey] as List<PageSettings>;
      if (pages != null)
      {
        return pages;
      }

      pages = new List<PageSettings>();

      List<int> pageIds = new List<int>();
      using (IDataReader reader = mojoPortal.Data.DBModule.PageModuleGetReaderByModule(mid))
      {
        while (reader.Read())
        {
          pageIds.Add(int.Parse(reader["PageID"].ToString()));
        }
      }

      int siteId = 1;
      SiteSettings siteSettings = mojoPortal.Business.WebHelpers.CacheHelper.GetCurrentSiteSettings();
      if (siteSettings != null) siteId = siteSettings.SiteId;


      foreach (int ids in pageIds)
      {
        pages.Add(new PageSettings(siteId, ids));
      }



      var fileDependencyPath = GetCacheDependencyFilePath(mid);
      var sitemapDependencyPath = GetCacheDependencyFilePath("sitemap");

      CacheDependency dep = new CacheDependency(new string[] { fileDependencyPath, sitemapDependencyPath }, DateTime.Now);
      HttpContext.Current.Cache.Insert(cacheKey, pages, dep);




      return pages;


    }

    public static PageSettings GetPage(int pageId)
    {

      int siteId = 1;
      SiteSettings siteSettings = mojoPortal.Business.WebHelpers.CacheHelper.GetCurrentSiteSettings();
      if (siteSettings != null) siteId = siteSettings.SiteId;

      return GetPage(siteId, pageId);
    }

    public static PageSettings GetPage(int siteId, int pageId)
    {
      var cacheKey = "GetPage[" + pageId.ToString() + "]for[" + siteId.ToString() + "]";

      var page = HttpContext.Current.Cache[cacheKey] as PageSettings;
      if (page != null)
        return page;



      page = new PageSettings(siteId, pageId);
      page.RefreshModules();


      List<string> filenames = new List<string>() { GetCacheDependencyFilePath("sitemap") };
      foreach (Module m in page.Modules)
      {
        filenames.Add(GetCacheDependencyFilePath(m.ModuleId));
      }


      CacheDependency dep = new CacheDependency(filenames.ToArray(), DateTime.Now);
      HttpContext.Current.Cache.Insert(cacheKey, page, dep);

      return page;
    }

  }
}