﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using log4net;
using mojoPortal.Business;
using Quantifi.Data;


namespace Quantifi.Business
{
  /// <summary>
  /// Represents an instance of a links module
  /// </summary>
  public class ImageLink : IIndexableContent
  {

    #region Constructors

    public ImageLink()
    { }

    public ImageLink(int itemId)
    {
      if (itemId > -1)
      {
        GetLink(itemId);
      }

    }

    public ImageLink(IDataReader reader)
    {
      

    }

    #endregion

    #region Private Properties

    private static readonly ILog log = LogManager.GetLogger(typeof(ImageLink));

    private Guid itemGuid = Guid.Empty;
    private Guid moduleGuid = Guid.Empty;
    private Int32 itemID = -1;
    private Int32 moduleID = -1;
    private Int32 createdByUser = -1;
    private DateTime createdDate = DateTime.Now;



    private String title = string.Empty;
    private String location = string.Empty;

    //private DateTime pubStartDate = DateTime.Now;
    //private DateTime pubEndDate = DateTime.MaxValue;
    private DateTime pubDate = DateTime.Now;

    private String description = string.Empty;
    private String url = string.Empty;
    private String imageUrl = string.Empty;
    private String target = String.Empty;
    private Int32 viewOrder = 500;

    private Guid userGuid = Guid.Empty;


    #endregion

    #region Public Properties

    public Guid ItemGuid
    {
      get { return itemGuid; }
      private set { itemGuid = value; }
    }

    public Guid ModuleGuid
    {
      get { return moduleGuid; }
      set { moduleGuid = value; }
    }

    public Int32 ItemId
    {
      get { return itemID; }
      set { itemID = value; }
    }

    public Int32 ModuleId
    {
      get { return moduleID; }
      set { moduleID = value; }
    }

    public Int32 CreatedByUser
    {
      get { return createdByUser; }
      set { createdByUser = value; }
    }

    public Guid UserGuid
    {
      get { return userGuid; }
      set { userGuid = value; }
    }

    public DateTime CreatedDate
    {
      get { return createdDate; }
      set { createdDate = value; }
    }

    //public DateTime PubStartDate
    //{
    //  get { return pubStartDate; }
    //  set { pubStartDate = value; }
    //}

    //public DateTime PubEndDate
    //{
    //  get { return pubEndDate; }
    //  set { pubEndDate = value; }
    //}

    public DateTime PubDate
    {
      get { return pubDate; }
      set { pubDate = value; }
    }

    public String Title
    {
      get { return title; }
      set { title = value; }
    }

    public String Url
    {
      get { return url; }
      set { url = value; }
    }

    public String Target
    {
      get { return target; }
      set { target = value.Trim(); }
    }

    public String ImageUrl
    {
      get { return imageUrl; }
      set { imageUrl = value; }
    }

    public Int32 ViewOrder
    {
      get { return viewOrder; }
      set { viewOrder = value; }
    }

    public String Description
    {
      get { return description; }
      set { description = value; }
    }

    public String Location { get { return location; } set { location = value; } }

    #endregion

    #region Private Methods

    private void GetLink(Int32 itemId)
    {
      using (IDataReader reader = Quantifi.Data.LinksDB.GetLink(itemId))
      {
        if (reader.Read())
        {
          this.itemID = Convert.ToInt32(reader["ItemID"], CultureInfo.InvariantCulture);
          this.moduleID = Convert.ToInt32(reader["ModuleID"], CultureInfo.InvariantCulture);
          this.createdByUser = Convert.ToInt32(reader["CreatedBy"], CultureInfo.InvariantCulture);
          this.createdDate = Convert.ToDateTime(reader["CreatedDate"], CultureInfo.CurrentCulture);
          this.title = reader["Title"].ToString();
          this.url = reader["Url"].ToString();

          this.viewOrder = Convert.ToInt32(reader["ViewOrder"]);
          this.description = reader["Description"].ToString();
          this.target = reader["Target"].ToString();

          this.location = reader["Location"].ToString();
          this.imageUrl = reader["ImageUrl"].ToString();
          this.pubDate = Convert.ToDateTime(reader["PubDate"], CultureInfo.CurrentCulture);

          string test = reader["ItemGuid"].ToString();

          this.itemGuid = new Guid(reader["ItemGuid"].ToString());
          this.moduleGuid = new Guid(reader["ModuleGuid"].ToString());
          string user = reader["UserGuid"].ToString();
          if (user.Length == 36) this.userGuid = new Guid(user);

        }

      }

    }

 
    private bool Create()
    {
      int newID = -1;
      this.itemGuid = Guid.NewGuid();

      newID = LinksDB.AddLink(
          this.itemGuid,
          this.moduleGuid,
          this.moduleID,
          this.title,
          this.url,
          this.imageUrl,
          this.location,
          this.pubDate,
          this.viewOrder,
          this.description,
          this.createdDate,
          this.createdByUser,
          this.target,
          this.userGuid);

      this.itemID = newID;

      bool result = (newID > 0);

      //IndexHelper.IndexItem(this);
      if (result)
      {
        ContentChangedEventArgs e = new ContentChangedEventArgs();
        OnContentChanged(e);
      }

      return result;

    }

    private bool Update()
    {

      bool result = LinksDB.UpdateLink(
            this.itemID,
            this.moduleID,
            this.title,
            this.url,
            this.imageUrl,
            this.location,
            this.pubDate,
            this.viewOrder,
            this.description,
            this.createdDate,
            this.target,
            this.createdByUser);

    
      if (result)
      {
        var e = new ContentChangedEventArgs();
        OnContentChanged(e);
      }

      return result;

    }


    #endregion

    #region Public Methods

    public bool Save()
    {
      if (this.itemID > -1)
      {
        return Update();
      }
      else
      {
        return Create();
      }
    }

    public bool Delete()
    {
      bool result = LinksDB.DeleteLink(itemID);

      if (result)
      {
        ContentChangedEventArgs e = new ContentChangedEventArgs();
        e.IsDeleted = true;
        OnContentChanged(e);
      }


      return result;
    }

    #endregion

    #region Static Methods

    public static List<ImageLink> GetLinks(Int32 moduleId, DateTime fromDate)
    {
      var items = new List<ImageLink>();

      using (var reader = LinksDB.GetLinks(moduleId, fromDate))
      {
        while (reader.Read())
        {

          var link = new ImageLink();
          link.itemID = Convert.ToInt32(reader["ItemID"], CultureInfo.InvariantCulture);
          link.moduleID = Convert.ToInt32(reader["ModuleID"], CultureInfo.InvariantCulture);
          link.createdByUser = Convert.ToInt32(reader["CreatedBy"], CultureInfo.InvariantCulture);
          link.createdDate = Convert.ToDateTime(reader["CreatedDate"], CultureInfo.CurrentCulture);
          link.title = reader["Title"].ToString();
          link.url = reader["Url"].ToString();

          link.viewOrder = Convert.ToInt32(reader["ViewOrder"]);
          link.description = reader["Description"].ToString();
          link.target = reader["Target"].ToString();

          link.location = reader["Location"].ToString();
          link.imageUrl = reader["ImageUrl"].ToString();
          link.pubDate = Convert.ToDateTime(reader["PubDate"], CultureInfo.CurrentCulture);

          //string test = reader["ItemGuid"].ToString();

          link.itemGuid = new Guid(reader["ItemGuid"].ToString());
          link.moduleGuid = new Guid(reader["ModuleGuid"].ToString());

          var user = reader["UserGuid"].ToString();
          if (user.Length == 36) 
            link.userGuid = new Guid(user);

          items.Add(link);
        }
      }
      return items;
    }

    public static IDataReader GetPage(
        int moduleId,
        int pageNumber,
        int pageSize,
        out int totalPages)
    {
      return LinksDB.GetPage(moduleId, pageNumber, pageSize, out totalPages);
    }




    public static DataTable GetLinksByPage(int siteId, int pageId)
    {
      DataTable dataTable = new DataTable();

      dataTable.Columns.Add("ItemID", typeof(int));
      dataTable.Columns.Add("ModuleID", typeof(int));
      dataTable.Columns.Add("ModuleTitle", typeof(string));
      dataTable.Columns.Add("Title", typeof(string));
      dataTable.Columns.Add("Description", typeof(string));
      dataTable.Columns.Add("ViewRoles", typeof(string));

      using (IDataReader reader = LinksDB.GetLinksByPage(siteId, pageId))
      {
        while (reader.Read())
        {
          DataRow row = dataTable.NewRow();

          row["ItemID"] = reader["ItemID"];
          row["ModuleID"] = reader["ModuleID"];
          row["ModuleTitle"] = reader["ModuleTitle"];
          row["Title"] = reader["Title"];
          row["Description"] = reader["Description"];
          row["ViewRoles"] = reader["ViewRoles"];

          dataTable.Rows.Add(row);
        }
      }

      return dataTable;
    }


    public static bool DeleteByModule(int moduleId)
    {
      return LinksDB.DeleteByModule(moduleId);
    }

    public static bool DeleteBySite(int siteId)
    {
      return LinksDB.DeleteBySite(siteId);
    }



    #endregion

    #region IIndexableContent

    public event ContentChangedEventHandler ContentChanged;

    protected void OnContentChanged(ContentChangedEventArgs e)
    {
      if (ContentChanged != null)
      {
        ContentChanged(this, e);
      }
    }




    #endregion


  }


}
