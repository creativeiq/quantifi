﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quantifi.Data.EF;
using System.Text.RegularExpressions;



namespace Quantifi.Business.Security
{

  public class RestrictedEmailsUtils
  {

    public static int CheckEmail(string emailAddress, out string msg)
    {
      return CheckEmail(emailAddress, true, out msg);
    }

    public static int CheckEmail(string emailAddress, bool logCheck, out string msg)
    {
      if (string.IsNullOrWhiteSpace(emailAddress))
      {
        throw new ArgumentNullException("emailAddress");
      }

      msg = string.Empty;
      var emailEnd = emailAddress.Substring(emailAddress.IndexOf("@") + 1);
      Regex regEx = null;

      using (var dc = new CorpWebEntities())
      {
        //todo: add some caching here if perf hit?
        var items = dc.EmailRestrictions.Where(ec => ec.isActive == true)
                                        .OrderByDescending(ec => ec.EmailChecks.Count);

        EmailRestrictions matchedRestriction = null;

        foreach (var check in items)
        {
          if (!string.IsNullOrWhiteSpace(check.RegExMatch))
          {
            regEx = new Regex(check.RegExMatch, RegexOptions.IgnoreCase);
            if (regEx.IsMatch(emailEnd))
            {
              matchedRestriction = check;
              break;
            }
          }
        }

        int result = (matchedRestriction == null) ? 0 : matchedRestriction.Id;

        msg = (result > 0) ? string.Format("{0} email domain was restricted, failed check against restriction: {1}", emailEnd, matchedRestriction.Name) : string.Empty;

        if (logCheck)
        {
          var ecl = dc.EmailChecks.CreateObject();

          ecl.EmailChecked = emailAddress;
          ecl.RestrictionId = result;
          ecl.Timestamp = DateTime.UtcNow;
          ecl.IsRestricted = (result > 0);

          dc.EmailChecks.AddObject(ecl);
          dc.SaveChanges();
        }



        return result;
      }


    }

  }


}

