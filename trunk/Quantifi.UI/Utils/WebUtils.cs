﻿
using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Web.UI.HtmlControls;
using System.Web.UI;




namespace Quantifi.UI.Utils
{
  public static class WebUtil
  {

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string HtmlEncode(this string text)
		{
			return HttpUtility.HtmlEncode(text);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string HtmlDecode(this string text)
		{
			return HttpUtility.HtmlDecode(text);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string UrlEncode(this string text)
		{
			return HttpUtility.UrlEncode(text);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string UrlDecode(this string text)
		{
			return HttpUtility.UrlDecode(text);
		}




    /// <summary>
    /// Strips HTML, CSS, and Script tags from string 
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string StripHTML(this string text)
    {
      return StripHTML(text, false);
    }

    /// <summary>
    /// Strips HTML, CSS, and Script tags from string  
    /// </summary>
    /// <param name="text"></param>
    /// <param name="leaveSafeHtml">leave some of the safer html in the string if true
    /// Not Safe Elements Removed: script|embed|object|frameset|frame|iframe|meta|link|style
    /// </param>
    /// <returns></returns>
    public static string StripHTML(this string text, bool leaveSafeHtml)
    {
      if (leaveSafeHtml)
        return Regex.Replace(text, @"</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>", "");
      //
      return Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
    }

    /// <summary>
    /// Disable button with a JavaScript function call.
    /// </summary>
    /// <param name="buttonControl"></param>
    /// <param name="clientFunction">Name of JS function to call</param>
    public static void DisableButtonOnClick(this Button buttonControl, string clientFunction)
    {
      StringBuilder sb = new StringBuilder(128);

      // If the page has ASP.NET validators on it, this code ensures the
      // page validates before continuing.
      if (buttonControl.CausesValidation && !String.IsNullOrEmpty(buttonControl.ValidationGroup))
      {
        sb.Append("if ( typeof( Page_ClientValidate ) == 'function' ) { ");
        sb.AppendFormat(@"if ( ! Page_ClientValidate('{0}') ) {{ return false; }} }} ", buttonControl.ValidationGroup);
      }
      // Disable this button.
			sb.Append("this.disabled = true; this.value='Please Wait'; ");

      // If a secondary JavaScript function has been provided, and if it can be found,
      // call it. Note the name of the JavaScript function to call should be passed without
      // parens.
      if (!String.IsNullOrEmpty(clientFunction))
      {
        sb.AppendFormat("if ( typeof( {0} ) == 'function') {{ {0}() }};", clientFunction);
      }

      // GetPostBackEventReference() obtains a reference to a client-side script function 
      // that causes the server to post back to the page (ie this causes the server-side part 
      // of the "click" to be performed).

      sb.Append(buttonControl.Page.ClientScript.GetPostBackEventReference(buttonControl, string.Empty).Replace("\"", "'") + ";");
      //sb.Append( ButtonControl.Page.GetPostBackEventReference(ButtonControl ) + ";");

      // Add the JavaScript created a code to be executed when the button is clicked.
      buttonControl.Attributes.Add("onclick", sb.ToString());
    }

    /// <summary>
    /// Adds ClientSide confirm and if true: disable button, run clientfunction ( if provided ), then postback
    /// </summary>
    /// <param name="buttonControl">Button to apply client side logic to</param>
    /// <param name="confirmMessage"> Message in Confirm Pop up
    /// </param>
    /// <param name="clientFunction">
    /// If a secondary JavaScript function has been provided, and if it can be found,
    /// call it. Note the name of the JavaScript function to call should be passed without parens.
    /// Use String.Empty to call no function
    /// </param>
    public static void ConfirmThenDisableButtonOnClick(this Button buttonControl, string confirmMessage, string clientFunction)
    {
      // remove old click handling if existing.
      if (buttonControl.Attributes["onclick"] != null)
        buttonControl.Attributes.Remove("onclick");

      StringBuilder sb = new StringBuilder();

      // If the page has ASP.NET validators on it, this code ensures the
      // page validates before continuing.
      if (buttonControl.CausesValidation && !String.IsNullOrEmpty(buttonControl.ValidationGroup))
      {
        sb.Append("if ( typeof( Page_ClientValidate ) == 'function' ) { ");
        sb.AppendFormat(@"if ( ! Page_ClientValidate('{0}') ) {{ return false; }} }} ", buttonControl.ValidationGroup);
      }
      string msg = HttpContext.Current.Server.HtmlEncode(confirmMessage);
      //pop confirm, if confirm==true, disable button
      sb.AppendFormat("if(confirm('{0}'))", msg);
			sb.Append("{this.disabled=true; this.value='Please Wait'; ");
      // If a secondary JavaScript function has been provided, and if it can be found,
      // call it. Note the name of the JavaScript function to call should be passed without
      // parens.
      if (!String.IsNullOrEmpty(clientFunction))
      {
        sb.AppendFormat("if ( typeof( {0} ) == 'function' ) {{ {0}() }};", clientFunction);
      }

      // GetPostBackEventReference() obtains a reference to a client-side script function 
      // that causes the server to post back to the page (ie this causes the server-side part 
      // of the "click" to be performed).


      sb.Append(buttonControl.Page.ClientScript.GetPostBackEventReference(buttonControl, string.Empty).Replace("\"", "'"));

      // if confirm==false do nothing
      sb.Append(";}else{return false;}");


      Debug.WriteLineIf(!string.IsNullOrEmpty(buttonControl.Attributes["onclick"]),
                          string.Format("ButtonOnClick Already Set: ", buttonControl.Attributes["onclick"]),
                          "fun?");


      buttonControl.Attributes.Add("onclick", sb.ToString());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    public static void ScrollTo(this Control control)
    {
      var script = string.Format(@"

        <script type='text/javascript'> 

                $(document).ready(function() {{
                        var element = document.getElementById('{0}');
                        element.scrollIntoView();
                        element.focus();
                }});

        </script>

    ", control.ClientID);

  
      ScriptManager.RegisterClientScriptBlock(control,
        control.GetType(),
            "ScrollTo",
            script,
            false);

    }

  }

}