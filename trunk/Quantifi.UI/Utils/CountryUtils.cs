﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Quantifi.UI.Utils
{
	public static class CountryUtils
	{


		public static CultureInfo ResolveCulture()
		{
			string[] languages = HttpContext.Current.Request.UserLanguages;
			if (languages == null || languages.Length == 0)
				return null;

			try
			{

				string language = languages[0].ToLowerInvariant().Trim();

				return CultureInfo.CreateSpecificCulture(language);

			}
			catch (ArgumentException)
			{

				return null;

			}

		}

		public static RegionInfo ResolveCountry()
		{

			CultureInfo culture = ResolveCulture();

			if (culture != null)
				return new RegionInfo(culture.LCID);

			return null;

		}

	}
}