﻿
using System;
using System.Web;
using System.Linq;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net.Mail;
using System.Configuration;
using mojoPortal.Net;
using log4net;
using System.Collections.Generic;
using mojoPortal.Web.Framework;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Quantifi.UI.Utils
{
	public static class EmailUtils
	{

		private static readonly ILog log = LogManager.GetLogger(typeof(Email));

		public const string PriorityLow = "Low";
		public const string PriorityNormal = "Normal";
		public const string PriorityHigh = "High";

		const int SmtpAuthenticated = 1;

		public static bool Send(
			SmtpSettings smtpSettings,
			string from,
			string replyTo,
			string to,
			string cc,
			string bcc,
			string subject,
			string messageBody,
			bool html,
			string priority,
			IEnumerable<Attachment> attachments)
		{
			if ((ConfigurationManager.AppSettings["DisableSmtp"] != null) && (ConfigurationManager.AppSettings["DisableSmtp"] == "true"))
			{
				log.Info("Not Sending email because DisableSmtp is true in config.");
				return false;
			}

			if ((smtpSettings == null) || (!smtpSettings.IsValid))
			{
				log.Error("Invalid smtp settings detected in SendEmail ");
				return false;
			}

			if (log.IsDebugEnabled) log.DebugFormat("In SendEmailNormal({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})",
																							from,
																							to,
																							cc,
																							bcc,
																							subject,
																							messageBody,
																							html,
																							priority);

			using (MailMessage mail = new MailMessage())
			{
				if (smtpSettings.PreferredEncoding.Length > 0)
				{
					switch (smtpSettings.PreferredEncoding)
					{
						case "ascii":
						case "us-ascii":
							// do nothing since this is the default
							break;

						case "utf32":
						case "utf-32":

							mail.BodyEncoding = Encoding.UTF32;
							mail.SubjectEncoding = Encoding.UTF32;

							break;

						case "unicode":

							mail.BodyEncoding = Encoding.Unicode;
							mail.SubjectEncoding = Encoding.Unicode;

							break;

						case "utf8":
						case "utf-8":
						default:

							mail.BodyEncoding = Encoding.UTF8;
							mail.SubjectEncoding = Encoding.UTF8;

							break;
					}

				}

				MailAddress fromAddress;
				try
				{
					fromAddress = new MailAddress(from);
				}
				catch (ArgumentException)
				{
					log.Error("invalid from address " + from);
					log.Info("no valid from address was provided so not sending message " + messageBody);
					return false;
				}
				catch (FormatException)
				{
					log.Error("invalid from address " + from);
					log.Info("no valid from address was provided so not sending message " + messageBody);
					return false;
				}

				mail.From = fromAddress;

				List<string> toAddresses = to.Replace(";", ",").SplitOnChar(',');
				foreach (string toAddress in toAddresses)
				{
					try
					{
						MailAddress a = new MailAddress(toAddress);
						mail.To.Add(a);
					}
					catch (ArgumentException)
					{
						log.Error("ignoring invalid to address " + toAddress);
					}
					catch (FormatException)
					{
						log.Error("ignoring invalid to address " + toAddress);
					}

				}


				if (replyTo.Length > 0)
				{
					try
					{
                        mail.ReplyToList.Add(new MailAddress(replyTo));
					}
					catch (ArgumentException)
					{
						log.Error("ignoring invalid replyto address " + replyTo);
					}
					catch (FormatException)
					{
						log.Error("ignoring invalid replyto address " + replyTo);
					}
				}

				if (cc.Length > 0)
				{
					List<string> ccAddresses = cc.Replace(";", ",").SplitOnChar(',');

					foreach (string ccAddress in ccAddresses)
					{
						try
						{
							MailAddress a = new MailAddress(ccAddress);
							mail.CC.Add(a);
						}
						catch (ArgumentException)
						{
							log.Error("ignoring invalid cc address " + ccAddress);
						}
						catch (FormatException)
						{
							log.Error("ignoring invalid cc address " + ccAddress);
						}
					}

				}

				if (bcc.Length > 0)
				{
					List<string> bccAddresses = bcc.Replace(";", ",").SplitOnChar(',');

					foreach (string bccAddress in bccAddresses)
					{
						try
						{
							MailAddress a = new MailAddress(bccAddress);
							mail.Bcc.Add(a);
						}
						catch (ArgumentException)
						{
							log.Error("invalid bcc address " + bccAddress);
						}
						catch (FormatException)
						{
							log.Error("invalid bcc address " + bccAddress);
						}
					}

				}


				if (mail.To.Count == 0 && mail.CC.Count == 0 && mail.Bcc.Count == 0)
				{
					log.Error("no valid to address was provided so not sending message " + messageBody);
					return false;
				}


				mail.Subject = subject.RemoveLineBreaks();

				switch (priority)
				{
					case PriorityHigh:
						mail.Priority = MailPriority.High;
						break;

					case PriorityLow:
						mail.Priority = MailPriority.Low;
						break;

					case PriorityNormal:
					default:
						mail.Priority = MailPriority.Normal;
						break;

				}



				if (html)
				{
					mail.IsBodyHtml = true;
					// this char can reportedly cause problems in some email clients so replace it if it exists
					mail.Body = messageBody.Replace("\xA0", "&nbsp;");
				}
				else
				{
					mail.Body = messageBody;
				}

				if (attachments != null)
				{
					foreach (var attatchment in attachments)
					{
						mail.Attachments.Add(attatchment);
					}
				}

				int timeoutMilliseconds = ConfigHelper.GetIntProperty("SMTPTimeoutInMilliseconds", 15000);
				SmtpClient smtpClient = new SmtpClient(smtpSettings.Server, smtpSettings.Port);
				smtpClient.EnableSsl = smtpSettings.UseSsl;
				smtpClient.Timeout = timeoutMilliseconds;

				if (smtpSettings.RequiresAuthentication)
				{

					NetworkCredential smtpCredential
							= new NetworkCredential(
									smtpSettings.User,
									smtpSettings.Password);

					CredentialCache myCache = new CredentialCache();
					myCache.Add(smtpSettings.Server, smtpSettings.Port, "LOGIN", smtpCredential);

					smtpClient.Credentials = myCache;
				}
				else
				{
					//aded 2010-01-22 JA
					smtpClient.UseDefaultCredentials = true;
				}


				try
				{
					smtpClient.Send(mail);
					//log.Debug("Sent Message: " + subject);
					//log.Info("Sent Message: " + subject);
					return true;
				}
				catch (System.Net.Mail.SmtpException ex)
				{
					//log.Error("error sending email to " + to + " from " + from, ex);
					log.Error("error sending email to " + mail.To.ToString() + " from " + mail.From.ToString() + ", will retry");
					return RetrySend(mail, smtpClient, ex);

				}
				catch (WebException ex)
				{
					log.Error("error sending email to " + to + " from " + from + ", message was: " + messageBody, ex);
					return false;
				}
				catch (SocketException ex)
				{
					log.Error("error sending email to " + to + " from " + from + ", message was: " + messageBody, ex);
					return false;
				}
				catch (InvalidOperationException ex)
				{
					log.Error("error sending email to " + to + " from " + from + ", message was: " + messageBody, ex);
					return false;
				}
				catch (FormatException ex)
				{
					log.Error("error sending email to " + to + " from " + from + ", message was: " + messageBody, ex);
					return false;
				}

			}// end using MailMessage



		}
		private static bool RetrySend(MailMessage message, SmtpClient smtp, Exception ex)
		{
			//retry
			int timesToRetry = ConfigHelper.GetIntProperty("TimesToRetryOnSmtpError", 3);
			for (int i = 1; i <= timesToRetry; )
			{
				if (RetrySend(message, smtp, i)) { return true; }
				i += 1;
				Thread.Sleep(1000); // 1 second sleep in case it is a temporary network issue
			}

			// allows use of localhost as  backup 
			if (ConfigurationManager.AppSettings["BackupSmtpServer"] != null)
			{
				string backupServer = ConfigurationManager.AppSettings["BackupSmtpServer"];
				int timeoutMilliseconds = ConfigHelper.GetIntProperty("SMTPTimeoutInMilliseconds", 15000);
				int backupSmtpPort = ConfigHelper.GetIntProperty("BackupSmtpPort", 25);
				SmtpClient smtpClient = new SmtpClient(backupServer, backupSmtpPort);
				smtpClient.UseDefaultCredentials = true;

				try
				{
					smtpClient.Send(message);
					log.Info("success using backup smtp server sending email to " + message.To.ToString() + " from " + message.From);
					return true;
				}
				catch (System.Net.Mail.SmtpException) { }
				catch (WebException) { }
				catch (SocketException) { }
				catch (InvalidOperationException) { }
				catch (FormatException) { }

			}

			//log.Info("all retries failed sending email to " + message.To.ToString() + " from " + message.From);
			log.Error("all retries failed sending email to " + message.To.ToString() + " from " + message.From.ToString() + ", message was: " + message.Body, ex);

			return false;

		}

		private static bool RetrySend(MailMessage message, SmtpClient smtp, int tryNumber)
		{
			try
			{
				smtp.Send(message);
				log.Info("success on retry " + tryNumber.ToInvariantString() + " sending email to " + message.To.ToString() + " from " + message.From);
				return true;
			}
			catch (System.Net.Mail.SmtpException) { }
			catch (WebException) { }
			catch (SocketException) { }
			catch (InvalidOperationException) { }
			catch (FormatException) { }

			return false;
		}



	}


}