﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;

namespace Quantifi.UI.Utils
{
  public static class CollectionsUtils
  {

    public static bool IsNullOrEmpty<T>(this IEnumerable<T> items)
    {
      return items == null || !items.Any();
    }

		/// <summary>
		/// Takes all strings, calls Trim then appends the delimiter
		/// </summary>
		/// <param name="items">list of string</param>
		/// <param name="delimiter">Default ", "</param>
		/// <returns>string</returns>
		public static string ToDelimitedString(this IEnumerable<string> items, string delimiter = ", ")
		{
			if (items.IsNullOrEmpty())
				return string.Empty;


			StringBuilder sb = new StringBuilder();

			foreach(string s in items)
			{
				sb.Append(s.Trim() + delimiter);
			}

			return sb.ToString(0, sb.Length - delimiter.Length);
		}



    public static string ToDelimitedString<T>(this IEnumerable<T> items, string delimiter = ", ")
    {
      if (items.IsNullOrEmpty())
        return string.Empty;


      StringBuilder sb = new StringBuilder();

      foreach (T obj in items)
      {
        sb.Append(obj.ToString() + delimiter);
      }

      return sb.ToString(0, sb.Length - delimiter.Length);

    }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="s"></param>
		/// <param name="delimiter"></param>
		/// <returns></returns>
		public static IEnumerable<string> FromDelimitedString(this string s, string delimiter = ",")
		{
			if (string.IsNullOrWhiteSpace(s))
				return new List<string>();


			return s.Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries).Select(str => str.Trim());

		}


		/// <summary>
		/// Shuffle List ( Base off predicatable Random() )
		/// http://stackoverflow.com/questions/273313/randomize-a-listt-in-c
		/// http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		public static void QuickShuffle<T>(this IList<T> list)
		{
			Random rng = new Random();
			int n = list.Count;
			while (n > 1)
			{
				n--;
				int k = rng.Next(n + 1);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}


		/// <summary>
		/// Shuffle List ( Better randomization using RNGCryptoServiceProvider  )
		/// 
		/// http://blog.thijssen.ch/2010/02/when-random-is-too-consistent.html
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		public static void Shuffle<T>(this IList<T> list)
		{
			RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
			int n = list.Count;
			while (n > 1)
			{
				byte[] box = new byte[1];
				do provider.GetBytes(box);
				while (!(box[0] < n * (Byte.MaxValue / n)));
				int k = (box[0] % n);
				n--;
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}
	
	}
}