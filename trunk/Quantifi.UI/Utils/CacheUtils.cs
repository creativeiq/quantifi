﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using mojoPortal.Business;
using System.Globalization;

namespace Quantifi.UI.Utils
{
  public static class CacheUtils
  {
    public static string GetCacheDependencyFilePath(String cacheDependencyKey)
    {
      if (HttpContext.Current == null)
        return string.Empty;

      var siteSettings = mojoPortal.Business.WebHelpers.CacheHelper.GetCurrentSiteSettings();
      if (siteSettings == null)
        return string.Empty;

      var cacheUrl = "~/Data/Sites/"
                        + siteSettings.SiteId.ToString(CultureInfo.InvariantCulture)
                        + "/systemfiles/"
                        + cacheDependencyKey
                        + "cachedependecy.config";

      var pathToCacheDependencyFile = HttpContext.Current.Server.MapPath(cacheUrl);

      return pathToCacheDependencyFile;
    }

  }
}