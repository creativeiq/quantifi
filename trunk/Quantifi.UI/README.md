# Project Informaton

MojoPortal Source is located in Mercurial repository at Codeplex

https://hg01.codeplex.com/mojoportal

Some info on how to get the files:

http://www.mojoportal.com/getting-the-code-with-tortoisehg.aspx

Quantifi's code is copied after build into the correct directoties in the MojoPortal Web folder

## Front End Development setup

1. Check that you have Node.js installed: http://nodejs.org/
2. Change to project root and run these commands:
    
    ```
    sudo npm install -g grunt-cli
    sudo npm install -g bower
    npm install
    bower install
    ```
3. To start front end dev work, run ```grunt watch```
    