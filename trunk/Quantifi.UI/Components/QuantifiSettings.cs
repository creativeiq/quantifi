﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Quantifi.UI.Utils;
using Quantifi.Shared;

namespace Quantifi.UI
{
	/// <summary>
	/// Settings
	/// </summary>
	public static class QuantifiSettings
	{
		const int TenMB = 10240; 

		#region Properties

		private static int? _Webpage_Body_MinWidth = null;
		private static int? _Webpage_Body_MaxWidth = null;
		private static string _ClientLoginUrl = null;
		private static string _TagsViewPageUrl = null;
		private static List<int> _BlogSideBarNavModuleIds = null;
		private static  string _DownloadFileDefaultAdminEmailAddress = null;
		private static  string _GeneralResponseEmailAddress = null;
		private static  int? _MaxEmailAttatchmentSizeKB;


		private static string _RegEx_EmailDomainRestrictions = null;
		private static string _RegEx_EmailTopLevelDomainRestrictions = null;


		#endregion


		/// <summary>
		/// RegEx expression to test email domains(.gmail,.yahoo,...), 
		/// should be used against emailaddress that has been stripped up to the '@'
		/// </summary>
		public static string RegEx_EmailDomainRestrictions
		{
			get
			{

				if (_RegEx_EmailDomainRestrictions == null)
				{
					_RegEx_EmailDomainRestrictions = ConfigurationManager.AppSettings["RegEx_EmailDomainRestrictions"];

					if (string.IsNullOrWhiteSpace(_RegEx_EmailDomainRestrictions))
						_RegEx_EmailDomainRestrictions = @"^(gmail\.)|(yahoo\.)|(msn\.)|(hotmail\.)";
				}

				return _RegEx_EmailDomainRestrictions;
			}
		}

		/// <summary>
		/// RegEx expression to test email Top Level Domains(.edu,.ac.uk...), 
		/// should be used against emailaddress that has been stripped up to the '@'
		/// </summary>
		public static string RegEx_EmailTopLevelDomainRestrictions
		{
			get
			{

				if (_RegEx_EmailTopLevelDomainRestrictions == null)
				{
					_RegEx_EmailTopLevelDomainRestrictions = ConfigurationManager.AppSettings["RegEx_EmailTopLevelDomainRestrictions"];

					if (string.IsNullOrWhiteSpace(_RegEx_EmailTopLevelDomainRestrictions))
						_RegEx_EmailTopLevelDomainRestrictions = @"(\.edu)|(\.ac\.uk)$";
				}

				return _RegEx_EmailTopLevelDomainRestrictions;
			}
		}

		public static string DownloadFileDefaultAdminEmailAddress
		{
			get
			{

				if (_DownloadFileDefaultAdminEmailAddress == null)
				{
					_DownloadFileDefaultAdminEmailAddress = ConfigurationManager.AppSettings["DownloadFileDefaultAdminEmailAddress"];

					if (string.IsNullOrWhiteSpace(_DownloadFileDefaultAdminEmailAddress))
						_DownloadFileDefaultAdminEmailAddress = "enquire@quantifisolutions.com";
				}

				return _DownloadFileDefaultAdminEmailAddress;
			}
		}
 
		public static string GeneralResponseEmailAddress
		{
			get
			{

				if (_GeneralResponseEmailAddress == null)
				{
					_GeneralResponseEmailAddress = ConfigurationManager.AppSettings["GeneralResponseEmailAddress"];

					if (string.IsNullOrWhiteSpace(_GeneralResponseEmailAddress))
						_GeneralResponseEmailAddress = "enquire@quantifisolutions.com";
				}

				return _GeneralResponseEmailAddress;
			}
		}

		public static int MaxEmailAttatchmentSizeKB
		{
			get
			{
				if (!_MaxEmailAttatchmentSizeKB.HasValue)
				{
					int val;

					if (!Int32.TryParse(ConfigurationManager.AppSettings["MaxEmailAttatchmentSizeKB"], out val))
						val = TenMB;

					_MaxEmailAttatchmentSizeKB = val;
				}

				return _MaxEmailAttatchmentSizeKB.Value;
			}
		}


 
		/// <summary>
		/// Webpage Body MinWidth
		/// </summary>
		public static int Webpage_Body_MinWidth
		{
			get
			{
				if (!_Webpage_Body_MinWidth.HasValue)
				{
					int val;

					if (!Int32.TryParse(ConfigurationManager.AppSettings["Webpage_Body_MinWidth"], out val))
						val = 955;

					_Webpage_Body_MinWidth = val;
				}

				return _Webpage_Body_MinWidth.Value;
			}
		}

		/// <summary>
		/// Webpage Body MaxWidth
		/// </summary>
		public static int Webpage_Body_MaxWidth
		{
			get { 
				if (!_Webpage_Body_MaxWidth.HasValue)
				{
						int val;

						if (!Int32.TryParse(ConfigurationManager.AppSettings["Webpage_Body_MaxWidth"], out val))
							val = 1193;

						_Webpage_Body_MaxWidth = val;
				}

				return _Webpage_Body_MaxWidth.Value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public static string ClientLoginUrl
		{
			get
			{
				if (_ClientLoginUrl == null)
				{
					_ClientLoginUrl = ConfigurationManager.AppSettings["ClientLoginUrl"];
					if (string.IsNullOrWhiteSpace(_ClientLoginUrl))
						_ClientLoginUrl = "https://portal.quantifisolutions.com/QuantifiClientPortal/";
				}
				return _ClientLoginUrl;


			}
		}
	

		public static string TagsViewPageUrl
		{
			get
			{

				if (_TagsViewPageUrl == null)
				{
					_TagsViewPageUrl = ConfigurationManager.AppSettings["TagsViewPageUrl"];

					if (string.IsNullOrWhiteSpace(_TagsViewPageUrl))
						_TagsViewPageUrl = "~/Tags.aspx";
				}

				return _TagsViewPageUrl;
			}
		}

		private static object _BlogSideBarNavModuleIds_lock = new object();
		public static List<int> BlogSideBarNavModuleIds
		{
			get
			{
				if (_BlogSideBarNavModuleIds == null)
				{

					lock (_BlogSideBarNavModuleIds_lock)
					{
						if (_BlogSideBarNavModuleIds != null)
							return _BlogSideBarNavModuleIds;

						_BlogSideBarNavModuleIds = new List<int>();
						if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["BlogSideBarNavModuleIds"]))
							return _BlogSideBarNavModuleIds;

						var stringIds = ConfigurationManager.AppSettings["BlogSideBarNavModuleIds"].FromDelimitedString();

						foreach (var strId in stringIds)
						{
							int id;
							if (int.TryParse(strId, out id))
							{
								if (!_BlogSideBarNavModuleIds.Contains(id))
									_BlogSideBarNavModuleIds.Add(id);
							}
						}

					}

				}


				return _BlogSideBarNavModuleIds;
			}

		}
 
		public static bool IncludeContentArchiveInTagsSearch
		{
			get
			{
				return mojoPortal.Web.Framework.ConfigHelper.GetBoolProperty("IncludeContentArchiveInTagsSearch", false);
			}
		}



    public static bool DownloadFiles_RedirectToTrackingPageFirst
    {
      get
      {
        return mojoPortal.Web.Framework.ConfigHelper.GetBoolProperty("DownloadFiles_RedirectToTrackingPageFirst", false);
      }
    }

    public static int DownloadFiles_TrackingPageRedirectTimeoutInSeconds
    {
      get
      {
        return mojoPortal.Web.Framework.ConfigHelper.GetIntProperty("DownloadFiles_TrackingPageRedirectTimeoutInSeconds", 3);
      }
    }

    public static int DownloadFiles_TrackingCookieTimeOutInMin
    {
      get
      {
        return mojoPortal.Web.Framework.ConfigHelper.GetIntProperty("DownloadFiles_TrackingCookieTimeOutInMin", 60);
      }
    }


	}
}