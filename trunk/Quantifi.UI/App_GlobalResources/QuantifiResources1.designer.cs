//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class QuantifiResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal QuantifiResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.QuantifiResources", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accordian Left Nav.
        /// </summary>
        internal static string AccordianLeftNavFeatureName {
            get {
                return ResourceManager.GetString("AccordianLeftNavFeatureName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Left Column.
        /// </summary>
        internal static string AccordianLeftNavLeftColumnVisibleSetting {
            get {
                return ResourceManager.GetString("AccordianLeftNavLeftColumnVisibleSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accordian  LeftNav Page Ids.
        /// </summary>
        internal static string AccordianLeftNavPageIdSetting {
            get {
                return ResourceManager.GetString("AccordianLeftNavPageIdSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to loading.
        /// </summary>
        internal static string AjaxLoadingMessage {
            get {
                return ResourceManager.GetString("AjaxLoadingMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crop Image.
        /// </summary>
        internal static string CropImageLink {
            get {
                return ResourceManager.GetString("CropImageLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tracked Download Files.
        /// </summary>
        internal static string DownloadFilesAdminLink {
            get {
                return ResourceManager.GetString("DownloadFilesAdminLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Browser.
        /// </summary>
        internal static string FileBrowseDialogHeading {
            get {
                return ResourceManager.GetString("FileBrowseDialogHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Browser.
        /// </summary>
        internal static string FileBrowser {
            get {
                return ResourceManager.GetString("FileBrowser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create Folder.
        /// </summary>
        internal static string FileBrowserCreateFolderButton {
            get {
                return ResourceManager.GetString("FileBrowserCreateFolderButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create Folder.
        /// </summary>
        internal static string FileBrowserCreateFolderHeading {
            get {
                return ResourceManager.GetString("FileBrowserCreateFolderHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First select the folder in the tree below where you would like the new FOLDER to be created. Then give the folder a name and press the &quot;Create Folder&quot; button..
        /// </summary>
        internal static string FileBrowserCreateFolderInstructions {
            get {
                return ResourceManager.GetString("FileBrowserCreateFolderInstructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reduce Image Size For Web.
        /// </summary>
        internal static string FileBrowserResizeForWeb {
            get {
                return ResourceManager.GetString("FileBrowserResizeForWeb", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First select a file from the tree menu to the left, then press the &quot;Select&quot; button..
        /// </summary>
        internal static string FileBrowserSelectFileInstructions {
            get {
                return ResourceManager.GetString("FileBrowserSelectFileInstructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upload a file.
        /// </summary>
        internal static string FileBrowserUploadHeading {
            get {
                return ResourceManager.GetString("FileBrowserUploadHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First select the folder in the tree below where you would like the new FILE to be placed. Then browse to the file on your computer and press &quot;Upload&quot;..
        /// </summary>
        internal static string FileBrowserUploadInstructions {
            get {
                return ResourceManager.GetString("FileBrowserUploadInstructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upload.
        /// </summary>
        internal static string FileManagerUploadButton {
            get {
                return ResourceManager.GetString("FileManagerUploadButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This type of file is not allowed..
        /// </summary>
        internal static string FileTypeNotAllowed {
            get {
                return ResourceManager.GetString("FileTypeNotAllowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Folder Name is limited to 150 chars.
        /// </summary>
        internal static string FolderName150Limit {
            get {
                return ResourceManager.GetString("FolderName150Limit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Name is required.
        /// </summary>
        internal static string FolderNameRequired {
            get {
                return ResourceManager.GetString("FolderNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No File Selected.
        /// </summary>
        internal static string NoFileSelectedWarning {
            get {
                return ResourceManager.GetString("NoFileSelectedWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restricted Email Domains.
        /// </summary>
        internal static string RestrictedEmailDomainsAdminLink {
            get {
                return ResourceManager.GetString("RestrictedEmailDomainsAdminLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select.
        /// </summary>
        internal static string SelectButton {
            get {
                return ResourceManager.GetString("SelectButton", resourceCulture);
            }
        }
    }
}
