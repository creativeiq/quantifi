module.exports = function(grunt) {

  //Initializing the configuration object
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    webroot: 'Quantifi/Data',
    scriptroot: 'Quantifi/ClientScript',
    skinroot: 'Quantifi/Data/skins/QC_v12',
    skin2root: 'Quantifi/Data/skins/QC_v12-Homepage',

    // Build LESS files
    less: {
        dist: {
            options: {
              sourceMap: true,
              compress: true,
              sourceMapFilename: '<%= skinroot %>/style.css.map',
              sourceMapBasepath: '<%= skinroot %>/',
              sourceMapRootpath: ''
            },
            files: {
              '<%= skinroot %>/style.css': '<%= skinroot %>/less/style.less'
            }
        }
    },

    // Concatenate and minify vendor CSS
    cssmin: {
      vendor: {
        files: {
          '<%= skinroot %>/vendor.css': [
            '<%= webroot %>/vendor/jQuery.TosRUs/src/css/jquery.tosrus.all.css'
            //'<%= webroot %>/components/bxslider-4/jquery.bxslider.css'
          ]
        }
      }
    },

    // Concatenate and compress JS
    uglify: {
      // Vendor libraries
      vendor: {
        options: {
          mangle: false,
          sourceMap: true,
          sourceMapName: '<%= scriptroot %>/combined/vendor.min.map'
        },
        files: {
          '<%= scriptroot %>/combined/vendor.min.js': [
            '<%= scriptroot %>/site/modernizr.js',
            '<%= webroot %>/vendor/es5-shim/es5-shim.js',
            '<%= webroot %>/vendor/html5shiv/dist/html5shiv.min.js',
            '<%= webroot %>/vendor/respond/dest/respond.min.js',
            '<%= webroot %>/vendor/fiber/src/fiber.min.js',
            '<%= webroot %>/vendor/jquery/dist/jquery.min.js',
            '<%= webroot %>/vendor/jquery-cookie/jquery.cookie.js',
            '<%= webroot %>/vendor/jquery.easing/js/jquery.easing.min.js',
            '<%= webroot %>/vendor/jquery-misc/jquery.ba-scrollbarwidth.js',
            '<%= webroot %>/vendor/jquery-placeholder/jquery.placeholder.js',
            '<%= webroot %>/vendor/imagesloaded/imagesloaded.pkgd.min.js',
            '<%= webroot %>/vendor/sticky/jquery.sticky.js',
            '<%= webroot %>/vendor/bootstrap/dist/js/bootstrap.js',
            '<%= webroot %>/vendor/bxslider-4/jquery.bxslider.js',
            '<%= webroot %>/vendor/stellar/jquery.stellar.js',
            '<%= webroot %>/vendor/d3/d3.js',
            '<%= webroot %>/vendor/d3.geo.projection.v0.min.js',
            '<%= webroot %>/vendor/jasny-bootstrap/dist/js/jasny-bootstrap.js',
            '<%= webroot %>/vendor/fitvids/jquery.fitvids.js',
            '<%= webroot %>/vendor/jQuery.TosRUs/src/js/jquery.tosrus.min.all.js'
          ]
        }
      },

      // Custom code
      core: {
        options: {
          mangle: false,
          sourceMap: true,
          sourceMapName: '<%= scriptroot %>/combined/core.min.map',
          banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */'
        },
        files: {
          '<%= scriptroot %>/combined/core.min.js': [
            '<%= webroot %>/scripts/site/core.js'
          ],
        }
      }
    },

    modernizr: {

        dist: {
            // [REQUIRED] Path to the build you're using for development.
            "devFile" : "<%= webroot %>/vendor/modernizr/modernizr.js",

            // [REQUIRED] Path to save out the built file.
            "outputFile" : "<%= scriptroot %>/site/modernizr.js",

            // Based on default settings on http://modernizr.com/download/
            "extra" : {
                "shiv" : false,
                "printshiv" : false,
                "load" : true,
                "mq" : false,
                "cssclasses" : true
            },

            // Based on default settings on http://modernizr.com/download/
            "extensibility" : {
                "addtest" : false,
                "prefixed" : false,
                "teststyles" : false,
                "testprops" : false,
                "testallprops" : false,
                "hasevents" : false,
                "prefixes" : false,
                "domprefixes" : false
            },

            // By default, source is uglified before saving
            "uglify" : false,

            // Define any tests you want to implicitly include.
            "tests" : [
              "rgba",
              "borderradius",
              "csstransitions",
              "csstransforms",
              "csstransforms3d",
              "svg",
              "geolocation",
              "backgroundsize"
            ],

            // By default, this task will crawl your project for references to Modernizr tests.
            // Set to false to disable.
            "parseFiles" : false,

            // When parseFiles = true, matchCommunityTests = true will attempt to
            // match user-contributed tests.
            "matchCommunityTests" : false,

            // Have custom Modernizr tests? Add paths to their location here.
            "customTests" : []
        }

    },

    // Build sprite file
    sprite: {
      all: {
        src: '<%= skinroot %>/images/sprites/**/*.png',
        destImg: '<%= skinroot %>/images/sprite.png',
        destCSS: '<%= skinroot %>/less/utilities/sprites.less',
        cssTemplate: '<%= skinroot %>/less/utilities/sprites.template.mustache',
        padding: 2,
        algorithm: 'binary-tree'
      }
    },

    // Duplicate skin copy
    copy: {
      dupe: {
        files: [
          {
            cwd: '<%= skinroot %>/',
            src: '**/*.css',
            dest: '<%= skin2root %>/',
            expand: true
          },
        ],
      },
    },

    // Watch task
    watch: {
      options: {
        livereload: true
      },
      less: {
        files: ['<%= skinroot %>/less/**/*.less'],
        tasks: ['less:dist']
      },
      sprite: {
        files: ['<%= skinroot %>/images/sprites/**/*.png'],
        tasks: ['sprite']
      },
      vendorjs: {
        files: ['<%= webroot %>/vendor/**/*.js'],
        tasks: ['uglify:vendor']
      },
      corejs: {
        files: ['<%= webroot %>/scripts/**/*.js', '!<%= scriptroot %>/**/*.js'],
        tasks: ['uglify:core']
      },
      vendorcss: {
        files: ['<%= webroot %>/vendor/**/*.css'],
        tasks: ['cssmin:vendor']
      },
      dupe: {
        files: ['<%= skinroot %>/**/*.css'],
        tasks: ['copy:dupe']
      }
    }

  });

  // Plugin loading
  grunt.loadNpmTasks('grunt-spritesmith');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-modernizr');

  // Task definition
  grunt.registerTask('dist', ['modernizr:dist', 'sprite', 'less:dist', 'cssmin:vendor', 'uglify:vendor', 'uglify:core']);
  grunt.registerTask('default', ['dist']);

};