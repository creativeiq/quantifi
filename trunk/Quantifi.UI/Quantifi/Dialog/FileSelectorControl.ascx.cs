﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;

namespace Quantifi.UI.Dialog
{



  public partial class FileSelectorControl : System.Web.UI.UserControl
  {
    public enum FileTypes
    {
      image,
      file
    }

    protected string SiteRoot
    {
      get { return SiteUtils.GetNavigationSiteRoot(); }
    }

    #region Page Life Cycle

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);


    }

    protected void Page_Load(object sender, EventArgs e) { }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (ValidatorContentTemplate != null)
      {
        this.reqValidator.Controls.Clear();




        ValidatorContentTemplate.InstantiateIn(this.reqValidator);


      }

      if (string.IsNullOrEmpty(Value))
        imgPreview.ImageUrl = SiteRoot + "/Quantifi/Images/no-image.gif";

      this.wrapper.CssClass += this.CssClass;
    }



    #endregion

    #region Properties

    private FileTypes _FileType = FileTypes.file;

    public FileTypes FileType
    {
      get { return _FileType; }
      set { _FileType = value; }
    }

    public string CssClass { get; set; }


    public string Value
    {
      get
      {
        return txtUrl.Text;
      }
      set
      {
        txtUrl.Text = value;
        imgPreview.ImageUrl = value;
      }
    }


    #endregion


    #region Validator Properties

    public bool isRequired
    {
      get
      {
        return this.reqValidator.Enabled;
      }
      set
      {
        this.reqValidator.Enabled = value;
      }

    }

    public string ErrorMessage
    {
      get
      {
        return this.reqValidator.ErrorMessage;
      }
      set
      {
        this.reqValidator.ErrorMessage = value;
      }

    }


    public string ValidationGroup
    {
      get
      {
        return this.reqValidator.ValidationGroup;
      }
      set
      {
        this.reqValidator.ValidationGroup = value;
      }
    }

    public ValidatorDisplay Display
    {
      get
      {
        return this.reqValidator.Display;
      }
      set
      {
        this.reqValidator.Display = value;
      }
    }

    public string ValidatorCssClass
    {
      get
      {
        return this.reqValidator.CssClass;
      }
      set
      {
        this.reqValidator.CssClass = value;
      }
    }

    public string ValidatorText
    {
      get
      {
        return this.reqValidator.Text;
      }
      set
      {
        this.reqValidator.Text = value;
      }
    }


    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate ValidatorContentTemplate
    {
      get;
      set;
    }

    #endregion
  }
}