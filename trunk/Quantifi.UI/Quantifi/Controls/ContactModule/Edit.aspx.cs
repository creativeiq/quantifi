﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using log4net;
using System.Collections;
using mojoPortal.Web.Editor;
using Quantifi.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Business;

namespace Quantifi.UI.Controls.ContactModule
{
  public partial class Edit : mojoBasePage
  {
    #region Properties

    private static readonly ILog log = LogManager.GetLogger(typeof(Edit));

    protected int moduleId = -1;
    protected int itemId = -1;
    protected int pageId = -1;
    protected String cacheDependencyKey;
    protected string virtualRoot;
    protected Double timeOffset = 0;
    private TimeZoneInfo timeZone = null;
    //protected Hashtable moduleSettings;

		private Module module = null;

		//private int pageNumber = 1;
		//private int pageSize = 10;
		//private int totalPages = 1;
    private Guid restoreGuid = Guid.Empty;


    protected Contact ContactUsContent = null;

    #endregion


    protected override void OnPreInit(EventArgs e)
    {

      base.OnPreInit(e);
      SiteUtils.SetupEditor(edTopDesc);
      SiteUtils.SetupEditor(edBottomDesc);
    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      btnCancel.Click += new EventHandler(btnCancel_Click);
      btnUpdate.Click += new EventHandler(btnUpdate_Click);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Request.IsAuthenticated)
      {
        SiteUtils.RedirectToLoginPage(this);
        return;
      }

      SecurityHelper.DisableBrowserCache();
      SuppressPageMenu();
      LoadParams();

      if (!UserCanEditModule(moduleId))
      {
        SiteUtils.RedirectToEditAccessDeniedPage();
        return;
      }


      LoadControls();

    }

    private void LoadParams()
    {
      timeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();
      pageId = WebUtils.ParseInt32FromQueryString("pageid", -1);
      moduleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      itemId = WebUtils.ParseInt32FromQueryString("ItemID", -1);
      //restoreGuid = WebUtils.ParseGuidFromQueryString("r", restoreGuid);
      //cacheDependencyKey = "Module-" + moduleId.ToInvariantString();
      virtualRoot = WebUtils.GetApplicationRoot();


    }
  
  
    private void LoadControls()
    {
      if ((!Page.IsPostBack) && (!Page.IsCallback))
      {

				module = new Module(moduleId);
				if (module != null)
					this.legendTitle.InnerHtml += " for [" + module.ModuleTitle + "]";


        if ((Request.UrlReferrer != null) && (hdnReturnUrl.Value.Length == 0))
        {
          hdnReturnUrl.Value = Request.UrlReferrer.ToString();
        }


        ContactUsContent = Contact.Get(this.moduleId);

        if (ContactUsContent != null)
        {
          edTopDesc.Text = ContactUsContent.TopContent;
          edBottomDesc.Text = ContactUsContent.ThankYouContent;
        }

        edTopDesc.WebEditor.ToolBar = ToolBar.FullWithTemplates;
        edBottomDesc.WebEditor.ToolBar = ToolBar.FullWithTemplates;

      }


    }


    void btnUpdate_Click(object sender, EventArgs e)
    {
      
        ContactUsContent = Contact.Get(this.moduleId);

        if (ContactUsContent == null)
          ContactUsContent = new Contact() { ModuleId = this.moduleId };

        ContactUsContent.TopContent = edTopDesc.Text;
        ContactUsContent.ThankYouContent = edBottomDesc.Text;

      
        ContactUsContent.Save();

        CurrentPage.UpdateLastModifiedTime();
        //CacheHelper.TouchCacheDependencyFile(cacheDependencyKey);

    }

    void btnCancel_Click(object sender, EventArgs e)
    {


      if (!string.IsNullOrWhiteSpace(this.hdnReturnUrl.Value) )
      {
        Response.Redirect(hdnReturnUrl.Value);
        return;
      }
    }

  
  
  }
}