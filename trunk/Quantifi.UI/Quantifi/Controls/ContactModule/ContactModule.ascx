﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactModule.ascx.cs"
  Inherits="Quantifi.UI.Controls.ContactModule.ContactModule" %>
<portal:ModulePanel ID="ModulePanel1" runat="server">
  <mp:CornerRounderTop ID="ctop1" runat="server" />
  <portal:ModuleTitleControl ID="Title1" runat="server" />
  <asp:Panel ID="pnlColapsableHeader" runat="server" CssClass="toggleHeader">
    <asp:Literal ID="litModuleAnchor" runat="server" />
    <h2>
      <asp:HyperLink ID="linkOpenClose" runat="server" CssClass="expand" EnableViewState="true">+</asp:HyperLink>
      <asp:HyperLink ID="linkTitle" runat="server" CssClass="title"></asp:HyperLink><asp:HyperLink
        ID="linkSettings" runat="server" CssClass="edit">Settings</asp:HyperLink><asp:HyperLink
          ID="linkEdit" runat="server" CssClass="edit">Edit</asp:HyperLink>
    </h2>
  </asp:Panel>
  <asp:Panel ID="pnlCollapsibleContent" runat="server" CssClass="" EnableViewState="true">
    <asp:Panel ID="pnlErrorMsg" runat="server" Visible="false" EnableViewState="false"
      CssClass="errorPanel">
      <div class="ErrorContainer" style="margin: 5px; padding: 5px; color: #FFF; border: solid 1px #AA0000; background-color: #AA0000">
        <img src="/Data/SiteImages/RejectChanges.gif" alt="Error" border="0" />
        Sorry, something strange happened.
      </div>
      <p>
        <asp:Literal ID="litError" runat="server"></asp:Literal>
      </p>
      <p>
        While trying to submit your request for more information and error occurred. Please
        try and submit your request again. If the error continues, please call our offices.<br />
        We are sorry for the inconvenience.
      </p>
      <br />
      <br />
      <script type="text/javascript">
        $(document).ready(function () {
          $(".ErrorContainer").animate({
            backgroundColor: "#FFF",
            color: "#000000"
          }, 5000);

        });
      </script>
    </asp:Panel>
    <asp:Panel ID="pnlContent" runat="server" DefaultButton="btnSubmit" EnableViewState="true"
      CssClass="pnlContent contactus">
      <asp:Panel ID="pnlTopContent" CssClass="contactDescWrapper" runat="server" ViewStateMode="Disabled"
        Visible="false" />
      <div class="contactFormWrapper">
        <table class="contactForm">
          <%--FIRST NAME--%>
          <tr>
            <th>
              <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">First Name</asp:Label>
            </th>
            <td>
              <asp:TextBox ID="txtFirstName" runat="server" AutoCompleteType="FirstName"></asp:TextBox><asp:RequiredFieldValidator
                ID="reqFieldVal_FirstName" runat="server" ControlToValidate="txtFirstName" CssClass="validator"
                EnableClientScript="true" ErrorMessage="Please enter your first name">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <%--LAST NAME--%>
          <tr>
            <th>
              <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Last Name</asp:Label>
            </th>
            <td>
              <asp:TextBox ID="txtLastName" runat="server" AutoCompleteType="LastName"></asp:TextBox><asp:RequiredFieldValidator
                ID="reqFieldVal_LastName" runat="server" ControlToValidate="txtLastName" CssClass="validator"
                EnableClientScript="true" ErrorMessage="Please enter your last name">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <%--Company--%>
          <asp:PlaceHolder ID="phCompany" runat="server">
            <tr>
              <th>
                <asp:Label ID="lblCompany" runat="server" AssociatedControlID="txtCompany">Organisation</asp:Label>
              </th>
              <td>
                <asp:TextBox ID="txtCompany" runat="server" AutoCompleteType="Company"></asp:TextBox><asp:RequiredFieldValidator
                  ID="reqFieldVal_Company" runat="server" ControlToValidate="txtCompany" CssClass="validator"
                  EnableClientScript="true" ErrorMessage="Please enter your company info">*</asp:RequiredFieldValidator>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Job Title--%>
          <asp:PlaceHolder ID="phJobTitle" runat="server">
            <tr>
              <th>
                <asp:Label ID="lblTitle" runat="server" AssociatedControlID="txtTitle">Job Title</asp:Label>
              </th>
              <td>
                <asp:TextBox ID="txtTitle" runat="server" AutoCompleteType="JobTitle"></asp:TextBox><asp:RequiredFieldValidator
                  ID="reqFieldVal_Title" runat="server" ControlToValidate="txtTitle" CssClass="validator"
                  EnableClientScript="true" ErrorMessage="Please enter your job title">*</asp:RequiredFieldValidator>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Phone--%>
          <tr>
            <th>
              <asp:Label ID="lblPhone" runat="server" AssociatedControlID="txtPhone">Contact Number</asp:Label>
            </th>
            <td>
              <asp:TextBox ID="txtPhone" runat="server" AutoCompleteType="BusinessPhone"></asp:TextBox><asp:RequiredFieldValidator
                ID="reqFieldVal_Phone" runat="server" ControlToValidate="txtPhone" CssClass="validator"
                EnableClientScript="true" ErrorMessage="Please enter your phone number">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <%--Email--%>
          <tr>
            <th>
              <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail">Email Address</asp:Label>
            </th>
            <td>
              <asp:TextBox ID="txtEmail" runat="server" AutoCompleteType="Email"></asp:TextBox><asp:RequiredFieldValidator
                ID="reqFieldVal_Email" runat="server" ControlToValidate="txtEmail" Display="Dynamic"
                CssClass="validator" EnableClientScript="true" ErrorMessage="Please enter your email">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                  ID="regexEmail" runat="server" EnableClientScript="true" Display="Dynamic" ControlToValidate="txtEmail"
                  CssClass="validator" ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,9})$"
                  ErrorMessage="Please enter a valid email">*</asp:RegularExpressionValidator>
            </td>
          </tr>
          <%--Choices--%>
          <asp:PlaceHolder ID="phChoices" runat="server">
            <tr>
              <th>
                <asp:Label ID="lblChoices" runat="server" AssociatedControlID="ddlChoices"></asp:Label>
              </th>
              <td>
                <asp:DropDownList ID="ddlChoices" runat="server" Width="275">
                </asp:DropDownList>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Country--%>
          <asp:PlaceHolder ID="phCountry" runat="server">
            <tr>
              <th>
                <asp:Label ID="lblCountry" runat="server" AssociatedControlID="ddlCountry">Country</asp:Label>
              </th>
              <td>
                <asp:DropDownList ID="ddlCountry" runat="server" Width="275">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqFieldVal_Country" runat="server" 
                  CssClass="validator"  
                  Display="Dynamic" ControlToValidate="ddlCountry"
                  ErrorMessage="Please enter your country"  
                  InitialValue="" 
                  EnableClientScript="true"  >*</asp:RequiredFieldValidator>
       
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Multi-Select-1 --%>
          <asp:PlaceHolder ID="phMultiSelect1" runat="server">
            <tr>
              <td colspan="2" class="choices">
                <fieldset>
                  <legend>
                    <asp:Literal ID="litMultiSelect1" runat="server" Text="Choose"></asp:Literal>
                  </legend>
                  <asp:CheckBoxList ID="checkboxMultiSelect1" runat="server" />
                  <div class="clear">
                  </div>
                  <asp:Label ID="lblMultiSelect1Other" runat="server">Other</asp:Label>
                  <asp:TextBox ID="txtMultiSelect1Other" runat="server"></asp:TextBox>
                </fieldset>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Multi-Select-2 --%>
          <asp:PlaceHolder ID="phMultiSelect2" runat="server">
            <tr>
              <td colspan="2" class="choices">
                <fieldset>
                  <legend>
                    <asp:Literal ID="litMultiSelect2" runat="server" Text="Choose"></asp:Literal>
                  </legend>
                  <asp:CheckBoxList ID="checkboxMultiSelect2" runat="server" />
                  <div class="clear">
                  </div>
                  <asp:Label ID="lblMultiSelect2Other" runat="server">Other</asp:Label>
                  <asp:TextBox ID="txtMultiSelect2Other" runat="server"></asp:TextBox>
                </fieldset>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Multi-Select-3 --%>
          <asp:PlaceHolder ID="phMultiSelect3" runat="server">
            <tr>
              <td colspan="2" class="choices">
                <fieldset>
                  <legend>
                    <asp:Literal ID="litMultiSelect3" runat="server" Text="Choose"></asp:Literal>
                  </legend>
                  <asp:CheckBoxList ID="checkboxMultiSelect3" runat="server" />
                  <div class="clear">
                  </div>
                  <asp:Label ID="lblMultiSelect3Other" runat="server">Other</asp:Label>
                  <asp:TextBox ID="txtMultiSelect3Other" runat="server"></asp:TextBox>
                </fieldset>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Select Options--%>
          <asp:PlaceHolder ID="phRadioOptions" runat="server">
            <tr>
              <th colspan="2">
                <asp:Label ID="lblRadioOptions" runat="server" AssociatedControlID="rblOptions">Radio Options</asp:Label>
              </th>
            </tr>
            <tr>
              <td colspan="2">
                <asp:RadioButtonList ID="rblOptions" runat="server" AutoPostBack="false" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Extra--%>
          <asp:PlaceHolder ID="phExtra" runat="server" Visible="true">
            <tr>
              <th colspan="2" class="extra">
                <asp:Label ID="lblExtra" runat="server" AssociatedControlID="txtExtra">Extra</asp:Label>
              </th>
            </tr>
            <tr>
              <td colspan="2" class="extra">
                <asp:TextBox ID="txtExtra" runat="server" AutoCompleteType="None"></asp:TextBox><asp:RequiredFieldValidator
                  ID="reqFieldVal_Extra" runat="server" ControlToValidate="txtExtra" CssClass="validator"
                  EnableClientScript="true" Enabled="false" ErrorMessage="*">*</asp:RequiredFieldValidator>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Questions--%>
          <asp:PlaceHolder ID="phQuestions" runat="server" Visible="true">
            <tr>
              <th colspan="2" class="questions">
                <asp:Label ID="lblQuestions" runat="server" AssociatedControlID="txtQuestions">Question(s)</asp:Label>
              </th>
            </tr>
            <tr>
              <td colspan="2" class="questions">
                <asp:TextBox ID="txtQuestions" runat="server" AutoCompleteType="None" TextMode="MultiLine"
                  Rows="8" MaxLength="1000" Width="100%"></asp:TextBox>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Attachment--%>
          <asp:PlaceHolder ID="phAttachment" runat="server" Visible="true">
            <tr>
              <th class="attachment">
                <asp:Label ID="lblAttachment" runat="server">Attachment</asp:Label>
              </th>
              <td class="attachment">
                <asp:FileUpload ID="fileUploadAttachment" runat="server" /><asp:CustomValidator ID="customVal_Attachment"
                  runat="server" Display="Dynamic" CssClass="validator" ErrorMessage="Please select a file to upload">*</asp:CustomValidator>
                <script language="javascript" type="text/javascript">
                  function <%= customVal_Attachment.ClientID %>_Validate(Source, args) {
                    var controlId = '<%= fileUploadAttachment.ClientID %>';
                    var fileUploadData = document.getElementById(controlId);
                    var FileUploadPath = fileUploadData.value;

                    if (FileUploadPath == '') {
                      // There is no file selected 
                      args.IsValid = false;
                    }
                    else {
                      args.IsValid = true;
                    }
                  }
                </script>
              </td>
            </tr>
          </asp:PlaceHolder>
          <%--Marketing Optin--%>
          <asp:PlaceHolder ID="phMarketingOptin" runat="server">
            <tr>
              <td colspan="2">
                <asp:CheckBox ID="checkboxOptIn" runat="server" TabIndex="8" Text=" Please check if you do not wish to receive occasional information from Quantifi" />
              </td>
            </tr>
          </asp:PlaceHolder>
        </table>
      </div>

      <asp:ValidationSummary ID="valSummary" runat="server" EnableClientScript="true" DisplayMode="List"
        ShowMessageBox="true" ShowSummary="false" />
      <p class="info">
        <span>* Required Fields</span><br />
        This information will only be used by Quantifi Solutions and will not be disclosed
        to any third parties. For further details please visit our <a href="/privacy-policy.aspx"
          target="_blank">Privacy&nbsp;Policy</a>.
      </p>
      <asp:Button ID="btnSubmit" runat="server" CausesValidation="true" Text="Submit Request" />


<%--      <script type="text/javascript">
        $('#<%=pnlContent.ClientID %>').ready(function () {

          $(this).find("input").each(function () {
            $(this).blur(function () { $(this).removeClass("inputFocused"); Quantifi.util.callMyValidators(this); });
            $(this).focus(function () { $(this).addClass("inputFocused"); });
          });
          $(this).find("textarea").each(function () {
            $(this).blur(function () { $(this).removeClass("inputFocused"); Quantifi.util.callMyValidators(this); });
            $(this).focus(function () { $(this).addClass("inputFocused"); });
          });



          var fname = $("#<%= this.txtFirstName.ClientID %>");
          var lname = $("#<%= this.txtLastName.ClientID %>");
          var jobtitle = $("#<%= this.txtTitle.ClientID %>");
          var company = $("#<%= this.txtCompany.ClientID %>");
          var email = $("#<%= this.txtEmail.ClientID %>");

          var phone = $("#<%= this.txtPhone.ClientID %>");
          var country = $("#<%= this.ddlCountry.ClientID %>");
          var submitBtn = $("#<%= this.btnSubmit.ClientID %>");

          var allFields = $([]).add(fname).add(lname).add(jobtitle).add(company).add(email);



          function initForm() {

            fname.val($.cookie("u.fname"));
            lname.val($.cookie("u.lname"));
            jobtitle.val($.cookie("u.jobtitle"));
            company.val($.cookie("u.company"));
            email.val($.cookie("u.email"));
            phone.val($.cookie("u.phone"));

            if ($.cookie("u.country") != null) {
              var newSelection = country.find('option[value = "' + $.cookie("u.country") + '"]');
              if (newSelection.length > 0) {
                var oldSelection = country.find(':selected').attr("selected", false);
                newSelection.attr("selected", true);
              }
            }
          }

          function saveForm() {

            $.cookie("u.fname", fname.val(), { expires: 90 });
            $.cookie("u.lname", lname.val(), { expires: 90 });
            $.cookie("u.jobtitle", jobtitle.val(), { expires: 90 });
            $.cookie("u.company", company.val(), { expires: 90 });
            $.cookie("u.email", email.val(), { expires: 90 });
            $.cookie("u.phone", phone.val(), { expires: 90 });

            var option = country.find(':selected').val();
            if (option != null)
              $.cookie("u.country", option, { expires: 90 });

          }

          initForm();


          $(submitBtn).click(function (event) {
            saveForm();
          });

        });


      </script>--%>

    </asp:Panel>
    <asp:Panel ID="pnlSuccessMsg" runat="server" Visible="false" />
  </asp:Panel>
  <mp:CornerRounderBottom ID="cbottom1" runat="server" EnableViewState="false" />
</portal:ModulePanel>
