﻿
using System;
using System.Collections.Generic;
using System.Text;
using Quantifi.Business;
using mojoPortal.Business.WebHelpers;




namespace Quantifi.UI
{
  public class QuantifiContactUsDeleteHandler : ContentDeleteHandlerProvider
  {
    public QuantifiContactUsDeleteHandler()
    { }

    public override void DeleteContent(int moduleId, Guid moduleGuid)
    {

      Contact.DeleteByModule(moduleId);


      mojoPortal.Business.ContentMetaRespository metaRepository = new mojoPortal.Business.ContentMetaRespository();
      metaRepository.DeleteByModule(moduleGuid);

      mojoPortal.Business.FriendlyUrl.DeleteByPageGuid(moduleGuid);

    }

  }
}
