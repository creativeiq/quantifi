﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using mojoPortal.Web.Framework;
using Quantifi.Shared;

namespace Quantifi.UI.ContactModule
{
	public class ContactUsSettings
	{
		#region Properties

		public bool ShowJobTitle { get; set; }
		public bool ShowFileUpload { get; set; }
		public bool ShowQuestions { get; set; }
		public bool ShowCompany { get; set; }


		public bool ShowChoices { get; set; }
		public bool isChoicesCheckBoxes { get; set; }
		public string ChoicesOptionsCSV { get; set; }
		public string ChoicesLabel { get; set; }



		public bool ShowCountrySelect { get; set; }

		public bool ShowExtraField { get; set; }
		public bool isExtraFieldRequired { get; set; }
		public string ExtraFieldLabel { get; set; }
		public string ExtraFieldValidationErrorMsg { get; set; }


		public string SendToEmail { get; set; }
		public bool SendConfirmEmail { get; set; }
		public bool SendFormContentInConfirmEmail { get; set; }

		public string EmailThankYouText { get; set; }

		public string SubmitButtonText { get; set; }

		public bool IsCollapsible { get; set; }

		public bool MultiSelect1Show { get; set; }
		public string MultiSelect1Title { get; set; }
		public string MultiSelect1Options { get; set; }
		public bool MultiSelect1AllowOther { get; set; }
		public string MultiSelect1OtherChoice { get; set; }
		public string MultiSelect1OtherChoicSelection { get; set; }

		public bool MultiSelect2Show { get; set; }
		public string MultiSelect2Title { get; set; }
		public string MultiSelect2Options { get; set; }
		public bool MultiSelect2AllowOther { get; set; }
		public string MultiSelect2OtherChoice { get; set; }
		public string MultiSelect2OtherChoicSelection { get; set; }

		public bool MultiSelect3Show { get; set; }
		public string MultiSelect3Title { get; set; }
		public string MultiSelect3Options { get; set; }
		public bool MultiSelect3AllowOther { get; set; }
		public string MultiSelect3OtherChoice { get; set; }
		public string MultiSelect3OtherChoicSelection { get; set; }


		public bool ShowMarketingOptin { get; set; }

		public bool ShowRadioOptions { get; set; }
		public string RadioOptionTitle { get; set; }
		public List<string> RadioOptions { get; set; }

		#endregion


		public ContactUsSettings()
		{ }

		public ContactUsSettings(Hashtable settings)
		{
			LoadSettings(settings);
		}

		private void LoadSettings(Hashtable settings)
		{
			if (settings == null) { throw new ArgumentException("must pass in a hashtable of settings"); }

			ShowJobTitle = WebUtils.ParseBoolFromHashtable(settings, "ShowJobTitleSetting", false);
			ShowFileUpload = WebUtils.ParseBoolFromHashtable(settings, "ShowFileUploadSetting", false);
			ShowQuestions = WebUtils.ParseBoolFromHashtable(settings, "ShowQuestionsSetting", false);
			ShowCompany = WebUtils.ParseBoolFromHashtable(settings, "ShowCompanySetting", false);


			ShowChoices = WebUtils.ParseBoolFromHashtable(settings, "ShowChoicesSetting", false);
			isChoicesCheckBoxes = WebUtils.ParseBoolFromHashtable(settings, "isChoicesCheckBoxesSetting", false);
			if (settings.Contains("ChoicesOptionsCSVSetting"))
				ChoicesOptionsCSV = settings["ChoicesOptionsCSVSetting"].ToString();

			if (settings.Contains("ChoicesLabelSetting"))
				ChoicesLabel = settings["ChoicesLabelSetting"].ToString();
 


			ShowExtraField = WebUtils.ParseBoolFromHashtable(settings, "ShowExtraFieldSetting", false);
			isExtraFieldRequired = WebUtils.ParseBoolFromHashtable(settings, "isExtraFieldRequiredSetting", false);
			if (settings.Contains("ExtraFieldLabelSetting"))
				ExtraFieldLabel = settings["ExtraFieldLabelSetting"].ToString();

			if (settings.Contains("ExtraFieldValidationErrorSetting"))
				ExtraFieldValidationErrorMsg = settings["ExtraFieldValidationErrorSetting"].ToString();

			SendConfirmEmail = WebUtils.ParseBoolFromHashtable(settings, "SendConfirmEmailSetting", false);
			SendFormContentInConfirmEmail = WebUtils.ParseBoolFromHashtable(settings, "SendFormContentInConfirmEmailSetting", false);
			
			if (settings.Contains("SendToEmailSetting"))
				SendToEmail = settings["SendToEmailSetting"].ToString();
			if (settings.Contains("EmailThankYouTextSetting"))
				EmailThankYouText = settings["EmailThankYouTextSetting"].ToString();

			if (settings.Contains("SubmitButtonTextSetting"))
				SubmitButtonText = settings["SubmitButtonTextSetting"].ToString();

			IsCollapsible = WebUtils.ParseBoolFromHashtable(settings, "IsCollapsibleSetting", false);

 
			ShowCountrySelect = WebUtils.ParseBoolFromHashtable(settings, "ShowCountrySelectSetting", false);


			MultiSelect1Show = WebUtils.ParseBoolFromHashtable(settings, "MultiSelect1ShowSetting", false);
			MultiSelect1AllowOther = WebUtils.ParseBoolFromHashtable(settings, "MultiSelect1AllowOtherSetting", false);

			if (settings.Contains("MultiSelect1TitleSetting"))
				MultiSelect1Title = settings["MultiSelect1TitleSetting"].ToString();
			if (settings.Contains("MultiSelect1OptionsSetting"))
				MultiSelect1Options = settings["MultiSelect1OptionsSetting"].ToString();
			if (settings.Contains("MultiSelect1OtherChoiceSetting"))
				MultiSelect1OtherChoice = settings["MultiSelect1OtherChoiceSetting"].ToString();
			if (settings.Contains("MultiSelect1OtherChoiceSelectionSetting"))
				MultiSelect1OtherChoicSelection = settings["MultiSelect1OtherChoiceSelectionSetting"].ToString();


			MultiSelect2Show = WebUtils.ParseBoolFromHashtable(settings, "MultiSelect2ShowSetting", false);
			MultiSelect2AllowOther = WebUtils.ParseBoolFromHashtable(settings, "MultiSelect2AllowOtherSetting", false);

			if (settings.Contains("MultiSelect2TitleSetting"))
				MultiSelect2Title = settings["MultiSelect2TitleSetting"].ToString();
			if (settings.Contains("MultiSelect2OptionsSetting"))
				MultiSelect2Options = settings["MultiSelect2OptionsSetting"].ToString();
			if (settings.Contains("MultiSelect2OtherChoiceSetting"))
				MultiSelect2OtherChoice = settings["MultiSelect2OtherChoiceSetting"].ToString();
			if (settings.Contains("MultiSelect2OtherChoiceSelectionSetting"))
				MultiSelect2OtherChoicSelection = settings["MultiSelect2OtherChoiceSelectionSetting"].ToString();


			MultiSelect3Show = WebUtils.ParseBoolFromHashtable(settings, "MultiSelect3ShowSetting", false);
			MultiSelect3AllowOther = WebUtils.ParseBoolFromHashtable(settings, "MultiSelect3AllowOtherSetting", false);

			if (settings.Contains("MultiSelect3TitleSetting"))
				MultiSelect3Title = settings["MultiSelect3TitleSetting"].ToString();
			if (settings.Contains("MultiSelect3OptionsSetting"))
				MultiSelect3Options = settings["MultiSelect3OptionsSetting"].ToString();
			if (settings.Contains("MultiSelect3OtherChoiceSetting"))
				MultiSelect3OtherChoice = settings["MultiSelect3OtherChoiceSetting"].ToString();
			if (settings.Contains("MultiSelect3OtherChoiceSelectionSetting"))
				MultiSelect3OtherChoicSelection = settings["MultiSelect3OtherChoiceSelectionSetting"].ToString();



			ShowMarketingOptin = WebUtils.ParseBoolFromHashtable(settings, "ShowMarketingOptinSetting", false);
			
			
			ShowRadioOptions = WebUtils.ParseBoolFromHashtable(settings, "ShowRadioOptionsSetting", false);

			if (settings.Contains("RadioOptionsCSVSetting"))
			{
				var options = settings["RadioOptionsCSVSetting"].ToString();
				RadioOptions = options.SplitOnCharAndTrim(',');
			}
			else
			{
				RadioOptions = new List<string>();
			}
		
			if (settings.Contains("RadioOptionTitleSetting"))
				RadioOptionTitle = settings["RadioOptionTitleSetting"].ToString();

		}

	}
}