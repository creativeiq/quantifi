﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using System.Text;
using System.IO;
using mojoPortal.Web.Editor;
using mojoPortal.Net;
using log4net;
using Quantifi.Business;
using Quantifi.UI.Utils;
using System.Globalization;
using System.Net.Mail;
using MojoUtils = mojoPortal.Web.Framework;
using System.Net.Mime;
using Quantifi.UI.ContactModule;
using mojoPortal.Business.WebHelpers;
using System.Xml;
using Quantifi.Business.Countries;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.ContactModule
{
  public partial class ContactModule : SiteModuleControl
  {
    #region Properties

    private static readonly ILog log = LogManager.GetLogger(typeof(ContactModule));
    private string EditUrl = "/Quantifi/Controls/ContactModule/Edit.aspx";

    private Contact contactContent = null;
    private ContactUsSettings contactUsSettings;
    private int MaxFileAttatchmentKBSize = 2;

    private bool isAdmin = false;

    private string AttatchmentFilePath;

    #endregion


    protected override void OnInit(EventArgs e)
    {

      base.OnInit(e);

      customVal_Attachment.ServerValidate += new ServerValidateEventHandler(customVal_Attachment_ServerValidate);
      btnSubmit.Click += new EventHandler(btnSubmit_Click);
    }


    protected void Page_Load(object sender, EventArgs e)
    {

      if ((WebUser.IsAdminOrContentAdmin) || (SiteUtils.UserIsSiteEditor())) { isAdmin = true; }

      contactUsSettings = new ContactUsSettings(Settings);

      AttatchmentFilePath = Server.MapPath(MojoUtils.WebUtils.GetApplicationRoot() + "/Data/Sites/"
          + siteSettings.SiteId.ToString(CultureInfo.InvariantCulture) + "/ContactUs/-" + this.ModuleId.ToString() + "/");

      Title1.EditText = "Edit";
      Title1.EditUrl = EditUrl;

      SetUpControls();

      SetupValidationControls();

      LoadHtmlContent();


      //AddJsUtils();
      SetupJS(false);

    }

    private void SetUpControls()
    {

      litModuleAnchor.Text = "<a id='module" + this.ModuleId.ToString() + "'></a>";

      if (IsPostBack)
        return;

      phCompany.Visible = contactUsSettings.ShowCompany;
      reqFieldVal_Company.Enabled = contactUsSettings.ShowCompany; ;

      phQuestions.Visible = contactUsSettings.ShowQuestions;
      phExtra.Visible = contactUsSettings.ShowExtraField;

      if (contactUsSettings.ShowExtraField)
      {
        reqFieldVal_Extra.Enabled = contactUsSettings.isExtraFieldRequired;
        lblExtra.Text = contactUsSettings.ExtraFieldLabel;
        reqFieldVal_Extra.Enabled = contactUsSettings.isExtraFieldRequired;
      }
      else
      {
        reqFieldVal_Extra.Enabled = false;
      }

      phAttachment.Visible = contactUsSettings.ShowFileUpload;
      customVal_Attachment.Enabled = contactUsSettings.ShowFileUpload;

      phJobTitle.Visible = contactUsSettings.ShowJobTitle;
      reqFieldVal_Title.Enabled = contactUsSettings.ShowJobTitle;

      if (!string.IsNullOrWhiteSpace(contactUsSettings.SubmitButtonText))
        btnSubmit.Text = contactUsSettings.SubmitButtonText;


      string userCountry = string.Empty;
      var formCookie = HttpContext.Current.Request.Cookies["qform"];
      if (formCookie != null)
      {
        if (formCookie["fname"] != null)
          this.txtFirstName.Text = formCookie["fname"];
        if (formCookie["lname"] != null)
          this.txtLastName.Text = formCookie["lname"];
        if (formCookie["jobtitle"] != null)
          this.txtTitle.Text = formCookie["jobtitle"];
        if (formCookie["company"] != null)
          this.txtCompany.Text = formCookie["company"];
        if (formCookie["email"] != null)
          this.txtEmail.Text = formCookie["email"];
        if (formCookie["phone"] != null)
          this.txtPhone.Text = formCookie["phone"];
        //if (formCookie["country"] != null)
        //  userCountry = formCookie["country"];
      }




      #region IsCollapsible

      if (contactUsSettings.IsCollapsible)
      {
        this.ModulePanel1.CssClass = "contactus pwCollapsible";
        this.linkTitle.Text = this.Title;
        this.linkOpenClose.ToolTip = "View " + this.Title;
        this.pnlCollapsibleContent.CssClass = "content hide";

        if (Request.IsAuthenticated && isAdmin)
        {
          var editLink = EditUrl + "?mid=" + this.ModuleId + "&pageid=" + this.PageId;

          this.linkEdit.Text = "Edit";
          this.linkEdit.NavigateUrl = editLink;
          this.linkEdit.Visible = true;

          var settingLink = "/Admin/ModuleSettings.aspx?mid=" + this.ModuleId + "&pageid=" + this.PageId;

          this.linkSettings.Text = "Settings";
          this.linkSettings.NavigateUrl = settingLink;
          this.linkSettings.Visible = true;

        }
        else
        {
          this.linkEdit.Visible = false;
          this.linkSettings.Visible = false;
        }
        this.Title1.Visible = false;
      }
      else
      {
        this.ModulePanel1.CssClass = "contactus panelwrapper";
        this.pnlColapsableHeader.Visible = false;
      }

      #endregion

      #region Choices

      if (contactUsSettings.ShowChoices)
      {

        lblChoices.Text = contactUsSettings.ChoicesLabel;

       var choiceData = contactUsSettings.ChoicesOptionsCSV.Split(new char[] { ',' },
          StringSplitOptions.RemoveEmptyEntries);

        ddlChoices.DataSource = choiceData;
         ddlChoices.DataBind();

        phChoices.Visible = true;
      }
      else
      {
        phChoices.Visible = false;
      }

      #endregion

      #region Country

      if (contactUsSettings.ShowCountrySelect)
      {

        bool trySelectUserCountry = false;
        bool.TryParse(ConfigurationManager.AppSettings["Quantifi_Form_AutoSelectCountry"], out trySelectUserCountry);
 
        var defaultItem = new ListItem("-- Please Select --", "");

        ddlCountry.Items.Add(defaultItem);

        var currentCountry = CountryUtils.ResolveCountry();
        var countries = Country.GetAll();

        foreach (var country in countries)
        {
          var item = new ListItem(country.Name, country.Id.ToString());

          if (trySelectUserCountry)
          {
            if (!string.IsNullOrWhiteSpace(userCountry) &&
                country.Id.ToString().Equals(userCountry, StringComparison.InvariantCulture))
            {
              item.Selected = true;
            }
            else if (currentCountry != null && currentCountry.TwoLetterISORegionName == country.IsoTwoCharCode)
            {
              item.Selected = true;
            }
          }

          ddlCountry.Items.Add(item);
        }

      }
      else
      {
        phCountry.Visible = false;
      }

      #endregion

      #region MultiSelect1

      if (contactUsSettings.MultiSelect1Show)
      {
        var items = contactUsSettings.MultiSelect1Options.FromDelimitedString().Select(s => s.Replace(" ", "&nbsp;"));

        if (items.IsNullOrEmpty())
        {
          this.phMultiSelect1.Visible = false;
        }
        else
        {
          this.checkboxMultiSelect1.DataSource = items;
          this.checkboxMultiSelect1.DataBind();

          this.checkboxMultiSelect1.RepeatDirection = RepeatDirection.Vertical;
          this.checkboxMultiSelect1.RepeatLayout = RepeatLayout.UnorderedList;
          this.checkboxMultiSelect1.RepeatColumns = 1;

          this.litMultiSelect1.Text = contactUsSettings.MultiSelect1Title;
          this.txtMultiSelect1Other.Visible = contactUsSettings.MultiSelect1AllowOther;
          this.lblMultiSelect1Other.Text = contactUsSettings.MultiSelect1OtherChoice;
          this.lblMultiSelect1Other.Visible = contactUsSettings.MultiSelect1AllowOther;
        }
      }
      else
      {
        this.phMultiSelect1.Visible = false;
      }


      #endregion

      #region MultiSelect2

      if (contactUsSettings.MultiSelect2Show)
      {
        var items = contactUsSettings.MultiSelect2Options.FromDelimitedString().Select(s => s.Replace(" ", "&nbsp;"));

        if (items.IsNullOrEmpty())
        {
          this.phMultiSelect2.Visible = false;
        }
        else
        {
          this.checkboxMultiSelect2.DataSource = items;
          this.checkboxMultiSelect2.DataBind();

          this.checkboxMultiSelect2.RepeatDirection = RepeatDirection.Vertical;
          this.checkboxMultiSelect2.RepeatLayout = RepeatLayout.UnorderedList;
          this.checkboxMultiSelect2.RepeatColumns = 1;

          this.litMultiSelect2.Text = contactUsSettings.MultiSelect2Title;
          this.txtMultiSelect2Other.Visible = contactUsSettings.MultiSelect2AllowOther;
          this.lblMultiSelect2Other.Text = contactUsSettings.MultiSelect2OtherChoice;
          this.lblMultiSelect2Other.Visible = contactUsSettings.MultiSelect2AllowOther;
        }
      }
      else
      {
        this.phMultiSelect2.Visible = false;
      }

      #endregion

      #region MultiSelect3

      if (contactUsSettings.MultiSelect3Show)
      {
        var items = contactUsSettings.MultiSelect3Options.FromDelimitedString().Select(s => s.Replace(" ", "&nbsp;"));

        if (items.IsNullOrEmpty())
        {
          this.phMultiSelect3.Visible = false;
        }
        else
        {
          this.checkboxMultiSelect3.DataSource = items;
          this.checkboxMultiSelect3.DataBind();



          this.checkboxMultiSelect3.RepeatDirection = RepeatDirection.Vertical;
          this.checkboxMultiSelect3.RepeatLayout = RepeatLayout.UnorderedList;
          this.checkboxMultiSelect3.RepeatColumns = 1;

          this.litMultiSelect3.Text = contactUsSettings.MultiSelect3Title;
          this.txtMultiSelect3Other.Visible = contactUsSettings.MultiSelect3AllowOther;
          this.lblMultiSelect3Other.Text = contactUsSettings.MultiSelect3OtherChoice;
          this.lblMultiSelect3Other.Visible = contactUsSettings.MultiSelect3AllowOther;
        }
      }
      else
      {
        this.phMultiSelect3.Visible = false;
      }
      #endregion



      phMarketingOptin.Visible = contactUsSettings.ShowMarketingOptin;


      phRadioOptions.Visible = contactUsSettings.ShowRadioOptions;

      if (contactUsSettings.ShowRadioOptions)
      {
        for (int i = 0; i < contactUsSettings.RadioOptions.Count(); i++)
        {
          var item = new ListItem()
          {
            Text = contactUsSettings.RadioOptions[i],
            Value = i.ToString()
          };

          if (i == 0)
            item.Selected = true;
          rblOptions.Items.Add(item); //= contactUsSettings.RadioOptions;
        }
      }

      lblRadioOptions.Text = contactUsSettings.RadioOptionTitle;
    }

    //private void AddJsUtils()
    //{
    //  ScriptManager Smgr = ScriptManager.GetCurrent(Page);
    //  if (Smgr == null) throw new Exception("ScriptManager not found.");
    //  ScriptReference SRef = new ScriptReference();
    //  SRef.Path = "~/ClientScript/quantifi.js";
    //  Smgr.Scripts.Add(SRef);
    //}

    private void LoadHtmlContent()
    {
      contactContent = Contact.Get(this.ModuleId);


      // not using viewstate, so need to set each time.
      if (contactContent == null)
        return;

      if (string.IsNullOrWhiteSpace(contactContent.TopContent))
        return;

      var literal = new LiteralControl(contactContent.TopContent);
      pnlTopContent.Controls.Add(literal);
      pnlTopContent.Visible = true;

    }

    private void SetupValidationControls()
    {


      var validationGrpName = this.ClientID + "_ValGrp";

      if (!string.IsNullOrWhiteSpace(contactUsSettings.ExtraFieldValidationErrorMsg))
        reqFieldVal_Extra.ErrorMessage = contactUsSettings.ExtraFieldValidationErrorMsg;

      regexEmail.ValidationGroup =
      reqFieldVal_Company.ValidationGroup =
      reqFieldVal_Email.ValidationGroup =
      reqFieldVal_Extra.ValidationGroup =
      reqFieldVal_FirstName.ValidationGroup =
      reqFieldVal_LastName.ValidationGroup =
      reqFieldVal_Phone.ValidationGroup =
      reqFieldVal_Title.ValidationGroup =
      reqFieldVal_Country.ValidationGroup =
      customVal_Attachment.ValidationGroup =
      valSummary.ValidationGroup =
      btnSubmit.ValidationGroup = validationGrpName;


      customVal_Attachment.ClientValidationFunction = customVal_Attachment.ClientID + "_Validate";

      btnSubmit.Enabled = true;
      btnSubmit.DisableButtonOnClick(string.Empty);
    }

    private void SendEmail()
    {

      var sendToEmailStr = contactUsSettings.SendToEmail;
      SmtpSettings smtpSettings = SiteUtils.GetSmtpSettings();


      if (!String.IsNullOrWhiteSpace(sendToEmailStr))
      {
        try
        {
          var attachments = new List<Attachment>();

          if (contactUsSettings.ShowFileUpload)
          {

            ContentType ct = new ContentType();
            ct.MediaType = MediaTypeNames.Application.Octet;
            ct.Name = fileUploadAttachment.PostedFile.FileName;
            var a = new Attachment(fileUploadAttachment.PostedFile.InputStream, ct);

            attachments.Add(a);
          }

          EmailUtils.Send(smtpSettings,//Settings
                        siteSettings.DefaultEmailFromAddress, //FROM
                        txtEmail.Text,// REPLY TO
                        sendToEmailStr, // TO
                        string.Empty, // CC
                        string.Empty, // BCC
                        BuildSubject(true),
                        BuildBody(true),
                        true,
                        "Normal",
                        attachments);

        }
        catch (Exception ex)
        {
          log.Error("error sending email from address was " + siteSettings.DefaultEmailFromAddress + " to address was " + sendToEmailStr, ex);
          throw new Exception("An error occured while sending email to Quantifi Staff", ex);
        }
      }


      if (contactUsSettings.SendConfirmEmail)
      {
        try
        {

          EmailUtils.Send(smtpSettings,//Settings
                        siteSettings.DefaultEmailFromAddress, //FROM
                        siteSettings.DefaultEmailFromAddress, // REPLY TO
                        txtEmail.Text, // TO
                        string.Empty, // CC
                        string.Empty, // BCC
                        BuildSubject(false),
                        BuildBody(false),
                        true,
                        "Normal",
                        new List<Attachment>());

        }
        catch (Exception ex)
        {
          log.Error("error sending email from address was " + siteSettings.DefaultEmailFromAddress + " to address was " + txtEmail.Text, ex);
          throw new Exception("An error occured while sending email to user's email [" + txtEmail.Text + "]", ex);
        }


      }


    }

    private string BuildBody(bool isInternalEmail)
    {
      return "<html><head ><title>"
        + Server.HtmlEncode(BuildSubject())
        + "</title>"
        + BuildStyles()
        + "<body>"
        + BuildThankYouContent(isInternalEmail)
        + BuildUserResponseTable(isInternalEmail)
        + "<br/><br/>"
        + BuildRequestInfoTable(isInternalEmail)
        + "</body></html>";


    }

    private string BuildStyles()
    {
      return @"<style type=""text/css"" >
	table.Responses {  text-align:left; }
	table.Responses th { text-align:left;  font-weight: bold; padding:2px; }
	table.Responses td { text-align:left;  font-weight: normal; padding:2px; }
	table.RequestInfo { text-align:left;  font-size: 12px;  text-align:left; }
	table.RequestInfo th { text-align:left; font-size: 12px; font-weight: bold; padding:2px; }
	table.RequestInfo td { text-align:left;  font-size: 12px; font-weight: normal; padding:2px; }
</style>";
    }

    private string BuildThankYouContent(bool isInternalEmail = true)
    {
      if (contactContent == null || isInternalEmail == true)
        return string.Empty;


      return GetThankYouContent() + "<br/><br/>";
    }

    private string BuildUserResponseTable(bool isInternalEmail = true)
    {
      if (isInternalEmail == false && !contactUsSettings.SendFormContentInConfirmEmail)
        return string.Empty;


      StringBuilder message = new StringBuilder();

      message.AppendLine("<table cellpadding='2' cellspacing='2'   class=\"Responses\"  >");
      message.AppendLine("<tr><th colspan='2' align='left' >Responses</th></tr>");
      message.AppendLine("<tr><th align='left' >First&nbsp;Name</th><td>" + this.txtFirstName.Text + "<td /></tr>");
      message.AppendLine("<tr><th align='left' >Last&nbsp;Name</th><td>" + this.txtLastName.Text + "<td /></tr>");


      message.AppendLine("<tr><th align='left' >Email</th><td>" + this.txtEmail.Text + "<td /></tr>");
      message.AppendLine("<tr><th align='left' >Phone</th><td>" + this.txtPhone.Text + "<td /></tr>");


      if (contactUsSettings.ShowJobTitle)
        message.AppendLine("<tr><th align='left' >Title</th><td>" + txtTitle.Text + "<td /></tr>");

      if (contactUsSettings.ShowCompany)
        message.AppendLine("<tr><th align='left' >" + lblCompany.Text + "</th><td>" + txtCompany.Text + "<td /></tr>");

      if (contactUsSettings.ShowCountrySelect)
        message.AppendLine("<tr><th align='left' >Country</th><td>" + ddlCountry.SelectedItem.Text + "<td /></tr>");


      if (contactUsSettings.ShowExtraField)
      {
        message.AppendLine("<tr><th colspan='2' align='left' >" + contactUsSettings.ExtraFieldLabel + "</th></tr>");
        message.AppendLine("<tr><td colspan='2' align='left' >" + this.txtExtra.Text + "<td /></tr>");
      }



      #region MultiSelect1
      if (contactUsSettings.MultiSelect1Show)
      {
        message.AppendLine("<tr><th colspan='2' align='left' >" + contactUsSettings.MultiSelect1Title + "</th><td>");

        message.AppendLine("<tr><td colspan='2' align='left' >");

        foreach (ListItem item in this.checkboxMultiSelect1.Items)
          if (item.Selected)
            message.AppendLine("<span style=\"padding:3px; \" >" + item.Text + "</span>,&nbsp;");

        if (contactUsSettings.MultiSelect1AllowOther && !string.IsNullOrWhiteSpace(this.txtMultiSelect1Other.Text))
          message.AppendLine("<span style=\"padding:3px; \" >" + this.txtMultiSelect1Other.Text + "</span>,&nbsp;");

        message.AppendLine("<td /></tr>");
      }
      #endregion

      #region MultiSelect2
      if (contactUsSettings.MultiSelect2Show)
      {
        message.AppendLine("<tr><th colspan='2' align='left' >" + contactUsSettings.MultiSelect2Title + "</th><td>");

        message.AppendLine("<tr><td colspan='2' align='left' >");

        foreach (ListItem item in this.checkboxMultiSelect2.Items)
          if (item.Selected)
            message.AppendLine("<span style=\"padding:3px; \" >" + item.Text + "</span>,&nbsp;");

        if (contactUsSettings.MultiSelect2AllowOther && !string.IsNullOrWhiteSpace(this.txtMultiSelect2Other.Text))
          message.AppendLine("<span style=\"padding:3px; \" >" + this.txtMultiSelect2Other.Text + "</span>,&nbsp;");

        message.AppendLine("<td /></tr>");
      }
      #endregion

      #region MultiSelect3
      if (contactUsSettings.MultiSelect3Show)
      {
        message.AppendLine("<tr><th colspan='2' align='left' >" + contactUsSettings.MultiSelect3Title + "</th><td>");

        message.AppendLine("<tr><td colspan='2' align='left' >");

        foreach (ListItem item in this.checkboxMultiSelect3.Items)
          if (item.Selected)
            message.AppendLine("<span style=\"padding:3px; \" >" + item.Text + "</span>,&nbsp;");

        if (contactUsSettings.MultiSelect3AllowOther && !string.IsNullOrWhiteSpace(this.txtMultiSelect3Other.Text))
          message.AppendLine("<span style=\"padding:3px; \" >" + this.txtMultiSelect3Other.Text + "</span>,&nbsp;");

        message.AppendLine("<td /></tr>");
      }
      #endregion


      if (contactUsSettings.ShowQuestions)
        message.AppendLine("<tr><th colspan='2' align='left' >Questions</td></tr><tr><td colspan='2'>" + txtQuestions.Text + "<th /></tr>");


      if (contactUsSettings.ShowFileUpload)
      {
        message.AppendLine("<tr><th colspan='2' align='left' >Attatchment</th><td>");
        message.AppendLine("<tr><td colspan='2' align='left' > FileName: " + this.fileUploadAttachment.PostedFile.FileName + "<td /></tr>");
      }


      if (contactUsSettings.ShowRadioOptions)
      {
        message.AppendFormat("<tr> <th align='left' >{0}</th><td   align='left' >{1}<td /></tr>", this.lblRadioOptions.Text, this.rblOptions.SelectedItem.Text);
        message.AppendLine("");

      }

      if (contactUsSettings.ShowMarketingOptin)
      {
        // opt in language it currently reverse of check. (unchecked = OptIn , checked = OptedOut )
        message.AppendLine("<tr> <th align='left' >Marketing Optin</th><td   align='left' > " + ((!this.checkboxOptIn.Checked) ? "false" : "true") + "<td /></tr>");

      }

      message.AppendLine("</table>");

      return message.ToString();
    }

    private string BuildRequestInfoTable(bool isInternalEmail = true)
    {
      if (!isInternalEmail)
        return string.Empty;


      StringBuilder message = new StringBuilder();

      message.AppendLine("<table cellpadding='2' cellspacing='2'  class=\"RequestInfo\" >");
      message.AppendLine("<tr><th colspan='2'  align='left' >Request Info</th></tr>");
      message.AppendLine("<tr><th align='left' >HTTP_USER_AGENT:</th><td align='left' >" + Page.Request.ServerVariables["HTTP_USER_AGENT"] + "<td /></tr>");
      message.AppendLine("<tr><th align='left' >REMOTE_HOST:</th><td align='left' >" + Page.Request.ServerVariables["REMOTE_HOST"] + "<td /></tr>");
      message.AppendLine("<tr><th align='left' >REMOTE_ADDR:</th><td align='left' >" + SiteUtils.GetIP4Address() + "<td /></tr>");
      message.AppendLine("<tr><th align='left' >LOCAL_ADDR:</th><td align='left' >" + Page.Request.ServerVariables["LOCAL_ADDR"] + "<td /></tr>");
      message.AppendLine("</table>");


      return message.ToString();
    }

    private string BuildSubject(bool isInternalEmail = true)
    {
      return this.Title + " - " + this.txtFirstName.Text + " " + this.txtLastName.Text;


    }


    private void SubmitUserResponse()
    {
      try
      {

        var qFormCookie = new HttpCookie("qform");

        qFormCookie["fname"] = this.txtFirstName.Text;
        qFormCookie["lname"] = this.txtLastName.Text;
        qFormCookie["jobtitle"] = this.txtTitle.Text;
        qFormCookie["company"] = this.txtCompany.Text;
        qFormCookie["email"] = this.txtEmail.Text;
        qFormCookie["phone"] = this.txtPhone.Text;


        qFormCookie["country"] = this.ddlCountry.SelectedValue;
        
        qFormCookie.Expires = DateTime.Now.AddDays(90);
        Response.Cookies.Add(qFormCookie);



        SendEmail();

        this.pnlContent.Visible = false;
        this.pnlSuccessMsg.Visible = true;

        if (contactContent != null && !string.IsNullOrWhiteSpace(GetThankYouContent()))
        {

          this.pnlSuccessMsg.Controls.Add(new LiteralControl(GetThankYouContent()));

        }


      }
      catch (Exception ex)
      {
        this.pnlContent.Visible = true;
        this.pnlSuccessMsg.Visible = false;
        this.pnlErrorMsg.Visible = true;
        this.litError.Text = ex.Message;
      }
    }

    private void SetupJS(bool isControlInFocus = false)
    {
      var strModuleId = this.ModuleId.ToString();
      var hashId = "module" + strModuleId;
      var addHashScript = string.Format(@"
			$(document).ready(function() {{
				window.location.hash = '#{0}';
			}});
				", hashId);


      ClientScriptManager cs = Page.ClientScript;

      if (isControlInFocus && !cs.IsStartupScriptRegistered(GetType(), "AddHash" + strModuleId))
        cs.RegisterStartupScript(GetType(), "AddHash" + strModuleId, addHashScript, true);



      var initClickEventsJS = string.Format(@"
			$('#{0}').ready(function () {{
				$(this).find('#{1}').click(function () {{
					$('#{2}').click();
				}});

				$(this).find('#{2}').click(function () {{
					$(this).toggleClass('collapse');
					$('#{3}').toggleClass('hide');

				}});
			}});
			", ModulePanel1.ClientID, linkTitle.ClientID, linkOpenClose.ClientID, pnlCollapsibleContent.ClientID);


      if (!cs.IsStartupScriptRegistered(GetType(), "InitClickEvents" + strModuleId))
        cs.RegisterStartupScript(GetType(), "InitClickEvents" + strModuleId, initClickEventsJS, true);

      var showFormJS = string.Format(@"
			$(document).ready(function() {{
					$('#{0}').click();
			}});
			", linkOpenClose.ClientID);

      if (isControlInFocus && !cs.IsStartupScriptRegistered(GetType(), "ShowForm" + strModuleId))
        cs.RegisterStartupScript(GetType(), "ShowForm" + strModuleId, showFormJS, true);
    }

    private string _ThankYouContent = null;
    private string GetThankYouContent()
    {
      if (string.IsNullOrWhiteSpace(_ThankYouContent))
      {
        _ThankYouContent = contactContent.ThankYouContent.Replace("{firstname}", this.txtFirstName.Text)
                                             .Replace("{lastname}", this.txtLastName.Text);




      }
      return _ThankYouContent;
    }

    #region EVENTS

    protected void customVal_Attachment_ServerValidate(object source, ServerValidateEventArgs args)
    {

      CustomValidator validator = (CustomValidator)source;


      if (!contactUsSettings.ShowFileUpload)
      {
        args.IsValid = true;
        return;
      }

      string UploadFileName = fileUploadAttachment.PostedFile.FileName;

      if (string.IsNullOrWhiteSpace(UploadFileName))
      {
        // There is no file selected 
        args.IsValid = false;
        //validator.ErrorMessage = "There is no file selected";
        return;
      }
      // 
      if (fileUploadAttachment.FileBytes.Length > MaxFileAttatchmentKBSize * 1024 * 1024)
      {
        args.IsValid = false;
        validator.ErrorMessage = "File is too large, please select a file under 2 MB";
        return;
      }


      args.IsValid = true;

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

      SetupJS(true);

      if (!Page.IsValid)
        return;

      SubmitUserResponse();

    }

    #endregion
  }
}