﻿<%@ Page Title="Edit Featured Item" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.FeaturedRotator.Edit" %>

<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>
<%@ Register TagPrefix="quantifi" TagName="FileSelectorControl" Src="~/Quantifi/Dialog/FileSelectorControl.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <style type="text/css">
    .hdrRow
    {
      border-bottom: 1px dotted #4F7997;
      margin: 12px 0;
      min-height: 28px;
      position: relative;
    }
    .hdrRowLable
    {
      font-size: 22px;
      color: #7AB800;
      font-weight: 400;
      padding-right: 30px;
    }
  </style>
  <div>
    <asp:Panel ID="pnlWrapper" runat="server">
      <div class="">
        <asp:HyperLink ID="linkManage" runat="server" CssClass="link-manage">Manage Featured Items</asp:HyperLink></div>
      <div class="hdrRow " style="">
        <asp:Label ID="lblTitle" runat="server" CssClass="hdrRowLable">EDIT - NEW</asp:Label>
        <asp:LinkButton ID="btnSave" runat="server" ValidationGroup="featured-val-grp" CausesValidation="True"
          EnableTheming="False" CssClass="link-save">Save</asp:LinkButton>
        <asp:LinkButton ID="btnDelete" runat="server" EnableTheming="False" CssClass="link-trash">Delete</asp:LinkButton>
      </div>
      <%--TITLE--%>
      <div class="settingrow">
        <label class="settinglabel">
          Title</label>
        <asp:TextBox ID="txtTitle" runat="server" CssClass="widetextbox forminput"></asp:TextBox>
        <quantifi:ExRequiredFieldValidator ControlToValidate="txtTitle" runat="server" ValidationGroup="featured-val-grp"
          Display="Dynamic" EnableClientScript="true">
            <span  style="padding: 5px 2px; vertical-align: bottom;"><img src="/Data/SiteImages/warning.png"  alt="* Required" title="Required"  /></span>
        </quantifi:ExRequiredFieldValidator>
      </div>
      <%--Title Url--%>
      <div class="settingrow">
        <label class="settinglabel">
          Title Url</label>
        <asp:TextBox ID="txtTitleLinkUrl" runat="server" CssClass="widetextbox forminput"></asp:TextBox>    
                <asp:DropDownList ID="ddlTitleLinkTarget" runat="server" CssClass="forminput">
          <asp:ListItem   Text="Current Window" Value="0"></asp:ListItem>
          <asp:ListItem Text="New Window" Value="1"></asp:ListItem>
        </asp:DropDownList>
      </div>
      <%--Show Title--%>
      <div class="settingrow">
        <label class="settinglabel">
          Show Title</label>
        <asp:CheckBox ID="checkboxShowTitle" runat="server" Checked="True" CssClass="forminput" />
      </div>
      <%--Render Mode--%>
      <div class="settingrow">
        <label class="settinglabel">
          Render Mode</label>
        <asp:DropDownList ID="ddlRenderMode" runat="server" CssClass="forminput">
          <asp:ListItem Text="Text Left - Image Right" Value="0"></asp:ListItem>
          <asp:ListItem Text="Text Right - Image Left" Value="1"></asp:ListItem>
          <asp:ListItem Text="Text Only" Value="2"></asp:ListItem>
          <asp:ListItem Text="Image Only" Value="3"></asp:ListItem>
        </asp:DropDownList>
      </div>
      <%--Link--%>
      <div class="settingrow imageMode">
        <label class="settinglabel">
          Link</label>
        <asp:TextBox ID="txtLinkUrl" runat="server" CssClass="widetextbox forminput"></asp:TextBox>
        <asp:DropDownList ID="ddlLinkTarget" runat="server" CssClass="forminput">
          <asp:ListItem   Text="Current Window" Value="0"></asp:ListItem>
          <asp:ListItem Text="New Window" Value="1"></asp:ListItem>
        </asp:DropDownList>
      </div>
      <%--Image--%>
      <div class="settingrow imageMode">
        <label class="settinglabel">
          Image
        </label>
        <quantifi:FileSelectorControl ID="imgSelector" runat="server" FileType="image" isRequired="False"
          Display="Dynamic">
        </quantifi:FileSelectorControl>
      </div>
      <%--Pub Date--%>
      <div class=" settingRow">
        <label class="settinglabel">
          Pub Date</label>
        <mp:DatePickerControl ID="dpPubDate" runat="server" ShowTime="True" CssClass="forminput">
        </mp:DatePickerControl>
      </div>
      <%--IsActive--%>
      <div class="settingrow">
        <label class="settinglabel">
          IsActive</label>
        <asp:CheckBox ID="checkboxIsActive" runat="server" Checked="True" CssClass="forminput" />
      </div>
      <%--Description--%>
      <div class="settingrow textMode">
        <label class="settinglabel">
          Description
        </label>
        <div style="height: 300px; width: 550px;">
          <mpe:EditorControl ID="edContent" runat="server">
          </mpe:EditorControl>
        </div>
      </div>
      <div class="settingrow">
      </div>
    </asp:Panel>
  </div>
  <script type="text/javascript">

    function updateEditModeVisibility(ddlValue) {
      switch (ddlValue) {
        case "0":
          $(".imageMode").show();
          $(".textMode").show();
          break;
        case "1":
          $(".imageMode").show();
          $(".textMode").show();
          break;
        case "2":
          $(".imageMode").hide();
          $(".textMode").show();
          break;
        case "3":
          $(".imageMode").show();
          $(".textMode").hide();
          break;
        default:
          $(".imageMode").show();
          $(".textMode").show();
      }

    }

    $(document).ready(function () {
      var dllRenderMode = $('#<%= ddlRenderMode.ClientID %>');

      updateEditModeVisibility(dllRenderMode.val());
      dllRenderMode.change(function () {
        updateEditModeVisibility($(this).val());
      });


    });

  </script>
</asp:Content>
