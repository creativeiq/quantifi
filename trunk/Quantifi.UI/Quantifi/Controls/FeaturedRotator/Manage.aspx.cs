﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Data.EF;
using Quantifi.Shared;
using log4net;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Controls.FeaturedRotator
{
  public partial class Manage : NonCmsBasePage
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(Manage));
    #region Properties

    protected int moduleId = -1;
    protected int itemId = -1;
    protected int pageId = -1;

    private int pageNum = 1;
    private int pageSize = 20;
    private int totalPages = 0;

    protected string virtualRoot;
    protected Double timeOffset = 0;
    private TimeZoneInfo timeZone = null;


    #endregion

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      LoadParams();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (moduleId < 1)
      {
        
        SiteUtils.RedirectToDefault();
      }

      if (!UserCanEditModule(moduleId))
      {
        SiteUtils.RedirectToAccessDeniedPage(this);
      }

      if (!IsPostBack)
        PopulateControls();
    }

    private void PopulateControls()
    {
      if (moduleId < 1)
      {
        this.pnlWrapper.ShowErrorMessageBanner("No ModuleId Passed");
        return;
      }

      var settings = CacheHelper.GetPage(pageId);

      if (settings != null)
      {
        this.linkBack.NavigateUrl = settings.Url;
      }
      else
      {
        this.linkBack.Visible = false;
      }

      this.linkCreate.NavigateUrl = string.Format("~/Quantifi/Controls/FeaturedRotator/Edit.aspx?mid={0}&pageid={1}",
                                                  moduleId, pageId);

      LoadData();
    }

    private void LoadData()
    {


      using (var dc = new CorpWebEntities())
      {

        var allItems = dc.FeaturedItems.Where(f => f.ModuleId == moduleId)
                                        .OrderByDescending(f => f.PubDate);

        totalPages = allItems.GetTotalPages(pageSize);




        this.rptItems.DataSource = allItems.Page(pageNum, pageSize);
        this.rptItems.DataBind();



        pgr.PageURLFormat = string.Format("/Quantifi/Controls/FeaturedRotator/Manage.aspx?mid={0}&pageid={1}&pg={{0}}",
                                          moduleId, pageId);
        pgr.ShowFirstLast = true;
        pgr.PageSize = pageSize;
        pgr.PageCount = totalPages;
        pgr.CurrentIndex = pageNum;
        pgr.Visible = (totalPages > 1);


      }
    }

    private void LoadParams()
    {
      timeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();

      pageId = WebUtils.ParseInt32FromQueryString("pageid", -1);
      moduleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      itemId = WebUtils.ParseInt32FromQueryString("id", -1);


      virtualRoot = WebUtils.GetApplicationRoot();


    }
  }
}