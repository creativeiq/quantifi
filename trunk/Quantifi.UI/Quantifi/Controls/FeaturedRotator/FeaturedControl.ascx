﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedControl.ascx.cs"
  Inherits="Quantifi.UI.Controls.FeaturedRotator.FeaturedControl" %>
<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>
 
 
<asp:Panel ID="pnlWrapper" runat="server">
  <div class="hp-featured">
    <div class="hp-featured-wrapper">
      <div class="mask">
      </div>
      <div class="content">
        <h2 class="header">
          <span>FEATURED</span>&nbsp; <span title='PREV' class='prev mediabtn'>&lt;</span>&nbsp;
          <span title='Pause' class='pause mediabtn'>||</span>&nbsp; <span title='NEXT' class='next mediabtn'>
            &gt;</span>&nbsp;
          <asp:HyperLink ID="linkSettings" ToolTip="Edit Settings" runat="server" CssClass="link-settings-img float-right "
            EnableViewState="False">Settings</asp:HyperLink>
          <asp:HyperLink ID="linkManage" ToolTip="Manage News Feed Items" runat="server" CssClass="link-manage-img float-right"
            EnableViewState="False">Manage</asp:HyperLink>
          <asp:HyperLink ID="linkCreate" ToolTip="Creat News Feed Item" runat="server" CssClass="link-create-img float-right"
            EnableViewState="False">Create New Item</asp:HyperLink>
        </h2>
        <quantifi:ExRepeater EnableViewState="False" ID="rptFeaturedItems" runat="server">
          <EmptyTemplate>
            <div>
              <span class="hide">No Content</span>
            </div>
          </EmptyTemplate>
          <HeaderTemplate>
            <div class="feature-content">
          </HeaderTemplate>
          <ItemTemplate>
            <div class="feature-item <%#  GetRenderModeCss((int)Eval("RenderMode"),(bool)Eval("ShowTitle")) %>  "  itemid='<%# Eval("Id") %>'>
              <asp:HyperLink runat="server" NavigateUrl='<%# GetEditLink(  Eval("Id"),Eval("ModuleId"))%>'
                ToolTip="Edit" CssClass="link-edit-img" EnableViewState="False" Visible="<%# IsEditable %>">EDIT</asp:HyperLink>
              <div class="feature-txt">
                <h3 runat="server" visible='<%# (bool)Eval("ShowTitle") %>' class="title">
                  <a href="<%# GetLinkUrl( Eval("TitleLinkUrl").ToString()) %>" 
                  title='<%# GetTitleHtml(Eval("Title").ToString(), false)%>' target='<%# GetLinkTarget((int)Eval("TitleLinkTarget") )%>' class="track-link">
                    <%# GetTitleHtml(Eval("Title").ToString(), true)%></a>
                </h3>
                <%#  ((int)Eval("RenderMode") != 3 )? Eval("Description") : string.Empty %>
              </div>
              <div class="feature-img">
                <a style="background-image: url('<%# GetImageUrl( Eval("ImgSrcUrl").ToString())%>');"  title='<%# GetTitleHtml(Eval("Title").ToString(), false)%>'   class="track-link"
                  href='<%# GetLinkUrl(Eval("ImgLinkUrl").ToString()) %>' target='<%# GetLinkTarget((int)Eval("ImgLinkTarget") )%>'>
                </a>
              </div>
            </div>
          </ItemTemplate>
          <FooterTemplate>
            </div>
          </FooterTemplate>
        </quantifi:ExRepeater>
      </div>
    </div>            

  </div>
</asp:Panel>
