﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using mojoPortal.Web;
using mojoPortal.Web.Editor;
using mojoPortal.Web.Framework;
using Quantifi.Data.EF;
using Quantifi.Shared;
using System.Data.Entity;


namespace Quantifi.UI.Controls.FeaturedRotator
{
  public partial class Edit : NonCmsBasePage
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(Edit));

    #region Properties

    protected int moduleId = -1;
    protected int itemId = -1;
    protected int pageId = -1;

    protected string virtualRoot;
    protected Double timeOffset = 0;
    private TimeZoneInfo timeZone = null;

    #endregion

    protected override void OnPreInit(EventArgs e)
    {

      base.OnPreInit(e);
      SiteUtils.SetupEditor(edContent);

    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      this.btnSave.Click += new EventHandler(btnSave_Click);
      this.btnDelete.Click += new EventHandler(btnDelete_Click);

      LoadParams();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
      edContent.WebEditor.ToolBar = ToolBar.Full;

      if (IsPostBack)
        return;

      PopulateControls();
    }


    private void LoadParams()
    {
      timeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();

      pageId = WebUtils.ParseInt32FromQueryString("pageid", -1);
      moduleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      itemId = WebUtils.ParseInt32FromQueryString("id", -1);


      virtualRoot = WebUtils.GetApplicationRoot();


    }

    private void PopulateControls()
    {
      this.linkManage.NavigateUrl = string.Format("~/Quantifi/Controls/FeaturedRotator/manage.aspx?mid={0}&pageid={1}",
                                                  moduleId, pageId);


      if (itemId < 0)
      {
        this.lblTitle.Text = "Create Featured Item";

        if (timeZone != null)
        {
          dpPubDate.Text = DateTime.UtcNow.ToLocalTime(timeZone).ToString();
        }
        else
        {
          dpPubDate.Text = DateTime.UtcNow.AddHours(timeOffset).ToString();
        }


        this.btnDelete.Visible = false;
        return;
      }

      using (var dc = new CorpWebEntities())
      {

        var item = dc.FeaturedItems.SingleOrDefault(f => f.Id == itemId && f.ModuleId == moduleId);
        if (item == null)
        {
          this.lblTitle.Text = "Create Featured Item";

          if (timeZone != null)
          {
            dpPubDate.Text = DateTime.UtcNow.ToLocalTime(timeZone).ToString();
          }
          else
          {
            dpPubDate.Text = DateTime.UtcNow.AddHours(timeOffset).ToString();
          }


          this.btnDelete.Visible = false;
          this.pnlWrapper.ShowErrorMessageBanner(string.Format("Unable to find Featured Item with Id [{0}] for Module [{1}]", itemId, moduleId));
          return;
        }

        this.ddlRenderMode.SetActiveListItem(item.RenderMode.ToInvariantString());

        this.txtTitle.Text = item.Title;

        this.txtTitleLinkUrl.Text = item.TitleLinkUrl;
        this.ddlTitleLinkTarget.SetActiveListItem(item.TitleLinkTarget.ToInvariantString());
        this.checkboxShowTitle.Checked = item.ShowTitle;

        this.txtLinkUrl.Text = item.ImgLinkUrl;
        this.imgSelector.Value = item.ImgSrcUrl;
        this.txtTitle.Text = item.Title;
        this.edContent.Text = item.Description;
        this.checkboxIsActive.Checked = item.IsActive;




        if (timeZone != null)
        {
          dpPubDate.Text = item.PubDate.ToLocalTime(timeZone).ToString();
        }
        else
        {
          dpPubDate.Text = DateTimeHelper.LocalizeToCalendar(item.PubDate.AddHours(timeOffset).ToString());
        }


        this.ddlLinkTarget.SetActiveListItem(item.ImgLinkTarget.ToInvariantString());
        //this.ddlLinkTarget.SelectedIndex =
        //  this.ddlLinkTarget.Items.IndexOf(ddlLinkTarget.Items.FindByValue(item.ImgLinkTarget.ToInvariantString()));

        this.lblTitle.Text = "Edit Featured Item";

      }


    }



    private bool ParamsAreValid()
    {
      try
      {
        var localTime = DateTime.Parse(dpPubDate.Text);
      }
      catch (FormatException)
      {

        return false;
      }
      catch (ArgumentNullException)
      {

        return false;
      }
      return true;
    }


    private void Save()
    {



      using (var dc = new CorpWebEntities())
      {
        FeaturedItems item;
        bool isNewObject = false;
        if (itemId < 1)
        {
          if (moduleId < 1)
          {
            pnlWrapper.ShowErrorMessageBanner("No Module Id Passed... Unable to Save");
            return;
          }

          item = dc.FeaturedItems.CreateObject();
          item.ModuleId = moduleId;
          isNewObject = true;

          dc.FeaturedItems.AddObject(item);
        }
        else
        {
          item = dc.FeaturedItems.SingleOrDefault(f => f.Id == itemId && f.ModuleId == moduleId);
          if (item == null)
          {
            this.pnlWrapper.ShowErrorMessageBanner(string.Format("Error while saving: Unable to find FeaturedItem[ID:{0}]",
                                                                 itemId));

            return;
          }
        }

        item.Title = this.txtTitle.Text;
        item.TitleLinkUrl  = this.txtTitleLinkUrl.Text;
        item.TitleLinkTarget = Int32.Parse(this.ddlTitleLinkTarget.SelectedValue);
        item.ShowTitle = checkboxShowTitle.Checked;
        item.ImgLinkUrl = this.txtLinkUrl.Text;
        item.ImgSrcUrl = this.imgSelector.Value;

        item.RenderMode = Int32.Parse(this.ddlRenderMode.SelectedValue);

        item.Description = this.edContent.Text;
        item.IsActive = this.checkboxIsActive.Checked;

        DateTime localTime = DateTime.Parse(dpPubDate.Text);
        if (timeZone != null)
        {
          item.PubDate = localTime.ToUtc(timeZone);
        }
        else
        {
          item.PubDate = localTime.AddHours(-timeOffset);
        }


        item.ImgLinkTarget = Int32.Parse(this.ddlLinkTarget.SelectedValue);



        var affecteditems = dc.SaveChanges();


        if (affecteditems > 0)
        {
          pnlWrapper.ShowSuccessMessageBanner("Featured Item Saved");

          if (isNewObject)
          {
            SimpleSessionMessageUtil.AddSuccessMessage("Save Successful, Featured Item Created");


            Response.Redirect(string.Format("~/Quantifi/Controls/FeaturedRotator/Edit.aspx?id={0}&mid={1}&pageid={2}",
                                            item.Id, moduleId, pageId));

          }
          else
          {
            this.pnlWrapper.ShowSuccessMessageBanner("Save Successful, Featured Item Updated");
          }

        }
      }
    }

    private void Delete()
    {
      using (var dc = new CorpWebEntities())
      {
        try
        {
          var item = dc.FeaturedItems.SingleOrDefault(f => f.Id == itemId && f.ModuleId == moduleId);
          if (item == null)
          {
            this.pnlWrapper.ShowErrorMessageBanner(string.Format("Error while deleting: Unable to find Featured Item[ID:{0}]",
                                                                 itemId));

            return;
          }

          dc.FeaturedItems.DeleteObject(item);

          var rowsAffected = dc.SaveChanges();

          if (rowsAffected > 0)
          {
            SimpleSessionMessageUtil.AddSuccessMessage("Delete Successful, Featured Item removed");

            Response.Redirect(string.Format("~/Quantifi/Controls/FeaturedRotator/manage.aspx?mid={0}&pageid={1}",
                                            moduleId, pageId));

          }
          else
          {

          }

        }
        catch (Exception ex)
        {
          this.pnlWrapper.ShowErrorMessageBanner(string.Format("Error while deleting: {0}", ex.Message));


        }



      }
    }


    #region Button Click


    private void btnDelete_Click(object sender, EventArgs e)
    {
      Delete();
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      if (IsValid && ParamsAreValid())
      {
        Save();
      }
    }


    #endregion


  }
}