﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Data.EF;
using log4net;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Controls.FeaturedRotator
{
  public partial class FeaturedControl : SiteModuleControl
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(FeaturedControl));


    protected void Page_Load(object sender, EventArgs e)
    {
      PopulateControls();

      LoadData();
    }


    private void PopulateControls()
    {
      var queryParams = "?mid=" + this.ModuleId + "&pageid=" + this.PageId;
      this.linkManage.NavigateUrl = SiteUtils.GetNavigationSiteRoot() + "/Quantifi/Controls/FeaturedRotator/Manage.aspx" + queryParams;
      this.linkSettings.NavigateUrl = SiteUtils.GetNavigationSiteRoot() + "/Admin/ModuleSettings.aspx" + queryParams;
      this.linkCreate.NavigateUrl = SiteUtils.GetNavigationSiteRoot() + "/Quantifi/Controls/FeaturedRotator/edit.aspx" + queryParams;

      this.linkManage.Visible = this.IsEditable;
      this.linkCreate.Visible = this.IsEditable;
      this.linkSettings.Visible = this.IsEditable;


    }


    private void LoadData()
    {
      using (var dc = new CorpWebEntities())
      {

        var items = Queries.GetTopActiveFeaturedItemsByPubdate(dc, this.ModuleId, 5, DateTime.UtcNow);

        this.rptFeaturedItems.DataSource = items;

        this.rptFeaturedItems.DataBind();

      }

    }


    protected string GetEditLink(object id, object moduleId)
    {
      return string.Format("{0}/Quantifi/Controls/FeaturedRotator/Edit.aspx?id={1}&mid={2}",
                           SiteRoot,
                           id,
                           moduleId);

    }


    protected string GetTitleHtml(string title, bool addBreaks)
    {
      if (addBreaks)
      {
        return title.Replace(@"\r", @"<br/>");

      }
      else
      {
        return Server.HtmlEncode(title.Replace(@"\r", " ").Replace("  ", " "));
      }

    }

    protected string GetLinkUrl(string url)
    {

      if (url.StartsWith("~/"))
        return SiteRoot + url.Replace("~/", "/");


      if (url.StartsWith("/"))
        return SiteRoot + url;

      return url;
    }

    protected string GetImageUrl(string url)
    {

      if (url.StartsWith("~/"))
        return SiteRoot + url.Replace("~/", "/");

      if (url.StartsWith("/"))
        return SiteRoot + url;


      return url;
    }

    protected string GetLinkTarget(int linkTarget)
    {
      return ((linkTarget) > 0) ? "_blank" : "_self";
    }

    protected string GetRenderModeCss(int mode, bool showTitle)
    {
      var titelcss = (showTitle) ? "st-" : "nt-";
      /*
      0 Text Left - Image Right 
      1 Text Right - Image Left
      2 Text Only
      3 Image Only
      */
      switch (mode)
      {
        case 1:
          return titelcss + "tr-il";

        case 2:
          return titelcss + "txt-only";

        case 3:
          return titelcss + "img-only";

        default:
          return titelcss + "tl-ir";

      }
    }

  }
}