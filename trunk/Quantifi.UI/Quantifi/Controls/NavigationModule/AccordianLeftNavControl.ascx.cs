﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using mojoPortal.Business;
using mojoPortal.Web.UI;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;
using log4net;




namespace Quantifi.UI.Controls.NavigationModule
{


  public partial class AccordianLeftNavControl : SiteModuleControl
  {

    #region Properties

    private static readonly ILog log = LogManager.GetLogger(typeof(AccordianLeftNavControl));
    private string PageIDList = string.Empty;
    private bool isAdmin = false;
    private bool isContentAdmin = false;
    new private SiteSettings siteSettings = null;
    private int activeIndex = -1;
    protected int ActiveIndex { get { return activeIndex; } }
    new private mojoBasePage currentPage = null;
    private PageSettings currentPageSettings = null;

    private bool showLeftColumn = true;


    List<int> PageIds = null;
    #endregion



 
    protected void Page_Load(object sender, EventArgs e)
    {
      isAdmin = WebUser.IsAdmin;
      isContentAdmin = WebUser.IsContentAdmin;

      currentPageSettings = CacheHelper.GetCurrentPage();

      currentPage = this.Page as mojoBasePage;
      if (currentPage != null)
        currentPage.SuppressPageMenu();

      //showLeftColumn = WebUtils.ParseBoolFromHashtable(Settings, "AccordianLeftNavLeftColumnVisibleSetting", showLeftColumn);



      if (Settings.Contains("AccordianLeftNavPageIdSetting"))
      {
        var value = Settings["AccordianLeftNavPageIdSetting"].ToString().Trim();
        if (string.IsNullOrWhiteSpace(value))
        {
          this.pnlWrapper.Visible = false;
          return;
        }
        else
        {
          PageIDList = value;
        }
      }

      this.linkEdit.Visible = isAdmin;
      if (isAdmin)
      {
        this.linkEdit.NavigateUrl = SiteRoot + "/Admin/ModuleSettings.aspx?mid=" + ModuleId.ToInvariantString() + "&pageid=" + PageId.ToInvariantString();
      }

      siteSettings = CacheHelper.GetCurrentSiteSettings();
      if (siteSettings == null) return;


      var idArray = PageIDList.Split(new char[] { ',' });
      if (idArray.Length == 0)
        return;


      try
      {
        PageIds = Array.ConvertAll(idArray, s => int.Parse(s)).ToList();
      }
      catch (Exception ex)
      {
        log.Error("Error while parsing ints for page ids", ex);
        return;
      }

      var pages = PageIds.Select(id => new PageSettings(siteSettings.SiteId, id));

      if (!pages.Any())
      {
        this.pnlWrapper.Visible = false;
        return;
      }

      var nodeStatuses = new List<NodeInfo>();
      int count = 0;
      foreach (var settings in pages)
      {

        nodeStatuses.Add(new NodeInfo() { Page = settings, Index = count, DepthToSelectedPage = GetSelectedDepth(currentPageSettings, settings, 0) });


        AddPageLinks(settings, count);
        count++;
      }


      var selectedNodes = nodeStatuses.OrderByDescending(n => n.DepthToSelectedPage).Where(n => n.DepthToSelectedPage > -1).ToList();
      if (selectedNodes.Any())
      {
        activeIndex = selectedNodes.Last().Index;
      }
    }

 

    private void AddPageLinks(PageSettings settings, int currentIndex)
    {
 

       var lit = new LiteralControl("<h3> <a class='acclink' href=\"" + SiteRoot + settings.Url.Replace("~/", "/") + "\">" + settings.PageName + "</a></h3>");

      this.pnlContainer.Controls.Add(lit);


      var subPanel = new Panel() { CssClass = "subNav" };

      this.pnlContainer.Controls.Add(subPanel);


      var smDataSource = new SiteMapDataSource();

      smDataSource.SiteMapProvider = "mojosite" + siteSettings.SiteId.ToInvariantString();



      var treeMenu1 = new mojoTreeView();
      treeMenu1.EnableClientScript = true;
      subPanel.Controls.Add(treeMenu1);


      treeMenu1.ShowExpandCollapse = true;
      treeMenu1.EnableViewState = false;

      treeMenu1.TreeNodeDataBound += new TreeNodeEventHandler(treeMenu1_TreeNodeDataBound);


      smDataSource.StartingNodeUrl = settings.Url;


      var pageNode = SiteUtils.GetSiteMapNodeForPage(smDataSource.Provider.RootNode, settings);

      var startingNodeOffset = pageNode.Depth;
      treeMenu1.ExpandDepth = pageNode.Depth;
      //treeMenu1.HoverNodeStyle.CssClass = "AspNet-TreeView-Hover";


      smDataSource.ShowStartingNode = false;
      treeMenu1.PathSeparator = '|';
      treeMenu1.DataSource = smDataSource;
      try
      {
        treeMenu1.DataBind();
      }
      catch (ArgumentException ex)
      {
        log.Error(ex);
      }




      if (treeMenu1.SelectedNode != null)
      {
        mojoTreeView.ExpandToValuePath(treeMenu1, treeMenu1.SelectedNode.ValuePath);
      }
      else
      {

        var valuePath = string.Empty;

        valuePath = SiteUtils.GetPageMenuActivePageValuePath(smDataSource.Provider.RootNode);

        mojoTreeView.ExpandToValuePath(treeMenu1, valuePath);

        var nodeToSelect = treeMenu1.FindNode(valuePath) ?? treeMenu1.FindNode(currentPageSettings.PageName);

        if (nodeToSelect != null)
        {
          try
          {
            nodeToSelect.Selected = true;
          }
          catch (InvalidOperationException)
          {
            //can happen if node disabled or un-selectable
          }
        }



      }

      if (treeMenu1.Nodes.Count == 0) treeMenu1.Visible = false;

    }



    protected void treeMenu1_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
    {
      if (sender == null) { return; }
      if (e == null) { return; }


      var menu = (TreeView)sender;
      var mapNode = (mojoSiteMapNode)e.Node.DataItem;
      if (mapNode.MenuImage.Length > 0)
      {
        e.Node.ImageUrl = mapNode.MenuImage;
      }



      if (mapNode.OpenInNewWindow)
      {
        e.Node.Target = "_blank";
      }

      if (mapNode.Url == currentPageSettings.Url)
      {
        e.Node.Selected = true;
      }
      // added this 2007-09-07
      // to solve treeview expand issue when page name is the same
      // as Page Name was used for value if not set explicitly
      e.Node.Value = mapNode.PageGuid.ToString();

      //log.Info("databound tree node with value path " + e.Node.ValuePath);

      bool remove = false;

      if (!((isAdmin)
          || ((isContentAdmin) && (mapNode.Roles != null) && (!(mapNode.Roles.Count == 1) && (mapNode.Roles[0].ToString() == "Admins")))
          || ((isContentAdmin) && (mapNode.Roles == null))
          || ((mapNode.Roles != null) && (WebUser.IsInRoles(mapNode.Roles)))))
      {
        remove = true;
      }

      if (PageIds.Contains(mapNode.PageId))
        remove = true;

      if (!mapNode.IncludeInMenu) remove = true;
      if (mapNode.IsPending && !WebUser.IsAdminOrContentAdminOrContentPublisherOrContentAuthor) remove = true;
      if ((mapNode.HideAfterLogin) && (Request.IsAuthenticated)) remove = true;

      if (remove)
      {
        if (e.Node.Depth == 0)
        {
          menu.Nodes.Remove(e.Node);
        }
        else
        {
          var parent = e.Node.Parent;
          if (parent != null)
          {
            parent.ChildNodes.Remove(e.Node);
          }
        }
      }
      else
      {
        e.Node.PopulateOnDemand = false;
      }

    }


    private bool SetSelectedIndex(PageSettings current, PageSettings parent)
    {
      // if selection is already set... continue
      if (this.activeIndex >= 0)
        return false;

      if (current.PageId == parent.PageId)
        return true;


      if (current.ParentId > 0)
      {
        var parentSettings = new PageSettings(siteSettings.SiteId, current.ParentId);
        return SetSelectedIndex(parentSettings, parent);
      }
      return false;
    }


    private int GetSelectedDepth(PageSettings current, PageSettings parent, int depth)
    {


      if (current.PageId == parent.PageId)
        return depth;


      if (current.ParentId > 0)
      {
        var parentSettings = new PageSettings(siteSettings.SiteId, current.ParentId);
        return GetSelectedDepth(parentSettings, parent, depth + 1);
      }


      return -1;
    }


  }



  public struct NodeInfo
  {
    public PageSettings Page { get; set; }
    public int DepthToSelectedPage { get; set; }
    public int Index { get; set; }
  }
}