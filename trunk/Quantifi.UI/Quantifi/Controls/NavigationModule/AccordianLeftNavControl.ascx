﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccordianLeftNavControl.ascx.cs"   ViewStateMode="Disabled"
	Inherits="Quantifi.UI.Controls.NavigationModule.AccordianLeftNavControl" %>

<asp:Panel ID="pnlWrapper" runat="server" CssClass="" >
<div class="AccordianLeftNavPanel" style="">
	<asp:HyperLink ID="linkEdit" runat="server" CssClass="ModuleEditLink">Edit</asp:HyperLink>
	<asp:Panel ID="pnlContainer" runat="server" CssClass="AccordianLeftNavControl QLightTheme"
		EnableViewState="false">
	</asp:Panel>
</div>
<script type="text/javascript">
	$(".AccordianLeftNavControl").accordion({ active: <%= this.ActiveIndex.ToInvariantString() %>, collapsible: true, autoHeight: false  }); 


	$(".AccordianLeftNavPanel").find(".AspNet-TreeView-Leaf a").hover(
		function(){ $(this).addClass("Menu-Hover");},
		function(){ $(this).removeClass("Menu-Hover");}
		);

	$(".AccordianLeftNavControl").find(".acclink").click(function(){
			window.location.href = this.href; return false;
		 });

</script>
</asp:Panel>