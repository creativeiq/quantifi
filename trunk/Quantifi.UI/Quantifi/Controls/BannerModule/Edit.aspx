﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.BannerModule.Edit" %>

<%@ Register Src="../../Dialog/FileSelectorControl.ascx" TagName="FileSelectorControl"
	TagPrefix="uc1" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="leftContent" runat="server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
<br />
	<asp:PlaceHolder ID="phParentBanner" runat="server">
		<div class="bannerEdit">
			<fieldset>
				<legend>Parent Banner</legend>
				<div class="parentImage">
					<asp:Image ID="imgParentBanner" runat="server" />
				</div>
			</fieldset>

 			<asp:Panel ID="pnlParentError" runat="server" CssClass="error" Visible="false" >
				<asp:Label ID="lblParentError" runat="server" >Sorry there was an error saving</asp:Label>
				<p id="msgParentError" runat="server"></p>
			</asp:Panel>

			<div class="buttons">
				<asp:Button ID="btnUseParentBanner" runat="server" Text="Use Parent's Banner"  EnableViewState="false" />
				<asp:HyperLink ID="linkGoToParent" runat="server" >Go To Parent</asp:HyperLink>
			</div>
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="phActiveBanner" runat="server">
		<div class="bannerEdit">
			<fieldset>
				<legend>Active Banner</legend>
				<uc1:FileSelectorControl ID="ImageSelector_Active" runat="server" FileType="image" />
			</fieldset>
			<asp:Panel ID="pnlActiveError" runat="server" CssClass="error" Visible="false" >
				<asp:Label ID="lblActiveError" runat="server" >Sorry there was an error saving</asp:Label>
				<p id="msgActiveError" runat="server"></p>
			</asp:Panel>

			<div class="buttons">
				<asp:Button ID="btnSaveActive" runat="server" Text="Save"  EnableViewState="false" />
				<asp:Button ID="btnDeleteActive" runat="server" Text="Delete"  EnableViewState="false" />
			</div>
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="phWIPBanner" runat="server">
		<div class="bannerEdit">
			<fieldset>
				<legend>Work In Progress</legend>
				<uc1:FileSelectorControl ID="ImageSelector_WIP" runat="server" FileType="image" />
			</fieldset>
			<asp:Panel ID="pnlWIPError" runat="server" CssClass="error" Visible="false" >
				<asp:Label ID="lblWIPError" runat="server" >Sorry there was an error saving</asp:Label>
				<p id="msgWIPError" runat="server"></p>
			</asp:Panel>

			<div class="buttons">
				<asp:Button ID="btnSave" runat="server" Text="Save" EnableViewState="false"  />
				<asp:Button ID="btnSetActive" runat="server" Text="Save as Active" EnableViewState="false"  />
				<asp:Button ID="btnDelete" runat="server" Text="Delete"  EnableViewState="false" />
			</div>
		</div>
	</asp:PlaceHolder>

	
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="rightContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageEditContent" runat="server">
</asp:Content>--%>
