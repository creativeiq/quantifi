﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using Quantifi.Business.BannerModule;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Business;

namespace Quantifi.UI.Controls.BannerModule
{
	public partial class TopBannerControl : UserControl
	{
		
		#region Properties
		
		private Banner banner = null;
		private bool isAdmin = false;
		private PageSettings currentPage = null;
		private int pageId = -1;
		private bool showActiveBanner = true;
		
 
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.Page is NonCmsBasePage)
			{
				this.pnlBanner.Visible = false;
			}
			else
			{ 
				this.pnlBanner.Visible = true;
				LoadSettings();
				LoadBanner();
			}
		}

		private void LoadSettings()
		{

			if ((WebUser.IsAdminOrContentAdmin) || (SiteUtils.UserIsSiteEditor()))
			{
				isAdmin = true;
			}

	
			var mojoPage = this.Page as mojoBasePage;


			showActiveBanner = !Request.IsAuthenticated || (mojoPage.ViewMode == PageViewMode.Live);

			if (currentPage == null) 
				currentPage = CacheHelper.GetCurrentPage();

			pageId = currentPage.PageId;
		}

		private void LoadBanner()
		{
			var mojoPage = this.Page as mojoBasePage;

			if (showActiveBanner)
			{
				banner = BannerCacheUtil.GetByPageId(pageId);
			}
			else
			{
				banner = Banner.GetByPageId(pageId, false);

				// if nothing in the DB for work in progress get active banners
				if (banner == null )
					banner = BannerCacheUtil.GetByPageId(pageId);
			}

			this.pnlAdmin.Visible = isAdmin;
			this.linkEdit.NavigateUrl = "/Quantifi/Controls/BannerModule/Edit.aspx?pageId=" + pageId.ToString();


			if (banner.isSaved)
			{
				this.pnlBanner.Style.Add(HtmlTextWriterStyle.BackgroundImage, GetLinkUrl(banner.ImageUrl));
				
				if ( banner.PageId != this.pageId )
					this.hndBannerInfo.Value = "Banner From Page: " + banner.PageId.ToString() +" shown on Page: " +  pageId.ToString();
			}
			else
			{
				LoadDefaultBanner();
			}
		}

		private void LoadDefaultBanner()
		{				


			this.pnlBanner.Style.Add(HtmlTextWriterStyle.BackgroundImage, "/Quantifi/Images/content_top_banner_wide.jpg");
			this.hndBannerInfo.Value = "Banner not found for Page: " + pageId.ToString();
			
			var siteMapDataSource = (SiteMapDataSource)Page.Master.FindControl("SiteMapData");
			if (siteMapDataSource == null)
			{
				siteMapDataSource = (SiteMapDataSource)Page.FindControl("SiteMapData");
				if (siteMapDataSource == null)
					return;

			}

			var titleText = string.Empty;
 
			try
			{
				var currentPageNode = SiteUtils.GetCurrentPageSiteMapNode(siteMapDataSource.Provider.RootNode);

				var topNode = SiteUtils.GetTopLevelParentNode(currentPageNode);

				titleText = topNode.Title;
			}
			catch (Exception)
			{
				return;
			}

			if (titleText.ToLowerInvariant().Equals("home") && currentPage != null)
			{
					titleText = currentPage.PageTitle;
			}
 

			this.lblText.Text = titleText;
			this.pnlText.Visible = true;

		}


    protected string GetLinkUrl(string url)
    {

      if (url.StartsWith("~/"))
        return SiteUtils.GetNavigationSiteRoot() + url.Replace("~/", "/");


      if (url.StartsWith("/"))
        return SiteUtils.GetNavigationSiteRoot() + url;

      return url;
    }

	}
}