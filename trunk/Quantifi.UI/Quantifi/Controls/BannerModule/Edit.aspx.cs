﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using Quantifi.Business.BannerModule;
using Quantifi.UI.Utils;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.BannerModule
{
	public partial class Edit : NonCmsBasePage
	{
		private int pageId = -1;

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);


			this.btnSave.Click += new EventHandler(btnSave_Click);
			this.btnDelete.Click += new EventHandler(btnDelete_Click);

			this.btnSaveActive.Click += new EventHandler(btnSaveActive_Click);
			this.btnDeleteActive.Click += new EventHandler(btnDeleteActive_Click);
			this.btnSetActive.Click += new EventHandler(btnSetActive_Click);

			this.btnUseParentBanner.Click += new EventHandler(btnUseParentBanner_Click);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(Request.QueryString["pageId"]))
				pageId = Convert.ToInt32(Request.QueryString["pageId"]);

			if (!Request.IsAuthenticated)
			{
				SiteUtils.RedirectToLoginPage(this);
				return;
			}

			SecurityHelper.DisableBrowserCache();
			SuppressPageMenu();
			//LoadParams();

			this.pnlWIPError.Visible = false;
			this.pnlParentError.Visible = false;
			this.pnlActiveError.Visible = false;
			LoadButtons();


			if (!Page.IsCallback && !Page.IsPostBack)
			{
				LoadControls();
			}
		}

		#region LoadControls

		private void LoadControls()
		{

			LoadCurrentBanner();
			LoadWorkInProgress();
			LoadParentBanner();

		}

		private void LoadButtons()
		{
			this.btnDelete.Text = "Delete";
			this.btnDelete.ConfirmThenDisableButtonOnClick("Are you sure you want to delete this work in progress", string.Empty);

			this.btnDeleteActive.Text = "Delete";
			this.btnDeleteActive.ConfirmThenDisableButtonOnClick("Are you sure you want to delete this active banner", string.Empty);

			this.btnSave.Text = "Save";
			this.btnSave.DisableButtonOnClick(string.Empty);
			
			this.btnSaveActive.Text = "Save";
			this.btnSaveActive.DisableButtonOnClick(string.Empty);
			
			this.btnSetActive.Text = "Save as Active";
			this.btnSetActive.DisableButtonOnClick(string.Empty);
			
			this.btnUseParentBanner.Text = "Use Parent's Banner";
			this.btnUseParentBanner.ConfirmThenDisableButtonOnClick("This action will delete the active banner and the page will use the parent page's banner",string.Empty);

		}

		private void LoadCurrentBanner()
		{
			var banner = Banner.GetByPageId(this.pageId, true);

			if (banner == null)
			{
				this.phActiveBanner.Visible = false;
			}
			else
			{
				this.ImageSelector_Active.Value = banner.ImageUrl;
				this.phActiveBanner.Visible = true;
			}

		}

		private void LoadWorkInProgress()
		{
			var banner = Banner.GetByPageId(this.pageId, false);

			if (banner == null)
			{
				this.ImageSelector_WIP.Value = string.Empty;
				this.btnDelete.Visible = false;
			}
			else
			{
				this.btnDelete.Visible = true;
				this.ImageSelector_WIP.Value = banner.ImageUrl;
			}

		}

		private void LoadParentBanner()
		{
			var banner = Banner.GetParentByPageId(this.pageId, true);

			if (banner == null)
			{
				this.phParentBanner.Visible = false;
				this.btnUseParentBanner.Visible = false;
			}
			else
			{

				this.btnUseParentBanner.Visible = true;
				this.phParentBanner.Visible = true;
				imgParentBanner.ImageUrl = banner.ImageUrl;
			}
		}

		#endregion

		protected void btnUseParentBanner_Click(object sender, EventArgs e)
		{
			var banner = Banner.GetByPageId(this.pageId, true);

			if (banner == null)
			{
				this.msgWIPError.InnerHtml = "Unable to find Active Banner for Page: " + this.pageId.ToString();
				this.pnlWIPError.Visible = true;
			}

			try
			{
				banner.Delete();
				this.ImageSelector_Active.Value = string.Empty;
				LoadControls();

			}
			catch (Exception ex)
			{
				this.msgWIPError.InnerHtml = ex.Message;
				this.pnlWIPError.Visible = true;
			}
		}

		#region Active - Button Events

		protected void btnSaveActive_Click(object sender, EventArgs e)
		{
			var banner = Banner.GetByPageId(this.pageId, true);

			if (banner == null)
			{
				banner = new Banner();
			}

			banner.ImageUrl = this.ImageSelector_WIP.Value;
			banner.PageId = this.pageId;
			banner.isActive = true;

			try
			{
				banner.Save();
				LoadControls();
			}
			catch (Exception ex)
			{
				this.msgWIPError.InnerHtml = ex.Message;
				this.pnlWIPError.Visible = true;
			}
		}

		protected void btnDeleteActive_Click(object sender, EventArgs e)
		{
			var banner = Banner.GetByPageId(this.pageId, true);

			if (banner == null)
			{
				this.msgWIPError.InnerHtml = "Unable to find Active Banner for Page: " + this.pageId.ToString();
				this.pnlWIPError.Visible = true;
			}

			try
			{
				banner.Delete();
				this.ImageSelector_Active.Value = string.Empty;
				LoadControls();

			}
			catch (Exception ex)
			{
				this.msgWIPError.InnerHtml = ex.Message;
				this.pnlWIPError.Visible = true;
			}


		}
		
		#endregion

		#region Work In Proccess - Button Events

		protected void btnSave_Click(object sender, EventArgs e)
		{
			var banner = Banner.GetByPageId(this.pageId, false);

			if (banner == null)
			{
				banner = new Banner();
			}

			banner.ImageUrl = this.ImageSelector_WIP.Value;
			banner.PageId = this.pageId;

			try
			{
				banner.Save();
				LoadControls();
			}
			catch (Exception ex)
			{
				this.msgWIPError.InnerHtml = ex.Message;
				this.pnlWIPError.Visible = true;
			}

		}

		protected void btnDelete_Click(object sender, EventArgs e)
		{
			var banner = Banner.GetByPageId(this.pageId, false);

			if (banner == null)
			{
				this.msgWIPError.InnerHtml = "Unable to find Work In Progress Banner for Page: " + this.pageId.ToString();
				this.pnlWIPError.Visible = true;			 
			}

			try
			{
				banner.Delete();

				this.ImageSelector_WIP.Value = string.Empty;
				LoadControls();

			}
			catch (Exception ex)
			{
				this.msgWIPError.InnerHtml = ex.Message;
				this.pnlWIPError.Visible = true;
			}



		}

		protected void btnSetActive_Click(object sender, EventArgs e)
		{
			var banner = Banner.GetByPageId(this.pageId, false);

			var activeBanner  = Banner.GetByPageId(this.pageId, true);

			if (banner == null)
			{
				banner = new Banner();
			}

			banner.ImageUrl = this.ImageSelector_WIP.Value;
			banner.PageId = this.pageId;
			banner.isActive = true;

			try
			{
				// Take active banner and set to working progress
				if (activeBanner != null)
				{
					activeBanner.isActive = false;
					activeBanner.Save();
				}

				banner.Save();
				LoadControls();
			}
			catch (Exception ex)
			{
				this.msgWIPError.InnerHtml = ex.Message;
				this.pnlWIPError.Visible = true;
			}
		}
		
		#endregion

	}
}