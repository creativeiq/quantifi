﻿<%@ Control Language="C#" ViewStateMode="Disabled" AutoEventWireup="true" CodeBehind="TopBannerControl.ascx.cs"
  EnableViewState="false" Inherits="Quantifi.UI.Controls.BannerModule.TopBannerControl" %>


<asp:Panel ID="pnlBanner" runat="server" CssClass="topbanner">
  <asp:Panel ID="pnlText" runat="server" CssClass="content" Visible="false">
    <asp:Label ID="lblText" runat="server"></asp:Label>
  </asp:Panel>
  <asp:Panel ID="pnlAdmin" runat="server" CssClass="topbannerEdit">
    <asp:HyperLink ID="linkEdit" runat="server"><img src="/Data/SiteImages/page_gear.png" alt="Edit Banner" /> EDIT</asp:HyperLink>
    <asp:HiddenField ID="hndBannerInfo" runat="server" />
  </asp:Panel>
</asp:Panel>
