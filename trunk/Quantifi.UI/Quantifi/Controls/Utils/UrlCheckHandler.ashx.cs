﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace Quantifi.UI.Controls.Utils
{
  /// <summary>
  /// Summary description for UrlCheckHandler
  /// </summary>
  public class UrlCheckHandler : IHttpHandler
  {

    /// <summary>
    /// Helps check links from external domains via Ajax, 
    /// 
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest(HttpContext context)
    {
      var statusCode = 404;
      var statusDescription = string.Empty;

      try
      {
        var urlParam = context.Request.QueryString["check"];

        if (!string.IsNullOrWhiteSpace(urlParam))
        {
          var request = (HttpWebRequest)WebRequest.Create(urlParam);
          request.Method = "HEAD";
          request.Timeout = 12000;


          var response = (HttpWebResponse)request.GetResponse();

          statusCode = (int)response.StatusCode;
          statusDescription = response.StatusDescription;
        }


      }
      catch (WebException webEx)
      {
        statusCode = (int)((HttpWebResponse)webEx.Response).StatusCode;
        statusDescription = ((HttpWebResponse)webEx.Response).StatusDescription;

      }
      catch (Exception ex)
      {

      }

      context.Response.Clear();
      context.Response.StatusCode = statusCode;
      context.Response.StatusDescription = statusDescription;
      context.Response.End();

    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }
  }
}