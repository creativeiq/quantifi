﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Shared;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using Quantifi.Business.Security;
using mojoPortal.Business.WebHelpers;

namespace Quantifi.UI.Controls.Utils.RestrictedEmails
{
  public partial class QuickTest : Page
  {

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      this.btnTestEmail.Click += new EventHandler(btnTestEmail_Click);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!WebUser.IsAdminOrContentAdminOrContentAuthor)
      {
        SiteUtils.RedirectToAccessDeniedPage(this);
        return;
      }
    }



    private void btnTestEmail_Click(object sender, EventArgs e)
    {

      if (string.IsNullOrWhiteSpace(this.txtEmailToTest.Text))
      {
        // no email value to test

        this.pnlResult.ShowInfoMessageBanner("No email value to test");
        return;
      }


      string msg;
      try
      {
        
        var result = RestrictedEmailsUtils.CheckEmail(this.txtEmailToTest.Text, false, out msg);

        if (result == 0)
        {
          // email was not found to be restricted
          this.pnlResult.ShowInfoMessageBanner("Email was not found to be restricted");
          return;
        }

        this.pnlResult.ShowErrorMessageBanner(msg);


      }
      catch (Exception ex)
      {

        this.pnlResult.ShowErrorMessageBanner("An error happened: " + ex);
        
      }



    }


  }
}