﻿<%@ Page Title="Quick Test - Restricted Emails" Language="C#" MasterPageFile="~/App_MasterPages/DialogMaster.Master"
  AutoEventWireup="true" CodeBehind="QuickTest.aspx.cs" Inherits="Quantifi.UI.Controls.Utils.RestrictedEmails.QuickTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phHead" runat="server">
  <style type="text/css">
    body { font-family:Arial; font-size:12px; color:#666666; }
    
    .pnlWrapper{ padding: 15px; }
    
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phMain" runat="server">
  <asp:Panel ID="pnlWrapper" runat="server" CssClass="pnlWrapper">
    <div>
      <span style="font-size: 18px;">Test Email</span>
    </div>
    <div class="">
      <asp:TextBox ID="txtEmailToTest" runat="server"></asp:TextBox>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <asp:Button ID="btnTestEmail" runat="server" Text="Test" />
    
    </div>
    <asp:Panel ID="pnlResult" runat="server" ></asp:Panel>
  </asp:Panel>
</asp:Content>
