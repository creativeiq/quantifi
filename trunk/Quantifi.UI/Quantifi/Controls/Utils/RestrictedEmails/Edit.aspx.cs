﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using Quantifi.Data.EF;
using Quantifi.Shared;
using mojoPortal.Business.WebHelpers;
using Quantifi.Business.Security;

namespace Quantifi.UI.Controls.Utils.RestrictedEmails
{
  public partial class Edit : NonCmsBasePage
  {
    #region Properties

    private static readonly ILog log = LogManager.GetLogger(typeof(Edit));

    private int itemId;
    private string restrictedEmail;

    private List<string> testEmailDomains = new List<string>() { "gmail.com", "quantifisolutions.com", "nyu.edu", "london.ac.uk", "counterpartyriskmanagement.org" };

    #endregion

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      this.btnSave.Click += new EventHandler(btnSave_Click);
      this.btnDelete.Click += new EventHandler(btnDelete_Click);


      LoadParams();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

      if (!WebUser.IsAdminOrContentAdminOrContentAuthor)
      {
        SiteUtils.RedirectToAccessDeniedPage(this);
        return;
      }

      var basePage = this.Page as mojoBasePage;
      if (!basePage.ClientScript.IsClientScriptIncludeRegistered("jTip2"))
      {
        basePage.ClientScript.RegisterClientScriptInclude("jTip2", "/ClientScript/qtip2/jquery.qtip.min.js");
        basePage.Header.Controls.Add(new LiteralControl("\n\r<link href='/ClientScript/qtip2/jquery.qtip.min.css' type='text/css' rel='stylesheet' />\n\r"));
      }


      if (IsPostBack)
        return;


      this.btnDelete.ConfirmThenDisableButtonOnClick("Are you sure you want to delete this?");

      SimpleSessionMessageUtil.ShowMessageIfAny(this.pnlWrapper);

      if (itemId > 0)
      {
        LoadData(itemId);
        return;
      }
      else if (!string.IsNullOrWhiteSpace(restrictedEmail) && mojoPortal.Net.Email.IsValidEmailAddressSyntax(restrictedEmail))
      {
        string msg;
        var id = RestrictedEmailsUtils.CheckEmail(restrictedEmail, false, out msg);

        if (id > 0)
        {
          LoadData(id);
          return;
        }
      }


      LoadCreateMode();





    }



    private void LoadParams()
    {
      itemId = WebUtils.ParseInt32FromQueryString("id", 0);

      restrictedEmail = WebUtils.ParseStringFromQueryString("email", string.Empty);

    }

    private void LoadData(int id)
    {


      using (var dc = new CorpWebEntities())
      {
        var item = dc.EmailRestrictions.SingleOrDefault(er => er.Id == id);

        if (item == null)
        {
          LoadCreateMode();
          return;
        }


        this.txtName.Text = item.Name;
        this.txtRegEx.Text = item.RegExMatch;
        this.chkboxIsCompetitor.Checked = item.isCompetitor;
        this.chkboxIsActive.Checked = item.isActive;


        this.headerControl.Text = "Edit Email Restriction - " + Server.HtmlEncode(item.Name);
        this.Page.Title = "Edit - Email Restriction - " + Server.HtmlEncode(item.Name);

      }
    }

    private void LoadCreateMode()
    {

      if (!string.IsNullOrWhiteSpace(restrictedEmail) && mojoPortal.Net.Email.IsValidEmailAddressSyntax(restrictedEmail))
      {
        var restrictedDomain = restrictedEmail.Substring(restrictedEmail.IndexOf("@") + 1);


        this.headerControl.Text = "Create New Email Restriction [" + restrictedDomain + "]";
        this.Page.Title = "Create - Email Restriction ";
        this.txtName.Text = restrictedDomain;

        this.txtRegEx.Text = "^(" + restrictedDomain.Replace("-", @"\-").Replace(".", @"\.") + ")";
        this.chkboxIsCompetitor.Checked = false;
        this.chkboxIsActive.Checked = true;
      }
      else
      {

        this.headerControl.Text = "Create New Email Restriction";
        this.Page.Title = "Create - Email Restriction";
        this.btnDelete.Visible = false;
      }
    }




    #region Button Clicks

    private void btnDelete_Click(object sender, EventArgs e)
    {
      Delete();
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      Save();
    }

    #endregion



    /// <summary>
    /// 
    /// </summary>
    private void Save()
    {
      if (string.IsNullOrWhiteSpace(this.txtRegEx.Text))
      {
        this.pnlRegEx.ShowErrorMessageBanner("ReGex is required.");
        return;
      }

      try
      {
        var regExTest = new Regex(this.txtRegEx.Text, RegexOptions.IgnoreCase);
        foreach (var domain in testEmailDomains)
        {
          var result = regExTest.IsMatch(domain);
        }
      }
      catch (Exception ex)
      {
        this.pnlRegEx.ShowErrorMessageBanner("An error happened while checking the ReGex: " + ex.Message);
        return;

      }


      using (var dc = new CorpWebEntities())
      {
        var testItem = dc.EmailRestrictions.SingleOrDefault(er => er.Name == this.txtName.Text && er.Id != this.itemId);
        if (testItem != null)
        {
          this.pnlWrapper.ShowErrorMessageBanner("An entry was already made for this name: <a href='?id=" + testItem.Id.ToString() + "' >View</a>");
          return;
        }

        testItem = dc.EmailRestrictions.SingleOrDefault(er => er.RegExMatch == this.txtRegEx.Text && er.Id != this.itemId);
        if (testItem != null)
        {
          this.pnlWrapper.ShowErrorMessageBanner("An entry was already made for this pattern: <a href='?id=" + testItem.Id.ToString() + "' >View</a>");
          return;
        }

        bool isNewObject = false;
        var item = dc.EmailRestrictions.SingleOrDefault(er => er.Id == this.itemId);
        if (item == null)
        {
          item = dc.EmailRestrictions.CreateObject();

          dc.EmailRestrictions.AddObject(item);
          isNewObject = true;
        }


        item.Name = this.txtName.Text;
        item.RegExMatch = this.txtRegEx.Text;
        item.isCompetitor = this.chkboxIsCompetitor.Checked;
        item.isActive = this.chkboxIsActive.Checked;

        bool result = false;
        try
        {
          result = dc.SaveChanges() > 0;

        }
        catch (Exception ex)
        {
          log.Error(ex);

          this.pnlWrapper.ShowErrorMessageBanner("An Error happened " + ex.Message);
          return;
        }

        if (result)
        {


          if (isNewObject)
          {
            SimpleSessionMessageUtil.AddSuccessMessage("Save Successful, Email Restriction Created");

            Response.Redirect("~/Quantifi/Controls/Utils/RestrictedEmails/Edit.aspx?id=" + item.Id);
          }
          else
          {
            this.pnlWrapper.ShowSuccessMessageBanner("Save Successful, Email Restriction Updated");
          }
        }

      }

    }

    /// <summary>
    /// 
    /// </summary>
    private void Delete()
    {

      bool result = false;
      using (var dc = new CorpWebEntities())
      {

        var item = dc.EmailRestrictions.SingleOrDefault(er => er.Id == this.itemId);



        if (item != null)
        {
          var name = item.Name;
          dc.DeleteObject(item);


          try
          {
            result = dc.SaveChanges() > 0;
            var msg = string.Format("Deleted Email Restriction [{0}] {1} ", this.itemId, name);
            log.Info(msg);
            SimpleSessionMessageUtil.AddInfoMessage(msg);
          }
          catch (Exception ex)
          {
            log.Error(ex);

            this.pnlWrapper.ShowErrorMessageBanner("An Error happened while deleting  " + ex.Message);
            return;
          }

        }
      }

      if (result)
      {
        Response.Redirect("~/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx");
      }
    }

  }
}