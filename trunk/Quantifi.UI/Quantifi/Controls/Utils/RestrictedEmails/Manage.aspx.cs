﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Data.EF;
using Quantifi.Shared;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using mojoPortal.Business.WebHelpers;

namespace Quantifi.UI.Controls.Utils.RestrictedEmails
{
  public partial class Manage : NonCmsBasePage
  {
    #region Properties

    private int pageNum = 1;
    private int pageSize = 50;
    private int totalPages = 0;

    private string Param_SortColumn = "Name";
    private string Param_SortAsc = "Ascending";
    private string Param_SortDesc = "Descending";
    private string Param_Search = string.Empty;

    private SortDirection currentSortDirection = SortDirection.Ascending;

    private string CSS_SortAsc = "link-sort-up";
    private string CSS_SortDesc = "link-sort-down";

    private string ColName_Name = "Name";
    private string ColName_IsCompetitor = "IsCompetitor";
    private string ColName_IsActive = "IsActive";
    private string ColName_Count = "Count";

    #endregion


    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      LoadParams();


      this.btnExport.Click += new EventHandler(btnExport_Click);

      this.btnSearch.Click += new EventHandler(btnSearch_Click);
      this.btnClearSearch.Click += new EventHandler(btnClearSearch_Click);
    }


    private void LoadParams()
    {
      Param_Search = WebUtils.ParseStringFromQueryString("search", string.Empty);
      pageNum = WebUtils.ParseInt32FromQueryString("pg", 1);

      Param_SortColumn = WebUtils.ParseStringFromQueryString("col", Param_SortColumn);



      currentSortDirection = (string.Equals(WebUtils.ParseStringFromQueryString("sort", Param_SortAsc), Param_SortAsc, StringComparison.InvariantCultureIgnoreCase))
                                ? SortDirection.Ascending
                                : SortDirection.Descending;

    }


    protected void Page_Load(object sender, EventArgs e)
    {
      if (!WebUser.IsAdminOrContentAdminOrContentAuthor)
      {
        SiteUtils.RedirectToAccessDeniedPage(this);
        return;
      }
      SimpleSessionMessageUtil.ShowMessageIfAny(this.pnlWrapper);

      if (!IsPostBack)
      {
        LoadData();

        LoadControls();
      }

    }



    private void LoadData()
    {


      using (var dc = new CorpWebEntities())
      {

        var allItems = dc.EmailRestrictions
                            .Select(ef => new
                            {
                              Id = ef.Id,
                              Name = ef.Name,
                              RegExMatch = ef.RegExMatch,
                              IsActive = ef.isActive,
                              IsCompetitor = ef.isCompetitor,
                              Count = ef.EmailChecks.Count
                            });

        if (!string.IsNullOrWhiteSpace(Param_Search))
        {
          allItems = allItems.Where(ef => ef.Name.Contains(Param_Search));

        }

        if (Param_SortColumn.Equals(ColName_Name, StringComparison.InvariantCultureIgnoreCase))
        {
          if (currentSortDirection == SortDirection.Ascending)
          {
            allItems = allItems.OrderBy(er => er.Name);
          }
          else
          {
            allItems = allItems.OrderByDescending(er => er.Name);
          }
        }
        else
        {
          if (currentSortDirection == SortDirection.Ascending)
          {

            allItems = allItems
                        .OrderBy(Param_SortColumn)
                        .ThenBy(ColName_Name);

          }
          else
          {

            allItems = allItems
                        .OrderByDescending(Param_SortColumn)
                        .ThenBy(ColName_Name);
          }

        }




        totalPages = allItems.Count() / pageSize;
        var skip = ((pageNum - 1) * pageSize);

        var dataPage = allItems.Skip(skip).Take(pageSize);


        this.rptItems.DataSource = dataPage;
        this.rptItems.DataBind();




        pgr.PageURLFormat = string.Format("/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx?col={0}&sort={1}", Param_SortColumn, currentSortDirection) + "&pg={0}" + ((string.IsNullOrWhiteSpace(Param_Search)) ? string.Empty : "&search=" + Param_Search.UrlEncode());
        pgr.ShowFirstLast = true;
        pgr.PageSize = pageSize;
        pgr.PageCount = totalPages;
        pgr.CurrentIndex = pageNum;
        pgr.Visible = (totalPages > 1);


      }
    }

    private void LoadControls()
    {


      SetHeaderSortLinks(this.linkSortName, ColName_Name);
      SetHeaderSortLinks(this.linkSortIsCompetitor, ColName_IsCompetitor);
      SetHeaderSortLinks(this.linkSortIsActive, ColName_IsActive);
      SetHeaderSortLinks(this.linkSortCount, ColName_Count);

    }


    private void SetHeaderSortLinks(HyperLink link, string colname)
    {
      if (string.Equals(Param_SortColumn, colname, StringComparison.InvariantCultureIgnoreCase))
      {
        if (currentSortDirection == SortDirection.Ascending)
        {
          link.NavigateUrl = string.Format("/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx?col={0}&sort={1}&pg={2}", colname, Param_SortDesc, pageNum) + ((string.IsNullOrWhiteSpace(Param_Search)) ? string.Empty : "&search=" + Param_Search.UrlEncode());
          link.CssClass = CSS_SortDesc;
        }
        else
        {
          link.NavigateUrl = string.Format("/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx?col={0}&sort={1}&pg={2}", colname, Param_SortAsc, pageNum) + ((string.IsNullOrWhiteSpace(Param_Search)) ? string.Empty : "&search=" + Param_Search.UrlEncode());
          link.CssClass = CSS_SortAsc;
        }

      }
      else
      {
        link.NavigateUrl = string.Format("/Quantifi/Controls/Utils/RestrictedEmails/Manage.aspx?col={0}&sort={1}&pg={2}", colname, Param_SortAsc, pageNum) + ((string.IsNullOrWhiteSpace(Param_Search)) ? string.Empty : "&search=" + Param_Search.UrlEncode());
        link.CssClass = CSS_SortAsc;
      }

    }



    private void btnClearSearch_Click(object sender, EventArgs e)
    {
      Param_Search = string.Empty;

      LoadData();

      LoadControls();
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
      Param_Search = txtSearch.Text;

      LoadData();

      LoadControls();
    }

    private void btnExport_Click(object sender, EventArgs e)
    {
      using (var dc = new CorpWebEntities())
      {

        var csv = dc.EmailRestrictions
                            .Select(ef => new
                            {
                              Id = ef.Id,
                              Name = ef.Name,
                              RegExMatch = ef.RegExMatch,
                              isActive = ef.isActive,
                              isCompetitor = ef.isCompetitor,
                              Count = ef.EmailChecks.Count
                            }).ToList()
                            .ToCSV();


        CSVHelper.ExportCSV(csv, string.Format("Restricted-Email-Domains-{0:MM-dd-yyyy}.csv", DateTime.Now));

      }
    }


  }
}