﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using Quantifi.UI.Controls.RandomizerSidebarModule.Components;
using mojoPortal.Business;
using Quantifi.UI.Utils;


namespace Quantifi.UI.Controls.RandomizerSidebarModule
{
	public partial class RandomizedSidebarControl : SiteModuleControl
	{
		RandomizedSidebarConfiguration Config = null;

		protected string CookieName { get { return "ModuleId_" + this.ModuleId.ToString(); } }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			Config = new RandomizedSidebarConfiguration(Settings);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			LoadModules();
			
		}


		private void LoadModules()
		{
			HttpCookie trackingCookie = Request.Cookies[CookieName];

			if (trackingCookie == null)
			{
				trackingCookie = new HttpCookie(CookieName);
			}
			else
			{

				for (int i = 0; i < Config.ItemsPerPage; i++)
				{
					//trackingCookie.Values[""]

				}

				
			}

			trackingCookie.Values["userName"] = "patrick";
			trackingCookie.Values["lastVisit"] = DateTime.Now.ToString();
			trackingCookie.Expires = DateTime.Now.AddDays(1);
			Response.Cookies.Add(trackingCookie);




			var ModuleIds = Config.ModuleIdList;

			for (int i = 0; i < Config.ItemsPerPage; i++)
			{


			}

			foreach (int id in ModuleIds)
			{
				if (LoadModule(id))
				{

				}

			}

		}

		private bool LoadModule(int moduleId)
		{
			if (moduleId > -1)
			{
				this.Controls.Clear();
				Module module = new Module(moduleId);

				//SiteModuleControl siteModule =
				//    Page.LoadControl("~/" + module.ControlSource) as SiteModuleControl;

				if (module.ModuleGuid.Equals(Guid.Empty))
					throw new NullReferenceException("Empty Module Passed");

				Control c = Page.LoadControl("~/" + module.ControlSource);
				if (c == null)
				{
					throw new ArgumentException("Unable to load control from ~/" + module.ControlSource);
				}

				if (c is SiteModuleControl)
				{
					SiteModuleControl siteModule = (SiteModuleControl)c;
					siteModule.SiteId = siteSettings.SiteId;
					siteModule.ModuleConfiguration = module;
					this.Title = module.ModuleTitle;

				}

				this.Controls.Add(c);
				return true;
			}
			return false;
		}



	}
}