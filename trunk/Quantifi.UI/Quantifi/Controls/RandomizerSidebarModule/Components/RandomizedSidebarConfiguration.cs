﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using log4net;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Controls.RandomizerSidebarModule.Components
{


	public class RandomizedSidebarConfiguration
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(RandomizedSidebarConfiguration));

		public List<int> ModuleIdList { get; private set; }
		public int ItemsPerPage { get; private set; }
 

		public RandomizedSidebarConfiguration() { }
		public RandomizedSidebarConfiguration(Hashtable settings)
    {
        LoadSettings(settings);
    }

		private void LoadSettings(Hashtable settings)
		{
			if (settings == null) { throw new ArgumentException("Must pass in a hashtable of settings"); }

			ItemsPerPage = WebUtils.ParseInt32FromHashtable(settings, "NumberOfModulesDisplayedSetting", 3);

			ModuleIdList = new List<int>();
			var moduleIdsString = string.Empty;
			if (settings.Contains("ModuleIdsSetting"))
			{
				moduleIdsString = settings["ModuleIdsSetting"].ToString();


				var idArray = moduleIdsString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
				if (idArray.Length > 0)
				{
					try
					{
						ModuleIdList = Array.ConvertAll(idArray, s => int.Parse(s)).ToList();
					}
					catch (Exception ex)
					{
						log.Error("Error while parsing ints for module ids", ex);
					}
				}

			}


			
		


		}


	}



}