﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Quantifi.UI.Controls.ColorPicker
{
	public partial class ColorPickerControl : System.Web.UI.UserControl
	{
		public string Text { get { return this.txtColor.Text; } set { this.txtColor.Text = value; } }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack || Page.IsCallback)
				return;

			this.Page.Header.Controls.Add(new LiteralControl("<link href=\"/Quantifi/Includes/ColorPicker/colorpicker.css\" rel=\"stylesheet\" type=\"text/css\" />"));
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			this.divPreview.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#" + this.txtColor.Text);

		}

	}
}