﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColorPickerControl.ascx.cs" Inherits="Quantifi.UI.Controls.ColorPicker.ColorPickerControl" %>


<style type="text/css">
	.colorWrapper {width:150px;}
	.colorWrapper .txtColor	 {float:left;width:50px;}
	.colorWrapper .divPreview	 {float:left; height:14px; width:14px; margin-left:10px; margin-top:2px; border:solid 1px #545454;}
</style>

<div style=" clear:left;" class="colorWrapper" >
	<span style="float:left;padding:2px 1px;">&#35;</span>
	<asp:TextBox ID="txtColor" CssClass="txtColor"  runat="server" ClientIDMode="Predictable"  AutoCompleteType="None" MaxLength="6"  ></asp:TextBox>

	<div id="divPreview" class="divPreview" runat="server" clientidmode="Predictable" style=" background-color:Transparent;" >&nbsp;</div>
	<div style="clear:left;"></div>
</div>

<script type="text/javascript"	src="/Quantifi/Includes/ColorPicker/colorpicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#<%=txtColor.ClientID %>').ColorPicker({
			onSubmit: function (hsb, hex, rgb, el) {
				$(el).val(hex);
				$("#<%=divPreview.ClientID %>").css('backgroundColor', '#' + hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		})
		.bind('keyup', function () {
			$(this).ColorPickerSetColor(this.value);
		});
	});
</script>