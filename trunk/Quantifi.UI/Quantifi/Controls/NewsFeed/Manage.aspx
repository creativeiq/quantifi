﻿<%@ Page Title="Manage News Feed Items" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="Manage.aspx.cs"
  Inherits="Quantifi.UI.Controls.NewsFeed.Manage" %>


<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %> 

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <asp:Panel runat="server" ID="pnlWrapper" CssClass="">
    <div>
      <asp:HyperLink ID="linkBack" runat="server" CssClass="link-cancel ">Back to Page</asp:HyperLink>
    </div>
    <h1 class="moduletitle" style="margin: 16px 0;">
      Manage New Feed Items &nbsp;&nbsp;&nbsp;&nbsp;
      <asp:HyperLink ID="linkCreate" runat="server" CssClass="link-add" NavigateUrl="~/Quantifi/Controls/NewsFeed/Edit.aspx">Add</asp:HyperLink>
    </h1>
    <quantifi:ExRepeater ID="rptItems" runat="server">
      <EmptyTemplate>
        <div>
          No Data</div>
      </EmptyTemplate>
      <HeaderTemplate>
        <div class="tblGrid">
          <table cellpadding="0" cellspacing="0">
            <tr class="headerRow">
              <th>
              </th>
              <th>
                PubDate
              </th>
              <th>
                Title
              </th>
              <th>
                isActive
              </th>
            </tr>
      </HeaderTemplate>
      <ItemTemplate>
        <tr class="itemRow">
          <td>
            <a href="<%# string.Format("/Quantifi/Controls/NewsFeed/Edit.aspx?id={0}&mid={1}", Eval("Id"), this.moduleId ) %>"
              class="link-edit">edit</a>
          </td>
          <td>
            <%#  ((DateTime)Eval("PubDate")).ToString("dd MMMM yyyy") %>
          </td>
          <td>
            <%#  (Eval("Title")).ToString() %>
          </td>
          <td>
            <%#  (Eval("IsActive")).ToString()%>
            <asp:LinkButton ID="btnToggleActive" runat="server"></asp:LinkButton>
          </td>
        </tr>
      </ItemTemplate>
      <FooterTemplate>
        </table> </div>
      </FooterTemplate>
    </quantifi:ExRepeater>
    <div>
      <mp:CutePager ID="pgr" runat="server" />
    </div>
  </asp:Panel>
</asp:Content>
