﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;
using Quantifi.Data.EF;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Controls.NewsFeed
{
  public partial class NewsFeedModuleControl : SiteModuleControl
  {

    new protected bool IsEditable = false;
    protected Double timeOffset = 0;
    private TimeZoneInfo timeZone = null;

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      LoadParams();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (WebUser.IsAdminOrContentAdminOrContentPublisherOrContentAuthor)
      {
        IsEditable = true;
      }


      PopulateControls();

      LoadData();
    }

    private void LoadParams()
    {
      timeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();

    }

    private void PopulateControls()
    {
      var queryParams = "?mid=" + this.ModuleId + "&pageid=" + this.PageId;
      this.linkManage.NavigateUrl = SiteRoot + "/Quantifi/Controls/NewsFeed/Manage.aspx" + queryParams;
      this.linkSettings.NavigateUrl = SiteRoot + "/Admin/ModuleSettings.aspx" + queryParams;
      this.linkCreate.NavigateUrl = SiteRoot + "/Quantifi/Controls/NewsFeed/edit.aspx" + queryParams;

      this.linkManage.Visible = this.IsEditable;
      this.linkCreate.Visible = this.IsEditable;
      this.linkSettings.Visible = this.IsEditable;


    }


    private void LoadData()
    {
      using (var dc = new CorpWebEntities())
      {

        var items = Queries.GetTopActiveNewsFeedItemsByPubdate(dc, this.ModuleId, 5, DateTime.UtcNow);

        this.rptNewsItem.DataSource = items;

        this.rptNewsItem.DataBind();
      }

    }

    protected string GetLinkUrl(string url)
    {

      if (url.StartsWith("~/"))
        return SiteRoot + url.Replace("~/", "/");


      if (url.StartsWith("/"))
        return SiteRoot + url;

      return url;
    }

    protected string GetNewsPubDate(DateTime itemUtcTime)
    {
      DateTime localTime;
      if (timeZone != null)
      {
        localTime = itemUtcTime.ToLocalTime(timeZone);
      }
      else
      {
        localTime = itemUtcTime.AddHours(timeOffset);
      }



      if (localTime.Date == DateTime.Now.Date)
      {
        return "Today";
      }

      return localTime.ToString("d MMMM yyyy");

    }

  }
}