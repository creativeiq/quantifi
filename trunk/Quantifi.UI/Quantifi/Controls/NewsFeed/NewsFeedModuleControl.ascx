﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsFeedModuleControl.ascx.cs"
  Inherits="Quantifi.UI.Controls.NewsFeed.NewsFeedModuleControl" %>
<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>
<asp:Panel ID="pnlWrapper" runat="server" EnableViewState="false">
  <div class="news-wrapper">
    <div class="content">
      <h2 class="header">
        <span>NEWS</span>&nbsp;&nbsp;<span title="Move Up" class="btnUP mediabtn">UP</span>&nbsp;<span
          title="Move Down" class="btnDOWN mediabtn">DOWN</span>
        <asp:HyperLink ID="linkSettings" ToolTip="Edit Settings" runat="server" CssClass="link-settings-img float-right "
          EnableViewState="False">Settings</asp:HyperLink>
        <asp:HyperLink ID="linkManage" ToolTip="Manage News Feed Items" runat="server" CssClass="link-manage-img float-right"
          EnableViewState="False">Manage</asp:HyperLink>
        <asp:HyperLink ID="linkCreate" ToolTip="Creat News Feed Item" runat="server" CssClass="link-create-img float-right"
          EnableViewState="False">Create New Item</asp:HyperLink>
      </h2>
      <quantifi:ExRepeater EnableViewState="False" ID="rptNewsItem" runat="server">
        <EmptyTemplate>
          <div>
            <!--  No Content -->
          </div>
        </EmptyTemplate>
        <HeaderTemplate>
          <div class="news-container">
        </HeaderTemplate>
        <ItemTemplate>
          <div class="news-item" itemid='<%# Eval("Id") %>'>
            <h3>
              <a href="<%# GetLinkUrl( Eval("LinkUrl" ).ToString()) %>" class="track-link" <%# (((int)Eval("LinkTarget"))== 1)? "target='_blank'" : string.Empty %>
                title="<%# Eval("Title") %>">
                <%# Eval("Title") %></a>
              <asp:HyperLink ID="editlink" runat="server" NavigateUrl='<%# string.Format("~/Quantifi/Controls/NewsFeed/Edit.aspx?id={0}&mid={1}", Eval("Id"),Eval("ModuleId"))%>'
                ToolTip="Edit" CssClass="link-edit-img  float-right" EnableViewState="False" Visible="<%# IsEditable %>">EDIT</asp:HyperLink>
            </h3>
            <span class="date">
              <%# GetNewsPubDate((DateTime)Eval("PubDate"))%></span>
            <%# Eval("Description") %>
          </div>
        </ItemTemplate>
        <FooterTemplate>
          </div>
        </FooterTemplate>
      </quantifi:ExRepeater>
    </div>
    <div class="mask">
    </div>
  </div>
</asp:Panel>