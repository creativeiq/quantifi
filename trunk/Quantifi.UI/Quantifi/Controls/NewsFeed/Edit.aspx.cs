﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using mojoPortal.Web.Editor;
using mojoPortal.Web.Framework;
using Quantifi.Data.EF;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.NewsFeed
{
  public partial class Edit : NonCmsBasePage
  {

    protected int moduleId = -1;
    protected int itemId = -1;
    protected int pageId = -1;

    protected string virtualRoot;
    protected Double timeOffset = 0;
    private TimeZoneInfo timeZone = null;

    protected override void OnPreInit(EventArgs e)
    {

      base.OnPreInit(e);
      SiteUtils.SetupEditor(edContent);

    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      this.btnSave.Click += new EventHandler(btnSave_Click);
      this.btnDelete.Click += new EventHandler(btnDelete_Click);

      LoadParams();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
      if (IsPostBack)
        return;

      PopulateControls();
    }


    private void LoadParams()
    {
      timeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();

      pageId = WebUtils.ParseInt32FromQueryString("pid", -1);
      moduleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      itemId = WebUtils.ParseInt32FromQueryString("id", -1);


      virtualRoot = WebUtils.GetApplicationRoot();


    }

    private void PopulateControls()
    {
      this.linkManage.NavigateUrl = string.Format("~/Quantifi/Controls/NewsFeed/Manage.aspx?mid={0}&pageid={1}", moduleId,
                                                  pageId);


      edContent.WebEditor.ToolBar = ToolBar.SimpleWithSource;
      edContent.WebEditor.Height = new Unit(250);

      if (itemId < 1)
      {
        SetupCreateMode();
        return;
      }

      using (var dc = new CorpWebEntities())
      {


        var newsItem = dc.NewsFeedItems.SingleOrDefault(n => n.Id == itemId);
        if (newsItem == null)
        {
          this.pnlWrapper.ShowErrorMessageBanner(string.Format("Unable to find Item[ID:{0}]", itemId));
          SetupCreateMode();
          return;
        }


        LoadNewsFeedItem(newsItem);
      }

    }


    private void SetupCreateMode()
    {



      if (timeZone != null)
      {
        dpPubDate.Text = DateTime.UtcNow.ToLocalTime(timeZone).ToString();
      }
      else
      {
        dpPubDate.Text = DateTime.UtcNow.AddHours(timeOffset).ToString();
      }

      lblTitle.Text = Page.Title = "Create New Item ";

      this.btnDelete.Visible = false;
      if (moduleId < 1)
      {
        pnlWrapper.ShowErrorMessageBanner("No Module Id Passed... ");
      }
    }

    private void Save()
    {



      using (var dc = new CorpWebEntities())
      {
        NewsFeedItems item;
        bool isNewObject = false;
        if (itemId < 1)
        {
          if (moduleId < 1)
          {
            pnlWrapper.ShowErrorMessageBanner("No Module Id Passed... Unable to Save");
            return;
          }

          item = dc.NewsFeedItems.CreateObject();
          item.ModuleId = moduleId;
          isNewObject = true;

          dc.NewsFeedItems.AddObject(item);
        }
        else
        {
          item = dc.NewsFeedItems.SingleOrDefault(n => n.Id == itemId);
          if (item == null)
          {
            this.pnlWrapper.ShowErrorMessageBanner(string.Format("Error while saving: Unable to find Item[ID:{0}]",
                                                                 itemId));

            return;
          }
        }




        SetNewsFeedItem(item);


        var affecteditems = dc.SaveChanges();


        if (affecteditems > 0)
        {
          pnlWrapper.ShowSuccessMessageBanner("News Feed Item Saved");

          if (isNewObject)
          {
            SimpleSessionMessageUtil.AddSuccessMessage("Save Successful, News Feed Item Created");

            Response.Redirect("~/Quantifi/Controls/NewsFeed/Edit.aspx?id=" + item.Id + "&mid=" + moduleId);
          }
          else
          {
            this.pnlWrapper.ShowSuccessMessageBanner("Save Successful, News Feed Item Updated");
          }

        }
      }
    }

    private void Delete()
    {
      using (var dc = new CorpWebEntities())
      {
        try
        {
          var item = dc.NewsFeedItems.SingleOrDefault(n => n.Id == itemId);
          if (item == null)
          {
            this.pnlWrapper.ShowErrorMessageBanner(string.Format("Error while deleting: Unable to find Item[ID:{0}]",
                                                                 itemId));

            return;
          }

          dc.NewsFeedItems.DeleteObject(item);

          var rowsAffected = dc.SaveChanges();

          if (rowsAffected > 0)
          {
            SimpleSessionMessageUtil.AddSuccessMessage("Delete Successful, News Feed Item removed");

            Response.Redirect("~/Quantifi/Controls/NewsFeed/manage.aspx?mid=" + moduleId);

          }
          else
          {

          }

        }
        catch (Exception ex)
        {
          this.pnlWrapper.ShowErrorMessageBanner(string.Format("Error while deleting: {0}", ex.Message));


        }



      }
    }


    private void LoadNewsFeedItem(NewsFeedItems item)
    {
      this.lblTitle.Text = string.Format("Edit News Feed Item ");

      this.txtTitle.Text = item.Title;
      this.txtLinkUrl.Text = item.LinkUrl;
      this.edContent.Text = item.Description;
      this.checkboxIsActive.Checked = item.IsActive;

      if (timeZone != null)
      {
        dpPubDate.Text = item.PubDate.ToLocalTime(timeZone).ToString();
      }
      else
      {
        dpPubDate.Text = DateTimeHelper.LocalizeToCalendar(item.PubDate.AddHours(timeOffset).ToString());
      }

      this.ddlLinkTarget.SelectedIndex =
        this.ddlLinkTarget.Items.IndexOf(ddlLinkTarget.Items.FindByValue(item.LinkTarget.ToInvariantString()));


    }

    private void SetNewsFeedItem(NewsFeedItems item)
    {
      item.Title = this.txtTitle.Text;
      item.LinkUrl = this.txtLinkUrl.Text;
      item.Description = this.edContent.Text;
      item.IsActive = this.checkboxIsActive.Checked;




      DateTime localTime = DateTime.Parse(dpPubDate.Text);
      if (timeZone != null)
      {
        item.PubDate = localTime.ToUtc(timeZone);
      }
      else
      {
        item.PubDate = localTime.AddHours(-timeOffset);
      }


      item.LinkTarget = Int32.Parse(this.ddlLinkTarget.SelectedValue);


    }

    private bool ParamsAreValid()
    {
      try
      {
        var localTime = DateTime.Parse(dpPubDate.Text);
      }
      catch (FormatException)
      {

        return false;
      }
      catch (ArgumentNullException)
      {

        return false;
      }
      return true;
    }


    #region Button Click


    private void btnDelete_Click(object sender, EventArgs e)
    {
      Delete();
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      if (ParamsAreValid())
      {
        Save();
      }
    }


    #endregion


  }
}