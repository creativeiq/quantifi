﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.UI.Controls.ContentArchiveModule.Components;
using mojoPortal.Business;
using System.Collections;
using Quantifi.Business.ContentArchiveModule;
using mojoPortal.Web;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Controls.ContentArchiveModule
{
	public partial class ViewTag : mojoPortal.Web.mojoBasePage
	{
		#region Properties
		protected int pageId = -1;
		protected int moduleId = -1;
		private int pageNumber = 1;
		private int totalPages = 1;
		private int pageSize = 15;


		private Hashtable moduleSettings;
		private ContentArchiveConfiguration Config;
		protected Double timeOffset = 0;
		protected TimeZoneInfo timeZone = null;
		private Module module = null;
		#endregion


		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

			moduleSettings = ModuleSettings.GetModuleSettings(moduleId);

			if (moduleSettings == null)
				return;

			Config = new ContentArchiveConfiguration(moduleSettings);

			LoadControls();
			LoadData();
		}

		private void LoadControls()
		{

			if (!string.IsNullOrWhiteSpace(Config.Description))
			{
				this.pnlModuleDesc.Visible = true;
				this.pnlModuleDesc.Controls.Add(new LiteralControl(Config.Description));
			}
			else
			{
				this.pnlModuleDesc.Visible = false;
			}


		}

		private void LoadData()
		{
			var items = ContentArchiveCacheUtil.GetPageByModuleId(moduleId, pageNumber, pageSize, out totalPages);

			this.rptItems.DataSource = items.Take(Config.ItemCount);
			this.rptItems.DataBind();



			string pageUrlFormat = SiteRoot
															+ "/Quantifi/Controls/ContentArchiveModule/Archive.aspx?mid=" + moduleId.ToString()
															+ "&amp;pageid=" + pageId.ToString()
															+ "&amp;pagenumber={0}";

			pgr.PageURLFormat = pageUrlFormat;
			pgr.ShowFirstLast = true;
			pgr.PageSize = Config.ArchiveItemCount;
			pgr.PageCount = totalPages;
			pgr.CurrentIndex = pageNumber;
			pgr.Visible = (totalPages > 1);


		}

		private void LoadParams()
		{
			timeOffset = SiteUtils.GetUserTimeOffset();
			timeZone = SiteUtils.GetUserTimeZone();
			pageId = WebUtils.ParseInt32FromQueryString("pageid", pageId);
			moduleId = WebUtils.ParseInt32FromQueryString("mid", moduleId);
			pageNumber = WebUtils.ParseInt32FromQueryString("pagenumber", pageNumber);


			module = GetModule(moduleId);
		}

	}
}