﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using mojoPortal.Web.Editor;
using log4net;
using System.Collections;
using mojoPortal.Web.Framework;
using Quantifi.Business.ContentArchiveModule;
using Quantifi.UI.Utils;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Business;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.ContentArchiveModule
{
	public partial class Edit : NonCmsBasePage
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(Edit));

		#region Properties

		protected int moduleId = -1;
		protected int itemId = -1;
		protected int pageId = -1;
		protected String cacheDependencyKey;
		protected string virtualRoot;
		protected Double timeOffset = 0;
		private TimeZoneInfo timeZone = null;
		protected Hashtable moduleSettings;
		protected PageSettings Settings;

		#endregion

		protected override void OnInit(EventArgs e)
		{
			AllowSkinOverride = true;
			base.OnInit(e);
			SiteUtils.SetupEditor(edContent);

			this.btnSave.Click += new EventHandler(btnSave_Click);
			this.btnDelete.Click += new EventHandler(btnDelete_Click);
			//this.btnCancel.Click += new EventHandler(btnCancel_Click);
 
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			base.SuppressPageMenu();
			SecurityHelper.DisableBrowserCache();

			LoadParams();

			if (!UserCanEditModule(moduleId))
			{
				SiteUtils.RedirectToEditAccessDeniedPage();
				return;
			}


			if (!IsCallback && !IsPostBack && Request.UrlReferrer != null)
			{
				hdnPrevUrl.Value = Request.UrlReferrer.OriginalString.HtmlEncode();
			}

			
			SetupControls();
		}

		#region Click Events

		void btnCancel_Click(object sender, EventArgs e)
		{
			Cancel();
		}

		void btnDelete_Click(object sender, EventArgs e)
		{
			Delete();
		}

		void btnSave_Click(object sender, EventArgs e)
		{
			if (IsValid)
				Save();
		}

		#endregion

		private void LoadParams()
		{
			timeOffset = SiteUtils.GetUserTimeOffset();
			timeZone = SiteUtils.GetUserTimeZone();
			pageId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pageid", -1);
			moduleId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("mid", -1);
			itemId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("itemid", -1);
			cacheDependencyKey = "Module-" + moduleId.ToInvariantString();
			virtualRoot = mojoPortal.Web.Framework.WebUtils.GetApplicationRoot();


			Settings = new PageSettings(CacheHelper.GetCurrentSiteSettings().SiteId, pageId);

		}

		private void SetupControls ()
		{
			edContent.WebEditor.ToolBar = ToolBar.Full;
			 
 

			this.linkCancel.NavigateUrl = Settings.Url;



			if (!IsPostBack && !IsCallback)
			{
				if (itemId > 0)
				{
					ContentItem item = ContentItem.Load(itemId);

					if (item != null)
					{
						LoadItemContent(item);
						return;
					}
				}
				this.dpBeginDate.Text = DateTime.Now.ToString();
				this.Title = "Add New Item";
				this.litLegend.Text = "Add New Item";
			}
		



			
		}

		private void LoadItemContent(ContentItem item)
		{
			this.Title = "Edit Item";
			this.litLegend.Text = "Edit Item";
 

			this.txtTitle.Text = item.Title.HtmlDecode();
			this.txtSubTitle.Text = item.SubTitle.HtmlDecode();

			this.edContent.Text = item.Description;
			this.txtLinkDesc.Text = item.LinkDescription.HtmlDecode();
			this.txtLinkText.Text = item.LinkText.HtmlDecode();
			this.txtLinkUrl.Text = item.LinkUrl;

			//this.dpBeginDate.Text = item.StartDate.ToString();

			dpBeginDate.ShowTime = true;
			if (timeZone != null)
			{
				dpBeginDate.Text = item.StartDate.ToLocalTime(timeZone).ToString();
			}
			else
			{
				dpBeginDate.Text = item.StartDate.AddHours(timeOffset).ToString();
			}

			//this.TagSelectorControl1.Text = item.Tags;
	 
			this.checkboxIsExternalLink.Checked = item.isExternalLink; 

		}


		private void Save()
		{

			if (moduleId < 0)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "<b>No ModuleId when saving</b>  <br/> We need a module to add the item to";
				return;
			}


			ContentItem item;

			if (itemId > 0)
			{
				item = ContentItem.Load(itemId);
				if (item == null)
					item = new ContentItem();
			}
			else
			{
				item = new ContentItem();
			}

			item.ModuleId = moduleId;

			item.Title = this.txtTitle.Text.HtmlEncode();
			item.SubTitle = this.txtSubTitle.Text.HtmlEncode();
			item.Description = this.edContent.Text;
			item.LinkDescription = this.txtLinkDesc.Text.HtmlEncode();
			item.LinkText = this.txtLinkText.Text.HtmlEncode();
			item.LinkUrl = this.txtLinkUrl.Text;
			//item.StartDate = DateTime.Parse(dpBeginDate.Text);

			DateTime localTime = DateTime.Parse(dpBeginDate.Text);
			if (timeZone != null)
			{
				item.StartDate = localTime.ToUtc(timeZone);
			}
			else
			{
				item.StartDate = localTime.AddHours(-timeOffset);
			}

		 
			item.isExternalLink = this.checkboxIsExternalLink.Checked;


 
			//item.Tags = this.TagSelectorControl1.Tags.ToDelimitedString();

			bool saved = false;

			try 
			{ 
				item.Save();



				saved = true;


			}
			catch (Exception ex) 
			{
				log.Error("Error Saving ContentItem", ex);

				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "<b>Error Saving ContentItem</b><br/>" + ex.Message;
			}
 

			if (saved)
			{
        ContentArchiveCacheUtil.ClearCache();
				//CacheHelper.TouchCacheDependencyFile(cacheDependencyKey);
				Settings.UpdateLastModifiedTime();
				Response.Redirect(Settings.Url);
			}

		}

		private void Delete()
		{
			bool saved = false;
			try
			{

				ContentItem item;

				if (itemId > 0)
				{
					item = ContentItem.Load(itemId);
					if (item != null)
					 item.Delete();

				}

				saved = true;
			}
			catch (Exception ex)
			{
				log.Error("Error Deleting ContentItem", ex);

				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "<b>Error Deleting ContentItem</b><br/>" + ex.Message;
			}

			if (saved)
      {
        ContentArchiveCacheUtil.ClearCache();		
				//CacheHelper.TouchCacheDependencyFile(cacheDependencyKey);
				Settings.UpdateLastModifiedTime();
				Response.Redirect(Settings.Url);

			}

		}

		private void Cancel()
		{
			if (string.IsNullOrWhiteSpace(hdnPrevUrl.Value))
				Response.Redirect("~/");
			else
				Response.Redirect(hdnPrevUrl.Value.HtmlDecode());
 

			return;
		}


	}
}