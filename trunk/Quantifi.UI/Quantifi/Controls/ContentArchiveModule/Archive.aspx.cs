﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.UI.Controls.ContentArchiveModule.Components;
using System.Collections;
using mojoPortal.Business;
using mojoPortal.Web.Framework;
using Quantifi.Business.ContentArchiveModule;
using mojoPortal.Web;
using Quantifi.UI.Utils;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.ContentArchiveModule
{
	public partial class Archive : mojoPortal.Web.mojoBasePage
	{

		#region Properties
		protected int pageId = -1;
		protected int moduleId = -1;
		private int pageNumber = 1;
		private int totalPages = 1;
		private int pageSize = 15;


		private Hashtable moduleSettings;
		private ContentArchiveConfiguration Config;
		protected Double timeOffset = 0;
		protected TimeZoneInfo timeZone = null;
		private Module module = null;
		#endregion


		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			LoadParams();

      if (module == null)
      {
        SiteUtils.RedirectToAccessDeniedPage();
        return;
      }

			moduleSettings = ModuleSettings.GetModuleSettings(moduleId);

			if (moduleSettings == null)
				return;

			Config = new ContentArchiveConfiguration(moduleSettings);

			LoadControls();
			LoadData();


			LoadSideContent(true, true);
			base.ForceSideContent(true, null);
		}

		private void LoadControls()
		{

			if (!string.IsNullOrWhiteSpace(Config.Description))
			{
				this.pnlModuleDesc.Visible = true;
				this.pnlModuleDesc.Controls.Add(new LiteralControl(Config.Description));
			}
			else
			{
				this.pnlModuleDesc.Visible = false;
			}



			var title = module.ModuleTitle + " Archive";

			litTitle.Text = title;

			if (pageNumber > 1)
				title += "page " + pageNumber.ToLocalString();

			Page.Title = title;


		}

		private void LoadData()
		{
			var items = ContentArchiveCacheUtil.GetPageByModuleId(moduleId, pageNumber, Config.ArchiveItemCount, out totalPages);

			this.rptItems.DataSource = items;
			this.rptItems.DataBind();



			string pageUrlFormat = SiteRoot
															+ "/Quantifi/Controls/ContentArchiveModule/Archive.aspx?mid=" + moduleId.ToInvariantString()
															+ "&amp;pageid=" + pageId.ToInvariantString()
															+ "&amp;pagenumber={0}";

			pgr.PageURLFormat = pageUrlFormat;
			pgr.ShowFirstLast = true;
			pgr.PageSize = this.pageSize = Config.ArchiveItemCount;
			pgr.PageCount = totalPages;
			pgr.CurrentIndex = pageNumber;
			pgr.Visible = (totalPages > 1);


		}

		private void LoadParams()
		{
			timeOffset = SiteUtils.GetUserTimeOffset();
			timeZone = SiteUtils.GetUserTimeZone();

			pageId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pageid", pageId);
			moduleId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("mid", moduleId);
			pageNumber = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pagenumber", pageNumber);


			module = GetModule(moduleId);
		}

		protected string GetAdminLink(int itemId)
		{
			if (!Request.IsAuthenticated || !UserCanEditModule(moduleId))
				return string.Empty;


			var linkUrl = "/Quantifi/Controls/ContentArchiveModule/Edit.aspx?mid=" + moduleId.ToString() + "&amp;itemId=" + itemId.ToString() + "&amp;pageid=" + pageId.ToString();

			var linkHtml = "<a class='editItemLink' href=\"" + linkUrl + "\" >Edit</a>";

			return linkHtml;

		}

		protected string GetLocalDateTime(DateTime time)
		{
			string dateFormat = "d";
			if (DateTime.UtcNow.AddDays(-6) < time)
				dateFormat = "D";



			if (timeZone != null)
			{
				return time.ToLocalTime(timeZone).ToString(dateFormat);
			}

			return time.AddHours(timeOffset).ToString(dateFormat);



		}

		protected string GetLinkTarget(object o)
		{
			ContentItem item = o as ContentItem;

			if (item != null && item.isExternalLink)
			{
				return " target='_blank' ";
			}
			return string.Empty;

		}

		protected string GetLinkDescription(string desc)
		{
			if (string.IsNullOrWhiteSpace(desc))
				return string.Empty;

			if (!string.IsNullOrWhiteSpace(desc.Substring(desc.Length - 1, 1)))
				return desc.HtmlEncode() + "&nbsp;";

			return desc.HtmlEncode();
		}
	}
}