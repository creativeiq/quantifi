﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quantifi.Business.ContentArchiveModule;
using Quantifi.UI.Utils;
using log4net;


namespace Quantifi.UI.Quantifi.Controls.ContentArchiveModule
{
	/// <summary>
	/// Summary description for TagsHandler
	/// </summary>
	public class TagsHandler : IHttpHandler
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(TagsHandler));

		public void ProcessRequest(HttpContext context)
		{
			if (!string.IsNullOrWhiteSpace( context.Request.QueryString["term"]))
			{
				TagSearch(context, context.Request.QueryString["term"].Trim(), context.Request.QueryString["exclude"]);

			}
			else if (!string.IsNullOrWhiteSpace( context.Request.QueryString["add"]))
			{
				CreateTag(context, context.Request.QueryString["add"]);
			}
			else
			{
				context.Response.ContentType = "text/plain";
				context.Response.Write(Tag.GetAllTags().ToJson());
			}
		}

		private void TagSearch(HttpContext context, string term, string exclude)
		{
			List<string> excludeList = null;
			if (string.IsNullOrWhiteSpace(exclude))
				 excludeList = new List<string>();
			else
				excludeList = exclude.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim().ToLower()).ToList();


			var searchedTags = Tag.GetAllTagsLike(term);


			if (excludeList.Any())
				searchedTags = searchedTags.Where(t => excludeList.IndexOf(t.Text.ToLower()) < 0).ToList();


			context.Response.ContentType = "text/plain";
			context.Response.Write(searchedTags.ToJson());

		}

		private void CreateTag(HttpContext context, string tagText)
		{
			var newtag = new Tag()
			{
				Id = -1,
				Text = tagText,
				TaggedCount = 0
			};

			try {
				newtag.Save();
			}
			catch(Exception ex)
			{
				
			}

			context.Response.ContentType = "text/plain";
			context.Response.Write(newtag.ToJson());
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}