﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using mojoPortal.Business.WebHelpers;
using Quantifi.Business.ContentArchiveModule;

 
namespace Quantifi.UI
{
	public class ContentArchiveDeleteHandler : ContentDeleteHandlerProvider
	{
		public ContentArchiveDeleteHandler()
		{ }

		public override void DeleteContent(int moduleId, Guid moduleGuid)
		{

			ContentItem.DeleteByModule(moduleId);


			mojoPortal.Business.ContentMetaRespository metaRepository = new mojoPortal.Business.ContentMetaRespository();
			metaRepository.DeleteByModule(moduleGuid);

			mojoPortal.Business.FriendlyUrl.DeleteByPageGuid(moduleGuid);

		}

	}
}