﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using mojoPortal.Web.Framework;
 

namespace Quantifi.UI.Controls.ContentArchiveModule.Components
{
	public class ContentArchiveConfiguration
	{
		public string Description { get; private set; }
		public int ItemCount { get; private set; }
		public int ArchiveItemCount { get; private set; }
		public string ArchiveLinkText { get; private set; }
		public bool showArchiveLink { get; private set; }

		public ContentArchiveConfiguration() { }
		public ContentArchiveConfiguration(Hashtable settings)
    {
        LoadSettings(settings);
    }

		private void LoadSettings(Hashtable settings)
		{
			if (settings == null) { throw new ArgumentException("Must pass in a hashtable of settings"); }


			if (settings.Contains("DescriptionSetting"))
			{
				Description = settings["DescriptionSetting"].ToString();
			}

			ItemCount = WebUtils.ParseInt32FromHashtable(settings, "ItemCountSetting", 5);
			ArchiveItemCount = WebUtils.ParseInt32FromHashtable(settings, "ArchiveItemCountSetting", 20);

			if (settings.Contains("AchiveLinkTextSetting"))
			{
				ArchiveLinkText = settings["AchiveLinkTextSetting"].ToString();
			}


			showArchiveLink = WebUtils.ParseBoolFromHashtable(settings, "ShowArchiveLinkSetting", false);

		}


	}
}