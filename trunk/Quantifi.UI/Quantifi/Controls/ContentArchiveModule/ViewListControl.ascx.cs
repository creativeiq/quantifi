﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using Quantifi.Business.ContentArchiveModule;
using Quantifi.UI.Controls.ContentArchiveModule.Components;

namespace Quantifi.UI.Controls.ContentArchiveModule
{
	public partial class ViewListControl : SiteModuleControl
	{
		private int itemId = -1;
		private ContentArchiveConfiguration Config;

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Config = new ContentArchiveConfiguration(Settings);
			
			LoadParams();
			LoadControls();
			LoadData();
		}

		private void LoadParams()
		{
			itemId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("itemId", itemId);

		}

		private void LoadControls()
		{
			Title1.EditText = "Add";
			Title1.EditUrl = "/Quantifi/Controls/ContentArchiveModule/Edit.aspx";


			if (!string.IsNullOrWhiteSpace(Config.Description))
			{
				this.pnlModuleDesc.Visible = true;
				this.pnlModuleDesc.Controls.Add(new LiteralControl(Config.Description));
			}
			else 
			{	
				this.pnlModuleDesc.Visible = false; 
			}

			if (Config.showArchiveLink && !string.IsNullOrWhiteSpace(Config.ArchiveLinkText))
			{
				this.pnlViewArchive.Visible = true;
				this.linkViewArchive.NavigateUrl = "/Quantifi/Controls/ContentArchiveModule/Archive.aspx?mid=" + ModuleId + "&pageid=" + this.PageId + "&pagenumber=1";
				this.linkViewArchive.Text = Config.ArchiveLinkText;
			}
			else
			{
				this.pnlViewArchive.Visible = false; 
			}
		}

		private void LoadData()
		{
			List<ContentItem> items;

			if (itemId > 0)
			{
				items = new List<ContentItem>();
				var item = ContentItem.Load(itemId);
				if (item != null)
					items.Add(item);
			}
			else
			{ 
				items = ContentArchiveCacheUtil.GetLatestItemsByModule(this.ModuleId, Config.ItemCount); 
			}



			this.rptItems.DataSource = items;
			this.rptItems.DataBind();


			if (this.rptItems.Items.Count < Config.ItemCount)
				this.pnlViewArchive.Visible = false;
		}
	
	
		protected string GetAdminLink( int itemId)
		{
			if (!Request.IsAuthenticated)
				return string.Empty;


			var linkUrl = "/Quantifi/Controls/ContentArchiveModule/Edit.aspx?mid=" + this.ModuleId.ToString() + "&amp;itemId=" + itemId.ToString() + "&amp;pageid=" + this.currentPage.PageId.ToString();

			var linkHtml = "<a class='editItemLink' href=\"" +linkUrl + "\" >Edit</a>";

			return linkHtml;

		}
	
		protected string GetLinkTarget(object o)
		{
			ContentItem item = o as ContentItem;

			if (item != null && item.isExternalLink)
			{
				return " target='_blank' ";
			}
			return string.Empty;

		}

	}
}