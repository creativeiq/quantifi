﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using Quantifi.Business.FlipBoxModule;

namespace Quantifi.UI.Controls.FlipBoxModule
{
	public partial class FlipBoxList : SiteModuleControl
	{


		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			LoadControls();
			LoadData();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!Page.ClientScript.IsClientScriptIncludeRegistered(typeof(FlipBoxList), "FLIPJS_Include"))
				Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "FLIPJS_Include", "/Quantifi/Includes/FlipBox/jquery.flip.min.js");

			if (!Page.ClientScript.IsStartupScriptRegistered(this.GetType(), "FLIPJS_Init"))
				Page.ClientScript.RegisterStartupScript(this.GetType(), "FLIPJS_Init", this.litScripts.Text, false);
		}

		private void LoadControls()
		{
			Title1.EditText = "Add";
			Title1.EditUrl = "/Quantifi/Controls/FlipBoxModule/Edit.aspx";

			pnlContainer.ModuleId = this.ModuleId;
		}

		private void LoadData()
		{



			this.rptItems.DataSource = FlipBoxCacheUtil.GetByModule(this.ModuleId);
			this.rptItems.DataBind();
 
		}



		protected string GetLink(object o)
		{
			FlipBox item = o as FlipBox;

			if (item != null && !string.IsNullOrWhiteSpace(item.LinkUrl) && !string.IsNullOrWhiteSpace(item.LinkText))
			{
				return "<a href=\"" + item.LinkUrl
					+ "\" title=\"" + item.LinkText
					+ "\" " + ((item.isExternalLink) ? " target='_blank' " : string.Empty)
					+ " >" + item.LinkText + "</a>";
			
			}





			return string.Empty;

		}


		protected string GetAdminLink(int itemId)
		{
			if (!Request.IsAuthenticated)
				return string.Empty;


			var linkUrl = "/Quantifi/Controls/FlipBoxModule/Edit.aspx?mid=" + this.ModuleId.ToString() + "&amp;itemId=" + itemId.ToString() + "&amp;pageid=" + this.currentPage.PageId.ToString();

			var linkHtml = "<a class='editItemLink' href=\"" + linkUrl + "\" >Edit</a>";

			return linkHtml;

		}
	
	}
}