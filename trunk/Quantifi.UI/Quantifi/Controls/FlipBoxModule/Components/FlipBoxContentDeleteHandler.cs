﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using mojoPortal.Business.WebHelpers;
using Quantifi.Business.FlipBoxModule;

namespace Quantifi.UI.Controls.FlipBoxModule
{
	public class FlipBoxContentDeleteHandler : ContentDeleteHandlerProvider
	{
		public FlipBoxContentDeleteHandler()
		{ }

		public override void DeleteContent(int moduleId, Guid moduleGuid)
		{
			FlipBox.DeleteByModule(moduleId);

			mojoPortal.Business.ContentMetaRespository metaRepository = new mojoPortal.Business.ContentMetaRespository();
			metaRepository.DeleteByModule(moduleGuid);
		}

	}
}