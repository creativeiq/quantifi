﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Quantifi.Controls.FlipBoxModule
{
	public class FlipBoxConfiguration
	{

		public string Description { get; private set; }
		public int PixelWidth { get; private set; }
 

		public FlipBoxConfiguration() { }
		public FlipBoxConfiguration(Hashtable settings)
    {
        LoadSettings(settings);
    }

		private void LoadSettings(Hashtable settings)
		{

			if (settings == null) { throw new ArgumentException("Must pass in a hashtable of settings"); }

			if (settings.Contains("DescriptionSetting"))
				Description = settings["DescriptionSetting"].ToString();

			PixelWidth = WebUtils.ParseInt32FromHashtable(settings, "PixelWidthSetting", 225);
 
		}




	}
}