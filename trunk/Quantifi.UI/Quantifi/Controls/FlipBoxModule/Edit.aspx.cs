﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using mojoPortal.Web.Editor;
using log4net;
using System.Collections;
using mojoPortal.Web.Framework;
using Quantifi.Business.ContentArchiveModule;
using Quantifi.UI.Utils;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Business;
using Quantifi.Business.FlipBoxModule; 


namespace Quantifi.UI.Controls.FlipBoxModule
{
	public partial class Edit : NonCmsBasePage
	{

		private static readonly ILog log = LogManager.GetLogger(typeof(Edit));

		#region Properties

		protected int moduleId = -1;
		protected int itemId = -1;
		protected int pageId = -1;
		protected String cacheDependencyKey;
		protected string virtualRoot;
		protected Double timeOffset = 0;
		private TimeZoneInfo timeZone = null;
		protected Hashtable moduleSettings;
		protected PageSettings Settings;

		#endregion

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.btnSave.Click += new EventHandler(bntSave_Click);
			this.btnDelete.Click += new EventHandler(bntDelete_Click);
			SiteUtils.SetupEditor(this.editorDescription);
			LoadParams();
		}



		protected void Page_Load(object sender, EventArgs e)
		{

			if (!Request.IsAuthenticated)
			{
				SiteUtils.RedirectToEditAccessDeniedPage();
				return;
			}

			LoadParams();

			if (!UserCanEditModule(moduleId))
			{
				SiteUtils.RedirectToEditAccessDeniedPage();
				return;
			}

			base.SuppressPageMenu();
			SecurityHelper.DisableBrowserCache();

			InitControls();

			if (!Page.IsPostBack && !Page.IsCallback)
			{
				Load();
			}
	
		}


		private void LoadParams()
		{
			timeOffset = SiteUtils.GetUserTimeOffset();
			timeZone = SiteUtils.GetUserTimeZone();
			pageId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pageid", -1);
			moduleId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("mid", -1);
			itemId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("itemid", -1);

			virtualRoot = mojoPortal.Web.Framework.WebUtils.GetApplicationRoot();

			cacheDependencyKey = "Module-" + moduleId.ToInvariantString();

			Settings = new PageSettings(CacheHelper.GetCurrentSiteSettings().SiteId, pageId);

		}

		private void InitControls()
		{
			this.linkCancel.NavigateUrl = Settings.Url;
			this.editorDescription.WebEditor.ToolBar = ToolBar.SimpleWithSource;

			editorDescription.WebEditor.Height = Unit.Pixel(150);
			editorDescription.WebEditor.Width = Unit.Pixel(626);
		}



		new private void Load()
		{
			if (moduleId < 1)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "Unable find Module";
				return;
			}

			FlipBox fb = null;
			if (itemId > 0)
			{
				fb = FlipBox.Get(itemId);
			}
			else
			{
				fb = new FlipBox();
				fb.Priority = FlipBox.GetNextPriority(moduleId);
			}

			if (fb == null)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "Unable to find FlipBox";
				return;
			}

			

			this.ImageSelector_ImageUrl.Value = fb.ImageUrl;
			this.editorDescription.Text = fb.Description;
			this.txtImgAlt.Text = fb.ImageAltText;
			this.txtLinkText.Text = fb.LinkText;
			this.txtLinkUrl.Text = fb.LinkUrl;
			this.checkboxIsExternalLink.Checked = fb.isExternalLink;
			this.txtPriority.Text = fb.Priority.ToString();
		 
		}

		private void Delete() 
		{
			if (moduleId < 1)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "Unable find Module";
				return;
			}


			FlipBox fb = null;
			if (itemId > 0)
			{
				fb = FlipBox.Get(itemId);
			}

			if (fb == null)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "Unable to find FlipBox";
				return;
			}

			bool delete = false;
			try
			{
				fb.Delete();

				delete = true;
			}
			catch (Exception ex)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = ex.Message;
			}


			if (delete)
			{
        FlipBoxCacheUtil.ClearCache(moduleId);
 
				Settings.UpdateLastModifiedTime();
				Response.Redirect(Settings.Url);

			}
		
		}

		private void Save()
		{
			if (moduleId < 1)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "Unable find Module";
				return;
			}



			FlipBox fb = null;
			if (itemId > 0)
			{
				fb = FlipBox.Get(itemId);
			}
			else
			{
				fb = new FlipBox();
				fb.Priority = FlipBox.GetNextPriority(moduleId);
			}

			if (fb == null)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "Unable to find FlipBox";
				return;
			}

	
			fb.ModuleId = moduleId;
			fb.ImageUrl = this.ImageSelector_ImageUrl.Value;
			fb.Description = this.editorDescription.Text;
			fb.ImageAltText = this.txtImgAlt.Text;
			fb.LinkText = this.txtLinkText.Text;
			fb.LinkUrl = this.txtLinkUrl.Text;
			fb.isExternalLink = this.checkboxIsExternalLink.Checked;


			int priority = -1;

			if (!int.TryParse(this.txtPriority.Text, out priority) || priority < 0)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = "Please Enter valid number for the Priority ";
				return;
			}

			fb.Priority = priority;



			bool saved = false;
			try
			{
				fb.Save();

				saved = true;
			}
			catch (Exception ex)
			{
				this.pnlError.Visible = true;
				this.litErrorMsg.Text = ex.Message;
			}


			if (saved)
			{

				//CacheHelper.TouchCacheDependencyFile(FlipBox.CacheDependencyKey(moduleId));
        FlipBoxCacheUtil.ClearCache(moduleId);
				Settings.UpdateLastModifiedTime();
				Response.Redirect(Settings.Url);

			}
		}




		protected void bntDelete_Click(object sender, EventArgs e)
		{
			Delete();
		}

		protected void bntSave_Click(object sender, EventArgs e)
		{
			Save();
		}



	}
}