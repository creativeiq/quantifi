﻿using System.Globalization;
using System;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Text;
using System.Net;
using System.IO;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;
using mojoPortal.Web.Controls;
using mojoPortal.Web.Editor;
using mojoPortal.Net;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Configuration;
using System.Xml.Linq;
using System.Linq;
using mojoPortal.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Quantifi.UI.Controls.Twitter
{
  public partial class TwitterFeed : SiteModuleControl
  {
    public static string FeatureGuid = "D8C69AFF-AB1F-4932-82C4-7C63A02DBA3B";

    #region OnInit
    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      this.Load += new EventHandler(Page_Load);
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        try
        {

          if (!Page.ClientScript.IsClientScriptBlockRegistered("jqscroller"))
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "jqscroller", "<script src=\""
              + ResolveUrl("~/ClientScript/jqmojo/jquery.scroller.min.js") + "\" type=\"text/javascript\"></script>");

          // oauth application keys specific for Index eBusiness MojoPortal Application
          var oauth_token = "135163747-2WEmBio6Jm8XPWofDM7fhHw0p1MPPdUYY8fNbWJh";
          var oauth_token_secret = "D5BTZZOl1V1dstMeyMsxaBLa2KDuKDbyosLe6ekbZDvTZ";
          var oauth_consumer_key = "ezN8t1YkfXqfFg1mEczsuRAfC";
          var oauth_consumer_secret = "eZzk6slSq1ReLljT6q4LSwWsisJvM5OJa7uSFAPEdmmpKARb8T";

          // oauth implementation details
          var oauth_version = "1.0";
          var oauth_signature_method = "HMAC-SHA1";

          // unique request details
          var oauth_nonce = Convert.ToBase64String(
              new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
          var timeSpan = DateTime.UtcNow
              - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
          var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

          // message api details
          var resource_url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
          var screen_name = string.IsNullOrEmpty(Settings["TwitterAccount"].ToString()) ? "Quantifi" : Settings["TwitterAccount"].ToString();
          var count = string.IsNullOrEmpty(Settings["TweetCount"].ToString()) ? "3" : Settings["TweetCount"].ToString();

          // create oauth signature
          var baseFormat = "count={7}&oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                          "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&screen_name={6}";

          var baseString = string.Format(baseFormat,
                                      oauth_consumer_key,
                                      oauth_nonce,
                                      oauth_signature_method,
                                      oauth_timestamp,
                                      oauth_token,
                                      oauth_version,
                                      Uri.EscapeDataString(screen_name),
                Uri.EscapeDataString(count)
                                      );

          baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));
          var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret), "&", Uri.EscapeDataString(oauth_token_secret));

          string oauth_signature;
          using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
          {
            oauth_signature = Convert.ToBase64String(
                hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
          }

          // create the request header
          var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                             "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                             "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                             "oauth_version=\"{6}\"";

          var authHeader = string.Format(headerFormat,
                                  Uri.EscapeDataString(oauth_nonce),
                                  Uri.EscapeDataString(oauth_signature_method),
                                  Uri.EscapeDataString(oauth_timestamp),
                                  Uri.EscapeDataString(oauth_consumer_key),
                                  Uri.EscapeDataString(oauth_token),
                                  Uri.EscapeDataString(oauth_signature),
                                  Uri.EscapeDataString(oauth_version)
                          );

          // make the request
          ServicePointManager.Expect100Continue = false;
          var postBody = string.Format("screen_name={0}&count={1}", Uri.EscapeDataString(screen_name), Uri.EscapeDataString(count));
          resource_url += "?" + postBody;
          HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
          request.Headers.Add("Authorization", authHeader);
          request.Method = "GET";
          request.ContentType = "application/x-www-form-urlencoded";
          WebResponse response = request.GetResponse();
          string responseData = new StreamReader(response.GetResponseStream()).ReadToEnd();

          dynamic dynObj = JsonConvert.DeserializeObject(responseData);

          DataTable dataTable = new DataTable();
          dataTable.Columns.Add("TweetText", typeof(String));

          if (dynObj != null && dynObj.Count > 0)
          {
            for (int i = 0; i < dynObj.Count; i++)
            {
              //string tweetdatestring = dynObj[i].created_at;
              //DateTime tweetdate = DateTime.ParseExact(tweetdatestring, "ddd MMM dd HH:mm:ss zzzz yyyy", CultureInfo.InvariantCulture);
              //this.ltlTweet.Text = tweet.ParseURL().ParseUsername().ParseHashtag() + " - " + tweetdate.ToString("dd MMM yyyy HH:mm");

              string tweet = dynObj[i].text;

              DataRow row = dataTable.NewRow();
              row["TweetText"] = tweet.ParseURL().ParseUsername().ParseHashtag();
              dataTable.Rows.Add(row);
            }
          }
          else
          {
            DataRow row = dataTable.NewRow();
            row["TweetText"] = "There are no tweets yet";
            dataTable.Rows.Add(row);
          }

          rptTweets.DataSource = dataTable;
          rptTweets.DataBind();
        }
        catch (Exception pageErr)
        {

        }
      }
    }

  }

  public static class HTMLParser
  {
    public static string Link(this string s, string url)
    {
      return string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url, s);
    }
    public static string ParseURL(this string s)
    {
      return Regex.Replace(s, @"(http(s)?://)?([\w-]+\.)+[\w-]+(/\S\w[\w- ;,./?%&=]\S*)?", new MatchEvaluator(URL));
    }
    public static string ParseUsername(this string s)
    {
      return Regex.Replace(s, "(@)((?:[A-Za-z0-9-_]*))", new MatchEvaluator(Username));
    }
    public static string ParseHashtag(this string s)
    {
      return Regex.Replace(s, "(#)((?:[A-Za-z0-9-_]*))", new MatchEvaluator(Hashtag));
    }
    private static string Hashtag(Match m)
    {
      string x = m.ToString();
      string tag = x.Replace("#", "%23");
      return x.Link("http://search.twitter.com/search?q=" + tag);
    }
    private static string Username(Match m)
    {
      string x = m.ToString();
      string username = x.Replace("@", "");
      return x.Link("http://twitter.com/" + username);
    }
    private static string URL(Match m)
    {
      string x = m.ToString();
      return x.Link(x);
    }
  }

}