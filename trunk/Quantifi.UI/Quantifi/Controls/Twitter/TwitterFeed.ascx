﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwitterFeed.ascx.cs" Inherits="Quantifi.UI.Controls.Twitter.TwitterFeed" %>
<portal:ModuleTitleControl ID="Title1" runat="server" />
<asp:Repeater ID="rptTweets" runat="server">
  <ItemTemplate>
<div class="tweet">
  <i class="fa fa-twitter"></i>
  <%# Eval("TweetText") %>
</div>
  </ItemTemplate>
</asp:Repeater>
