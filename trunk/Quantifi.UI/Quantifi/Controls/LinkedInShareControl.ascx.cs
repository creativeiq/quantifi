﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Quantifi.UI.Controls
{
  public partial class LinkedInShareControl : System.Web.UI.UserControl
  {

    /// <summary>
    /// 
    /// </summary>
    public string LinkUrl { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.ClientScript.IsClientScriptIncludeRegistered(this.GetType(), "Platform-LinkedIn-Share"))
      {
        Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Platform-LinkedIn-Share", "http://platform.linkedin.com/in.js");
      }
    }


    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      if (string.IsNullOrWhiteSpace(Request.RawUrl))
      {
        this.LinkUrl = Request.RawUrl;
      }
    }
  
  }
}