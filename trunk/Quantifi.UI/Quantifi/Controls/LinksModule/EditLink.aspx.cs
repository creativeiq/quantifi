/// Author:					    Joe Audette
/// Created:				    2004-12-24
///	Last Modified:              2009-06-20

using System;
using System.Collections;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;
using mojoPortal.Web.Editor;
using mojoPortal.Web.Framework;
using mojoPortal.SearchIndex;
using Quantifi.Business;
using Resources;

namespace Quantifi.UI.Controls.LinksModule
{
  public partial class EditLinks : NonCmsBasePage
  {
    #region DATA
    
    private Hashtable moduleSettings;
    private bool useDescription = false;
    private int moduleId = 0;
    private int itemId = -1;
    private String cacheDependencyKey;
    private bool descriptionOnly = false;

    #endregion

    #region OnInit

    protected override void OnPreInit(EventArgs e)
    {
      base.OnPreInit(e);
            
      SiteUtils.SetupEditor(edDescription);
            
    }

    override protected void OnInit(EventArgs e)
    {
      base.OnInit(e);
      this.Load += new EventHandler(this.Page_Load);
      this.updateButton.Click += new EventHandler(this.UpdateBtn_Click);
      this.deleteButton.Click += new EventHandler(this.DeleteBtn_Click);
            

      SuppressPageMenu();
    }
    #endregion

    private void Page_Load(object sender, EventArgs e)
    {
      if (!Request.IsAuthenticated)
      {
        SiteUtils.RedirectToLoginPage(this);
        return;
      }

      SecurityHelper.DisableBrowserCache();

      LoadParams();

      if (!UserCanEditModule(moduleId))
      {
        SiteUtils.RedirectToEditAccessDeniedPage();
        return;
      }

      LoadSettings();
      PopulateLabels();

      if (!IsPostBack) 
      {
        PopulateControls();
        if ((Request.UrlReferrer != null) && (hdnReturnUrl.Value.Length == 0))
        {
          hdnReturnUrl.Value = Request.UrlReferrer.ToString();
          lnkCancel.NavigateUrl = Request.UrlReferrer.ToString();

        }
      }
    }

    private void PopulateControls()
    {
      if (itemId > -1)
      {
        var linkItem = new ImageLink(itemId);

        txtTitle.Text = linkItem.Title;
                
        edDescription.Text = linkItem.Description;
                

        String linkProtocol = "http://";
        if (linkItem.Url.StartsWith("https://"))
        {
          linkProtocol = "https://";
        }
        if (linkItem.Url.StartsWith("~/"))
        {
          linkProtocol = "~/";
        }

        ListItem listItem = ddProtocol.Items.FindByValue(linkProtocol);
        if (listItem != null)
        {
          ddProtocol.ClearSelection();
          listItem.Selected = true;
        }

        txtUrl.Text = linkItem.Url.Replace(linkProtocol, String.Empty);
        txtViewOrder.Text = linkItem.ViewOrder.ToString();

        if (linkItem.Target == "_blank")
        {
          chkUseNewWindow.Checked = true;
        }


        FileSelectorControl.Value = linkItem.ImageUrl;
        txtLocation.Text = linkItem.Location;
        dpPubBegin.Text = DateTimeHelper.LocalizeToCalendar(linkItem.PubDate.AddHours(SiteUtils.GetUserTimeOffset()).ToString());
 

      }
      else
      {
        this.deleteButton.Visible = false;
      }

    }


    void linkItem_ContentChanged(object sender, ContentChangedEventArgs e)
    {
      IndexBuilderProvider indexBuilder = IndexBuilderManager.Providers["ImageLinksIndexBuilderProvider"];
      if (indexBuilder != null)
      {
        indexBuilder.ContentChangedHandler(sender, e);
      }
    }

    private void UpdateBtn_Click(Object sender, EventArgs e)
    {
      if (Page.IsValid) 
      {
        ImageLink linkItem = new ImageLink(itemId);
        linkItem.ContentChanged += new ContentChangedEventHandler(linkItem_ContentChanged);

        Module module = new Module(moduleId);
        linkItem.ModuleGuid = module.ModuleGuid;
        linkItem.ModuleId = this.moduleId;
        linkItem.Title = this.txtTitle.Text;
        if (chkUseNewWindow.Checked)
        {
          linkItem.Target = "_blank";
        }
        else
        {
          linkItem.Target = String.Empty;
        }
				
        linkItem.Description = edDescription.Text;
				

        linkItem.Url = ddProtocol.SelectedValue + this.txtUrl.Text.Replace("https://", String.Empty).Replace("http://", String.Empty).Replace("~/",String.Empty);

        linkItem.ImageUrl = FileSelectorControl.Value;
        linkItem.Location = txtLocation.Text;

        DateTime dt;
        if ( DateTime.TryParse(dpPubBegin.Text, out dt))
          linkItem.PubDate = dt;


        linkItem.ViewOrder = int.Parse(this.txtViewOrder.Text);
        SiteUser linkUser = SiteUtils.GetCurrentSiteUser();
        if (linkUser != null)
        {
          linkItem.CreatedByUser = linkUser.UserId;
          linkItem.UserGuid = linkUser.UserGuid;
        }
				
        if(linkItem.Save())
        {
          CurrentPage.UpdateLastModifiedTime();


          CacheHelper.ClearModuleCache(moduleId);
 

          SiteUtils.QueueIndexing();
          if (hdnReturnUrl.Value.Length > 0)
          {
            WebUtils.SetupRedirect(this, hdnReturnUrl.Value);
            return;
          }

          WebUtils.SetupRedirect(this, SiteUtils.GetCurrentPageUrl());
        }
				
      }
    }

    private void DeleteBtn_Click(Object sender, EventArgs e)
    {
      if (itemId != -1) 
      {
        ImageLink link = new ImageLink(itemId);
        link.ContentChanged += new ContentChangedEventHandler(linkItem_ContentChanged);
        link.Delete();
        CurrentPage.UpdateLastModifiedTime();

         
        CacheHelper.ClearModuleCache(moduleId);
        SiteUtils.QueueIndexing();
      }

      if (hdnReturnUrl.Value.Length > 0)
      {
        WebUtils.SetupRedirect(this, hdnReturnUrl.Value);
        return;
      }

      WebUtils.SetupRedirect(this, SiteUtils.GetCurrentPageUrl());
    }




    private void PopulateLabels()
    {
      Title = SiteUtils.FormatPageTitle(siteSettings, QuantifiLinkResources.EditLinkPageTitle);

      this.reqTitle.ErrorMessage = QuantifiLinkResources.LinksTitleRequiredHelp;
      this.reqUrl.ErrorMessage = QuantifiLinkResources.LinksUrlRequiredHelp;
      this.reqViewOrder.ErrorMessage = QuantifiLinkResources.LinksViewOrderRequiredHelp;

      if (descriptionOnly) { reqUrl.Enabled = false; }

      updateButton.Text = QuantifiLinkResources.EditLinksUpdateButton;
      SiteUtils.SetButtonAccessKey(updateButton, QuantifiLinkResources.EditLinksUpdateButtonAccessKey);

      lnkCancel.Text = QuantifiLinkResources.EditLinksCancelButton;
      //cancelButton.Text = LinkResources.EditLinksCancelButton;
      //SiteUtils.SetButtonAccessKey(cancelButton, LinkResources.EditLinksCancelButtonAccessKey);

      deleteButton.Text = QuantifiLinkResources.EditLinksDeleteButton;
      SiteUtils.SetButtonAccessKey(deleteButton, QuantifiLinkResources.EditLinksDeleteButtonAccessKey);
      UIHelper.AddConfirmationDialog(deleteButton, QuantifiLinkResources.LinksDeleteLinkWarning);

      ListItem listItem = ddProtocol.Items.FindByValue("~/");
      if (listItem != null)
      {
        listItem.Text = QuantifiLinkResources.LinksEditRelativeLinkLabel;
      }

    }

    private void LoadParams()
    {
      moduleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      itemId = WebUtils.ParseInt32FromQueryString("ItemID", -1);

    }

    private void LoadSettings()
    {
      cacheDependencyKey = "Module-" + moduleId.ToString();

      moduleSettings = ModuleSettings.GetModuleSettings(moduleId);
      useDescription = WebUtils.ParseBoolFromHashtable(
        moduleSettings, "LinksShowDescriptionSetting", false);

      descriptionOnly = WebUtils.ParseBoolFromHashtable(
        moduleSettings, "LinksShowOnlyDescriptionSetting", descriptionOnly);

      //divDescription.Visible = useDescription;

      edDescription.WebEditor.ToolBar = ToolBar.FullWithTemplates;

      lnkCancel.NavigateUrl = SiteUtils.GetCurrentPageUrl();
            
    }
       
  }
}