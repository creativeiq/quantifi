
using System;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using mojoPortal.Web.UI;
using mojoPortal.SearchIndex;
using Resources;
using Quantifi.Business;
using System.Text;

namespace Quantifi.UI.Controls.LinksModule
{
  public partial class LinksModule : SiteModuleControl
  {
    #region Private Properties

    protected string LinkImage = string.Empty;
    protected string DeleteImage = string.Empty;
    protected bool UseDescription = false;
    protected bool ShowDeleteIcon = false;
    protected string EditContentImage = WebConfigSettings.EditContentImage;
    protected string DeleteLinkImage = "~/Data/SiteImages/" + WebConfigSettings.DeleteLinkImage;
    private bool addWebSnaprCssToLinks = false;
    private string linkCssClass = "mojolink";
    private string webSnaprKey = string.Empty;
    private bool descriptionOnly = false;
    private bool enablePager = false;
    private int pageNumber = 1;
    private int pageSize = 3;
    private int totalPages = 1;
    private string extraCssClass = string.Empty;

    private bool showLocation_ = false;
    private bool showDate_ = false;
    private bool showImage_ = false;
    private string dateFormatStr = "d MMM yyyy";

    private String cacheDependencyKey;

    #endregion

    #region Properties
    
    public bool ShowImage { get { return showImage_; } }
    public bool ShowLocation { get { return showLocation_; } }
    public bool ShowDate { get { return showDate_; } }
    
    #endregion

    #region Page Life Cycle

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      this.Load += new EventHandler(Page_Load);
      this.rptLinks.ItemCommand += new RepeaterCommandEventHandler(rptLinks_ItemCommand);


      pgr.Command += new CommandEventHandler(pgr_Command);
      Page.EnableViewState = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      LoadSettings();
      SetupScripts();

     // if (Page.IsPostBack) { return; }
      PopulateControls();
    }

    #endregion

    private void PopulateControls()
    {

      var dt = DateTime.Now;
   
      // use mojo base page to see if user has toggled draft content:
      // will show future content
      var cmsPage = this.Page as CmsPage;
      if (cmsPage != null && cmsPage.ViewMode == PageViewMode.WorkInProgress)
      {
        dt = DateTime.MaxValue;
      }
      
   

      if (enablePager)
      {
        #region Paging Enabled

        using (var reader = ImageLink.GetPage(ModuleId, pageNumber, pageSize, out totalPages))
        {
          pgr.ShowFirstLast = true;
          pgr.PageSize = pageSize;
          pgr.PageCount = totalPages;

          if (descriptionOnly)
          {
            rptDescription.Visible = true;
            rptLinks.Visible = false;
            rptDescription.DataSource = reader;
            rptDescription.DataBind();

          }
          else
          {
            rptDescription.Visible = false;
            rptLinks.Visible = true;
            rptLinks.DataSource = reader;
            rptLinks.DataBind();

          }

          pgr.Visible = (totalPages > 1);

        }

        #endregion
      }
      else
      {


        var items = ImageLink.GetLinks(ModuleId, dt).Where(i => i.PubDate <= dt).Take(pageSize);

        if (descriptionOnly)
        {
          rptDescription.Visible = true;
          rptLinks.Visible = false;

          rptDescription.DataSource = items;
          rptDescription.DataBind();

        }
        else
        {
          rptDescription.Visible = false;
          rptLinks.Visible = true;


          rptLinks.DataSource = items;
          rptLinks.DataBind();

        }


      }

    }

    #region Events

    void pgr_Command(object sender, CommandEventArgs e)
    {
      pageNumber = Convert.ToInt32(e.CommandArgument);
      pgr.CurrentIndex = pageNumber;
      PopulateControls();
      //updPnl.Update();
    }

    void rptLinks_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
      if ((e.CommandSource is Button) && (e.CommandName.Equals("delete")))
      {
        int itemId = Convert.ToInt32(e.CommandArgument);
        ImageLink link = new ImageLink(itemId);
        link.ContentChanged += new ContentChangedEventHandler(link_ContentChanged);
        link.Delete();

        CacheHelper.TouchCacheFile(cacheDependencyKey);

        WebUtils.SetupRedirect(this, Page.Request.RawUrl);

      }
    }

    void link_ContentChanged(object sender, ContentChangedEventArgs e)
    {
      IndexBuilderProvider indexBuilder = IndexBuilderManager.Providers["ImageLinksIndexBuilderProvider"];
      if (indexBuilder != null)
      {
        indexBuilder.ContentChangedHandler(sender, e);
      }
    }



    #endregion
    
    #region Get Html

    protected string GetImageLink(string title, string url, string imageUrl, string description, string target, string cssClass)
    {
      if (!ShowImage || string.IsNullOrEmpty(imageUrl)) { return string.Empty; }

      var titleValue = GetTitle(title, description);



      return "<div class='" + cssClass
             + "' ><a href='"
             + GetLinkUrl(url) + "' "
             + GetOnClick(target)
             + titleValue
             + "><img src='"
             + imageUrl
             + "' "
             + titleValue
             + " /></a></div>";
    }

    protected string GetTitleLink(string title, string url, string description, string target, string cssClass)
    {
      if (string.IsNullOrEmpty(url)) { return string.Empty; }

      String link = "<div class='" + cssClass
                    + "' ><a class='" + linkCssClass
                    + "' href='" + GetLinkUrl(url) + "' "
                    + GetOnClick(target)
                    + GetTitle(title, description)
                    + ">"
                    + title
                    + "</a></div>";

      return link;
    }

    protected string GetDescriptionHtml(string desc, string url, string target, string cssClass)
    {
      if (!UseDescription)
        return string.Empty;

      return "<div class='" + cssClass
             + "' >" 
             + desc
             + "</div>";

    }

    protected string GetHeaderHtml(string cssClass)
    {
      if (Settings["LinksHeaderText"] != null)
      {
        var headerTxt = Settings["LinksHeaderText"].ToString();
        if (!string.IsNullOrEmpty(headerTxt))
        {

          var sb = new StringBuilder();

          sb.Append("<h2 class='");
          sb.Append(cssClass);
          sb.Append("'>");

          if (Settings["LinksHeaderUrl"] != null && !string.IsNullOrEmpty(Settings["LinksHeaderUrl"].ToString()))
          {
            var headerUrl = Settings["LinksHeaderUrl"].ToString();

            sb.Append("<a href='");
            sb.Append(headerUrl);
            sb.Append("'>");
            sb.Append(headerTxt);
            sb.Append("</a>");
          }
          else
          {
            sb.Append("<span>");
            sb.Append(headerTxt);
            sb.Append("</span>");
          }

          sb.Append("</h2>");
          return sb.ToString();
        }
      }

      return string.Empty;
    }

    protected string GetFooterHtml(string cssClass)
    {
      //LinksFooterText,LinksFooterUrl
      if (Settings["LinksFooterText"] != null)
      {
        var headerTxt = Settings["LinksFooterText"].ToString();
        if (!string.IsNullOrEmpty(headerTxt))
        {

          var sb = new StringBuilder();

          sb.Append("<div class='");
          sb.Append(cssClass);
          sb.Append("'>");

          if (Settings["LinksFooterUrl"] != null && !string.IsNullOrEmpty(Settings["LinksFooterUrl"].ToString()))
          {
            var headerUrl = Settings["LinksFooterUrl"].ToString();

            sb.Append("<a href='");
            sb.Append(headerUrl);
            sb.Append("'>");
            sb.Append(headerTxt);
            sb.Append("</a>");
          }
          else
          {
            sb.Append("<span>");
            sb.Append(headerTxt);
            sb.Append("</span>");
          }

          sb.Append("</div>");
          return sb.ToString();
        }
      }

      return string.Empty;
    }

    protected string GetLocationHtml (string location, string cssClass)
    {
      if (!ShowLocation || string.IsNullOrEmpty(location))
        return string.Empty;

      return string.Format("<div class='{0}'><span>{1}</span></div>", cssClass, location);
    }

    protected string GetDateHtml(string pubDate, string cssClass)
    {
      if (!ShowDate)
        return string.Empty;

      DateTime dt;

      if (!DateTime.TryParse(pubDate, out dt))
        return string.Empty;


      var sb = new StringBuilder();

      sb.AppendFormat("<div class='{0}'>", cssClass);

      sb.Append(dt.ToString(dateFormatStr));

      sb.Append("</div>");

      return sb.ToString();
    }

    protected string GetLinkUrl(string dbFormatUrl)
    {
      if (dbFormatUrl.StartsWith("~/"))
      {
        return Page.ResolveUrl(SiteRoot + dbFormatUrl.Replace("~/", "/"));
      }

      return dbFormatUrl;

    }


    protected bool isImageLinkInPreview(DateTime pubDate)
    {
      if (pubDate == null)
        return false;


      var cmsPage = this.Page as CmsPage;
      if (cmsPage != null && cmsPage.ViewMode == PageViewMode.WorkInProgress && pubDate > DateTime.Now)
      {
        return true;
      }

      return false;
    }

    #endregion

    private string GetTitle(string title, string description)
    {
      if (!string.IsNullOrEmpty(title))
      {
        return string.Format(" title='{0}' ", title);
      }

      return String.Empty;
    }

    private string GetOnClick(string target)
    {
      if ((target != null) && (target == "_blank"))
      {
        return " onclick=\"window.open(this.href,'_blank');return false;\" ";
      }

      return string.Empty;

    }

    private void SetupScripts()
    {
      if (!addWebSnaprCssToLinks) { return; }
      if (webSnaprKey.Length == 0) { return; }

      Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page),
                                                  "websnapr", "\n<script src=\"http://bubble.websnapr.com/"
                                                              + webSnaprKey + "/swh/" + "\" type=\"text/javascript\" ></script>");

    }

    private void LoadSettings()
    {
      pnlContainer.ModuleId = ModuleId;
      cacheDependencyKey = "Module-" + ModuleId.ToString();
      Title1.EditUrl = SiteRoot + "/Quantifi/Controls/LinksModule/EditLink.aspx";
      Title1.EditText = QuantifiLinkResources.EditLinksAddLinkLabel;
      Title1.Visible = !this.RenderInWebPartMode;
      if (this.ModuleConfiguration != null)
      {
        this.Title = this.ModuleConfiguration.ModuleTitle;
        this.Description = this.ModuleConfiguration.FeatureName;
      }

      if (IsEditable)
      {
        LinkImage = ImageSiteRoot + "/Data/SiteImages/" + EditContentImage;
        DeleteImage = ImageSiteRoot + "/Data/SiteImages/" + DeleteLinkImage;

        ShowDeleteIcon = WebUtils.ParseBoolFromHashtable(
          Settings, "LinksShowDeleteIconSetting", false);

      }

      UseDescription = WebUtils.ParseBoolFromHashtable(
        Settings, "LinksShowDescriptionSetting", UseDescription);

      descriptionOnly = WebUtils.ParseBoolFromHashtable(
        Settings, "LinksShowOnlyDescriptionSetting", descriptionOnly);

      if (descriptionOnly) { UseDescription = true; }

      enablePager = WebUtils.ParseBoolFromHashtable(
        Settings, "LinksEnablePagingSetting", enablePager);

      pageSize = WebUtils.ParseInt32FromHashtable(
        Settings, "LinksPageSizeSetting", pageSize);



      addWebSnaprCssToLinks = WebUtils.ParseBoolFromHashtable(
        Settings, "LinksAddWebSnaprCss", addWebSnaprCssToLinks);

      if (addWebSnaprCssToLinks) { linkCssClass += " websnapr"; }

      if (Settings.Contains("LinksWebSnaprKeySetting"))
      {
        webSnaprKey = Settings["LinksWebSnaprKeySetting"].ToString();
      }
 

      if (Settings.Contains("LinksExtraCssClassSetting"))
      {
        extraCssClass = Settings["LinksExtraCssClassSetting"].ToString().Trim();
        if (extraCssClass.Length > 0) { pnlWrapper.CssClass += " " + extraCssClass; }
      }


      //Image

      showImage_ = WebUtils.ParseBoolFromHashtable( Settings, "LinksShowImage", showLocation_);

      // Location

      showLocation_ = WebUtils.ParseBoolFromHashtable( Settings, "LinksShowLocation", showLocation_);

      // Date 
      showDate_ = WebUtils.ParseBoolFromHashtable(Settings, "LinksShowDate", showDate_);
      
      if (Settings.Contains("LinksDateFormat") )
      { 
        var fmtString = Settings["LinksDateFormat"].ToString().Trim();
        if (!String.IsNullOrEmpty(fmtString))
        {
          dateFormatStr = fmtString;
        }

      }

      

    }

  }
}