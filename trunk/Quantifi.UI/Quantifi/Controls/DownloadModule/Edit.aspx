﻿<%@ Page Title="Edit Download Module" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Quantifi.UI.Controls.DownloadModule.Edit" %>

<%@ Register Src="~/Quantifi/Controls/DownloadModule/Controls/EditDownloadFileControl.ascx"
	TagPrefix="Quantifi" TagName="EditDownload" %>
<%@ Register Src="~/Quantifi/Controls/DownloadModule/Controls/CreateDownloadFileControl.ascx"
	TagPrefix="Quantifi" TagName="CreateDownload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="leftContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
	<br />
	<div class="breadcrumbs">
		<asp:HyperLink ID="lnkAdminMenu" runat="server" NavigateUrl="~/Admin/AdminMenu.aspx"
			CssClass="unselectedcrumb" Text="Administration Menu" />&nbsp;&gt;
		<asp:HyperLink ID="lnkSiteList" runat="server" NavigateUrl="~/Quantifi/Controls/DownloadModule/ViewFiles.aspx"
			CssClass="selectedcrumb" Text="Download Files" />
	</div>
	<br />
	<Quantifi:EditDownload ID="editControl" runat="server" />
	<Quantifi:CreateDownload ID="createControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageEditContent" runat="server">
</asp:Content>
