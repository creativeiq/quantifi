﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Web;
using Quantifi.Business.DownloadModule;
using mojoPortal.Business.WebHelpers;

namespace Quantifi.UI.Controls.DownloadModule
{
	public partial class Edit : NonCmsBasePage
	{
		#region Properties

		protected SiteUser linkUser = null;
		protected int fileId;


		#endregion

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);


			LoadParams();



			if (!Page.IsCallback && !Page.IsPostBack)
			{
				if (fileId > 0)
				{
					this.editControl.FileId = fileId;
					this.editControl.Visible = true;
					this.createControl.Visible = false;
				}
				else
				{
					this.editControl.Visible = false;
					this.createControl.Visible = true;
				}

			}
		}


		protected void Page_Load(object sender, EventArgs e)
		{

      if (!WebUser.IsAdminOrContentAdminOrContentAuthor)
      {
        SiteUtils.RedirectToAccessDeniedPage(this);
        return;
      }


		}

		private void LoadParams()
		{
			linkUser = SiteUtils.GetCurrentSiteUser();

			if (!Int32.TryParse(Page.Request.QueryString[DownloadFile.QS_DownFileId], out fileId))
				fileId = -1;
		}




	}
}