﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using log4net;
using mojoPortal.Business.WebHelpers;
using Quantifi.Business.DownloadModule;
using Quantifi.Shared;
using System.Text;

namespace Quantifi.UI.Controls.DownloadModule
{
	/// <summary>
	/// Summary description for ActivityHandler
	/// </summary>
	public class ActivityHandler : IHttpHandler
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(ActivityHandler));

		private bool showForm = false;
		private int fileId = -1;
		private DownloadFile df = null;
 
		public void ProcessRequest(HttpContext context)
		{
			LoadParams();
			SecurityHelper.DisableDownloadCache();

			if (context.Request.IsAuthenticated && WebUser.IsAdminOrContentAdmin)
			{
				df =  DownloadFile.Get(fileId);
				if (df != null)
				{
					// Get List of Activity
					
					// 
					
					SendCSV(context);


					return;
				}
			


			}


			SiteUtils.RedirectToAccessDeniedPage();
		}

		private string GetCSVFileName()
		{
			return string.Concat(df.DisplayName , ((showForm) ? "_formactivity.csv" : "_activity.csv"));
		}

		private void SendCSV (HttpContext context)
		{

			try
			{
				if (showForm)
				{
					var activity = DownloadFileFormActivity.GetAll(fileId);

					var csvString = GetCSV(activity);

					CSVHelper.ExportCSV(csvString, GetCSVFileName());
				}
				else
				{
					var activity = DownloadFileActivity.GetAll(fileId);

					var csvString = GetCSV(activity);

					CSVHelper.ExportCSV(csvString, GetCSVFileName());
				}
 
			}
			catch (System.Threading.ThreadAbortException)
			{
				//Thrown when Response.End called in CSV Helper
			}
			catch (Exception ex)
			{
				log.Error("An error occured while exporting DownloadFile Activity to CSV", ex);
				context.Response.ContentType = "text/plain";
				context.Response.StatusCode = 500;
				context.Response.Write(string.Concat("An error occured while exporting DownloadFile Activity to CSV: ", ex.Message));
				context.Response.End();
			}

		}

		private string GetCSV(List<DownloadFileActivity> da)
		{
			StringBuilder sb = new StringBuilder();
 
			sb.Append("Id,UserHostAddress,UserHostName,Referrer,TimeStamp(UTC),FileId");

			sb.AppendLine();

			foreach (var item in da)
			{
				sb.AppendFormat("{0},{1},{2},{3},{4},{5}", item.Id, item.UserHostAddress, item.UserHostName, item.Referrer, item.TimeStamp, item.FileId);
				sb.AppendLine();
			}

			return sb.ToString();
		}


		private string GetCSV(List<DownloadFileFormActivity> dfa)
		{
 
			StringBuilder sb = new StringBuilder();

			sb.Append("Id,FirstName,LastName,JobTitle,Company,Email,Phone,Country,OptIn,User IP,Referrer,TimeStamp,FileId");

			sb.AppendLine();

			foreach (var item in dfa)
			{
				sb.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", item.Id, item.FirstName, item.LastName, item.JobTitle, item.CompanyName, item.Email, item.Phone, (item.Country == null) ? string.Empty : item.Country.Name, item.MarketingOptIn, item.Activity.UserHostAddress, item.Activity.Referrer, item.Activity.TimeStamp, item.Activity.FileId);
				sb.AppendLine();
			}

			return sb.ToString();
		}


		private void LoadParams()
		{
			this.fileId = WebUtils.ParseInt32FromQueryString("fileId", fileId);

			this.showForm = WebUtils.ParseBoolFromQueryString("showform", showForm);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}