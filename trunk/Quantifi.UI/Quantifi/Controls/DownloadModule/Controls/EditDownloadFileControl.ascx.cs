﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Business.DownloadModule;
using mojoPortal.Web;
using mojoPortal.Business;
using log4net;
using System.IO;
using Quantifi.UI.Utils;
using Quantifi.Shared;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Controls.DownloadModule.Controls
{
  public partial class EditDownloadFileControl : System.Web.UI.UserControl
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(EditDownloadFileControl));

    #region Properties

    private DownloadFile df = null;

    private int pageSize = 20;
    private int pageNum = 1;
    private int totalPages = 0;
    private int totalFormPages = 0;

    private bool showFormActivity = true;


    #endregion

    /// <summary>
    /// FileId
    /// </summary>
    public int FileId
    {
      get
      {
        object o = ViewState["FileId"];
        return (o == null) ? 0 : (int)o;
      }

      set
      {
        ViewState["FileId"] = value;
      }
    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      this.btnSave.Click += new EventHandler(btnSave_Click);
      this.btnDelete.Click += new EventHandler(btnDelete_Click);


      this.btnShowReUpload.Click += new EventHandler(btnShowReUpload_Click);
      this.btnCancelUpload.Click += new EventHandler(btnCancelUpload_Click);
      this.btnUpload.Click += new EventHandler(btnUpload_Click);

      LoadParams();

    }

    protected void Page_Load(object sender, EventArgs e)
    {

      var basePage = this.Page as mojoBasePage;
      if (!basePage.ClientScript.IsClientScriptIncludeRegistered("jTip2"))
      {
        basePage.ClientScript.RegisterClientScriptInclude("jTip2", "/ClientScript/qtip2/jquery.qtip.min.js");
        basePage.Header.Controls.Add(new LiteralControl("\n\r<link href='/ClientScript/qtip2/jquery.qtip.min.css' type='text/css' rel='stylesheet' />\n\r"));
      }


      this.btnSave.Enabled = true;
      this.btnDelete.Enabled = true;
      this.btnSave.DisableButtonOnClick(string.Empty);
      this.btnDelete.ConfirmThenDisableButtonOnClick("Are you sure you want to delete the file, and all the tracked download information?", string.Empty);

      if (this.FileId < 1)
      {
        return;
      }

      df = DownloadFile.Get(FileId);

      if (df == null)
      {
        return;
      }

      if (!Page.IsPostBack && !Page.IsCallback)
        SetControlValues();
    }

    #region Button Clicks

    private void btnUpload_Click(object sender, EventArgs e)
    {
      if (df == null)
        return; // error state
      try
      {
        var uploadPath = DownloadFileUtil.GetDownloadFileFilePath(df);
        var sizeInKB = (int)(this.fileUploader.PostedFile.ContentLength / 1024);
        if (sizeInKB == 0)
          sizeInKB = 1;


        this.fileUploader.PostedFile.SaveAs(uploadPath);

        df.SizeInKB = sizeInKB;
        df.OriginalFileName = Path.GetFileName(this.fileUploader.PostedFile.FileName);


        df.Save();

        this.phReUpload.Visible = false;
        this.phFileInfo.Visible = true;


        SetControlValues();
      }
      catch (Exception ex)
      {
        log.Error("Error while reuploading file ", ex);

        //this.phError.Visible = true;
        //this.labelError.Text = ex.Message;
        this.pnlWrapper.ShowErrorMessageBanner("Error while re-uploading file : " + ex.Message);
      }
    }

    private void btnCancelUpload_Click(object sender, EventArgs e)
    {
      this.phReUpload.Visible = false;
      this.phFileInfo.Visible = true;

    }

    private void btnShowReUpload_Click(object sender, EventArgs e)
    {
      this.phReUpload.Visible = true;
      this.phFileInfo.Visible = false;

    }

    private void btnDelete_Click(object sender, EventArgs e)
    {

      if (df == null)
        return;

      bool success = false;
      try
      {
        var filePath = DownloadFileUtil.GetDownloadFileFilePath(df);

        df.Delete();

        if (File.Exists(filePath))
          File.Delete(filePath);


        success = true;
      }
      catch (Exception ex)
      {
        log.Error(ex);


        this.pnlWrapper.ShowErrorMessageBanner("An error occured while deleting : " + ex.Message);
      }

      if (success)
      {
        Response.Redirect("~/Quantifi/Controls/DownloadModule/ViewFiles.aspx");
        return;

      }

    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      if (!Page.IsValid)
        return;

      GetControlValues();

      try
      {
        df.Save();


        this.pnlWrapper.ShowSuccessMessageBanner("Download File Saved");

        //lblMessage.Visible = true;
      }
      catch (Exception ex)
      {
        log.Error(ex);



        this.pnlWrapper.ShowErrorMessageBanner("An error occured while saving : " + ex.Message);
      }
    }

    #endregion

    private void LoadParams()
    {
      pageNum = WebUtils.ParseInt32FromQueryString("pageNum", pageNum);
      showFormActivity = WebUtils.ParseBoolFromQueryString("showForm", showFormActivity);
    }

    private void SetControlValues()
    {
      this.phReUpload.Visible = false;

      this.lblID.Text = df.Id.ToString();
      this.textboxDisplayName.Text = df.DisplayName;
      this.textboxDisplayDescription.Text = df.DisplayDescription;


      var SizeInMB = (int)(df.SizeInKB / 1024);
      if (SizeInMB > 1)
      {
        this.lblSizeInKB.Text = SizeInMB + " MB";
      }
      else
      {
        this.lblSizeInKB.Text = df.SizeInKB + " KB";
      }

      this.lblDownloadCount.Text = df.DownloadCount.ToString();
      this.lblGuid.Text = df.Guid.ToString();
      this.lblOriginalFileName.Text = df.OriginalFileName;
      this.lblServerFileName.Text = df.ServerFileName;


      this.Page.Title = string.Format("Edit File : {0}  ", df.DisplayName);

      this.lblCreatedTime.Text = df.CreatedTime.ToString("D");
      this.lblCreatedUser.Text = df.CreatedUser;

      this.lblLastUpdatedTime.Text = df.LastUpdatedTime.ToString("D");
      this.lblLastUpdatedUser.Text = df.LastUpdatedUser;


      this.textboxDownloadUrl.Text = DownloadFileUtil.GetRelativeDownloadUrl(df);
      this.textboxDownloadUrl.ReadOnly = true;

      this.linkDownloadTest.NavigateUrl = DownloadFileUtil.GetRelativeDownloadUrl(df);

      this.checkBoxIsActive.Checked = df.isActive;
      this.txtEmail.Text = df.AdminEmailAddress;
      this.checkBoxEmailActivity.Checked = df.EmailUserActivity;
      this.checkBoxRequireForm.Checked = df.RequireForm;

      this.checkBoxEmailFile.Checked = df.EmailFilesToUser;
      this.checkBoxAllowRestricedEmails.Checked = df.EmailFilesToRestrictedEmails;



      var filePath = DownloadFileUtil.GetDownloadFileFilePath(df);
      if (!File.Exists(filePath))
      {
        this.phFileCheck.Visible = true;
        this.lblFileCheckError.Text = "Error, Unable to locate file.";
      }


      if (!FriendlyUrl.Exists(1, "QDownloads.aspx"))
      {
        FriendlyUrl furl = new FriendlyUrl();
        furl.SiteId = 1;
        furl.RealUrl = "~/Quantifi/Controls/DownloadModule/Download.aspx";
        furl.Url = "QDownloads.aspx";
        furl.Save();
      }

      LoadActivity();
    }

    private void LoadActivity()
    {
      var fileActivityCounts = DownloadFileActivity.GetCount(df.Id);

      if (fileActivityCounts > 0)
      {
        this.linkDownloadActivity.Text = string.Format("Download Activity ({0:#,##0})", fileActivityCounts);
        this.linkDownloadActivity.NavigateUrl = string.Format("../edit.aspx?fileId={0}&pagenum={1}&showform=false", FileId, pageNum);
        this.linkExportActivityCSV.NavigateUrl = "../ActivityHandler.ashx?showform=false&fileId=" + df.Id;
      }

      var fileFormActivityCounts = DownloadFileFormActivity.GetCount(df.Id);

      if (fileFormActivityCounts > 0)
      {
        this.linkDownloadFormActivity.Text = string.Format("Download Form Activity ({0:#,##0})", fileFormActivityCounts);
        this.linkDownloadFormActivity.NavigateUrl = string.Format("../edit.aspx?fileId={0}&pagenum={1}&showform=true", FileId, pageNum);
        this.linkExportFormActivityCSV.NavigateUrl = "../ActivityHandler.ashx?showform=true&fileId=" + df.Id;
      }

      if (showFormActivity)
      {
        if (fileFormActivityCounts == 0 && fileActivityCounts > 0)
        {
          LoadFileActivity();
        }
        else
        {
          LoadFileFormActivity();
        }
      }
      else
      {
        if (fileActivityCounts == 0 && fileFormActivityCounts > 0)
        {
          LoadFileFormActivity();
        }
        else
        {
          LoadFileActivity();
        }
      }

    }

    private void LoadFileActivity()
    {
      var activity = DownloadFileActivity.GetActivityByPage(df.Id, pageSize, pageNum, out totalPages);

      if (activity.IsNullOrEmpty())
      {
        this.dgDownloadActivity.Visible = false;

      }
      else
      {
        this.pnlDownloadActivity.Visible = true;
        this.dgDownloadActivity.Visible = true;

        this.dgDownloadActivity.DataSource = activity;
        this.dgDownloadActivity.DataBind();
      }

      string pageUrlFormat = "?fileId=" + this.FileId.ToString() + "&pagenum={0}&showform=false";

      pgrDownloadActivity.PageURLFormat = pageUrlFormat;
      pgrDownloadActivity.ShowFirstLast = true;
      pgrDownloadActivity.PageSize = pageSize;
      pgrDownloadActivity.PageCount = totalPages;
      pgrDownloadActivity.CurrentIndex = pageNum;
      pgrDownloadActivity.Visible = (totalPages > 1);

    }

    private void LoadFileFormActivity()
    {


      var activity = DownloadFileFormActivity.GetPage(df.Id, pageNum, pageSize, out totalFormPages);

      if (activity.IsNullOrEmpty())
      {
        this.dgDownloadFormActivity.Visible = false;
      }
      else
      {
        this.pnlDownloadFormActivity.Visible = true;
        this.dgDownloadFormActivity.Visible = true;
        this.dgDownloadFormActivity.DataSource = activity;
        this.dgDownloadFormActivity.DataBind();

      }

      string pageUrlFormat = "?fileId=" + this.FileId.ToString() + "&pagenum={0}&showform=true";

      pgrDownloadFormActivity.PageURLFormat = pageUrlFormat;
      pgrDownloadFormActivity.ShowFirstLast = true;
      pgrDownloadFormActivity.PageSize = pageSize;
      pgrDownloadFormActivity.PageCount = totalFormPages;
      pgrDownloadFormActivity.CurrentIndex = pageNum;
      pgrDownloadFormActivity.Visible = (totalFormPages > 1);

    }

    private void GetControlValues()
    {
      if (df == null)
        return;

      df.DisplayName = Server.HtmlEncode(this.textboxDisplayName.Text);
      df.DisplayDescription = Server.HtmlEncode(this.textboxDisplayDescription.Text);
      df.isActive = this.checkBoxIsActive.Checked;
      df.RequireForm = this.checkBoxRequireForm.Checked;

      // df.RequireRequestVetting = this.checkBoxVetRequests.Checked;

      df.EmailUserActivity = this.checkBoxEmailActivity.Checked;


      df.EmailFilesToUser = this.checkBoxEmailFile.Checked;
      df.EmailFilesToRestrictedEmails = this.checkBoxAllowRestricedEmails.Checked;

      df.AdminEmailAddress = this.txtEmail.Text;
    }

    private string linkFormat = @"<a href='{0}' target='blank_' title='{1}'>{2}</a>";

    protected string GetUserHostAddressLink(DownloadFileActivity dfa)
    {

      if (string.IsNullOrWhiteSpace(dfa.UserHostAddress))
        return string.Empty;

      return string.Format(linkFormat, DownloadFileUtil.GetUserHostAddressLookUpUrl(dfa), dfa.UserHostAddress, dfa.UserHostAddress);

    }

    protected string GetReferrerLink(DownloadFileActivity dfa)
    {
      if (string.IsNullOrWhiteSpace(dfa.Referrer))
        return string.Empty;


      var displayValue = dfa.Referrer;

      if (displayValue.StartsWith(@"http://"))
      {
        displayValue = displayValue.Substring(7);
      }
      else if (displayValue.StartsWith(@"https://"))
      {
        displayValue = displayValue.Substring(8);
      }

      if (displayValue.StartsWith(@"www."))
        displayValue = displayValue.Substring(4);

      if (displayValue.Length > 65)
        displayValue = displayValue.Substring(0, 65) + "...";


      return string.Format(linkFormat, dfa.Referrer, dfa.Referrer, displayValue);





    }

    protected string GetCountry(DownloadFileFormActivity form)
    {
      if (form.Country == null)
        return string.Empty;

      return form.Country.Name.HtmlEncode();

    }

    protected string GetUserLocalTime(DownloadFileActivity dfa)
    {
      var localTime = dfa.TimeStamp.ToLocalTime();

      return localTime.ToShortDateString() + @"<br/>" + localTime.ToShortTimeString();
    }

  }
}