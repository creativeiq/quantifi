﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Business.DownloadModule;
using mojoPortal.Business;
using System.IO;
using mojoPortal.Web;
using log4net;
using Quantifi.UI.Utils;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.DownloadModule.Controls
{
  public partial class CreateDownloadFileControl : System.Web.UI.UserControl
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(CreateDownloadFileControl));

    protected string CustomValidatorJsFunctionName { get; private set; }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      this.customValidator_FileUpload.ServerValidate += new ServerValidateEventHandler(customValidator_FileUpload_ServerValidate);
      this.btnCreate.Click += new EventHandler(btnCreate_Click);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      this.btnCreate.Enabled = true;
      this.btnCreate.DisableButtonOnClick(string.Empty);


      SetupValidators();
    }


    private void CreateFile()
    {
      SiteUser linkUser = SiteUtils.GetCurrentSiteUser();
      DownloadFile df = new DownloadFile();


      var sizeInKB = (int)(this.fileUpload.PostedFile.ContentLength / 1024);

      df.Guid = Guid.NewGuid();
      df.DisplayName = this.textboxName.Text;
      df.DisplayDescription = this.textboxDesc.Text;
      df.OriginalFileName = Path.GetFileName(this.fileUpload.PostedFile.FileName);
      df.ServerFileName = df.Guid.ToString() + DownloadFile.DownFileServerFileExt;
      df.SizeInKB = (sizeInKB == 0) ? 1 : sizeInKB;
      df.DownloadCount = 0;
      df.isActive = false;
      df.LastUpdatedTime = DateTime.Now;
      df.LastUpdatedUser = linkUser.Name;
      df.CreatedTime = DateTime.Now;
      df.CreatedUser = linkUser.Name;

      df.RequireForm = false;
      df.EmailUserActivity = false;
      df.AdminEmailAddress = string.Empty;

      var folderPath = DownloadFileUtil.GetDownloadFileFolderPath(df);

      string uploadPath = DownloadFileUtil.GetDownloadFileFilePath(df);

      bool success = false;

      try
      {
        // MOVE FILE


        // Check to make sure FileDir Exists
        if (!Directory.Exists(folderPath))
        {
          Directory.CreateDirectory(folderPath);
        }

        if (File.Exists(uploadPath))
        {
          throw new Exception("Some how a file already exists with this name [" + df.ServerFileName + "]");
        }

        // Save file to Location
        fileUpload.PostedFile.SaveAs(uploadPath);

        // SAVE OBJECT TO DB
        df.Save();

        success = true;

      }
      catch (Exception ex)
      {

        log.Error(ex);
        this.pnlError.Visible = true;
        this.lblError.Text = ex.Message;
        this.litErrorBody.Text = ex.StackTrace;
      }


      if (success)
      {
        var redirect = "~/Quantifi/Controls/DownloadModule/Edit.aspx?" + DownloadFile.QS_DownFileId + "=" + df.Id.ToString();

        Response.Redirect(redirect);
        return;
      }
      else
      {
        // If the save was not a success clean up the file upload
        if (File.Exists(uploadPath))
        {
          File.Delete(uploadPath);
        }

      }
    }


    #region Events

    protected void btnCreate_Click(object sender, EventArgs e)
    {
      if (!Page.IsValid)
        return;

      CreateFile();


    }

    private void SetupValidators()
    {
      CustomValidatorJsFunctionName = customValidator_FileUpload.ClientID + "_Validate";
      this.customValidator_FileUpload.ClientValidationFunction = CustomValidatorJsFunctionName;
    }

    protected void customValidator_FileUpload_ServerValidate(object source, ServerValidateEventArgs args)
    {

      if (fileUpload.PostedFile == null || string.IsNullOrWhiteSpace(fileUpload.PostedFile.FileName))
      {
        args.IsValid = false;
      }
      else if (fileUpload.PostedFile.ContentLength > 2147483648)
      {
        customValidator_FileUpload.Text = "File Too Large. Files must be under 2 Gb";
        args.IsValid = false;
      }
      else
      {
        args.IsValid = true;
      }
    }

    #endregion

  }
}