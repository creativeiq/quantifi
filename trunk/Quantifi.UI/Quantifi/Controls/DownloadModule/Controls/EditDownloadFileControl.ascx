﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditDownloadFileControl.ascx.cs"
  Inherits="Quantifi.UI.Controls.DownloadModule.Controls.EditDownloadFileControl" %>
<%@ Import Namespace="Quantifi.Business.DownloadModule" %>
<style type="text/css">
  span.help-me
  {
    position: relative;
    padding: 0px 5px;
    background-image: url(/Data/SiteImages/FeatureIcons/help.gif);
    background-repeat: no-repeat;
    background-position: center center;
    height: 12px;
    width: 12px;
    display: inline-block;
  }
  
  span.help-me .tip
  {
    display: none;
    position: absolute;
    z-index: 1000;
    background-color: White;
    border: 1px solid #000000;
  }
  
  span.help-me .tip p
  {
    padding: 15px;
    margin: 0;
    min-width: 100px;
  }
</style>
<script type="text/javascript">

  $(document).ready(function () {

    $(".wrapperDownloadFile span.help-me").qtip({
      content: {
        attr: 'tip',
        title: { text: 'Help Tips:' }
      },
      style: {
        classes: 'ui-tooltip-tipped ui-tooltip-shadow'
      },
      position: {
        my: 'left top',
        at: 'right center'
      }
    });

  });

</script>
<fieldset class="wrapperDownloadFile">
  <legend>Download File </legend>
  <asp:Panel ID="pnlWrapper" runat="server">
    <div class="left">
      <asp:PlaceHolder ID="phReUpload" runat="server">
        <table class="formDownloadFile">
          <tr>
            <th colspan="2">
              Re-Upload File
            </th>
          </tr>
          <tr>
            <td colspan="2">
              <asp:FileUpload ID="fileUploader" runat="server" />
            </td>
          </tr>
          <tr>
            <td align="left">
              <asp:Button ID="btnUpload" runat="server" Text="Upload" />
            </td>
            <td align="right">
              <asp:Button ID="btnCancelUpload" runat="server" Text="Cancel" />
            </td>
          </tr>
        </table>
      </asp:PlaceHolder>
      <asp:Panel ID="phFileInfo" runat="server" Visible="true">
        <table class="formDownloadFile">
          <asp:PlaceHolder ID="phFileCheck" runat="server" Visible="false">
            <tr>
              <th colspan="2" class="FileError">
                <asp:Label ID="lblFileCheckError" runat="server" ForeColor="Red" Font-Size="14px" />
              </th>
            </tr>
          </asp:PlaceHolder>
          <tr class=" settings">
            <th colspan="2" class="Original">
              Original FileName
            </th>
          </tr>
          <tr class=" settings">
            <td colspan="2" class="Original settings">
              <asp:Label ID="lblOriginalFileName" runat="server" />
            </td>
          </tr>
          <tr class=" settings">
            <th class="Name">
              Display Name
            </th>
            <td class="Name settings">
              <asp:TextBox ID="textboxDisplayName" runat="server" MaxLength="255" Width="90%" Font-Size="11px" />
              <asp:RequiredFieldValidator runat="server" SetFocusOnError="true" ControlToValidate="textboxDisplayName"
                Display="Dynamic" CssClass="validator" EnableClientScript="true" ValidationGroup="EditFileGrp">Display Name is Required</asp:RequiredFieldValidator>
              <script type="text/javascript">
                $(function () {
                  $(document).ready(function () {
                    $("#<%=textboxDisplayName.ClientID %>").blur(function () { Quantifi.util.callMyValidators(this) });
                  });
                });
              </script>
            </td>
          </tr>
          <tr class=" settings">
            <th class="Description">
              Display Description
            </th>
            <td class="Description settings">
              <asp:TextBox ID="textboxDisplayDescription" runat="server" TextMode="MultiLine" Width="90%"
                Rows="1" Font-Size="11px" Font-Names="Arial" />
            </td>
          </tr>
          <tr class=" settings">
            <th colspan="2">
              Download Url
            </th>
          </tr>
          <tr class=" settings">
            <td colspan="2" class=" settings">
              <asp:TextBox ID="textboxDownloadUrl" runat="server" ReadOnly="true" Width="330px"
                Font-Size="11px" />
              <asp:HyperLink ID="linkDownloadTest" runat="server" Target="_blank" ToolTip="Test Link"><img src="/Data/SiteImages/markread.gif" alt="Test Link" /></asp:HyperLink>
              <script type="text/javascript">
                $(document).ready(function () {
                  $("#<%=textboxDownloadUrl.ClientID %>").click(function () {
                    this.focus();
                    this.select();
                  });
                });
              </script>
            </td>
          </tr>
          <tr class=" settings">
            <th colspan="2">
              <hr />
            </th>
          </tr>
          <tr class=" settings">
            <td colspan="2">
              <asp:CheckBox ID="checkBoxIsActive" runat="server" Text="IsActive : Must be active for users to download"
                ToolTip="Must be checked active for users to download" CssClass="isActive chbox" />
              <span class="help-me" tip="Email file to user."></span>
              <script type="text/javascript">
                $(function () {
                  var checkbox = $("#<%=checkBoxIsActive.ClientID %>");
                  var cbLabel = checkbox.parent().children("label");

                  function UpdateCheckStatus() {
                    if (checkbox.is(':checked')) {
                      cbLabel.addClass("status-good").removeClass("status-warning");

                    }
                    else {
                      cbLabel.addClass("status-warning").removeClass("status-good");
                    }
                  }

                  checkbox.change(function () { UpdateCheckStatus(this); });

                  $(document).ready(function () {
                    UpdateCheckStatus();
                  });

                });
					
              </script>
            </td>
          </tr>
          <tr class=" settings">
            <td colspan="2">
              <asp:CheckBox ID="checkBoxRequireForm" runat="server" Text="Require Form entry from user"
                ToolTip="" CssClass="chbox" />
              <span class="help-me" tip="Require user to enter there contact information, if emailing is disabled, the request will be recorded">
              </span>
            </td>
          </tr>
          <tr class=" settings">
            <td colspan="2">
              <asp:CheckBox ID="checkBoxEmailFile" runat="server" Text="Email file to user after form"
                ToolTip="" CssClass="chbox" />
              <span class="help-me" tip="Email file to user after form entry is filled out. You would want to disable this if you want to vet all requests">
              </span>
            </td>
          </tr>
          <tr class="settings ">
            <td colspan="2">
              <asp:CheckBox ID="checkBoxAllowRestricedEmails" runat="server" Text="Allow users with restricted emails to get emailed files"
                CssClass="chbox restriced" />
              <span class="help-me" tip="Allow users with restricted emails to get auto emailed files, but by default this probably should be disabled as poor">
              </span>
              <script type="text/javascript">
                $(function () {
                  var checkbox = $("#<%=checkBoxAllowRestricedEmails.ClientID %>");
                  var cbLabel = checkbox.parent().children("label");

                  function UpdateCheckStatus() {
                    if (!checkbox.is(':checked')) {
                      cbLabel.addClass("status-good").removeClass("status-warning");

                    }
                    else {
                      cbLabel.addClass("status-warning").removeClass("status-good");
                    }
                  }

                  checkbox.change(function () { UpdateCheckStatus(this); });

                  $(document).ready(function () {
                    UpdateCheckStatus();
                  });

                });
					
              </script>
            </td>
          </tr>
          <tr class="settings">
            <td colspan="2">
              <asp:CheckBox ID="checkBoxEmailActivity" runat="server" Text="Email Request Activity to Admin Email"
                CssClass="chbox" />
              <span class="help-me" tip="Email download activity to the admin email, all the form info is saved either way">
              </span>
            </td>
          </tr>
          <tr class="settings">
            <th class="" style="vertical-align: middle;">
              Override Admin Email
            </th>
            <td class="adminEmail">
              <div class="">
                <asp:TextBox ID="txtEmail" runat="server" AutoCompleteType="Email" Width="250" />
                <asp:RegularExpressionValidator ID="regexEmail" runat="server" EnableClientScript="true"
                  ValidationGroup="EditFileGrp" Display="Dynamic" ControlToValidate="txtEmail" CssClass="validator"
                  ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,9})$"
                  ErrorMessage="Please enter a valid email">Please enter a valid email</asp:RegularExpressionValidator>
                <span class="help-me" tip='The default email address: <%= Quantifi.UI.QuantifiSettings.DownloadFileDefaultAdminEmailAddress %>'>
                </span>
              </div>
              <script type="text/javascript">
                $(function () {
                  $(document).ready(function () {
                    $("#<%=txtEmail.ClientID %>").blur(function () { Quantifi.util.callMyValidators(this) });
                  });
                });
              </script>
            </td>
          </tr>
          <tr class="settings">
            <th class="SizeInKB">
              File Size
            </th>
            <td class=" SizeInKB ">
              <asp:Label ID="lblSizeInKB" runat="server" />
            </td>
          </tr>
          <tr class="settings">
            <th class="Count">
              DownloadCount
            </th>
            <td class="Count ">
              <asp:Label ID="lblDownloadCount" runat="server" />
            </td>
          </tr>
        </table>
        <script type="text/javascript">
          $(document).ready(function () {
            var chkFormRequired = $('#<%= checkBoxRequireForm.ClientID %>');

            var chkEmailActivity = $('#<%= checkBoxEmailActivity.ClientID %>');

            function FormUpdate() {
              if (!chkEmailActivity.is(':checked') && (chkFormRequired.is(':checked'))) {
                chkEmailActivity.next().addClass('status-yellow-warning');
              }
              else {
                chkEmailActivity.next().removeClass('status-yellow-warning');
              }
            }

            chkFormRequired.change(FormUpdate);
            chkEmailActivity.change(FormUpdate);

            FormUpdate();

          });
        </script>
        <br />
        <div id="moreInfo" class="">
          <h3 class="head">
            <a href="#">More info</a></h3>
          <div>
            <table class="formDownloadFile">
              <tr>
                <th class="Id">
                  ID
                </th>
                <td class="Id settings">
                  <asp:Label ID="lblID" runat="server" />
                </td>
              </tr>
              <tr>
                <th class="Guid">
                  Guid
                </th>
                <td class="Guid settings">
                  <asp:Label ID="lblGuid" runat="server" />
                </td>
              </tr>
              <tr>
                <th class="Server">
                  Server FileName
                </th>
                <td class="Server settings">
                  <asp:Label ID="lblServerFileName" runat="server" />
                </td>
              </tr>
              <tr>
                <th class="luTime">
                  LastUpdatedTime
                </th>
                <td class="luTime settings">
                  <asp:Label ID="lblLastUpdatedTime" runat="server" />
                </td>
              </tr>
              <tr>
                <th class="luUser">
                  LastUpdatedUser
                </th>
                <td class="luUser settings">
                  <asp:Label ID="lblLastUpdatedUser" runat="server" />
                </td>
              </tr>
              <tr>
                <th class="cuTime">
                  CreatedTime
                </th>
                <td class="cuTime settings">
                  <asp:Label ID="lblCreatedTime" runat="server" />
                </td>
              </tr>
              <tr>
                <th class="cuUser">
                  CreatedUser
                </th>
                <td class="cuUser settings">
                  <asp:Label ID="lblCreatedUser" runat="server" />
                </td>
              </tr>
            </table>
            <br />
          </div>
        </div>
        <script type="text/javascript">
          jQuery(document).ready(function () {
            $('#moreInfo .head').click(function () {
              $(this).toggleClass('open').next().toggle('slow');
              return false;
            }).next().hide();
          });
		 
        </script>
        <table class="formDownloadFile" width="400px">
          <tr>
            <td colspan="2">
              <asp:Label ID="lblMessage" runat="server" Visible="false" Font-Bold="true">Download File Saved!</asp:Label>
            </td>
          </tr>
          <tr>
            <td class="buttons" colspan="2">
              <table width="100%">
                <tr>
                  <td align="left">
                    <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Save" CausesValidation="true"
                      ValidationGroup="EditFileGrp" />
                  </td>
                  <td align="center">
                    <asp:Button ID="btnShowReUpload" runat="server" Text="Re-Upload" ToolTip="Re upload file" />
                  </td>
                  <td align="center">
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" ToolTip="Delete File and Activity" />
                  </td>
                  <td align="right">
                    <a href="ViewFiles.aspx" title="Cancel any changes and Exit">Cancel</a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </asp:Panel>
    </div>
    <div class="right" style="max-width: 700px;">
      <div class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons">
        <h3 id="FormActivityHeader" class="ui-accordion-header ui-helper-reset ui-state-default ui-state-default ui-corner-top">
          <asp:HyperLink ID="linkDownloadFormActivity" runat="server">Download Form Activity (0)</asp:HyperLink></h3>
        <asp:Panel ID="pnlDownloadFormActivity" runat="server" Visible="false" CssClass="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom">
          <div>
            <asp:HyperLink ID="linkExportFormActivityCSV" runat="server" Font-Overline="false"
              Font-Size="12px" Target="_blank"><img src="/Data/SiteImages/Icons/xls.png" 
					alt="csv" /> Export To CSV</asp:HyperLink>
          </div>
          <div style="overflow: auto; max-width: 600px" class="tblGrid">
            <asp:DataGrid ID="dgDownloadFormActivity" runat="server" GridLines="None" AutoGenerateColumns="false"
              Font-Size="11px" EnableTheming="false">
              <HeaderStyle Font-Bold="true" CssClass=" headerRow" />
              <AlternatingItemStyle CssClass=" altRow" />
              <ItemStyle CssClass="itemRow" />
              <Columns>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    Time Stamp</HeaderTemplate>
                  <ItemTemplate>
                    <%# GetUserLocalTime(((DownloadFileFormActivity)Container.DataItem).Activity) %>
                  </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="FirstName" HeaderText="First Name"></asp:BoundColumn>
                <asp:BoundColumn DataField="LastName" HeaderText="Last Name"></asp:BoundColumn>
                <asp:BoundColumn DataField="JobTitle" HeaderText="Job Title"></asp:BoundColumn>
                <asp:BoundColumn DataField="CompanyName" HeaderText="Company"></asp:BoundColumn>
                <asp:BoundColumn DataField="Email" HeaderText="Email"></asp:BoundColumn>
                <asp:BoundColumn DataField="Phone" HeaderText="Phone"></asp:BoundColumn>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    Country</HeaderTemplate>
                  <ItemTemplate>
                    <%# GetCountry(((DownloadFileFormActivity)Container.DataItem))  %>
                  </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    OptIn</HeaderTemplate>
                  <ItemTemplate>
                    <asp:CheckBox runat="server" Checked="<%# ((DownloadFileFormActivity)Container.DataItem).MarketingOptIn %>"
                      Enabled="false" />
                  </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    User IP</HeaderTemplate>
                  <ItemTemplate>
                    <%# GetUserHostAddressLink(((DownloadFileFormActivity)Container.DataItem).Activity)%>
                  </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    Referrer</HeaderTemplate>
                  <ItemStyle Wrap="false" />
                  <ItemTemplate>
                    <%# GetReferrerLink(((DownloadFileFormActivity)Container.DataItem).Activity)%>
                  </ItemTemplate>
                </asp:TemplateColumn>
              </Columns>
            </asp:DataGrid>
          </div>
          <div>
            <mp:CutePager ID="pgrDownloadFormActivity" runat="server" />
          </div>
        </asp:Panel>
        <h3 id="ActivityHeader" class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
          <asp:HyperLink ID="linkDownloadActivity" runat="server">Download Activity (0)</asp:HyperLink></h3>
        <asp:Panel ID="pnlDownloadActivity" runat="server" Visible="false" CssClass="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom">
          <div>
            <asp:HyperLink ID="linkExportActivityCSV" runat="server" Font-Size="12px" Font-Overline="false"
              Target="_blank"><img src="/Data/SiteImages/Icons/xls.png" 
					alt="csv" /> Export To CSV</asp:HyperLink>
          </div>
          <div style="overflow: auto; max-width: 600px" class="tblGrid">
            <asp:DataGrid ID="dgDownloadActivity" runat="server" AutoGenerateColumns="false"
              EnableTheming="false" GridLines="None" Font-Size="11px">
              <HeaderStyle Font-Bold="true" CssClass=" headerRow" />
              <AlternatingItemStyle CssClass=" altRow" />
              <ItemStyle CssClass="itemRow" />
              <Columns>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    User IP</HeaderTemplate>
                  <ItemTemplate>
                    <%# GetUserHostAddressLink((DownloadFileActivity)Container.DataItem)%>
                  </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    Referrer</HeaderTemplate>
                  <ItemStyle Wrap="false" />
                  <ItemTemplate>
                    <%# GetReferrerLink((DownloadFileActivity)Container.DataItem)%>
                  </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                  <HeaderTemplate>
                    TimeStamp</HeaderTemplate>
                  <ItemStyle Wrap="false" />
                  <ItemTemplate>
                    <%# GetUserLocalTime((DownloadFileActivity)Container.DataItem)%>
                  </ItemTemplate>
                </asp:TemplateColumn>
              </Columns>
            </asp:DataGrid>
          </div>
          <div>
            <mp:CutePager ID="pgrDownloadActivity" runat="server" />
          </div>
        </asp:Panel>
      </div>
      <script type="text/javascript">


        $(document).ready(function () {
          var dfa = $("#<%=pnlDownloadFormActivity.ClientID %>");

          if (dfa != null && dfa.length > 0) {
            $("#FormActivityHeader").addClass("ui-state-active").removeClass("ui-state-default");
            dfa.css("display", "block");
          }

          var da = $("#<%=pnlDownloadActivity.ClientID %>");
          if (da != null && da.length > 0) {
            $("#ActivityHeader").addClass("ui-state-active").removeClass("ui-state-default");

            da.css("display", "block");
          }


        });

      </script>
    </div>
  </asp:Panel>
</fieldset>
