﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DownloadModule.ascx.cs"
  Inherits="Quantifi.UI.Controls.DownloadModule.DownloadModule" %>
<%--FORM --%>
<asp:PlaceHolder ID="phForm" runat="server" Visible="false">
  <asp:Panel ID="pnlDownloadForm" runat="server" CssClass="downloadfileform">
    <div>
      Thank you for your interest in Quantifi. To receive a copy of the file please fill in your details.</div>
    <p id="validationTips" runat="server" class="validateTips" style="display: none;">
    </p>
    <fieldset>
      <asp:Label runat="server" AssociatedControlID="txtFirstName" CssClass="label" EnableViewState="false">First Name:*</asp:Label>
      <asp:TextBox ID="txtFirstName" runat="server" TabIndex="1" CssClass="text ui-widget-content ui-corner-all"
        AutoCompleteType="FirstName">
      </asp:TextBox>
      <asp:Label runat="server" AssociatedControlID="txtLastName" CssClass="label" EnableViewState="false">Last Name:*</asp:Label>
      <asp:TextBox ID="txtLastName" runat="server" TabIndex="2" CssClass="text ui-widget-content ui-corner-all"
        AutoCompleteType="LastName">
      </asp:TextBox>
      <asp:Label runat="server" AssociatedControlID="txtJobTitle" CssClass="label" EnableViewState="false">Job Title:*</asp:Label>
      <asp:TextBox ID="txtJobTitle" runat="server" TabIndex="3" CssClass="text ui-widget-content ui-corner-all"
        AutoCompleteType="JobTitle">
      </asp:TextBox>
      <asp:Label runat="server" AssociatedControlID="txtCompany" CssClass="label" EnableViewState="false">Company:*</asp:Label>
      <asp:TextBox ID="txtCompany" runat="server" TabIndex="4" CssClass="text ui-widget-content ui-corner-all"
        AutoCompleteType="Company">
      </asp:TextBox>
      <asp:Label runat="server" AssociatedControlID="txtEmail" CssClass="label" EnableViewState="false">Email:* Must be a valid <span style="font-weight: bold;">business</span> email</asp:Label>

      <asp:TextBox ID="txtEmail" runat="server" TabIndex="5" CssClass="text ui-widget-content ui-corner-all"
        AutoCompleteType="Email">
      </asp:TextBox>
      <asp:Label runat="server" AssociatedControlID="txtPhone" CssClass="label" EnableViewState="false">Phone:</asp:Label>
      <asp:TextBox ID="txtPhone" runat="server" TabIndex="6" CssClass="text ui-widget-content ui-corner-all"
        AutoCompleteType="BusinessPhone">
      </asp:TextBox>
      <asp:Label ID="lblCountry" runat="server" AssociatedControlID="ddlCountry" CssClass="label"
        EnableViewState="false">Country:</asp:Label>
      <asp:DropDownList ID="ddlCountry" runat="server" TabIndex="7" CssClass="ddl ui-widget-content ui-corner-all">
      </asp:DropDownList>
      <div class="optin">
        <asp:CheckBox ID="checkboxOptIn" runat="server" TabIndex="8" Text=" Please check if you do not wish to receive occasional information from Quantifi" />
      </div>
      <div class="privacy">
        Your privacy is important to us and we won’t share your email with anyone else.
        Read more in our <a title="View our Privacy Policy" target="_blank" href="/privacy-policy.aspx">
          Privacy Policy</a>.
      </div>
      <div>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
      </div>
    </fieldset>
  </asp:Panel>
  <asp:Literal ID="litEmailDomainErrorMsg" runat="server" Visible="false" ViewStateMode="Disabled">Sorry, we are unable to send requests to the email provided at this time. Please contact us at <a href='mailto:enquire@quantifisolutions.com?subject=Literature Request Problem'>enquire@quantifisolutions.com</a> if there are any problems.</asp:Literal>
  <script type="text/javascript">
    $(function () {
      var fname = $("#<%= this.txtFirstName.ClientID %>");
      var lname = $("#<%= this.txtLastName.ClientID %>");
      var jobtitle = $("#<%= this.txtJobTitle.ClientID %>");
      var company = $("#<%= this.txtCompany.ClientID %>");
      var email = $("#<%= this.txtEmail.ClientID %>");

      var phone = $("#<%= this.txtPhone.ClientID %>");
      var country = $("#<%= this.ddlCountry.ClientID %>");

      var tips = $("#<%= this.validationTips.ClientID %>");
      var allFields = $([]).add(fname).add(lname).add(jobtitle).add(company).add(email);

      var emailErrMsg = "<%= this.litEmailDomainErrorMsg.Text %>";


      //function initForm() {

      //  fname.val($.cookie("u.fname"));
      //  lname.val($.cookie("u.lname"));
      //  jobtitle.val($.cookie("u.jobtitle"));
      //  company.val($.cookie("u.company"));
      //  email.val($.cookie("u.email"));
      //  phone.val($.cookie("u.phone"));

      //  if ($.cookie("u.country") != null) {
      //    var newSelection = country.find('option[value = "' + $.cookie("u.country") + '"]');
      //    if (newSelection.length > 0) {
      //      var oldSelection = country.find(':selected').attr("selected", false);
      //      newSelection.attr("selected", true);
      //    }
      //  }
      //}

      //function saveForm() {

      //  $.cookie("u.fname", fname.val(), { expires: 90 });
      //  $.cookie("u.lname", lname.val(), { expires: 90 });
      //  $.cookie("u.jobtitle", jobtitle.val(), { expires: 90 });
      //  $.cookie("u.company", company.val(), { expires: 90 });
      //  $.cookie("u.email", email.val(), { expires: 90 });
      //  $.cookie("u.phone", phone.val(), { expires: 90 });

      //  var option = country.find(':selected').val();
      //  if (option != null)
      //    $.cookie("u.country", option, { expires: 90 });

      //}



      function updateTips(t) {
        tips.html(t).show().css({ backgroundColor: '#cd0a0a', border: '1px solid #cd0a0a', color: '#FFFFFF' });

        setTimeout(function () {
          tips.animate({ backgroundColor: "#fff", color: "#000" }, 2000)
        }, 500);
      }

      function checkLength(o, n, min) {
        if (o.val().length < min) {
          o.addClass("ui-state-error");
          updateTips("Length of " + n + " must be more than " +
	          min + ".");
          return false;
        } else {
          return true;
        }
      }

      function checkDropdown(o, n) {
        if (o.val() == "") {
          o.addClass("ui-state-error");
          updateTips("Please make a " + n + " selection");
          return false;
        } else {
          return true;
        }
      }

      function checkRegexp(o, regexp, n) {
        if (!(regexp.test(o.val()))) {
          o.addClass("ui-state-error");
          updateTips(n);
          return false;
        } else {
          return true;
        }
      }

      function validateForm() {


        var bValid = true;
        allFields.removeClass("ui-state-error");

        bValid = bValid && checkLength(fname, "First Name", 2);
        bValid = bValid && checkLength(lname, "Last Name", 2);
        bValid = bValid && checkLength(jobtitle, "Job Title", 2);
        bValid = bValid && checkLength(company, "Company", 2);
        bValid = bValid && checkLength(email, "Email", 6);
        
        bValid = bValid && checkDropdown(country, "Country");

        // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
        bValid = bValid && checkRegexp(email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "Please enter valid email: eg. joe@mycompany.com");

        if (bValid) {
          //saveForm();
          $("#<%= this.btnSubmit.ClientID %>").click();
        }

        return bValid;
      }
      function disableUIButton(b) {
        b.addClass('ui-state-disabled').attr('disabled', true).find('span').text("Please wait");
      }
      function enableUIButton(b) {
        b.removeClass('ui-state-disabled').attr('disabled', false).find('span').text("Submit");
      }

      $("#<%= this.pnlDownloadForm.ClientID %>").dialog({
        title: "Literature Request",
        width: 350,
        modal: true,
        hide: "drop",
        open: function (type, data) { $(this).parent().appendTo("form"); },
        buttons: {
          "Submit": function (e) {
            var bttn = $(e.target);

            var modalParent = $('#<%= this.pnlDownloadForm.ClientID %>').parent();
            modalParent.css("cursor", "wait");

            disableUIButton(bttn);

            if (validateForm() === false) {
              enableUIButton(bttn);
              modalParent.css("cursor", "default");
            }
          },
          "Cancel": function () {
            $(this).dialog("close");
          }
        }
      });
      //END DIALOG

      $(document).ready(function () {
        initForm();

        _gaq.push(['_trackEvent', 'Downloads', 'FormViewed', '<%= this.CleanFileName %>']);


        var msg = "<% = this.ErrorMsg %>";
        if (msg != null && msg.length) {
          updateTips(msg);
        }

        $(document).keyup(function (event) {
          if (event.which == '13' && $("#<%= this.pnlDownloadForm.ClientID %>").dialog("isOpen")) {
            event.preventDefault();
            validateForm();
          }
        });

      });
      //END DOCUMENT READY

    });
  </script>
</asp:PlaceHolder>
<%--THANK YOU--%>
<asp:PlaceHolder ID="phThankYou" runat="server" Visible="false">
  <asp:Panel ID="pnlDownloadFormThankYou" runat="server" CssClass="downloadfileform"
    EnableViewState="false">
    <p>
      Thank you for your request.</p>
    <p>
      We will send a download link to the email address provided. If you have not received
      this email, please check your spam folder. For further information, please contact
      us at <a title="Please contact us for further information" href="mailto:enquire@quantifisolutions.com?subject=Literature Request">
        enquire@quantifisolutions.com</a>
    </p>
    <h3>
      Request a demo and experience Quantifi’s award-winning solutions
    </h3>
  </asp:Panel>
  <script type="text/javascript">
    $(function () {
      $("#<%= this.pnlDownloadFormThankYou.ClientID %>").dialog({
        title: "Request Successful",
        width: 350,
        modal: true,
        dialogClass: "dlm tyd",
        hide: "drop",
        buttons: {
          "Yes": function () {
            _gaq.push(['_trackEvent', 'Downloads', 'Request-A-Demo', '<%= this.CleanFileName %>']);
            setTimeout(function () {
              window.location = '<%= SiteUtils.GetNavigationSiteRoot() + "/request-a-demo.aspx" %>';
            }, 100);
          },
          "No": function () { $(this).dialog("close"); }
        }
      });

      $(document).ready(function () {
        _gaq.push(['_trackEvent', 'Downloads', 'FormSubmit', '<%= this.CleanFileName %>']);
      });

    });
  </script>
</asp:PlaceHolder>
<%--THANK YOU RESTRICTED--%>
<asp:PlaceHolder ID="phThankYouRestricted" runat="server" Visible="false">
  <asp:Panel ID="pnlThankYouRestricted" runat="server" CssClass="downloadfileform"
    EnableViewState="false">
    <p>
      Thank you for your request.</p>
    <p>
      The requested document will be sent after approval, which typically takes less than
      one business day. We thank you for your patience. For further information, please
      contact us at <a title="Please contact us for further information" href="mailto:enquire@quantifisolutions.com?subject=Literature Request">
        enquire@quantifisolutions.com</a>
    </p>
    <h3>
      Request a demo and experience Quantifi’s award-winning solutions
    </h3>
  </asp:Panel>
  <script type="text/javascript">
    $(function () {
      $("#<%= this.pnlThankYouRestricted.ClientID %>").dialog({
        title: "Request Submitted",
        width: 350,
        modal: true,
        dialogClass: "dlm tyd",
        hide: "drop",
        buttons: {
          "Yes": function () {
            _gaq.push(['_trackEvent', 'Downloads', 'Request-A-Demo-Restricted', '<%= this.CleanFileName %>']);
            setTimeout(function () {
              window.location = '<%= SiteUtils.GetNavigationSiteRoot() + "/request-a-demo.aspx" %>';
            }, 100);
          },
          "No": function () { $(this).dialog("close"); }
        }
      });

      $(document).ready(function () {
        _gaq.push(['_trackEvent', 'Downloads', 'FormSubmit-Restricted', '<%= this.CleanFileName %>']);

      });

    });
  </script>
</asp:PlaceHolder>
