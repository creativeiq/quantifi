﻿
using System;
using System.IO;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Web;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;
using log4net;
using mojoPortal.Web;
using Quantifi.Business.DownloadModule;
using Quantifi.Shared;
using System.Collections.Specialized;

namespace Quantifi.UI.Controls.DownloadModule
{
  public partial class Download : mojoBasePage
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(Download));

    #region Props

    protected bool trackDownload = true;
    protected Guid fileGuid = Guid.Empty;
    protected DownloadFile downloadfile = null;
    private DateTime linkDate;
    private int formActivityId = -1;
    //private bool redirectToTrackingPage = false;
    private DateTime lastAccessTime;

    private int pageNum = 0;

    #endregion


    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      LoadParams();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

      SecurityHelper.DisableDownloadCache();


      if (fileGuid.Equals(Guid.Empty))
      {
        SiteUtils.RedirectToAccessDeniedPage(this);
        return;
      }

      downloadfile = DownloadFile.Get(fileGuid);

      if (downloadfile == null || !downloadfile.isActive)
      {
        SiteUtils.RedirectToAccessDeniedPage(this);
        return;
      }



      if (CheckFormStatus())
      {
        RedirectToForm(downloadfile);
        return;
      }


      if (RedirectToTrackingPage(downloadfile))
      {
        Response.Redirect(GetTrackingRedirectUrl());
        return;
      }



      StreamDownloadFile(downloadfile);

    }

    private bool RedirectToTrackingPage(DownloadFile file)
    {

      if (!QuantifiSettings.DownloadFiles_RedirectToTrackingPageFirst)
        return false;

      var browserCaps = Request.Browser as System.Web.Configuration.HttpCapabilitiesBase;
      if (browserCaps.Crawler)
      {
        log.InfoFormat("Request for download is coming from Web Crawler: [agent:{0}] ", (string.IsNullOrEmpty(Request.UserAgent)) ? "unknown" : Request.UserAgent);
        return false;
      }

      if (Request.IsAuthenticated)
        return false;


      var cookie = Request.Cookies[string.Format("df{0}", file.Id)];
      if (cookie == null)
        return true;



      if (!string.IsNullOrWhiteSpace(cookie.Value))
      {
        var ticks = Int64.Parse(cookie.Value, NumberStyles.HexNumber);

        lastAccessTime = new DateTime(ticks);

        if (DateTime.UtcNow <= (lastAccessTime.AddMinutes(QuantifiSettings.DownloadFiles_TrackingCookieTimeOutInMin)))
        {
          return false;
        }
        else
        {
          log.Info("Direct Download link is out of date: " + lastAccessTime.ToLocalTime().ToString());
          return true;
        }

      }





      return false;
    }

    private bool CheckFormStatus()
    {

      if (WebUser.IsAdminOrContentAdminOrContentPublisherOrContentAuthor || formActivityId > 0)
      {
        // if admin, there is no reason to redirect
        return false;
      }

      return downloadfile.RequireForm;



    }

    private void LoadParams()
    {
      var QueryParams = Request.QueryString;

      if (!string.IsNullOrWhiteSpace(Request.QueryString[DownloadFile.QS_Encoded]))
      {
        var data = Request.QueryString[DownloadFile.QS_Encoded].DecodeFrom64();
        QueryParams = HttpUtility.ParseQueryString(data);


        DateTime time;
        if (DateTime.TryParse(QueryParams[DownloadFile.QS_LinkDate], out time))
          linkDate = time;

      }



      formActivityId = WebUtils.ParseInt32FromQueryString(DownloadFile.QS_FormActivityId, 0);
      fileGuid = WebUtils.ParseGuidFromQueryString(DownloadFile.QS_DownFileGuid, Guid.Empty);


      //var browserCaps = Request.Browser as System.Web.Configuration.HttpCapabilitiesBase;
      //if (browserCaps.Crawler)
      //{
      //  redirectToTrackingPage = false;
      //}
      //else
      //{
      //  var hexString = WebUtils.ParseStringFromQueryString(DownloadFile.QS_NoRedirect, string.Empty);

      //  if (!string.IsNullOrWhiteSpace(hexString))
      //  {
      //    var ticks = Int64.Parse(hexString, NumberStyles.HexNumber);

      //    noRedirectDateTime = new DateTime(ticks);

      //    if (DateTime.UtcNow <= (noRedirectDateTime.AddMinutes(QuantifiSettings.DownloadFiles_TrackingCookieTimeOutInMin)))
      //    {

      //      redirectToTrackingPage = false;
      //    }
      //    else
      //    {
      //      log.Warn("Direct Download link is out of date: " + noRedirectDateTime.ToLocalTime().ToString());
      //      redirectToTrackingPage = true;
      //    }

      //  }

      //}

      if (Request.IsAuthenticated || Request.Browser.Crawler || formActivityId > 0)
      {
        // Dont Track Downloads from admin users or search engine bots
        trackDownload = false;
      }


    }

    private void StreamDownloadFile(DownloadFile file)
    {
      if (file == null)
        return;

      var folderPath = DownloadFileUtil.GetDownloadFileFolderPath(file);
      var downloadPath = DownloadFileUtil.GetDownloadFileFilePath(file);

      if (File.Exists(downloadPath))
      {
        FileInfo fileInfo = new System.IO.FileInfo(downloadPath);
        Page.Response.AppendHeader("Content-Length", fileInfo.Length.ToString(CultureInfo.InvariantCulture));
      }
      else
      {
        log.Error("Quantifi Download File Not Found. User tried to download file " + downloadPath);
        SiteUtils.RedirectToAccessDeniedPage(this);
        return;
      }

      string fileType = Path.GetExtension(file.OriginalFileName).Replace(".", string.Empty);

      string mimeType = SiteUtils.GetMimeType(fileType);
      Page.Response.ContentType = mimeType;

      if (formActivityId < 1 && SiteUtils.IsNonAttacmentFileType(fileType))
      {
        //this will display the pdf right in the browser
        Page.Response.AddHeader("Content-Disposition", "filename=" + HttpUtility.UrlEncode(Path.GetFileName(file.OriginalFileName).Replace(" ", "_"), Encoding.UTF8));
      }
      else
      {
        // other files just use file save dialog
        Page.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + HttpUtility.UrlEncode(Path.GetFileName(file.OriginalFileName).Replace(" ", "_"), Encoding.UTF8) + "\"");
      }



      try
      {
        Page.Response.Buffer = false;
        Page.Response.BufferOutput = false;


        if (Page.Response.IsClientConnected)
        {
          // In case user cancels download, we still track the request
          if (trackDownload)
          {
            var activity = new DownloadFileActivity(file.Id);
            activity.Save();
          }

          // TODO: a different solution, Response.TransmitFile has a 2 GB limit
          Page.Response.TransmitFile(downloadPath);


        }

      }
      catch (HttpException httpEx)
      {

        // If the user cancels the download you get:    
        // System.Web.HttpException (0x80072746): The remote host closed the connection. The error code is 0x80072746.
        if ((uint)httpEx.ErrorCode == 0x80072746)
        {
          log.Info(httpEx);
        }
        else
        {
          log.Error(httpEx);
        }
      }
      Page.Response.End();


    }

    private void RedirectToForm(DownloadFile file)
    {

      if (Request.UrlReferrer != null && Request.UrlReferrer.Host == Request.Url.Host)
      {
        var redirect = Request.UrlReferrer.AbsolutePath;

        var nvc = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);


        nvc[DownloadFile.QS_GlobalDownFileGuid] = file.Guid.ToString();
        nvc["prev"] = GetCleanReferrer();


        redirect += nvc.ToQueryString();

        Response.Redirect(redirect);
      }
      else
      {
        var nvc = new NameValueCollection();

        nvc.Add(DownloadFile.QS_GlobalDownFileGuid, file.Guid.ToString());

        if (Request.UrlReferrer != null)
          nvc["prev"] = GetCleanReferrer();

        var redirect = "~/home.aspx" + nvc.ToQueryString();

        Response.Redirect(redirect);

      }
    }

    private string GetCleanReferrer()
    {
      if (Request.UrlReferrer == null)
        return string.Empty;



      if (Request.UrlReferrer.Authority == Request.Url.Authority)
      {

        var redirect = Request.UrlReferrer.Scheme + "://" + Request.UrlReferrer.Authority + Request.UrlReferrer.AbsolutePath;

        var nvc = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);

        nvc.Remove(DownloadFile.QS_DownFileGuid);
        nvc.Remove(DownloadFile.QS_GlobalDownFileGuid);
        nvc.Remove("prev");


        redirect += nvc.ToQueryString();

        return redirect;
      }

      return Request.UrlReferrer.ToString();
    }

    //private bool CheckCookie(int fileId)
    //{

    //  var cookie = Request.Cookies[string.Format("df{0}", fileId)];
    //  if (cookie == null)
    //    return false;
    //  return true;

    //}

    private string GetTrackingRedirectUrl()
    {
      return WebUtils.GetApplicationRoot() + "/qredirect.aspx" + Request.QueryString.ToQueryString();
    }


    //public static bool IsClientCached()
    //{
    //  //HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));

    //  //// no-store makes firefox reload page
    //  //// no-cache makes firefox reload page only over SSL
    //  //// IE will fail when downloading a file over SSL if no-store or no-cache is set
    //  //HttpBrowserCapabilities oBrowser
    //  //    = HttpContext.Current.Request.Browser;

    //  //if (!oBrowser.Browser.ToLower().Contains("ie"))
    //  //{
    //  //  HttpContext.Current.Response.Cache.SetNoStore();
    //  //}

    //  ////HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

    //  //// private cache allows IE to download over SSL with no-store set. 
    //  //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
    //  //HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
    //  //HttpContext.Current.Response.Cache.AppendCacheExtension("post-check=0,pre-check=0");
    //  return false;
    //}

    //public static void SetDownloadCache()
    //{
    //  //HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));

    //  //// no-store makes firefox reload page
    //  //// no-cache makes firefox reload page only over SSL
    //  //// IE will fail when downloading a file over SSL if no-store or no-cache is set
    //  //HttpBrowserCapabilities oBrowser
    //  //    = HttpContext.Current.Request.Browser;

    //  //if (!oBrowser.Browser.ToLower().Contains("ie"))
    //  //{
    //  //  HttpContext.Current.Response.Cache.SetNoStore();
    //  //}

    //  ////HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

    //  //// private cache allows IE to download over SSL with no-store set. 
    //  //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
    //  //HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
    //  //HttpContext.Current.Response.Cache.AppendCacheExtension("post-check=0,pre-check=0");

    //}


  }
}