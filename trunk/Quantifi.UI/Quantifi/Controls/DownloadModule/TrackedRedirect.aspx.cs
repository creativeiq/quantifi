﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web.Framework;
using log4net;
using System.Collections.Specialized;
using Quantifi.Shared;
using Quantifi.Business;
using Quantifi.Business.DownloadModule;
using mojoPortal.Web;

namespace Quantifi.UI.Controls.DownloadModule
{
  public partial class TrackedRedirect : System.Web.UI.Page
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(TrackedRedirect));

    private Guid fileGuid;


    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      LoadParams();

      this.btnDirectLink.Click += new EventHandler(btnDirectLink_Click);
    }


    protected void Page_Load(object sender, EventArgs e)
    {

      SiteUtils.SetFormAction(this, Request.RawUrl);

      if (IsPostBack)
        return;

      if (fileGuid == Guid.Empty)
      {
        log.Warn("No DownloadFile Guid Passed");
        Response.Redirect("~/");
        return;
      }


      try
      {
        using (var dc = new Data.EF.CorpWebEntities())
        {
          var file = dc.DownloadFiles.SingleOrDefault(df => df.Guid == fileGuid);

          if (file != null)
          {
            if (!CheckCookie(file.Id))
            {
              //    _gaq.push(['_trackPageview', '/QDownloads/[id]-Name']);
              //AnalyticsAsyncTop.PageToTrack = string.Format("/QDownloads/{0}-{1}", file.Id, file.DisplayName.HtmlEncode());

              this.Page.Title = string.Format("Quantifi Download: {1} [{0}]", file.Id, file.DisplayName);

              SetCookie(file.Id);
              SetRedirect(GetDownloadLink(true), QuantifiSettings.DownloadFiles_TrackingPageRedirectTimeoutInSeconds);
              return;
            }
          }
        }

      }
      catch (Exception ex)
      {
        log.Error(ex);
      }


      Response.Redirect(GetDownloadLink(false));
    }

    private void LoadParams()
    {
      fileGuid = WebUtils.ParseGuidFromQueryString(DownloadFile.QS_DownFileGuid, Guid.Empty);
    }

    private bool CheckCookie(int fileId)
    {

      var cookie = Request.Cookies[string.Format("df{0}", fileId)];
      if (cookie == null)
        return false;
      return true;

    }

    private void SetCookie(int fileId)
    {
      Response.Cookies[string.Format("df{0}", fileId)].Value = DateTime.UtcNow.Ticks.ToString("X");
      Response.Cookies[string.Format("df{0}", fileId)].Expires = DateTime.Now.AddMinutes(QuantifiSettings.DownloadFiles_TrackingCookieTimeOutInMin);
    }


    private void SetRedirect(string url, int timeOut)
    {
      var metaFormat = @"<meta http-equiv='Refresh' content='{0};url={1}' />";
      this.Header.Controls.Add(new LiteralControl(string.Format(metaFormat, timeOut, url.Replace("~/", "/"))));
    }


    private string GetDownloadLink(bool isDirectLink)
    {
      var nvc = new NameValueCollection();
      nvc.Add(DownloadFile.QS_DownFileGuid, fileGuid.ToString());

      //if (isDirectLink)
      //{
      //  nvc.Add(DownloadFile.QS_NoRedirect, DateTime.UtcNow.Ticks.ToString("X"));
      //}

      return "~" + WebUtils.GetApplicationRoot() + "/qdownloads.aspx" + nvc.ToQueryString();

    }

    private string GetTrackingRedirectUrl()
    {
      return "~" + WebUtils.GetApplicationRoot() + "/qredirect.aspx" + Request.QueryString.ToQueryString();
    }


    protected void btnDirectLink_Click(object sender, EventArgs e)
    {

      log.Info("User manually redirecting to QDownload page");

      Response.Redirect(GetDownloadLink(true));

    }



  }
}