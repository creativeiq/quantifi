﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quantifi.Business.DownloadModule;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using System.Configuration;
using System.Collections.Specialized;
using Quantifi.Shared;
using System.IO;

namespace Quantifi.UI.Controls.DownloadModule
{
  public static class DownloadFileUtil
  {

    public static string GetRelativeDownloadUrl(DownloadFile df)
    {
      return "/QDownloads.aspx?" + DownloadFile.QS_DownFileGuid + "=" + df.Guid.ToString();
    }

    public static string GetRelativeDownloadUrl(DownloadFile df, DownloadFileFormActivity dfa)
    {
      var nvc = new NameValueCollection();
      nvc.Add(DownloadFile.QS_DownFileGuid, df.Guid.ToString());
      nvc.Add(DownloadFile.QS_FormActivityId, dfa.Id.ToString());
      var encoded = nvc.ToQueryString().EncodeTo64();

      return "/QDownloads.aspx?" + DownloadFile.QS_Encoded + "=" + encoded;

    }


    public static string GetDownloadUrl(DownloadFile df)
    {
      return SiteUtils.GetNavigationSiteRoot() + GetRelativeDownloadUrl(df);
    }

    public static string GetDownloadUrl(DownloadFile df, DownloadFileFormActivity dfa)
    {
      return SiteUtils.GetNavigationSiteRoot() + GetRelativeDownloadUrl(df, dfa);
    }

    public static string GetDownloadUrl(DownloadFile df, bool recordTracking)
    {
      return GetDownloadUrl(df)
            + "&" + DownloadFile.QS_DownFileTrackingEnabled
            + "=" + recordTracking.ToString();
    }

    public static string GetDownloadFileFolderPath(DownloadFile df)
    {
      var applicationPath = WebUtils.GetApplicationRoot() + "/Data/" + DownloadFile.DownFileFolder + "/" + df.CreatedTime.Year.ToString() + "/";
      return HttpContext.Current.Server.MapPath(applicationPath);
    }

    public static string GetDownloadFileFilePath(DownloadFile df)
    {
      return GetDownloadFileFolderPath(df) + df.ServerFileName;
    }

    public static string GetUserHostAddressLookUpUrl(DownloadFileActivity dfa)
    {

      var urlFormat = ConfigurationManager.AppSettings["UserHostAddressLookUpUrl"];


      if (String.IsNullOrWhiteSpace(urlFormat))
        return string.Format("http://whois.domaintools.com/{0}", dfa.UserHostAddress);


      return string.Format(urlFormat, dfa.UserHostAddress);
    }



    public static string GetFileTypeImageUrl(DownloadFile df)
    {
      string imgroot = "/Data/SiteImages/";

      string extension = Path.GetExtension(df.OriginalFileName);

      if (!string.IsNullOrWhiteSpace(extension))
      {
        string imgFile = extension.ToLower().Replace(".", "") + ".png";

        if (IconExists(imgFile))
        {
          return imgroot + "Icons/" + imgFile;
        }
      }

      return imgroot + "Icons/unknown.png";


    }

    private static FileInfo[] _iconList;

    private static bool IconExists(String iconFileName)
    {
      bool result = false;
      if (_iconList == null)
        _iconList = SiteUtils.GetFileIconList();

      if (_iconList != null)
      {
        foreach (FileInfo f in _iconList)
        {
          if (f.Name == iconFileName)
          {
            result = true;
          }
        }
      }

      return result;
    }

  }
}