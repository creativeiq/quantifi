﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using Quantifi.Business.DownloadModule;
using System.IO;
using mojoPortal.Web.Framework;



namespace Quantifi.UI.Controls.DownloadModule
{
  public partial class ViewFiles : NonCmsBasePage
  {
    private FileInfo[] iconList;
    
    private int pageSize = 20;
    private int pageNum = 1;
    private int totalPages = 0;

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      pageNum = WebUtils.ParseInt32FromQueryString("pg", pageNum);

    }


    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Request.IsAuthenticated)
      {
        SiteUtils.RedirectToLoginPage(this);
        return;
      }


      iconList = SiteUtils.GetFileIconList();


      var pagedata = DownloadFile.GetPage(this.pageSize, this.pageNum, out this.totalPages);

      this.dgDownloadFiles.DataSource = pagedata;
      this.dgDownloadFiles.DataBind();

      string pageUrlFormat = "?pg={0}";

      pager.PageURLFormat = pageUrlFormat;
      pager.ShowFirstLast = true;
      pager.PageSize = pageSize;
      pager.PageCount = totalPages;
      pager.CurrentIndex = pageNum;
      pager.Visible = (totalPages > 1);

    }


    public string GetFileDownloadUrl(DownloadFile df)
    {
      return DownloadFileUtil.GetDownloadUrl(df);

    }

    public string GetFileSize(DownloadFile df)
    {
      var SizeInMB = (int)(df.SizeInKB / 1024);
      if (SizeInMB > 1)
      {
        return SizeInMB + " MB";
      }
      else
      {
        return df.SizeInKB + " KB";
      }
    }

    public string GetFileTypeImageUrl(DownloadFile df)
    {
      string imgroot = "/Data/SiteImages/";
       
      string imgFile = Path.GetExtension(df.OriginalFileName).ToLower().Replace(".", "") + ".png";

      if (IconExists(imgFile))
      {
        return imgroot + "Icons/" + imgFile;
      }
      else
      {
        return  imgroot + "Icons/unknown.png";
      }

    }

    private bool IconExists(String iconFileName)
    {
      bool result = false;
      if (this.iconList != null)
      {
        foreach (FileInfo f in this.iconList)
        {
          if (f.Name == iconFileName)
          {
            result = true;
          }
        }
      }

      return result;

    }

  }
}