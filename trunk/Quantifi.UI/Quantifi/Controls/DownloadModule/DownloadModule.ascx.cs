﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Net;
using mojoPortal.Web;
using Quantifi.Business.DownloadModule;
using Quantifi.Shared;
using Quantifi.UI.Utils;
using System.Xml;
using Quantifi.Business.Countries;
using Quantifi.Business.Security;

namespace Quantifi.UI.Controls.DownloadModule
{
  public partial class DownloadModule : UserControl
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(DownloadModule));

    #region Properties


    private Guid fileGuid = Guid.Empty;
    private DownloadFile file = null;
    private string PreviousPageUrl = string.Empty;
    private bool showForm = false;

    protected string ErrorMsg { get; set; }

    protected string CleanFileName
    {
      get
      {
        if (file == null)
          return "unknown";

        return file.DisplayName.HtmlEncode().StripHTML() + " " + file.Id;

      }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {

      base.OnInit(e);

      this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
      // Hide Button because we will use jQuery UI Modal's buttons (since they keep the style uniform)
      this.btnSubmit.Style.Add(HtmlTextWriterStyle.Display, "none");

      LoadParams();

    }

    protected void Page_Load(object sender, EventArgs e)
    {
      this.phForm.Visible = showForm;

      if (!showForm)
        return;

      if (IsPostBack)
        return;

      InitForm();

    }

    private void InitForm()
    {
      string userCountry = string.Empty;
      var formCookie = HttpContext.Current.Request.Cookies["qform"];
      if (formCookie != null)
      {
        if (formCookie["fname"] != null)
          this.txtFirstName.Text = formCookie["fname"];
        if (formCookie["lname"] != null)
          this.txtLastName.Text = formCookie["lname"];
        if (formCookie["jobtitle"] != null)
          this.txtJobTitle.Text = formCookie["jobtitle"];
        if (formCookie["company"] != null)
          this.txtCompany.Text = formCookie["company"];
        if (formCookie["email"] != null)
          this.txtEmail.Text = formCookie["email"];
        if (formCookie["phone"] != null)
          this.txtPhone.Text = formCookie["phone"];
        if (formCookie["country"] != null)
          userCountry = formCookie["country"];
      }



      var defaultItem = new ListItem("-- Please Select --", "");

      ddlCountry.Items.Add(defaultItem);

      var currentCountry = CountryUtils.ResolveCountry();

      bool trySelectUserCountry = false;
      bool.TryParse(ConfigurationManager.AppSettings["Quantifi_Form_AutoSelectCountry"], out trySelectUserCountry);

      var countries = Country.GetAll();

      foreach (var country in countries)
      {
        var item = new ListItem(country.Name, country.Id.ToString());

        if (trySelectUserCountry)
        {
          if (!string.IsNullOrWhiteSpace(userCountry) &&
              country.Id.ToString().Equals(userCountry, StringComparison.InvariantCulture))
          {
            item.Selected = true;
          }
          else if (currentCountry != null && currentCountry.TwoLetterISORegionName == country.IsoTwoCharCode)
          {
            item.Selected = true;
          }
        }

        ddlCountry.Items.Add(item);
      }
    }


    private void LoadParams()
    {

      this.fileGuid = mojoPortal.Web.Framework.WebUtils.ParseGuidFromQueryString("fileguid", Guid.Empty);
      this.PreviousPageUrl = mojoPortal.Web.Framework.WebUtils.ParseStringFromQueryString("prev", string.Empty);

      if (fileGuid != Guid.Empty)
      {
        this.file = DownloadFile.Get(fileGuid);
        this.showForm = (file != null);
      }
      else
      {
        this.showForm = false;
      }

    }

    //private void LoadData()
    //{
    //  var file = DownloadFile.Get(fileGuid);
    //}

    private bool ValidateForm()
    {
      bool valid = true;
      ErrorMsg = string.Empty;

      //StringBuilder sb = new StringBuilder();
      if (this.txtFirstName.Text.IsNullEmptyWhiteSpaceOfLength(2))
      {
        valid = false;
        ErrorMsg += @"Please provide your First Name <br/>";
        this.txtFirstName.CssClass += " ui-state-error";
      }

      if (this.txtLastName.Text.IsNullEmptyWhiteSpaceOfLength(2))
      {
        valid = false;
        ErrorMsg += @"Please provide your Last Name <br/>";
        this.txtLastName.CssClass += " ui-state-error";
      }

      if (this.txtJobTitle.Text.IsNullEmptyWhiteSpaceOfLength(2))
      {
        valid = false;
        ErrorMsg += @"Please provide your Job Title <br/>";
        this.txtJobTitle.CssClass += " ui-state-error";
      }

      if (this.txtCompany.Text.IsNullEmptyWhiteSpaceOfLength(2))
      {
        valid = false;
        ErrorMsg += @"Please provide your Company <br/>";
        this.txtCompany.CssClass += " ui-state-error";
      }


      Regex reStrict = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,9})$");

      var emailAddress = this.txtEmail.Text;
      if (emailAddress.IsNullEmptyWhiteSpaceOfLength(6) || !reStrict.IsMatch(emailAddress))
      {
        valid = false;
        ErrorMsg += @"Please provide your valid Email <br/>";
        this.txtEmail.CssClass += " ui-state-error";
      }

      //var index = emailAddress.IndexOf("@");
      //if (index < 1)
      //{
      //  valid = false;
      //  ErrorMsg += @"Please provide your valid Email <br/>";
      //  this.txtEmail.CssClass += " ui-state-error";
      //}

      //var emailEnd = emailAddress.Substring(index + 1);
      //if (string.IsNullOrWhiteSpace(emailEnd))
      //{

      //  ErrorMsg += litEmailDomainErrorMsg.Text;
      //  this.txtEmail.CssClass += " ui-state-error";
      //  return false;

      //}





      return valid;
    }

    private void btnSubmit_Click(object sender, EventArgs e)
    {
      if (ValidateForm())
      {
        SubmitForm();
      }
    }

    private void SubmitForm()
    {

      this.file = DownloadFile.Get(this.fileGuid);
      if (file == null)
      {
        log.Error("Unable to submit Form, DownloadFile not found : " + this.fileGuid.ToString());
        return;
      }

      var formActivity = new DownloadFileFormActivity(file.Id);

      formActivity.FirstName = this.txtFirstName.Text;
      formActivity.LastName = this.txtLastName.Text;
      formActivity.JobTitle = this.txtJobTitle.Text;
      formActivity.CompanyName = this.txtCompany.Text;
      formActivity.Email = this.txtEmail.Text;
      formActivity.Phone = this.txtPhone.Text;

      int countryId;
      if (int.TryParse(this.ddlCountry.SelectedValue, out countryId))
      {
        formActivity.CountryId = countryId;
        formActivity.Country = Country.GetAll().FirstOrDefault(c => c.Id == countryId);
      }
      else
      {
        formActivity.CountryId = 0;
      }

      var qFormCookie = new HttpCookie("qform");

      qFormCookie["fname"] = this.txtFirstName.Text;
      qFormCookie["lname"] = this.txtLastName.Text;
      qFormCookie["jobtitle"] = this.txtJobTitle.Text;
      qFormCookie["company"] = this.txtCompany.Text;
      qFormCookie["email"] = this.txtEmail.Text;
      qFormCookie["phone"] = this.txtPhone.Text;
      qFormCookie["country"] = formActivity.CountryId.ToString();

     qFormCookie.Expires = DateTime.Now.AddDays(90);
     Response.Cookies.Add(qFormCookie);




      formActivity.MarketingOptIn = !this.checkboxOptIn.Checked;


      if (!string.IsNullOrWhiteSpace(this.PreviousPageUrl))
        formActivity.Activity.Referrer = PreviousPageUrl;

      var status = formActivity.Save();



      bool isRestricted = false;
      string msg = string.Empty;

      string emailEnd = string.Empty;

      if (!file.EmailFilesToUser)
      {
        msg = "Emails to users are disabled by default, please vet this request";
      }
      else if (file.EmailFilesToRestrictedEmails == false)
      {
        isRestricted = (RestrictedEmailsUtils.CheckEmail(formActivity.Email, out msg) > 0);
      }



      if (file.EmailFilesToUser == false || isRestricted)
      {
        this.phForm.Visible = false;
        this.phThankYou.Visible = false;
        this.phThankYouRestricted.Visible = true;
        log.Info(msg);
      }
      else
      {
        var emailStatus = EmailUser(file, formActivity);
        if (!emailStatus)
        {
          log.Error("Unable to send email to user for FormId = " + formActivity.Id);
        }


        this.phForm.Visible = false;
        this.phThankYouRestricted.Visible = false;
        this.phThankYou.Visible = true;

      }

      if (file.EmailUserActivity)
      {
        EmailAdminActivityReport(file, formActivity, isRestricted, emailEnd, msg);
      }


    }

    private bool EmailUser(DownloadFile file, DownloadFileFormActivity dfa)
    {
      if (file.SizeInKB < QuantifiSettings.MaxEmailAttatchmentSizeKB)
      {
        return EmailUserWithAttatchment(file, dfa);
      }
      else
      {
        //return EmailUserWithLink(file, dfa);

        //log.Error("Unable to Send email")
        return false;
      }

    }

    private bool EmailUserWithAttatchment(DownloadFile file, DownloadFileFormActivity dfa)
    {
      var siteSettings = CacheHelper.GetCurrentSiteSettings();

      var filepath = DownloadFileUtil.GetDownloadFileFilePath(file);

      using (var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
      {

        var files = new List<Attachment>() { new Attachment(fs, Path.GetFileName(file.OriginalFileName)) };


        var smtpSettings = SiteUtils.GetSmtpSettings();

        return EmailUtils.Send(smtpSettings,//Settings
                      siteSettings.DefaultEmailFromAddress, //FROM
                      QuantifiSettings.GeneralResponseEmailAddress,// REPLY TO
                      this.txtEmail.Text, // TO
                      string.Empty, // CC
                      string.Empty, // BCC
                      "Quantifi Literature Request - " + file.DisplayName,
                      GetEmailBody(file, dfa),
                      true,
                      "Normal",
                      files);

      }

    }

    private bool EmailAdminActivityReport(DownloadFile file, DownloadFileFormActivity dfa, bool isRestrictedEmail, string endEmail, string msg)
    {
      var siteSettings = CacheHelper.GetCurrentSiteSettings();

      var smtpSettings = SiteUtils.GetSmtpSettings();

      return EmailUtils.Send(smtpSettings,//Settings
                    siteSettings.DefaultEmailFromAddress, //FROM
                    siteSettings.DefaultEmailFromAddress,// REPLY TO
                    GetReportEmailAddress(file), // TO
                    string.Empty, // CC
                    string.Empty, // BCC
                    GetReportSubject(file, dfa, isRestrictedEmail),
                    GetReportEmailBody(file, dfa, isRestrictedEmail, endEmail, msg),
                    true,
                    "Normal",
                    null);
    }



    #region Build String Content


    private string GetReportSubject(DownloadFile file, DownloadFileFormActivity dfa, bool isRestrictedEmail)
    {
      if (isRestrictedEmail)
        return string.Format("Quantifi Literature Requested - Restricted Email [{0}] - {1}", dfa.Email, file.DisplayName);

      if (!file.EmailFilesToUser)
        return string.Format("Quantifi Literature Requested - Needs Review [{0}] - {1}", dfa.Email, file.DisplayName);


      return string.Format("Quantifi Literature Requested [{0}] - {1}", dfa.Email, file.DisplayName);
    }

    private string GetReportEmailBody(DownloadFile file, DownloadFileFormActivity dfa, bool isRestrictedEmail, string endEmail, string msg)
    {

      var sb = new StringBuilder();

      sb.AppendFormat("<div style='font-size: 11.0pt; font-family: \"Arial\",\"sans-serif\";'>{0}</div>", GetReportSubject(file, dfa, isRestrictedEmail));

      if (isRestrictedEmail || file.EmailFilesToUser == false)
      {
        sb.Append("<div style='font-size: 10.0pt; font-family: \"Arial\",\"sans-serif\";'>Please review the request and determine if the request merits communication</div>");
        sb.AppendFormat("<div style='font-size: 10.0pt; font-family: \"Arial\",\"sans-serif\";'>{0}</div>", msg);
      }

      sb.Append("<table  style='font-size: 9.0pt; font-family: \"Arial\",\"sans-serif\";' >");
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Display Name: ", file.DisplayName.HtmlEncode());
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Original FileName: ", Path.GetFileName(file.OriginalFileName));
      sb.AppendFormat("<tr><th align='left' >View File Info</th><td align='left' ><a href='{0}/Quantifi/Controls/DownloadModule/Edit.aspx?fileId={1}'>View File Info</td></tr>", SiteUtils.GetNavigationSiteRoot(), file.Id);
      sb.AppendFormat("<tr><th align='left' >Download File</th><td align='left' ><a href='{0}/Quantifi/Controls/DownloadModule/Download.aspx?{1}={2}&{3}={4}' ><img src='{0}{5}' border='0' alt='file type icon' /></a>&nbsp;<a href='{0}/Quantifi/Controls/DownloadModule/Download.aspx?{1}={2}&{3}={4}'>Download File</td></tr>",
                                              SiteUtils.GetNavigationSiteRoot(),
                                              DownloadFile.QS_DownFileGuid,
                                              file.Guid,
                                              DownloadFile.QS_FormActivityId,
                                              dfa.Id,
                                              DownloadFileUtil.GetFileTypeImageUrl(file));


      sb.Append("<tr><th colspan='2'>&nbsp;</th>");
      sb.Append("<tr><th colspan='2' style='font-size: 9.5pt; font-family: \"Arial\",\"sans-serif\"; font-weight: bold;'>User Form Entry</th>");
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "First Name: ", dfa.FirstName.HtmlEncode());
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Last Name: ", dfa.LastName.HtmlEncode());
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Job Title: ", dfa.JobTitle.HtmlEncode());
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Company Name: ", dfa.CompanyName.HtmlEncode());


      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}", "Email: ", dfa.Email.HtmlEncode());

      if (!isRestrictedEmail)
      {
        sb.AppendFormat("&nbsp;&nbsp;&nbsp;&nbsp;<a href='{0}/Quantifi/Controls/Utils/RestrictedEmails/Edit.aspx?email={1}'><img border='0' src='{0}/Data/SiteImages/lock.png'></a><a href='{0}/Quantifi/Controls/Utils/RestrictedEmails/Edit.aspx?email={1}'>&nbsp;Restrict This Email Domain</a></td></tr>", SiteUtils.GetNavigationSiteRoot(), dfa.Email.HtmlEncode());
      }
      else
      {
        sb.Append("</td></tr>");
      }


      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Phone: ", dfa.Phone.HtmlEncode());
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Country: ", ((dfa.Country == null) ? "unknown" : dfa.Country.Name));
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "MarketingOptIn: ", ((dfa.MarketingOptIn) ? "true" : "false"));
      sb.Append("<tr><th colspan='2'>&nbsp;</th>");
      sb.Append("<tr><th colspan='2'>&nbsp;</th>");
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "UserHostAddress: ", dfa.Activity.UserHostAddress);
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "Referrer: ", dfa.Activity.Referrer.HtmlEncode());
      sb.AppendFormat("<tr><th align='left' >{0}</th><td align='left' >{1}</td></tr>", "TimeStamp (UTC): ", dfa.Activity.TimeStamp);
      sb.Append("</table>");

      if (isRestrictedEmail || file.EmailFilesToUser == false)
      {
        var subject = "Subject : Quantifi Literature Request - " + file.DisplayName.HtmlEncode();
        sb.Append("<hr/><br/>");
        sb.AppendFormat("<div style='font-size: 10.0pt; font-family: \"Arial\",\"sans-serif\";'>UserEmail: {0}</div>", dfa.Email);
        sb.AppendFormat("<div style='font-size: 10.0pt; font-family: \"Arial\",\"sans-serif\";'>{0}</div>", subject);


        sb.Append("<hr/>");

        sb.Append(GetEmailBody(file, dfa));


        sb.Append("<br/><hr/>");

      }

      return GetEmailHtmlBody(sb.ToString());

    }

    private string GetReportEmailAddress(DownloadFile file)
    {
      return (string.IsNullOrWhiteSpace(file.AdminEmailAddress)) ? QuantifiSettings.DownloadFileDefaultAdminEmailAddress : file.AdminEmailAddress;
    }

    private string GetEmailBody(DownloadFile file, DownloadFileFormActivity dfa)
    {
      var innercontent = string.Format(GetEmailHtmlUserContent(), string.Format("{0} {1}", dfa.FirstName, dfa.LastName));

      return GetEmailHtmlBody(innercontent);
    }

    private string GetEmailHtmlBody(string innerContent)
    {

      try
      {
        var htmlFragment = System.IO.File.ReadAllText(Server.MapPath("/Quantifi/Controls/DownloadModule/Email/EmailTemplate.htm"));

        // String.Format cause porblems due to the Styles braces {}
        return htmlFragment.Replace("{0}", innerContent);
      }
      catch (FormatException ex)
      {
        log.Error("Error Formatting Email Body Content", ex);

        return "<html><head><title></title></head><body>" + innerContent + "</body></html>";
      }
      catch (Exception ex)
      {

        log.Error("Error Getting Email Body Content", ex);

        return "<html><head><title></title></head><body>" + innerContent + "</body></html>";

      }
    }

    private string GetEmailHtmlUserContent()
    {

      try
      {
        return System.IO.File.ReadAllText(Server.MapPath("/Quantifi/Controls/DownloadModule/Email/LiteratureRequest.htm"));
      }
      catch (Exception ex)
      {

        log.Error("Error Getting Literature Request User Email Html Content", ex);

        return DefaultResponseEmailContent;

      }
    }


    #endregion


    #region Email Content


    private const string DefaultResponseEmailContent =
      @"
<div>
	<p class='normal'>
		<span style='font-size: 10.0pt'>Dear {0}, </span>
	</p>
	<br />
	<p class='normal'>
		<span style='font-size: 10.0pt'>Thank you for your interest in Quantifi. We are a
			leading provider of analytics, trading and risk management software for the Global
			Capital Markets. Our award-winning solutions are trusted by the world's most sophisticated
			financial institutions including five of the six largest global banks, two of the
			three largest asset managers, leading hedge funds, insurance companies, pension
			funds and other market participants in over 15 countries.</span></p>
	<br />
	<p class='normal'>
		<span style='font-size: 10.0pt'>Learn more about <a href='/our-firm.aspx?utm_source=internal&amp;utm_medium=email&amp;utm_campaign=literature_request'>
			<span style='color: #1F497D; font-weight: bold;'>Quantifi</span></a>, our <a href='/solutions.aspx?utm_source=internal&amp;utm_medium=email&amp;utm_campaign=literature_request'>
				<span style='color: #1F497D; font-weight: bold;'>Solutions</span></a> and our
			<a href='/services.aspx?utm_source=internal&amp;utm_medium=email&amp;utm_campaign=literature_request'>
				<span style='color: #1F497D; font-weight: bold;'>Services</span></a>.</span></p>
	<br />
	<p class='normal'>
		<span style='font-size: 10.0pt'>The file you have requested is attached to this email.
			If you require any further information please don't hesitate to contact a member
			of our team at: </span>
	</p>
	<br />
	<p class='normal'>
		<span style='font-size: 10.0pt'>North America: +1 (212) 784 6815<br>
			EMEA: +44 (0) 20 7397 8788<br>
			APAC: +61 (02) 9221 0133<br>
		</span>
	</p>
	<br />
	<p class='normal'>
		<span style='font-size: 10.0pt'><a href='mailto:enquire@quantifisolutions.com'>enquire@quantifisolutions.com</a>
			<br />
			<a href='http://www.quantifisolutions.com?utm_source=internal&amp;utm_medium=email&amp;utm_campaign=literature_request'>
				www.quantifisolutions.com</a> </span>
	</p>
	<br />
	<p class='normal'>
		<span style='font-size: 10.0pt'>The Quantifi Team</span></p>
	<br />
	<p class='normal'>
		<span style='font-size: 10.0pt,'><a href='http://www.linkedin.com/company/quantifi-inc.'>
			<span style='color: #585858;'>Follow us on LinkedIn</span></a></span>
	</p>
</div>";

    #endregion

  }

}