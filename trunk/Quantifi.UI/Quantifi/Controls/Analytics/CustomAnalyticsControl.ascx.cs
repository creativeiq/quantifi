﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
 
using mojoPortal.Web.Framework;


namespace Quantifi.UI.Controls.Analytics
{
  public partial class CustomAnalyticsControl : System.Web.UI.UserControl
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(CustomAnalyticsControl));

 

    public static string QueryParam_Email = "_qem";
    public static string QueryParam_FirstName = "_qfn";
    public static string QueryParam_LastName = "_qln";


    public static string QueryParam_CampaignName = "_qem";
    public static string QueryParam_GoogleCampaignName = "utm_campaign";
    
 

    private string email;
    private string firstName;
    private string lastName;
    private string campaignName;
    
    
    
    
    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      LoadParams();
    }

    private void LoadParams()
    {

      email = WebUtils.ParseStringFromQueryString(QueryParam_Email, string.Empty);
      firstName = WebUtils.ParseStringFromQueryString(QueryParam_FirstName, string.Empty);
      lastName = WebUtils.ParseStringFromQueryString(QueryParam_LastName, string.Empty);
      campaignName = WebUtils.ParseStringFromQueryString(QueryParam_CampaignName, string.Empty);
    
    }

 
  }
}