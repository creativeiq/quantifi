﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Business.TagsModule;
using Quantifi.Business.Utils;
using mojoPortal.Web.Framework;

namespace Quantifi.UI.Controls.Tags
{
	public partial class ItemTagsView : System.Web.UI.UserControl
	{

		public iTaggable TaggedObject { get; set; }


		protected void Page_Load(object sender, EventArgs e)
		{
			var script = "$(document).ready(function () { var tag = Quantifi.Controls.TagsView(\"" + this.tagsPanel.ClientID + "\",  \"" + this.GetJSONUrl() + "\");  });";

			this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.tagsPanel.ClientID + "_s", script, true);


		}

		private string GetJSONUrl()
		{
			if (TaggedObject == null)
				return string.Empty;

			return WebUtils.GetHostRoot()
				+ "/Quantifi/Controls/Tags/TagsHandler.ashx?itemId="
				+ TaggedObject.ItemId.ToString()
				+ "&objTypeName="
				+ TaggedObject.GetType().Name;

		}




	}


}