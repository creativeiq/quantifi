﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewAllTagsControl.ascx.cs" Inherits="Quantifi.UI.Controls.Tags.ViewAllTagsControl" %>






<asp:Panel ID="pnlContainer" runat="server" >
	<div>
		<a class="alpha" href="#alpha">Alpha</a>
		<span class="spacer">|</span>
		<a class="count" href="#count">Count</a>
	</div>
	<div class="loading"><img src="/Data/SiteImages/indicators/indicator_arrows.gif" style="height: 12px; width: 12px; vertical-align: bottom; display: none;" class="loading">Loading...</div>
	
	<div class="tags"></div>

</asp:Panel>

											  
<script type="text/javascript">
	function makeScrollable(wrapper) {
		// Get jQuery elements
		var wrapper = $(wrapper), scrollable = $(".tags");

		// Hide images until they are not loaded
		scrollable.hide();
		var loading = wrapper.find(".loading");

		// Set function that will check if all images are loaded
		var interval = setInterval(function () {
			var divs = scrollable.find('div');
			var completed = 0;

			// Counts number of images that are succesfully loaded
			divs.each(function () {
				if (this.complete) completed++;
			});

			if (completed == divs.length) {
				clearInterval(interval);
				// Timeout added to fix problem with Chrome
				setTimeout(function () {

					loading.hide();
					// Remove scrollbars	
					wrapper.css({ overflow: 'hidden' });

					scrollable.slideDown('slow', function () {
						enable();
					});
				}, 1000);
			}
		}, 100);

		function enable() {
			// height of area at the top at bottom, that don't respond to mousemove
			var inactiveMargin = 99;
			// Cache for performance
			var wrapperWidth = wrapper.width();
			var wrapperHeight = wrapper.height();
			// Using outer height to include padding too
			var scrollableHeight = scrollable.outerHeight() + 2 * inactiveMargin;
			// Do not cache wrapperOffset, because it can change when user resizes window
			// We could use onresize event, but it's just not worth doing that 
			// var wrapperOffset = wrapper.offset();

			var lastTarget;
			//When user move mouse over menu			
			wrapper.mousemove(function (e) {
				// Save target
				lastTarget = e.target;

				var wrapperOffset = wrapper.offset();

				//			var tooltipLeft = e.pageX - wrapperOffset.left;
				//			// Do not let tooltip to move out of menu.
				//			// Because overflow is set to hidden, we will not be able too see it 
				//			tooltipLeft = Math.min(tooltipLeft, wrapperWidth - 75); //tooltip.outerWidth());
				//			
				//			var tooltipTop = e.pageY - wrapperOffset.top + wrapper.scrollTop() - 40;
				//			// Move tooltip under the mouse when we are in the higher part of the menu
				//			if (e.pageY - wrapperOffset.top < wrapperHeight/2){
				//				tooltipTop += 80;
				//			}				
				//			tooltip.css({top: tooltipTop, left: tooltipLeft});				

				// Scroll menu
				var top = (e.pageY - wrapperOffset.top) * (scrollableHeight - wrapperHeight) / wrapperHeight - inactiveMargin;
				if (top < 0) {
					top = 0;
				}
				wrapper.scrollTop(top);
			});

			// Setting interval helps solving perfomance problems in IE
			//		var interval = setInterval(function(){
			//			if (!lastTarget) return;	
			//										
			//			var currentText = tooltip.text();
			//			
			//			if (lastTarget.nodeName == 'IMG'){					
			//				// We've attached data to a link, not image
			//				var newText = $(lastTarget).parent().data('tooltipText');

			//				// Show tooltip with the new text
			//				if (currentText != newText) {
			//					tooltip
			//						.stop(true)
			//						.css('opacity', 0)	
			//						.text(newText)
			//						.animate({opacity: 1}, 1000);
			//				}					
			//			}
			//		}, 200);

			// Hide tooltip when leaving menu
			wrapper.mouseleave(function () {
				lastTarget = false;

			});

			/*
			//Usage of hover event resulted in performance problems
			scrollable.find('a').hover(function(){
			tooltip
			.stop()
			.css('opacity', 0)
			.text($(this).data('tooltipText'))
			.animate({opacity: 1}, 1000);
	
			}, function(){
			tooltip
			.stop()
			.animate({opacity: 0}, 300);
			});
			*/
		}
	}


	if (Quantifi.Controls.AllTagsView === "undefined" || !Quantifi.Controls.AllTagsView) {

		Quantifi.Controls.AllTagsView = function (panelId) {

			var container = $("#" + panelId);
			var tagsDiv = container.find(".tags");
			var loadingDiv = container.find(".loading");



			LoadTagsAlpha = function () {
				loadingDiv.show();
				tagsDiv.html("");

				try {
					$.ajax({
						url: "/Quantifi/Controls/Tags/TagsHandler.ashx?sort=alpha",
						dataType: 'json',
						data: {},
						success: function (data) {
							$.each(data, function (i, item) {
								var url = "/Tags.aspx?tag=" + escape(item.Text) + "&tid=" + item.Id;
								var link = "<div><a href='" + url + "' title='Click to view " + item.Text + " tagged content' >" + item.Text + "</a></div>";
								tagsDiv.append(link);
							});
 							loadingDiv.hide();
						}
					});

				}
				catch (e) {
					Quantifi.logError(e);
				}
			};


			LoadTagsCount = function () {
				loadingDiv.show();
				tagsDiv.html("");

				try {
					$.ajax({
						url: "/Quantifi/Controls/Tags/TagsHandler.ashx?sort=bycount",
						dataType: 'json',
						data: {},
						success: function (data) {
							$.each(data, function (i, item) {
								var url = "/Tags.aspx?tag=" + escape(item.Text) + "&tid=" + item.Id;
								var link = "<div><a href='" + url + "' title='Click to view " + item.Text + " tagged content' >" + item.Text + "</a></div>";
								tagsDiv.append(link);
							});
							loadingDiv.hide();
						}
					});

				}
				catch (e) {
					Quantifi.logError(e);
				}
			};


			container.find(".alpha").click(function () {
				LoadTagsAlpha();
			});

			container.find(".count").click(function () {
				LoadTagsCount();
			});

			$(function () {	 
				LoadTagsAlpha();
				makeScrollable("#" + panelId);

			});

			

		};


	}

	$(document).ready(function () {
		var tags = Quantifi.Controls.AllTagsView("<%=pnlContainer.ClientID %>");
 
	});

				/*
	//Set_Cookie('openstate', 'closed')
	var openState = Get_Cookie('openstate');

	if (openState != null) 
	{ 
		if (openState == 'closed') { HideMenuToolbar(); } 
		if (openState == 'open') { ShowMenuToolbar(); } 
	}
				*/
</script>
