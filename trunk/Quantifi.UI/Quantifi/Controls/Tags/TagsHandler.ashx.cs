﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Quantifi.Business.ContentArchiveModule;
using Quantifi.UI.Utils;
using Quantifi.Business.TagsModule;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.Tags
{
	/// <summary>
	/// Summary description for TagsHandler
	/// </summary>
	public class TagsHandler : IHttpHandler
	{

		private static readonly ILog log = LogManager.GetLogger(typeof(TagsHandler));

 

		public void ProcessRequest(HttpContext context)
		{

			if (!string.IsNullOrWhiteSpace(context.Request.QueryString["sort"]))
			{
				var sortName = context.Request.QueryString["sort"];

				GetAllTags(context, sortName.Equals("bycount"));
			}

			if (!string.IsNullOrWhiteSpace(context.Request.QueryString["term"]))
			{
				TagSearch(context, context.Request.QueryString["term"].Trim(), context.Request.QueryString["exclude"]);
			}

			if (!string.IsNullOrWhiteSpace(context.Request.QueryString["objTypeName"]) && !string.IsNullOrWhiteSpace(context.Request.QueryString["itemId"]) )
			{
				int itemId = -1;

				if (int.TryParse(context.Request.QueryString["itemId"], out itemId))
				{
					GetTags(context, itemId, context.Request.QueryString["objTypeName"]);
				}
			}

      if (!string.IsNullOrWhiteSpace(context.Request.QueryString["days"]))
      {
        	int days = -1;

          if (int.TryParse(context.Request.QueryString["days"], out days))
          {
            GetTagsByRelavance(context, days);
          }
      }
	
			context.Response.ContentType = "text/plain";
			context.Response.StatusCode = 404;
			context.Response.StatusDescription="Unhandled Request";
			context.Response.Write("<ContentTags></ContentTags>");
			context.Response.End();
			
		}


    private void  Export( HttpContext context,List<ContentTag> tags,  string filename)
    {
      var format = "json";
      if (!string.IsNullOrWhiteSpace(context.Request.QueryString["format"]))
      {
        format = context.Request.QueryString["format"].ToLower();
      }




      switch (format)
      {
        case "csv":

          CSVHelper.ExportCSV(tags.ToCSV(), filename);
          break;

        default:
          context.Response.ContentType = "text/plain";
          context.Response.Write(tags.ToJson());
          context.Response.End();


          break;
      }



    }
		

		private void GetAllTags(HttpContext context, bool byCount)
		{

			List<ContentTag> tags = null;

      if (byCount)
      {
        tags = ContentTagCacheUtil.GetAllTagsByTaggedCount();
        Export(context, tags, "GetAllTagsByCount.csv");
      }
      else
      {
        tags = ContentTagCacheUtil.GetAllTagsByText();

        Export(context, tags, "GetAllTagsAlpha.csv");
      }
      
      //context.Response.ContentType = "text/plain";
      //context.Response.Write(tags.ToJson());
      //context.Response.End();
		}
		
		
		private void TagSearch(HttpContext context, string term, string exclude)
		{
			List<string> excludeList = null;
			if (string.IsNullOrWhiteSpace(exclude))
				excludeList = new List<string>();
			else
				excludeList = exclude.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim().ToLower()).ToList();


			var searchedTags = ContentTag.GetAllTagsLike(term);


			if (excludeList.Any())
				searchedTags = searchedTags.Where(t => excludeList.IndexOf(t.Text.ToLower()) < 0).ToList();


			context.Response.ContentType = "text/plain";
			context.Response.Write(searchedTags.ToJson());
			context.Response.End();
		}


 

		private void GetTags(HttpContext context, int itemId, string objTypeName)
		{
			var tags = new List<ContentTag>();

			switch(objTypeName)
			{
				case "Blog":
					tags = ContentTagCacheUtil.GetBlogPostTags(itemId);

					break;

				case "ContentItem":
					tags = ContentTagCacheUtil.GetContentItemTags(itemId);

					break;

				default:
					break;
 
			}


			context.Response.ContentType = "text/plain";
			context.Response.Write(tags.ToJson());
			context.Response.End();
		}

    private void GetTagsByRelavance (HttpContext context,int numDays)
    {

      var tags = ContentTagCacheUtil.GetTopTagsByDate(numDays, 100);


      Export(context, tags, string.Format("TopTags_{0}_to_{1}.csv", DateTime.Now.ToShortDateString(),DateTime.Now.AddDays(-numDays).ToShortDateString() ));

    }

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}


 

	}
}
