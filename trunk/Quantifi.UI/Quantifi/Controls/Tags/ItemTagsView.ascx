﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemTagsView.ascx.cs"
	ViewStateMode="Disabled" ClientIDMode="Predictable" Inherits="Quantifi.UI.Controls.Tags.ItemTagsView" %>



<asp:Panel ID="tagsPanel" runat="server" CssClass="tagsPanel">
	<div class="tags">
		<span class="label">
			<img class="openclose" alt="open or close" src="/Data/SiteImages/plus.gif" />
			<a title="Click to View Tags">Tags:</a>
			<img class="loading" alt="loading" style="display: none; " src='/Data/SiteImages/indicators/indicator_arrows.gif' />
		</span>
	</div>
	<div style="clear: left;">
	</div>
</asp:Panel>
