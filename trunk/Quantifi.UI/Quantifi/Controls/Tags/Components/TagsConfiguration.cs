﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using log4net;
using mojoPortal.Web.Framework;

using Utils = Quantifi.UI.Utils;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.Tags.Components
{


	public class TagsCloudConfiguration
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(TagsCloudConfiguration));


		public string CssClass { get; private set; }
		public int FlashCloudWidth { get; private set; }
		public string TextColor1  { get; private set; }
		public string TextColor2  { get; private set; }
		public string HighlightTextColor{ get; private set; }


		public int MaxTagsCount { get; private set; }

    public int TaggedRelevanceInDays { get; set; }

		public int ViewTagContentPageId {get; private set; }

		public List<string> ForcedIncludeTags { get; private set; }
		//public List<string> ForcedExcludeTags { get; private set; }

		public TagsCloudConfiguration() { }
		public TagsCloudConfiguration(Hashtable settings)
		{
			LoadSettings(settings);
		}

		private void LoadSettings(Hashtable settings)
		{
			if (settings == null) { throw new ArgumentException("Must pass in a hashtable of settings"); }


			if (settings.Contains("CssClassSetting"))
			{
				CssClass = settings["CssClassSetting"].ToString();
			}

			FlashCloudWidth = WebUtils.ParseInt32FromHashtable(settings, "WidthSetting", 180);


			if (settings.Contains("TextColor1Setting"))
			{
				TextColor1 = settings["TextColor1Setting"].ToString();
			}
			if (settings.Contains("TextColor2Setting"))
			{
				TextColor2 = settings["TextColor2Setting"].ToString();
			}
			if (settings.Contains("HighlightTextColorSetting"))
			{
				HighlightTextColor = settings["HighlightTextColorSetting"].ToString();
			}

			MaxTagsCount = WebUtils.ParseInt32FromHashtable(settings, "MaxTagsCountSetting", 15);
			ViewTagContentPageId = WebUtils.ParseInt32FromHashtable(settings, "ViewTagContentPageIdSetting", -1);

			ForcedIncludeTags = new List<string>();
			if (settings.Contains("ForceIncludeTagsSetting"))
			{
				var csvstr = settings["ForceIncludeTagsSetting"].ToString();

				if (!string.IsNullOrWhiteSpace(csvstr))
				{
          ForcedIncludeTags =   csvstr.FromDelimitedString(",").ToList();

				}
			}

      TaggedRelevanceInDays = WebUtils.ParseInt32FromHashtable(settings, "TaggedRelevanceInDaysSetting", 120);
	
		}


	}

	public class ViewTaggedContentConfiguration
	{

		public int ItemsPerPage { get; private set; }

		public bool ForceShowLeftColumn { get; private set; }
		public bool ForceShowRightColumn { get; private set; }

    public bool ShowTimeStamp { get; private set; }
    public string TimeStampFormatString { get;  private set; }
    public string RecentTimeStampFormatString { get; private set; }

    public int RecentTimeInDays { get; private set; }


		public ViewTaggedContentConfiguration() { }
		public ViewTaggedContentConfiguration(Hashtable settings)
		{
			LoadSettings(settings);
		}

		private void LoadSettings(Hashtable settings)
		{

			if (settings == null) { throw new ArgumentException("Must pass in a hashtable of settings"); }


			ItemsPerPage = WebUtils.ParseInt32FromHashtable(settings, "ItemsPerPageSetting", 10);
			ForceShowLeftColumn = WebUtils.ParseBoolFromHashtable(settings, "ForceShowLeftColumnSetting", false);
			ForceShowRightColumn = WebUtils.ParseBoolFromHashtable(settings, "ForceShowRightColumnSetting", false);

      ShowTimeStamp = WebUtils.ParseBoolFromHashtable(settings, "ShowTimeStampSetting", true);

      TimeStampFormatString = settings.Contains("TimeStampFormatStringSetting") 
                                              ? settings["TimeStampFormatStringSetting"].ToString() 
                                              : "d MMMM yyyy";

      RecentTimeStampFormatString = settings.Contains("RecentTimeStampFormatStringSetting") 
                                              ? settings["RecentTimeStampFormatStringSetting"].ToString() 
                                              : "D";

      RecentTimeInDays = WebUtils.ParseInt32FromHashtable(settings, "RecentTimeInDaysSetting", 7);

		}
	}

}