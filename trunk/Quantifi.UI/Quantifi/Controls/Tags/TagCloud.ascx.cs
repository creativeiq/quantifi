﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using Quantifi.Business.TagsModule;
using Quantifi.Business.Utils;
using Quantifi.Shared;
using Quantifi.UI.Controls.Tags.Components;

namespace Quantifi.UI.Controls.Tags
{
	public partial class TagCloud : SiteModuleControl
	{
		#region Properties
		public string TagsXmlData { get; private set; }

		public int CloudWidth { get; private set; }
		public int CloudHeight { get; private set; }

		public string TagViewBaseUrl { get; private set; }


		public string TextColor1 { get; private set; }
		public string TextColor2 { get; private set; }
		public string HighlightColor { get; private set; }

		private TagsCloudConfiguration Config = null; 
		#endregion

		private void LoadSettings()
		{
			Config = new TagsCloudConfiguration(Settings);

			pnlWrapper.CssClass += " " + Config.CssClass;
			CloudHeight = CloudWidth = Config.FlashCloudWidth;

			TextColor1 = Config.TextColor1;
			TextColor2 = Config.TextColor2;
			HighlightColor = Config.HighlightTextColor;

			if (Config.ViewTagContentPageId > 0)
			{
				var tagsviewpage = CacheUtils.GetPage(Config.ViewTagContentPageId);

				TagViewBaseUrl = tagsviewpage.Url;
			}
			else
			{
				TagViewBaseUrl = "/Tags.aspx";
			}


      this.mojoTitle.EditText = "Manage Tags";
      this.mojoTitle.EditUrl =  "/Quantifi/Controls/Tags/ViewTags.aspx";

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var page = Page as mojoBasePage;

			if (page == null)
			{
				this.Visible = false;
				return;
			}

			LoadSettings();

			page.ScriptConfig.IncludeSwfObject = true;

			if (!Config.ForcedIncludeTags.IsNullOrEmpty())
			{ 
				LoadForcedTags(); 
			}
			else
			{ 
				LoadTopUsedTags(); 
			}
			
		}



		/// <summary>
		/// 
		/// Xml is base off xml needed for the swf
		/// http://www.roytanck.com/2008/05/19/how-to-repurpose-my-tag-cloud-flash-movie/
		/// </summary>
		private void LoadTopUsedTags()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("<tags>");
      var date = DateTime.UtcNow.AddDays(-(Config.TaggedRelevanceInDays));
      var tags = ContentTagCacheUtil.GetTopTagsByDate(date, Config.MaxTagsCount);

			int tagSize = 20;
			foreach (ContentTag t in tags)
			{
				var url = WebUtils.GetHostRoot() + TagViewBaseUrl.Replace("~",string.Empty) + "?tag=" + t.Text + "&tid=" + t.Id.ToString();
				sb.Append("<a href='" + HttpUtility.UrlEncode(url) + "' style='" + tagSize + "'  >" + t.Text + "</a>");

				if (tagSize != 10)
					tagSize--;
			}


			sb.Append("</tags>");


			TagsXmlData = sb.ToString();
		}

		private void LoadForcedTags()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("<tags>");

	 

			int tagSize = 20;
      int tagCount = 0;

			var tagStrings = Config.ForcedIncludeTags.Take(Config.MaxTagsCount);

      var date = DateTime.UtcNow.AddDays(-(Config.TaggedRelevanceInDays));
      var tags = ContentTagCacheUtil.GetTopTagsByDate(date, Config.MaxTagsCount);


      StringComparer strComp = StringComparer.CurrentCultureIgnoreCase;

      foreach (string t in tagStrings)
			{
        if (tagCount == Config.MaxTagsCount)
          break;

				var url = WebUtils.GetHostRoot() + TagViewBaseUrl.Replace("~", string.Empty) + "?tag=" + t;
				sb.Append("<a href='" + HttpUtility.UrlEncode(url) + "' style='" + tagSize + "'  >" + t + "</a>");

        tagCount++;
        

				if (tagSize != 10)
					tagSize--;
			}

      foreach (ContentTag t in tags)
      {
        if (tagCount == Config.MaxTagsCount)
          break;

        if (tagStrings.Contains(t.Text, strComp))
          continue;
  

        var url = WebUtils.GetHostRoot() + TagViewBaseUrl.Replace("~", string.Empty) + "?tag=" + t.Text + "&tid=" + t.Id.ToString();
        sb.Append("<a href='" + HttpUtility.UrlEncode(url) + "' style='" + tagSize + "'  >" + t.Text + "</a>");

        tagCount++;

        if (tagSize != 10)
          tagSize--;
      }



			sb.Append("</tags>");


			TagsXmlData = sb.ToString();

		}
	}
}