﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TagSelectorControl.ascx.cs"
	Inherits="Quantifi.UI.Controls.Tags.Controls.TagSelectorControl" %>
<span>
	<asp:TextBox ID="txtTagsAutoComplete" runat="server" Rows="3" Columns="50" TextMode="MultiLine"
		AutoCompleteType="None"></asp:TextBox>
    
 

	<a href="<%=SiteUtils.GetNavigationSiteRoot()%>/Quantifi/Controls/Tags/ViewTags.aspx" target="_blank" style="margin-left: 10px; font-size:smaller;
		vertical-align: top;">Edit All Tags</a></span>
    

<script type="text/javascript">

  $(document).ready(function() {


    function split(val) {
      return val.split( /,/ );
    }

    function extractLast(term) {
      return split(term).pop();
    }

    function excludeList(term) {
      var array = split(term);
      var last = array.pop();
      return array.join(",");
    }

    $("#<%=txtTagsAutoComplete.ClientID %>").autocomplete({
      source: function(request, response) {
        $.getJSON("<%=SiteUtils.GetNavigationSiteRoot()%>/Quantifi/Controls/Tags/TagsHandler.ashx", {
          term: extractLast(request.term),
          exclude: excludeList(request.term)
        }, response);
      },
      search: function() {
        // custom minLength
        var term = extractLast(this.value);
        if (term.length < 1) {
          return false;
        }
      },
      focus: function() {
        // prevent value inserted on focus
        return false;
      },
      select: function(event, ui) {

        var terms = split(this.value);
        // remove the current input
        terms.pop();
        // add the selected item
        terms.push(ui.item.Text);

        // add placeholder to get the comma-and-space at the end
        terms.push("");
        this.value = terms.join(",");
        return false;
      }
    })
      .data("autocomplete")._renderItem = function(ul, item) {
        return $("<li></li>")
          .data("item.autocomplete", item)
          .append("<a>" + item.Text + "</a>")
          .appendTo(ul);
      };

  });

</script>
