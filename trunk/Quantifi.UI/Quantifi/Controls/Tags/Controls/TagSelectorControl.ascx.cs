﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Shared;
using mojoPortal.Web.UI;

namespace Quantifi.UI.Controls.Tags.Controls
{
	public partial class TagSelectorControl : System.Web.UI.UserControl, ISettingControl
	{

		/// <summary>
		/// 
		/// </summary>
		public string Text
		{
			get { return this.txtTagsAutoComplete.Text; }
			set { this.txtTagsAutoComplete.Text = value; }
		}


		public int Rows
		{
			get { return this.txtTagsAutoComplete.Rows; }
			set { this.txtTagsAutoComplete.Rows = value; }
		}

		public int Columns
		{
			get { return this.txtTagsAutoComplete.Columns; }
			set { this.txtTagsAutoComplete.Columns = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public List<string> Tags
		{
			get
			{
				return this.txtTagsAutoComplete.Text.FromDelimitedString().ToList();
			}
			set
			{
				this.txtTagsAutoComplete.Text = value.ToDelimitedString(",");
			}
		}


		protected void Page_Load(object sender, EventArgs e)
		{
 
		}

		#region ISettingControl Members

		public void SetValue(string val)
		{
			this.Text = val;
		}

		public string GetValue()
		{
			return this.Text;
		}

		#endregion
	}
}