﻿<%@ Page Title="Manage Tags" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
  AutoEventWireup="true" CodeBehind="ViewTags.aspx.cs" Inherits="Quantifi.UI.Controls.Tags.ViewTags"
  MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <div id="tabs">
    <ul>
      <li><a href="#tabs-1">Manage Tags</a></li>
      <li><a href="#tabs-2">View By Relevance</a></li>
    </ul>
    <div id="tabs-1">
      <p>
        <asp:Label runat="server">All Tags</asp:Label><span style="padding: 0 16px;">&nbsp;</span>
        <asp:HyperLink runat="server" NavigateUrl="~/Quantifi/Controls/Tags/TagsHandler.ashx?sort=bycount&format=csv"
          Font-Overline="false" Font-Size="12px" Target="_blank"><img src="/Data/SiteImages/Icons/xls.png" 
					alt="csv" /> Export To CSV</asp:HyperLink>
      </p>
      <asp:DataList ID="dlTags" runat="server" ViewStateMode="Enabled" BorderColor="DarkGray"
        BorderWidth="1px" BorderStyle="Solid">
        <AlternatingItemStyle BackColor="LightGray" />
        <ItemTemplate>
          <div class="">
            <span>&nbsp;</span>
            <asp:ImageButton runat="server" ID="btnEdit" ImageUrl="/Data/SiteImages/pencil.png"
              CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>'
              CssClass="vMiddle" ToolTip='Edit Tag' AlternateText='Edit Tag' />
            <span>&nbsp;&nbsp;</span>
            <asp:ImageButton runat="server" ID="btnDelete" ImageUrl="/Data/SiteImages/trash.png"
              CssClass="vMiddle" CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>'
              ToolTip='Delete Tag' AlternateText='Delete Tag' />
            <span style="margin: 0 8px;">
              <%# (Container.ItemIndex + 1 ) %>
            </span><span style="margin: 0 8px;">
              <%# DataBinder.Eval(Container.DataItem, "Text") %></span> <span style="margin: 0 8px;
                float: right;"><a target="_blank" href='/Tags.aspx?tag=<%# DataBinder.Eval(Container.DataItem, "Text") %>&tid=<%# DataBinder.Eval(Container.DataItem, "Id") %>'>
                  Count:
                  <%# DataBinder.Eval(Container.DataItem, "TaggedCount")%></a></span>
          </div>
        </ItemTemplate>
        <EditItemTemplate>
          <div style="margin: 1px 10px; position: relative;">
            <div style="position: absolute; left: -30px; top: 4px;">
              <img src="/Data/SiteImages/indicators/indicator_arrows.gif" />
            </div>
            <asp:TextBox ID="textboxEditTagText" runat="server" MaxLength="50" Columns="50" CssClass="widetextbox"
              Text='<%# DataBinder.Eval(Container.DataItem, "Text") %>' />&nbsp; &nbsp;
            <asp:ImageButton runat="server" ImageUrl="/Data/SiteImages/ApproveChanges.gif" ToolTip="Save"
              CssClass="vMiddle" CommandName="apply" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>'
              runat="server" ID="btnApply" />
            &nbsp;&nbsp;&nbsp;
            <asp:ImageButton runat="server" ID="btnCancel" ImageUrl="/Data/SiteImages/close.png"
              CssClass="vMiddle" CommandName="cancel" ToolTip='Cancel' AlternateText='Cancel' />
            <asp:Panel ID="pnlMessage" runat="server" Visible="false" EnableViewState="false">
            </asp:Panel>
          </div>
        </EditItemTemplate>
      </asp:DataList>
    </div>
    <div id="tabs-2">
      <div class="dayOptions">
        <p>
          <asp:Label ID="lblRelavance" runat="server"></asp:Label>
          <span style="padding: 0 16px;">&nbsp;</span>
          <asp:HyperLink ID="linkGetTagsByRelavanceCSV" runat="server" NavigateUrl="~/Quantifi/Controls/Tags/TagsHandler.ashx?sort=bycount"
            Font-Overline="false" Font-Size="12px" Target="_blank"><img src="/Data/SiteImages/Icons/xls.png" 
					alt="csv" /> Export To CSV</asp:HyperLink>
        </p>
        <div id="subTab">
          <div class="dayOptionsData">
            <asp:DataList ID="dlCurrentTags" runat="server" ViewStateMode="Disabled" BorderColor="DarkGray"
              BorderWidth="1px" BorderStyle="Solid">
              <AlternatingItemStyle BackColor="LightGray" />
              <ItemTemplate>
                <div class="">
                  <span style="margin: 0 8px;">
                    <%# (Container.ItemIndex + 1 ) %>
                  </span><span style="margin: 0 8px;">
                    <%# DataBinder.Eval(Container.DataItem, "Text") %>
                  </span><span style="margin: 0 8px; float: right;"><a target="_blank" href='/Tags.aspx?tag=<%# DataBinder.Eval(Container.DataItem, "Text") %>&tid=<%# DataBinder.Eval(Container.DataItem, "Id") %>'>
                    Count:
                    <%# DataBinder.Eval(Container.DataItem, "TaggedCount")%></a> </span>
                </div>
              </ItemTemplate>
            </asp:DataList>
          </div>
        </div>
      </div>
      <script type="text/javascript">


        $("#tabs").tabs({
          cookie: {
            // store cookie for a day, without, it would be a session cookie
            expires: 1
          }
        });
      </script>
    </div>
  </div>
</asp:Content>
