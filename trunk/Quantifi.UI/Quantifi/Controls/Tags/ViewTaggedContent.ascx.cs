﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.UI.Blogs;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;
using Quantifi.Business.TagsModule;
using Quantifi.UI.Controls.Tags.Components;
using Quantifi.UI.Controls.BlogsModule.Controls;
using Quantifi.Business.Utils;
using Quantifi.Data.EF;
using Quantifi.Shared;
using mojoPortal.Web.Framework;


namespace Quantifi.UI.Controls.Tags
{
  public partial class ViewTaggedContent : SiteModuleControl
  {

    #region Properties
    private int pageId = -1;
    private int moduleId = -1;
    private int tagId = -1;
    private string tagText = string.Empty;

    new private SiteSettings siteSettings = null;
    private mojoBasePage basePage = null;
    protected Double TimeOffset = 0;
    private TimeZoneInfo timeZone = null;


    private ViewTaggedContentConfiguration config = null;

    private int pageNumber = 1;
    private int totalPages = 1;
    private int pageSize = 3;

    private readonly DateTime recentUpToDate = DateTime.UtcNow.AddDays(-7);

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
      basePage = this.Page as mojoBasePage;

      LoadParams();

      LoadData();

      SetupAdminLinks();
    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      bool? left = (config.ForceShowLeftColumn) ? true : null as bool?;
      bool? right = (config.ForceShowRightColumn) ? true : null as bool?;


      basePage.ForceSideContent(left, right);

    }

    private void SetupAdminLinks()
    {
      if (this.IsEditable)
      {
        this.pnlAdminWrapper.Visible = true;
        this.adminEditTagsLink.NavigateUrl = "~/Quantifi/Controls/Tags/ViewTags.aspx?mid=" + this.ModuleId + "&pageid=" + this.PageId;
        this.adminLink.NavigateUrl = "~/Admin/ModuleSettings.aspx?mid=" + this.ModuleId + "&pageid=" + this.PageId;
      }

    }

    private void LoadParams()
    {
      siteSettings = CacheHelper.GetCurrentSiteSettings();
      TimeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();


      pageId = WebUtils.ParseInt32FromQueryString("pageid", pageId);
      moduleId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("mid", moduleId);
      pageNumber = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pg", pageNumber);
      tagId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("tid", tagId);


      tagText = mojoPortal.Web.Framework.WebUtils.ParseStringFromQueryString("tag", tagText);
    }

    private void LoadData()
    {

      config = new ViewTaggedContentConfiguration(Settings);

      pageSize = config.ItemsPerPage;




      using (var dc = new CorpWebEntities())
      {

        var tag = dc.Tags.SingleOrDefault(t => t.Id == tagId);

        if (tag == null)
        {
          Response.Redirect("~/news.aspx");
          return;
        }



        totalPages = tag.BlogPosts.GetTotalPages(pageSize);


        var pageUrlFormat = "?tag=" + tag.Text
                        + "&amp;tid=" + tag.Id.ToString()
                        + "&amp;pg={0}";

        pgr.PageURLFormat = pageUrlFormat;
        pgr.ShowFirstLast = true;
        pgr.PageSize = pageSize;
        pgr.PageCount = totalPages;
        pgr.CurrentIndex = pageNumber;
        pgr.Visible = (totalPages > 1);


        rptItems.DataSource = tag.BlogPosts.OrderByDescending(bp => bp.StartDate).Page(pageNumber, pageSize);
        rptItems.DataBind();


        this.litTitle.Text = tag.Text;
        this.basePage.Title = tag.Text + ((pageNumber > 1) ? " - Page " + pageNumber.ToString() : string.Empty) +
                              " - Quantifi";


      }

 

      var mojoBasePage = this.Page as mojoBasePage;

      if (mojoBasePage == null) return;


      mojoBasePage.MPLeftPane.Visible = true;
      mojoBasePage.MPLeftPane.Parent.Visible = true;
    }

    private void SetNoDataView()
    {
      this.litTitle.Text = "All Tags";
      this.basePage.Title = "All Tags - Quantifi";
      //this.litTagName.Text = (string.IsNullOrWhiteSpace(tagText)) ? "-blank-" : tagText;
      this.pnlNoData.Visible = true;

      this.pgr.Visible = false;
      //this.rptItems.Visible = false;
      //this.pgr.Visible = false;

      this.rptAllTags.DataSource = ContentTag.GetAllTags().Where(t => t.TaggedCount > 0);
      this.rptAllTags.DataBind();
    }




    //protected string GetObjNameCss(object obj)
    //{
    //  var type = obj.GetType();

    //  return type.Name.ToString();
    //}

    //protected string GetTimeStamp(dynamic iTaggableObj)
    //{
    //  if (!config.ShowTimeStamp || iTaggableObj == null)
    //    return string.Empty;


    //  return iTaggableObj.GetStartDate.ToString(Config.TimeStampFormatString);


    //}

    //protected string GetTimeStamp(dynamic iTaggableObj)
    //{
    //  if (!Config.ShowTimeStamp || iTaggableObj == null)
    //    return string.Empty;


    //  return iTaggableObj.GetStartDate.ToString(Config.TimeStampFormatString);


    //}

    private Dictionary<int, Quantifi.UI.Blogs.BlogConfiguration> moduleConfigs = new Dictionary<int, BlogConfiguration>();

    protected string FormatBlogEntry(string blogHtml, string excerpt, string url, int itemId, int postModuleId)
    {
      BlogConfiguration moduleConfig;
      if (moduleConfigs.ContainsKey(postModuleId))
      {
        moduleConfig = moduleConfigs[postModuleId];
      }
      else
      {
        moduleConfig = new Quantifi.UI.Blogs.BlogConfiguration(ModuleSettings.GetModuleSettings(postModuleId));
        moduleConfigs.Add(postModuleId, moduleConfig);
      }

      var MoreLink = " <a href='" + FormatBlogUrl(url, itemId) + "' class='blogMoreLink' >" + moduleConfig.MoreLinkText + "</a>";

      if ((excerpt.Length > 0) && (excerpt != "<p>&#160;</p>"))
      {
        var trimedExcerpt = excerpt.Trim();

        if (trimedExcerpt.EndsWith("</p>"))
        {
          return trimedExcerpt.Substring(0, trimedExcerpt.Length - 4) + moduleConfig.ExcerptSuffix + MoreLink + "</p>";
        }
        else if (trimedExcerpt.EndsWith("</div>"))
        {
          return trimedExcerpt.Substring(0, trimedExcerpt.Length - 6) + moduleConfig.ExcerptSuffix + MoreLink + "</div>";
        }
        else
        {
          return excerpt + moduleConfig.ExcerptSuffix + MoreLink;
        }
      }



      string result = string.Empty;
      if ((blogHtml.Length > moduleConfig.ExcerptLength) && (moduleConfig.MoreLinkText.Length > 0))
      {

        result = UIHelper.CreateExcerpt(blogHtml, moduleConfig.ExcerptLength, moduleConfig.ExcerptSuffix);
        result += MoreLink;
        return result;
      }



      return blogHtml;
    }

    protected string FormatBlogDate(DateTime startDate)
    {


      var formatStr = "D";

      if (DateTime.Compare(recentUpToDate, startDate) < 0)
        formatStr = "d MMMM yyyy";
      else


        if (timeZone != null)
        {
          return TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone).ToString(formatStr);
        }

      return startDate.AddHours(TimeOffset).ToString(formatStr);

    }

    protected string FormatBlogUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
             + "&ItemID=" + itemId.ToInvariantString()
             + "&mid=" + ModuleId.ToInvariantString();

    }

    protected string FormatBlogTitleUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
          + "&ItemID=" + itemId.ToInvariantString()
          + "&mid=" + ModuleId.ToInvariantString();

    }
  }
}