﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using Quantifi.Business.TagsModule;
using mojoPortal.Web.Framework;
using Resources;
using Quantifi.Business.Utils;
using mojoPortal.Business.WebHelpers;
using System.Collections;
using mojoPortal.Business;
using Quantifi.UI.Controls.Tags.Components;


namespace Quantifi.UI.Controls.Tags
{

	
	public partial class ViewTags : NonCmsBasePage
	{

    protected int ModuleId
    {
      get
      {
        return mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("mid", -1);       
      }

    }

    private Hashtable settings;
    protected Hashtable Settings
    {
      get
      {
        if (settings == null) settings = ModuleSettings.GetModuleSettings(ModuleId);
        return settings;
      }
    }


		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.dlTags.ItemCommand += new DataListCommandEventHandler(dlTags_ItemCommand);
			this.dlTags.ItemDataBound += new DataListItemEventHandler(dlTags_ItemDataBound);
		}

		
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				SiteUtils.RedirectToLoginPage(this);
				return;
			}

			SecurityHelper.DisableBrowserCache();

			SuppressPageMenu();

 
			if (!WebUser.IsAdminOrContentAdmin && !SiteUtils.UserIsSiteEditor())
			{
				SiteUtils.RedirectToEditAccessDeniedPage();
				return;
			}


			if (!IsPostBack)
				LoadData();
		}

		private void LoadData()
		{
      //this.dlTags.DataSource = ContentTag.GetAllTagsByTaggedCount();
			this.dlTags.DataSource = ContentTag.GetAllTags();
			this.dlTags.DataBind();


      var config = new TagsCloudConfiguration(Settings);



      lblRelavance.Text = string.Format("Top {1} Tags in the last {0} days", config.TaggedRelevanceInDays, config.MaxTagsCount);
      linkGetTagsByRelavanceCSV.NavigateUrl = string.Format("~/Quantifi/Controls/Tags/TagsHandler.ashx?days={0}&format=csv", config.TaggedRelevanceInDays);

      dlCurrentTags.DataSource = ContentTagCacheUtil.GetTopTagsByDate(config.TaggedRelevanceInDays, config.MaxTagsCount);
      dlCurrentTags.DataBind();
		}
	
		protected void dlTags_ItemDataBound(object sender, DataListItemEventArgs e)
		{
      ImageButton btnDelete = e.Item.FindControl("btnDelete") as ImageButton;
			UIHelper.AddConfirmationDialog(btnDelete, QuantifiTagsResources.DeleteTagWarningMsg);
		}

		protected void dlTags_ItemCommand(object source, DataListCommandEventArgs e)
		{
			DataList dlTags = source as DataList;
			var tagId = -1;

			switch (e.CommandName)
			{
				case "edit":
					this.dlTags.EditItemIndex = e.Item.ItemIndex;
					LoadData();
					break;

				case "cancel":
					this.dlTags.EditItemIndex = -1;
					LoadData();
					break;

				case "apply":
					var textbox = e.Item.FindControl("textboxEditTagText") as TextBox;
					if (textbox == null) { return; }


					var panelMessage = e.Item.FindControl("pnlMessage") as Panel;
					if (panelMessage == null) { return; }



					var tagText = textbox.Text;
					if (String.IsNullOrWhiteSpace(tagText))
					{
						ShowErrorMsg(panelMessage, "Sorry Text Can't be empty");
						break;
					}
					tagId = int.Parse(e.CommandArgument.ToString());
					var tagCheck = ContentTag.TagCheck(tagText);

					if (tagCheck != null && tagCheck.Id != tagId)
					{
  
						ShowErrorMsg(panelMessage, "Sorry a Tag already exists with Text [ " + tagCheck.Text + " ]");
						break;
					}

					var tag = ContentTag.GetTag(tagId);

					if (tag != null)
					{
						tag.Text = tagText;
						tag.Save();

						var filePath = CacheUtils.GetCacheDependencyFilePath(ContentTag.GetCacheDependecyFileName());

						CacheHelper.TouchCacheFile(filePath);

					}
				

					this.dlTags.EditItemIndex = -1;
					LoadData();
					break;

				case "delete":

					tagId = int.Parse(e.CommandArgument.ToString());
					var DeleteTag = ContentTag.GetTag(tagId);

					if (DeleteTag != null)
					{
						DeleteTag.Delete();


						var filePath = CacheUtils.GetCacheDependencyFilePath(ContentTag.GetCacheDependecyFileName());

						CacheHelper.TouchCacheFile(filePath);
					}

					this.dlTags.EditItemIndex = -1;
				
					LoadData();
					break;

				default:
					break;

			}
		}
	
		private void ShowErrorMsg(Panel panel, string msg)
		{
			var litMessage = new LiteralControl(msg);
			panel.Controls.Add(litMessage);
			panel.Visible = true;
			panel.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
			panel.Style.Add(HtmlTextWriterStyle.Color, "Red");
			panel.Style.Add(HtmlTextWriterStyle.FontWeight, "700");
			panel.Style.Add(HtmlTextWriterStyle.MarginTop, "5px;");
			panel.Style.Add(HtmlTextWriterStyle.MarginBottom, "5px;");

			panel.Style.Add(HtmlTextWriterStyle.Padding, "3px;");
			panel.Style.Add(HtmlTextWriterStyle.BorderColor, "Red");
			panel.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
			panel.Style.Add(HtmlTextWriterStyle.BorderStyle, "solid");

		}

	}
}