﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Quantifi.Business.TagsModule;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using System.Text;
using System.Globalization;

namespace Quantifi.UI.Controls.Tags
{
  /// <summary>
  /// Summary description for TagsSiteMap
  /// </summary>
  public class TagsSiteMap : IHttpHandler
  {

    public void ProcessRequest(HttpContext context)
    {
      GenerateSiteMap(context);
    }

    private void GenerateSiteMap(HttpContext context)
    {
      context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(1));
      context.Response.Cache.SetCacheability(HttpCacheability.Public);

      context.Response.ContentType = "application/xml";
      Encoding encoding = new UTF8Encoding();
      context.Response.ContentEncoding = encoding;

      using (XmlTextWriter xmlTextWriter = new XmlTextWriter(context.Response.OutputStream, encoding))
      {
        xmlTextWriter.Formatting = Formatting.Indented;

        xmlTextWriter.WriteStartDocument();

        xmlTextWriter.WriteStartElement("urlset");
        xmlTextWriter.WriteStartAttribute("xmlns");
        xmlTextWriter.WriteValue("http://www.sitemaps.org/schemas/sitemap/0.9");
        xmlTextWriter.WriteEndAttribute();


        AddTagUrls(context, xmlTextWriter);


        xmlTextWriter.WriteEndElement(); //urlset

        //end of document
        xmlTextWriter.WriteEndDocument();
        xmlTextWriter.Close();
      }



    }

    private void AddTagUrls(HttpContext context, XmlTextWriter xmlTextWriter)
    {

      string baseUrl = mojoPortal.Web.SiteUtils.GetNavigationSiteRoot();
      SiteSettings siteSettings = CacheHelper.GetCurrentSiteSettings();
      if (siteSettings == null) { return; }
      if (siteSettings.SiteGuid == Guid.Empty) { return; }


      var tags = ContentTagCacheUtil.GetAllTagsByTaggedCount();

      foreach(var tag in tags)
      {
   
          xmlTextWriter.WriteStartElement("url");
          xmlTextWriter.WriteElementString("loc", baseUrl + string.Format("/tags.aspx?tag={0}&tid={1}", tag.Text, tag.Id));
          xmlTextWriter.WriteElementString(
                  "lastmod",
                  DateTime.UtcNow.ToString("u", CultureInfo.InvariantCulture).Replace(" ", "T"));

          // maybe should use never for blog posts but in case it changes we do want to be re-indexed
          xmlTextWriter.WriteElementString("changefreq", "monthly");

          xmlTextWriter.WriteElementString("priority", "0.5");

          xmlTextWriter.WriteEndElement(); //url
    
      }


    }


    public bool IsReusable
    {
      get
      {
        return false;
      }
    }
  }
}