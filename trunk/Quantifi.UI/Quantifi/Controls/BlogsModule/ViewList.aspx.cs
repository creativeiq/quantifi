﻿// Author:				    Joe Audette
// Created:			        2010-05-22
// Last Modified:		    2010-05-23
// 
// The use and distribution terms for this software are covered by the 
// Common Public License 1.0 (http://opensource.org/licenses/cpl.php)
// which can be found in the file CPL.TXT at the root of this distribution.
// By using this software in any fashion, you are agreeing to be bound by 
// the terms of this license.
//
// You must not remove this notice, or any other, from this software.

using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Web.Framework;
using mojoPortal.Web.UI;
using Resources;
using mojoPortal.Web;
using QB = Quantifi.Business;

namespace Quantifi.UI.Blogs
{
  public partial class ViewList : mojoBasePage
  {
		#region Properties 

		protected int pageId = -1;
		protected int moduleId = -1;
		private bool userCanEdit = false;
		private int countOfDrafts = 0;
		private int pageNumber = 1;
    private int year = 0;

		private BlogConfiguration config;

		protected DateTime CalendarDate;

		#endregion

    #region OnInit

    protected override void OnPreInit(EventArgs e)
    {
      AllowSkinOverride = true;
      base.OnPreInit(e);
    }

    override protected void OnInit(EventArgs e)
    {
      this.Load += new EventHandler(this.Page_Load);
      base.OnInit(e);


    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
      LoadParams();

      if (!UserCanViewPage(moduleId))
      {
        SiteUtils.RedirectToAccessDeniedPage();
        return;
      }

      LoadSettings();
      PopulateControls();
      LoadPage();
    }

		protected override void OnPreRender(EventArgs e)
		{
			base.ForceSideContent(true, true);
			base.OnPreRender(e);
		}

    private void PopulateControls()
    {
      var mojoModule = GetModule(moduleId);

      //mojoModule.ModuleTitle = mojoModule.ModuleTitle + ""

      moduleTitle.EditUrl = SiteRoot + "/Quantifi/Controls/BlogsModule/EditPost.aspx";
      moduleTitle.EditText = QuantifiBlogResources.BlogAddPostLabel;
      moduleTitle.ModuleInstance = mojoModule;
      moduleTitle.CanEdit = userCanEdit;
      moduleTitle.ModuleInstance.ModuleTitle = GetModuleTitle(mojoModule);

      if ((userCanEdit) && (countOfDrafts > 0))
      {
        moduleTitle.LiteralExtraMarkup =
            "&nbsp;<a href='"
            + SiteRoot
            + "/Quantifi/Controls/BlogsModule/EditCategory.aspx?pageid=" + pageId.ToInvariantString()
            + "&amp;mid=" + moduleId.ToInvariantString()
            + "' class='ModuleEditLink' title='" + QuantifiBlogResources.BlogEditCategoriesLabel + "'>" + QuantifiBlogResources.BlogEditCategoriesLabel + "</a>"
            + "&nbsp;<a href='"
            + SiteRoot
            + "/Quantifi/Controls/BlogsModule/Drafts.aspx?pageid=" + pageId.ToInvariantString()
            + "&amp;mid=" + moduleId.ToInvariantString()
            + "' class='ModuleEditLink' title='" + QuantifiBlogResources.BlogDraftsLink + "'>" + QuantifiBlogResources.BlogDraftsLink + "</a>";
      }
      else if (userCanEdit)
      {
        moduleTitle.LiteralExtraMarkup =
            "&nbsp;<a href='"
            + SiteRoot
            + "/Quantifi/Controls/BlogsModule/EditCategory.aspx?pageid=" + pageId.ToInvariantString()
            + "&amp;mid=" + moduleId.ToInvariantString()
            + "' class='ModuleEditLink' title='" + QuantifiBlogResources.BlogEditCategoriesLabel + "'>" + QuantifiBlogResources.BlogEditCategoriesLabel + "</a>";
      }

      config = new BlogConfiguration(ModuleSettings.GetModuleSettings(moduleId));

      postList.ModuleId = moduleId;
      postList.PageId = pageId;
      postList.IsEditable = userCanEdit;
      postList.Config = config;
      postList.SiteRoot = SiteRoot;
      postList.ImageSiteRoot = ImageSiteRoot;

      //make this page look as close as possible to the way a cms page with the blog module on it looks
      LoadSideContent(true, true);

    }

		//private void AddConnoicalUrl()
		//{
		//  if (Page.Header == null) { return; }

		//  Literal link = new Literal();
		//  link.ID = "blogcaturl";
		//  link.Text = "\n<link rel='canonical' href='"
		//      + SiteRoot
		//      + "/Quantifi/Controls/BlogsModule/ViewList.aspx?pageid="
		//      + pageId.ToInvariantString()
		//      + "&amp;mid=" + moduleId.ToInvariantString()
		//      + "&amp;pagenumber=" + pageNumber.ToInvariantString()
		//      + "' />";

		//  Page.Header.Controls.Add(link);

		//}

		//private void AddRssUrl()
		//{
		//  var rssId = "BlogRss" + moduleId.ToString();

		//  if (Page.Header == null) 
		//  { return; }

      
		//  foreach (Control c in Page.Header.Controls)
		//  {
		//    if ( c.ClientID.Equals(rssId))
		//      return;
		//  }


		//  Literal link = new Literal();
		//  link.ID = "blogcaturl";
		//  link.Text = "\n<link rel='canonical' id='" + rssId +  "' href='" 
		//      + SiteRoot
		//      + "/Quantifi/Controls/BlogsModule/ViewList.aspx?pageid="
		//      + pageId.ToInvariantString()
		//      + "&amp;mid=" + moduleId.ToInvariantString()
		//      + "&amp;pagenumber=" + pageNumber.ToInvariantString()
		//      + "' />";

		//  Page.Header.Controls.Add(link);

		//}

    private void LoadSettings()
    {
      userCanEdit = UserCanEditModule(moduleId);
      if (userCanEdit) { countOfDrafts = QB.Blog.CountOfDrafts(moduleId); }


    }

    private void LoadPage()
    {
      //EnsurePageAndSite();
      if (CurrentPage == null) 
        return;

      //LoadSettings();

      //bool redirected = RedirectIfNeeded();
      //if (redirected) { return; }
      //SetupAdminLinks();
      if (CurrentPage.PageId == -1) 
        return;

      if ((CurrentPage.ShowChildPageMenu) || (CurrentPage.ShowBreadcrumbs) || (CurrentPage.ShowChildPageBreadcrumbs) )
      {
        // this is needed to override some hide logic in
        // layout.Master.cs
        this.MPContent.Visible = true;
        this.MPContent.Parent.Visible = true;
      }

 
      Title = SiteUtils.FormatPageTitle(siteSettings, GetPageTitle(CurrentPage));
     


      if (CurrentPage.PageMetaKeyWords.Length > 0)
      {
        MetaKeywordCsv = CurrentPage.PageMetaKeyWords;
      }

      if (CurrentPage.PageMetaDescription.Length > 0)
      {
        MetaDescription = CurrentPage.PageMetaDescription;
      }

      if (CurrentPage.CompiledMeta.Length > 0)
      {
        AdditionalMetaMarkup = CurrentPage.CompiledMeta;
      }


      ForceSideContent(true, true);


    }

    private void LoadParams()
    {
      pageId = WebUtils.ParseInt32FromQueryString("pageid", pageId);
      moduleId = WebUtils.ParseInt32FromQueryString("mid", moduleId);
      pageNumber = WebUtils.ParseInt32FromQueryString("pg", pageNumber);
      year = WebUtils.ParseInt32FromQueryString("year", pageNumber);

    }

    private string GetPageTitle(PageSettings settings)
    {
      var title = settings.PageTitle;

      if (year > 2000 && year <= DateTime.Now.Year)
        title = title + " in " + year.ToInvariantString();

      if (pageNumber > 1 )
        title = title + " - Page " + pageNumber.ToInvariantString();

      return title;
    }

    private string GetModuleTitle(Module m)
    {
      if (year > 2000 && year <= DateTime.Now.Year)
        return m.ModuleTitle + " in " + year.ToInvariantString();

      return m.ModuleTitle;
    }

  }
}