///		Author:					Joe Audette
///		Created:				2005-03-15
///		Last Modified:			2009-11-22

using System;
using System.Linq;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Xml;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;
using mojoPortal.Web;
using QB = Quantifi.Business;

using log4net;
using System.Collections.Generic;
using Argotic.Syndication;
using Quantifi.UI.Controls.BlogsModule.Components;
using Quantifi.Shared;

namespace Quantifi.UI.Blogs
{

  public partial class RssPage : Page
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(RssPage));

    #region Properties

    private int moduleId = -1;
    private int pageID = -1;
    private mojoPortal.Business.SiteSettings siteSettings = null;
    private mojoPortal.Business.PageSettings currentPage = null;
    private mojoPortal.Business.Module module = null;
    //private string baseUrl = string.Empty;
    private bool addSignature = false;
    Hashtable moduleSettings = null;
    private bool addCommentsLink = false;
    private string navigationSiteRoot = string.Empty;
    private string blogBaseUrl = string.Empty;
    private string imageSiteRoot = string.Empty;
    private string cssBaseUrl = string.Empty;
    private bool feedIsDisabled = false;
    private bool useExcerptInFeed = false;
    private string ExcerptSuffix = "...";
    private string MoreLinkText = "read more";
    private int excerptLength = 250;
    private bool ShowPostAuthorSetting = false;
    private bool bypassPageSecurity = false;
    private Guid securityBypassGuid = Guid.Empty;

    private bool isAggregateBlogFeed = false;

    #endregion

    protected void Page_Load(object sender, System.EventArgs e)
    {
      // nothing should post here
      if (Page.IsPostBack) return;

      LoadSettings();

      #region AllowForcingPreferredHostName
      if (WebConfigSettings.AllowForcingPreferredHostName)
      {
        EnsureSiteSettings();


        if ((siteSettings != null) && (siteSettings.PreferredHostName.Length > 0))
        {
          string requestedHostName = WebUtils.GetHostName();
          if (siteSettings.PreferredHostName != requestedHostName)
          {
            string redirectUrl = "http://" + siteSettings.PreferredHostName + Request.RawUrl;


            log.Info("received a request for hostname " + requestedHostName + ", redirecting to preferred host name " + redirectUrl);

#if ! NET35
            if (WebConfigSettings.Use301RedirectWhenEnforcingPreferredHostName)
            {
              Response.RedirectPermanent(redirectUrl, true);
              return;
            }
#endif


            Response.Redirect(redirectUrl, true);
            return;

          }
        }
      }
      #endregion

      if ((moduleId > -1) && (module != null) && (currentPage != null))
      {
        if (isAggregateBlogFeed)
        {
          RenderAggregatedRss();
        }
        else
        {
          RenderRss(moduleId);
        }
      }
      else
      {
        RenderError("Invalid Request");
      }

    }

    protected void EnsureSiteSettings()
    {
      if (siteSettings == null) siteSettings = CacheHelper.GetCurrentSiteSettings();

    }

    private void LoadSettings()
    {
      pageID = WebUtils.ParseInt32FromQueryString("pageid", -1);
      moduleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      securityBypassGuid = WebUtils.ParseGuidFromQueryString("g", securityBypassGuid);
      currentPage = CacheHelper.GetCurrentPage();

      // this is for a special case of consuming a feed from a private page using the feed manager
      if ((securityBypassGuid != Guid.Empty) && (securityBypassGuid == WebConfigSettings.InternalFeedSecurityBypassKey))
      {
        bypassPageSecurity = true;
      }

      // TODO: need to make this bring back page view roles
      // so we don't have to get the pagesettings object
      //module = new Module(moduleId, pageID);

      if (WebConfigSettings.UseFoldersInsteadOfHostnamesForMultipleSites)
      {
        navigationSiteRoot = SiteUtils.GetNavigationSiteRoot();
        blogBaseUrl = navigationSiteRoot;
        imageSiteRoot = WebUtils.GetSiteRoot();
        cssBaseUrl = imageSiteRoot;
      }
      else
      {
        navigationSiteRoot = WebUtils.GetHostRoot();
        blogBaseUrl = SiteUtils.GetNavigationSiteRoot();
        imageSiteRoot = navigationSiteRoot;
        cssBaseUrl = WebUtils.GetSiteRoot();

      }



      object value = GetModule();


      if (value != null)
      {
        module = (Module)value;
      }

    }

    private mojoPortal.Business.Module GetModule()
    {

      Module m = null;
      if (currentPage != null)
      {
        foreach (Module module in currentPage.Modules)
        {
          if (module.ModuleId == moduleId)
            m = module;
        }
      }

      if (m == null) return null;
      if (m.ModuleId == -1) return null;

      if (m.ControlSource.ToLower().EndsWith("blogmodule.ascx"))
      {
        return m;
      }
      if (m.ControlSource.ToLower().EndsWith("aggregatedblogviewmodule.ascx"))
      {
        isAggregateBlogFeed = true;
        return m;
      }

      return null;
    }

    private void RenderRss(int moduleId)
    {
      /*
			 
      For more info on RSS 2.0
      http://www.feedvalidator.org/docs/rss2.html
			
      Fields not implemented yet:
      <blogChannel:blogRoll>http://radio.weblogs.com/0001015/userland/scriptingNewsLeftLinks.opml</blogChannel:blogRoll>
      <blogChannel:mySubscriptions>http://radio.weblogs.com/0001015/gems/mySubscriptions.opml</blogChannel:mySubscriptions>
      <blogChannel:blink>http://diveintomark.org/</blogChannel:blink>
      <lastBuildDate>Mon, 30 Sep 2002 11:00:00 GMT</lastBuildDate>
      <docs>http://backend.userland.com/rss</docs>
			 
      */



      Response.Cache.SetExpires(DateTime.Now.AddMinutes(30));
      Response.Cache.SetCacheability(HttpCacheability.Public);
      //Response.Cache.VaryByParams["g"] = true;

      Response.ContentType = "application/xml";

      moduleSettings = ModuleSettings.GetModuleSettings(moduleId);

      addSignature = WebUtils.ParseBoolFromHashtable(
                moduleSettings, "RSSAddSignature", false);

      ShowPostAuthorSetting = WebUtils.ParseBoolFromHashtable(
          moduleSettings, "ShowPostAuthorSetting", ShowPostAuthorSetting);

      addCommentsLink = WebUtils.ParseBoolFromHashtable(
          moduleSettings, "RSSAddCommentsLink", false);

      feedIsDisabled = WebUtils.ParseBoolFromHashtable(
          moduleSettings, "BlogDisableFeedSetting", feedIsDisabled);

      useExcerptInFeed = WebUtils.ParseBoolFromHashtable(
          moduleSettings, "UseExcerptInFeedSetting", useExcerptInFeed);

      excerptLength = WebUtils.ParseInt32FromHashtable(
          moduleSettings, "BlogExcerptLengthSetting", excerptLength);

      if (moduleSettings.Contains("BlogExcerptSuffixSetting"))
      {
        ExcerptSuffix = moduleSettings["BlogExcerptSuffixSetting"].ToString();
      }

      if (moduleSettings.Contains("BlogMoreLinkText"))
      {
        MoreLinkText = moduleSettings["BlogMoreLinkText"].ToString();
      }


      //


      //siteRoot = WebUtils.GetSiteRoot();

      Encoding encoding = new UTF8Encoding();
      Response.ContentEncoding = encoding;

      using (XmlTextWriter xmlTextWriter = new XmlTextWriter(Response.OutputStream, encoding))
      {
        xmlTextWriter.Formatting = Formatting.Indented;

        xmlTextWriter.WriteStartDocument();


        //////////////////
        // style for RSS Feed viewed in browsers
        if (ConfigurationManager.AppSettings["RSSCSS"] != null)
        {
          string rssCss = ConfigurationManager.AppSettings["RSSCSS"].ToString();
          xmlTextWriter.WriteWhitespace(" ");
          xmlTextWriter.WriteRaw("<?xml-stylesheet type=\"text/css\" href=\"" + cssBaseUrl + rssCss + "\" ?>");

        }

        if (ConfigurationManager.AppSettings["RSSXsl"] != null)
        {
          string rssXsl = ConfigurationManager.AppSettings["RSSXsl"].ToString();
          xmlTextWriter.WriteWhitespace(" ");
          xmlTextWriter.WriteRaw("<?xml-stylesheet type=\"text/xsl\" href=\"" + cssBaseUrl + rssXsl + "\" ?>");

        }
        ///////////////////////////


        xmlTextWriter.WriteComment("RSS generated by mojoPortal Blog Module V 1.0 on "
            + DateTime.Now.ToLongDateString());

        xmlTextWriter.WriteStartElement("rss");

        xmlTextWriter.WriteStartAttribute("version", "");
        xmlTextWriter.WriteString("2.0");
        xmlTextWriter.WriteEndAttribute();

        xmlTextWriter.WriteStartElement("channel");
        /*  
            RSS 2.0
            Required elements for channel are title link and description
        */

        xmlTextWriter.WriteStartElement("title");
        xmlTextWriter.WriteString(module.ModuleTitle);
        xmlTextWriter.WriteEndElement();

        // this assumes a valid pageid passed in url
        string blogUrl = SiteUtils.GetCurrentPageUrl();

        xmlTextWriter.WriteStartElement("link");
        xmlTextWriter.WriteString(blogUrl);
        xmlTextWriter.WriteEndElement();

        xmlTextWriter.WriteStartElement("description");
        xmlTextWriter.WriteString(moduleSettings["BlogDescriptionSetting"].ToString());
        xmlTextWriter.WriteEndElement();

        xmlTextWriter.WriteStartElement("copyright");
        xmlTextWriter.WriteString(moduleSettings["BlogCopyrightSetting"].ToString());
        xmlTextWriter.WriteEndElement();

        // begin optional RSS 2.0 fields

        //ttl = time to live in minutes, how long a channel can be cached before refreshing from the source
        xmlTextWriter.WriteStartElement("ttl");
        xmlTextWriter.WriteString(moduleSettings["BlogRSSCacheTimeSetting"].ToString());
        xmlTextWriter.WriteEndElement();

        //protection from scrapers wnating to add you to the spam list
        string authorEmail = moduleSettings["BlogAuthorEmailSetting"].ToString().Replace("@", "@nospam");

        xmlTextWriter.WriteStartElement("managingEditor");
        xmlTextWriter.WriteString(authorEmail);
        xmlTextWriter.WriteEndElement();

        xmlTextWriter.WriteStartElement("generator");
        xmlTextWriter.WriteString("Quantifi Blog Module V 1.1");
        xmlTextWriter.WriteEndElement();


        /*
        <image>
          <url>http://www.snook.ca/img/rss_banner.gif</url>
          <title>Snook.ca</title>
          <link>http://www.snook.ca/jonathan/</link>
          <width>111</width>
          <height>32</height>
          <description>Snook.ca features tips, tricks, and bookmarks on web development</description>
        </image>
        */
        // Start Feed Logo 
        xmlTextWriter.WriteStartElement("image");

        xmlTextWriter.WriteStartElement("url");
        xmlTextWriter.WriteString("http://www.quantifisolutions.com/Quantifi/Images/quantify_logo_214_50.png");
        xmlTextWriter.WriteEndElement();

        xmlTextWriter.WriteStartElement("title");
        xmlTextWriter.WriteString("Quantifi Solutions");
        xmlTextWriter.WriteEndElement();

        xmlTextWriter.WriteStartElement("link");
        xmlTextWriter.WriteString("http://www.quantifisolutions.com/?rsslogo");
        xmlTextWriter.WriteEndElement();

        xmlTextWriter.WriteStartElement("width");
        xmlTextWriter.WriteString("214");
        xmlTextWriter.WriteEndElement();

        xmlTextWriter.WriteStartElement("height");
        xmlTextWriter.WriteString("50");
        xmlTextWriter.WriteEndElement();


        xmlTextWriter.WriteStartElement("description");
        xmlTextWriter.WriteString("Quantifi is a leading provider of analytics, trading and risk management software.");
        xmlTextWriter.WriteEndElement();

        //end image
        xmlTextWriter.WriteEndElement();


        // check if the user has page view permission

        if (
            (!feedIsDisabled)
            && (currentPage.ContainsModule(moduleId))
            && ((bypassPageSecurity) || (WebUser.IsInRoles(currentPage.AuthorizedRoles)))
            )
        {
          RenderItems(xmlTextWriter);
        }
        else
        {
          //beginning of blog entry
          xmlTextWriter.WriteStartElement("item");
          xmlTextWriter.WriteStartElement("title");
          xmlTextWriter.WriteString("this feed is not available");
          xmlTextWriter.WriteEndElement();

          xmlTextWriter.WriteStartElement("link");
          xmlTextWriter.WriteString(navigationSiteRoot);
          xmlTextWriter.WriteEndElement();

          xmlTextWriter.WriteStartElement("pubDate");
          xmlTextWriter.WriteString(DateTime.UtcNow.ToString("r"));
          xmlTextWriter.WriteEndElement();

          xmlTextWriter.WriteStartElement("guid");
          xmlTextWriter.WriteString(navigationSiteRoot);
          xmlTextWriter.WriteEndElement();

          //end blog entry
          xmlTextWriter.WriteEndElement();

        }


        //end of document
        xmlTextWriter.WriteEndElement();

      }



    }

    private void RenderItems(XmlTextWriter xmlTextWriter)
    {
      //string blogCommentLabel = ConfigurationManager.AppSettings["BlogCommentCountLabel"];

      //var totalPages = 1;
      //List<QB.Blog> items = (this.isAggregateBlogFeed) ? 
      //  QB.BlogCacheUtil.GetAggregatePage(new List<int>() { }, 1, 10, out totalPages) : 
      //  QB.BlogCacheUtil.GetPage(module.ModuleId, 1, 10, out totalPages);




      using (IDataReader dr = QB.Blog.GetBlogs(moduleId, DateTime.UtcNow))
      {
        //write channel items
        while (dr.Read())
        {
          string inFeed = dr["IncludeInFeed"].ToString();
          if (inFeed == "True" || inFeed == "1")
          {
            //beginning of blog entry
            xmlTextWriter.WriteStartElement("item");

            var itemUrl = dr["ItemUrl"].ToString();
            var itemId = Convert.ToInt32(dr["ItemID"]);

            string blogItemUrl = FormatBlogUrl(itemUrl, itemId);

            /*  
            RSS 2.0
            All elements of an item are optional, however at least one of title or description 
            must be present.
            */

            xmlTextWriter.WriteStartElement("title");
            xmlTextWriter.WriteString(dr["Heading"].ToString());
            xmlTextWriter.WriteEndElement();

            xmlTextWriter.WriteStartElement("link");
            xmlTextWriter.WriteString(blogItemUrl);
            xmlTextWriter.WriteEndElement();

            xmlTextWriter.WriteStartElement("pubDate");
            xmlTextWriter.WriteString(Convert.ToDateTime(dr["StartDate"]).ToString("r"));
            xmlTextWriter.WriteEndElement();

            xmlTextWriter.WriteStartElement("guid");
            xmlTextWriter.WriteString(blogItemUrl);
            xmlTextWriter.WriteEndElement();

            if (ShowPostAuthorSetting)
            {
              xmlTextWriter.WriteStartElement("author");
              // techically this is supposed to be an email address
              // but wouldn't that lead to a lot of spam?
              xmlTextWriter.WriteString(dr["Name"].ToString());
              xmlTextWriter.WriteEndElement();


            }

            xmlTextWriter.WriteStartElement("comments");
            xmlTextWriter.WriteString(blogItemUrl);
            xmlTextWriter.WriteEndElement();

            string signature = string.Empty;

            //if (addSignature)
            //{
            //  signature = "<br /><a href='" + imageSiteRoot + "'>"
            //      + dr["Name"].ToString() + "</a>";

            //}
            ////else
            //{
            //    signature = "<br /><br />";

            //}

            //if (addCommentsLink)
            //{
            //  signature += "&nbsp;&nbsp;"
            //      + "<a href='" + blogItemUrl + "'>" + blogCommentLabel + "...</a>";
            //}



            xmlTextWriter.WriteStartElement("description");

            try
            {
              string blogPost = SiteUtils.ChangeRelativeUrlsToFullyQualifiedUrls(navigationSiteRoot, imageSiteRoot, dr["Description"].ToString());

              if ((!useExcerptInFeed) || (blogPost.Length <= excerptLength))
              {
                xmlTextWriter.WriteCData(blogPost.RemoveScriptTags().RemoveCDataTags() + signature);
              }
              else
              {
                string excerpt = SiteUtils.ChangeRelativeUrlsToFullyQualifiedUrls(navigationSiteRoot, imageSiteRoot, dr["Abstract"].ToString());

                if ((excerpt.Length > 0) && (excerpt != "<p>&#160;</p>"))
                {
                  excerpt = excerpt
                      + ExcerptSuffix
                      + " <a href='"
                      + blogItemUrl + "'>" + MoreLinkText + "</a><div>&nbsp;</div>";
                }
                else
                {
                  excerpt = UIHelper.CreateExcerpt(dr["Description"].ToString(), excerptLength, ExcerptSuffix)
                      + " <a href='"
                      + blogItemUrl + "'>" + MoreLinkText + "</a><div>&nbsp;</div>"; ;
                }

                xmlTextWriter.WriteCData(excerpt.RemoveScriptTags().RemoveCDataTags());
              }
            }
            catch (Exception ex)
            {
              log.Error(string.Format("Error adding blogpost:[id:{0}] [{1}]", itemId, itemUrl), ex);
              xmlTextWriter.WriteCData("<!-- error building item description : " + itemUrl + "-->");
            }

            //end description blog entry
            xmlTextWriter.WriteEndElement();



            //end blog entry
            xmlTextWriter.WriteEndElement();

          }
        }
      }

    }


    private void RenderAggregatedRss()
    {

      Argotic.Syndication.RssFeed feed = new Argotic.Syndication.RssFeed();
      RssChannel channel = new RssChannel();
      channel.Generator = "mojoPortal Blog Module";
      feed.Channel = channel;

      if (module.ModuleTitle.Length > 0)
      {
        channel.Title = "Quantifi - " + module.ModuleTitle;
      }
      else
      {
        channel.Title = "Blog"; // it will cause an error if this is left blank so we must populate it if the module title is an emty string.
      }

      channel.Image = new RssImage();

      channel.Image.Url = new Uri(navigationSiteRoot + "/Quantifi/Images/quantify_logo_214_50.png");
      channel.Image.Title = "Quantifi Solutions - " + module.ModuleTitle;
      channel.Image.Link = new Uri(navigationSiteRoot);
      channel.Image.Height = 214;
      channel.Image.Width = 50;
      channel.Image.Description = "Quantifi is a leading provider of analytics, trading and risk management software.";



      channel.Link = new System.Uri(WebUtils.ResolveServerUrl(SiteUtils.GetCurrentPageUrl()));
      //if (config.ChannelDescription.Length > 0) { channel.Description = config.ChannelDescription; }
      //if (config.Copyright.Length > 0) { channel.Copyright = config.Copyright; }
      //if (config.NotifyEmail.Length > 0) { channel.ManagingEditor = config.NotifyEmail; }
      //if (config.FeedTimeToLive > -1) { channel.TimeToLive = config.FeedTimeToLive; }


      //BlogConfiguration config;




      var config = new AggregateBlogsConfiguration(ModuleSettings.GetModuleSettings(module.ModuleId));


      //config = new BlogConfiguration(ModuleSettings.GetModuleSettings(aggregateConfig.IncludedBlogIds.First()));




      using (IDataReader dr = QB.Blog.GetBlogRSS(config.IncludedBlogIds))
      {
        while (dr.Read())
        {
          bool inFeed = Convert.ToBoolean(dr["IncludeInFeed"]);
          if (!inFeed) { continue; }

          RssItem item = new RssItem();
          string blogItemUrl = FormatBlogUrl(dr["ItemUrl"].ToString(), Convert.ToInt32(dr["ItemID"]));
          item.Link = new Uri(Request.Url, blogItemUrl);
          item.Guid = new RssGuid(blogItemUrl);
          item.Title = dr["Heading"].ToString();
          item.PublicationDate = Convert.ToDateTime(dr["StartDate"]);



          item.Comments = new Uri(blogItemUrl);

          string signature = string.Empty;


          // Let Feed Burner Handl
          //if (config.AddTweetThisToFeed)
          //{
          //  signature += GenerateTweetThisLink(item.Title, blogItemUrl);

          //}



          try
          {
            string blogPost = SiteUtils.ChangeRelativeUrlsToFullyQualifiedUrls(navigationSiteRoot, imageSiteRoot, dr["Description"].ToString());

            if ((!config.UseExcerpt) || (blogPost.Length <= config.ExcerptLength))
            {
              item.Description = blogPost + signature;
            }
            else
            {
              string excerpt = SiteUtils.ChangeRelativeUrlsToFullyQualifiedUrls(navigationSiteRoot, imageSiteRoot, dr["Abstract"].ToString());


              if ((excerpt.Length > 0) && (excerpt != "<p>&#160;</p>"))
              {


                var moreLink = " <a href='" + blogItemUrl + "'>" + config.MoreLinkText + "</a><div>&nbsp;</div>";

                if (excerpt.EndsWith("</p>"))
                {
                  excerpt = excerpt.Substring(0, excerpt.Length - 4) + config.ExcerptSuffix + moreLink + "</p>";
                }
                else if (excerpt.EndsWith("</div>"))
                {
                  excerpt = excerpt.Substring(0, excerpt.Length - 6) + config.ExcerptSuffix + moreLink + "</div>";
                }
                else
                {
                  excerpt = excerpt + config.ExcerptSuffix + moreLink;
                }

              }
              else
              {
                excerpt = UIHelper.CreateExcerpt(dr["Description"].ToString(), config.ExcerptLength, config.ExcerptSuffix)
                    + " <a href='"
                    + blogItemUrl + "'>" + config.MoreLinkText + "</a><div>&nbsp;</div>"; ;
              }

              item.Description = excerpt;

            }

          }
          catch (Exception ex)
          {
            log.Error(string.Format("Error building blogpost description :[{0}]", item.Link), ex);
            item.Description = "<!-- error building item description : " + item.Link + "-->";

          }

          channel.AddItem(item);

        }
      }

      if (config.FeedburnerFeedUrl.Length > 0)
      {
        Response.Cache.SetExpires(DateTime.Now.AddMinutes(-30));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.VaryByParams["r"] = true;
      }
      else
      {
        Response.Cache.SetExpires(DateTime.Now.AddMinutes(30));
        Response.Cache.SetCacheability(HttpCacheability.Public);
      }


      Response.ContentType = "application/xml";

      Encoding encoding = new UTF8Encoding();
      Response.ContentEncoding = encoding;

      using (XmlTextWriter xmlTextWriter = new XmlTextWriter(Response.OutputStream, encoding))
      {
        xmlTextWriter.Formatting = Formatting.Indented;

        //////////////////
        // style for RSS Feed viewed in browsers
        if (ConfigurationManager.AppSettings["RSSCSS"] != null)
        {
          string rssCss = ConfigurationManager.AppSettings["RSSCSS"].ToString();
          xmlTextWriter.WriteWhitespace(" ");
          xmlTextWriter.WriteRaw("<?xml-stylesheet type=\"text/css\" href=\"" + cssBaseUrl + rssCss + "\" ?>");

        }

        if (ConfigurationManager.AppSettings["RSSXsl"] != null)
        {
          string rssXsl = ConfigurationManager.AppSettings["RSSXsl"].ToString();
          xmlTextWriter.WriteWhitespace(" ");
          xmlTextWriter.WriteRaw("<?xml-stylesheet type=\"text/xsl\" href=\"" + cssBaseUrl + rssXsl + "\" ?>");

        }
        ///////////////////////////

        feed.Save(xmlTextWriter);


      }





    }

    private int maxTweetLength = 140;

    private string GenerateTweetThisLink(string titleToTweet, string urlToTweet)
    {
      string format = "<a class='tweetthislink' title='Tweet This' href='{0}'><img src='"
                              + imageSiteRoot + "/Data/SiteImages/tweetthis1.png' alt='Tweet This' /></a>";


      string suffix = "...";
      string twitterUrl;

      int maxTitleLength = maxTweetLength - (urlToTweet.Length + 1) - suffix.Length;
      if (maxTitleLength > 0)
      {
        if ((titleToTweet.Length > maxTitleLength) && (titleToTweet.Length > 3))
        {
          titleToTweet = titleToTweet.Substring(0, (maxTitleLength)) + suffix;
        }

        twitterUrl = "http://twitter.com/home?status=" + Page.Server.UrlEncode(titleToTweet + " " + urlToTweet);

      }
      else
      {
        twitterUrl = "http://twitter.com/home?status=" + Page.Server.UrlEncode(urlToTweet);
      }

      return string.Format(CultureInfo.InvariantCulture, format, twitterUrl);
    }



    private string FormatBlogUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return blogBaseUrl + itemUrl.Replace("~", string.Empty);

      return blogBaseUrl + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + pageID.ToString(CultureInfo.InvariantCulture)
          + "&ItemID=" + itemId.ToString(CultureInfo.InvariantCulture)
          + "&mid=" + moduleId.ToString(CultureInfo.InvariantCulture);

    }

    private void RenderError(string message)
    {

      Response.Write(message);
      Response.End();
    }

  }
}
