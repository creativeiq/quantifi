﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using Quantifi.UI.Controls.BlogsModule.Components;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using System.Globalization;
using Resources;
using mojoPortal.Web.Framework;
using Quantifi.Business;
using Quantifi.Data.EF;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.BlogsModule
{
  public partial class AggregatedBlogViewModule : SiteModuleControl
  {

    #region Props

    protected AggregateBlogsConfiguration Config = null;
    private int pageNumber = 1;
    private int totalPages = 1;
    protected Double TimeOffset = 0;
    private TimeZoneInfo timeZone = null;

    private int pageId = -1;
    private int moduleId = -1;

    new private SiteSettings siteSettings = null;

   new protected bool IsEditable = false;

    private int recentlyPostedNumDays = 90;
    private DateTime RecentlyPostedDate;

    #endregion

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      Config = new AggregateBlogsConfiguration(this.Settings);
      this.EnableViewState = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      LoadSettings();
     // SetupTagsScripts();


      using (var dc = new CorpWebEntities())
      {

        var blogPostData = dc.BlogPosts
                        .Include("Tags")
                        .Where(b => Config.IncludedBlogIds.Contains(b.ModuleID) && b.StartDate.HasValue && b.IsPublished.HasValue == true && b.IsPublished.Value == true)
                        .OrderByDescending(b => b.StartDate);

        totalPages = blogPostData.GetTotalPages(Config.PageSize);


        var page = blogPostData.Page(pageNumber, Config.PageSize);

        this.rptAggregatedPosts.DataSource = page;
        this.rptAggregatedPosts.DataBind();
      }



      //var currentPage = CacheHelper.GetCurrentPage();


      var pageUrl = SiteRoot
                        + currentPage.Url.Replace("~/", "/")
                        + ((currentPage.Url.Contains("?") ? "&" : "?"))
                        + "pg={0}";


      pgr.PageURLFormat = pageUrl;
      pgr.ShowFirstLast = true;
      pgr.PageSize = Config.PageSize;
      pgr.PageCount = totalPages;
      pgr.CurrentIndex = pageNumber;
      pgr.Visible = (totalPages > 1);



      if (pageNumber > 1)
      {
        this.Page.Title += " - Page " + pageNumber.ToString();
      }

      SetupRssLink();

    }

    private void LoadSettings()
    {
      siteSettings = CacheHelper.GetCurrentSiteSettings();
      TimeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();

      pageNumber = WebUtils.ParseInt32FromQueryString("pg", pageNumber);



      IsEditable = (Request.IsAuthenticated && WebUser.IsAdminOrContentAdminOrContentAuthor);
      RecentlyPostedDate = DateTime.UtcNow.AddDays(-(Config.DaysPostAreConsideredRecent));
    }



    protected string FormatPostAuthor(string authorName)
    {
      if (Config.ShowPostAuthor)
      {
        if (Config.BlogAuthor.Length > 0)
        {
          return string.Format(CultureInfo.InvariantCulture, QuantifiBlogResources.PostAuthorFormat, Config.BlogAuthor);
        }

        return string.Format(CultureInfo.InvariantCulture, QuantifiBlogResources.PostAuthorFormat, authorName);
      }

      return string.Empty;

    }

    protected string FormatBlogEntry(string blogHtml, string excerpt, string url, int itemId)
    {

      var MoreLink = " <a href='" + FormatBlogUrl(url, itemId) + "' class='blogMoreLink' >" + Config.MoreLinkText + "</a>";


      if ((excerpt.Length > 0) && (excerpt != "<p>&#160;</p>"))
      {
        if (excerpt.EndsWith("</p>"))
        {
          return excerpt.Substring(0, excerpt.Length - 4) + Config.ExcerptSuffix + MoreLink + "</p>";
        }
        else if (excerpt.EndsWith("</div>"))
        {
          return excerpt.Substring(0, excerpt.Length - 6) + Config.ExcerptSuffix + MoreLink + "</div>";
        }
        else
        {
          return excerpt + Config.ExcerptSuffix + MoreLink;
        }
      }

      string result = string.Empty;
      if ((blogHtml.Length > Config.ExcerptLength) && (Config.MoreLinkText.Length > 0))
      {

        result = UIHelper.CreateExcerpt(blogHtml, Config.ExcerptLength, Config.ExcerptSuffix);
        result += MoreLink;
        return result;
      }



      return blogHtml;
    }

    protected string FormatBlogDate(DateTime startDate)
    {
      if (!Config.ShowTimeStampOnBlogRoll)
        return string.Empty;



      string formatStr;
      if (DateTime.Compare(RecentlyPostedDate, startDate) < 0)
        formatStr = Config.DateTimeFormat;
      else
        formatStr = Config.RecentDateTimeFormat;



      if (timeZone != null)
      {
        return TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone).ToString(formatStr);
      }

      return startDate.AddHours(TimeOffset).ToString(formatStr);

    }

    protected string FormatBlogUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      //return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
      //    + "&ItemID=" + itemId.ToInvariantString()
      //    + "&mid=" + ModuleId.ToInvariantString();
      return string.Empty;
    }

    protected string FormatBlogTitleUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      //return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
      //    + "&ItemID=" + itemId.ToInvariantString()
      //    + "&mid=" + ModuleId.ToInvariantString();
      return string.Empty;
    }

    private string GetRssUrl()
    {
      if (Config.FeedburnerFeedUrl.Length > 0) return Config.FeedburnerFeedUrl;

      return SiteRoot + "/blog" + ModuleId.ToInvariantString() + "rss.aspx";

    }

    protected virtual void SetupRssLink()
    {

      if (Page.Master != null)
      {
        Control head = Page.Master.FindControl("Head1");
        if (head != null)
        {

          Literal rssLink = new Literal();
          rssLink.Text = "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"Quantifi - "
                  + this.Title + "\" href=\""
                  + GetRssUrl() + "\" />";

          head.Controls.Add(rssLink);

        }

      }


    }

//    private void SetupTagsScripts()
//    {
//      if (Page.ClientScript.IsStartupScriptRegistered(typeof(AggregatedBlogViewModule), "QTagsInit"))
//      {
//        var script = @"
//var quantifiTages = new QTagsManager(); 
//quantifiTages.init();";


//        Page.ClientScript.RegisterStartupScript(typeof(AggregatedBlogViewModule), "QTagsInit", script);

//      }

//    }
  }
}