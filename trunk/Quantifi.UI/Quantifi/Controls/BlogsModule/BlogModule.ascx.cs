//	Author:				Joe Audette
//	Created:			2004-08-15
//	Last Modified:		2010-05-23
//		
// The use and distribution terms for this software are covered by the 
// Common Public License 1.0 (http://opensource.org/licenses/cpl.php)
// which can be found in the file CPL.TXT at the root of this distribution.
// By using this software in any fashion, you are agreeing to be bound by 
// the terms of this license.
//
// You must not remove this notice, or any other, from this software.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Web.Framework;
using mojoPortal.Web.UI;
using Resources;
using mojoPortal.Web;
using QB = Quantifi.Business;
using Quantifi.UI.Blogs.Controls;
using Quantifi.UI.Controls.BlogsModule.Controls;
using mojoPortal.Business.WebHelpers;

namespace Quantifi.UI.Blogs
{
	public partial class BlogModule : SiteModuleControl
	{

		protected BlogConfiguration config = new BlogConfiguration();
		private int countOfDrafts = 0;
		private mojoBasePage basePage = null;

    private int pageNumber = 1;
    private int year = 0;

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			this.Load += new EventHandler(Page_Load);
			this.EnableViewState = false;


			LoadSettings();

		}

		protected virtual void Page_Load(object sender, EventArgs e)
		{
			SetupCss();
			PopulateLabels();
			PopulateControls();
		}
		
		protected override void OnPreRender(EventArgs e)
		{
			if (basePage != null)
			{
				basePage.MPLeftPane.Visible = true;
				basePage.MPLeftPane.Parent.Visible = true;
				basePage.ForceSideContent(config.ShowLeftContent, config.ShowRightContent);
			}
			base.OnPreRender(e);
		}

		protected  void LoadSettings()
		{
			pnlContainer.ModuleId = ModuleId;
			config = new BlogConfiguration(Settings);

      pageNumber = WebUtils.ParseInt32FromQueryString("pg", pageNumber);
      year = WebUtils.ParseInt32FromQueryString("year", pageNumber);

			if (config.InstanceCssClass.Length > 0) { pnlWrapper.CssClass += " " + config.InstanceCssClass; }

			if (this.ModuleConfiguration != null)
			{
				this.Title = GetModuleTitle(ModuleConfiguration.ModuleTitle);
				this.Description = ModuleConfiguration.FeatureName;
			}

    

			if (IsEditable)
			{
				countOfDrafts = QB.Blog.CountOfDrafts(ModuleId);
			}

			basePage = this.Page as mojoBasePage;



		}

		private void PopulateControls()
		{

			postList.ModuleId = ModuleId;
			postList.PageId = PageId;
			postList.IsEditable = IsEditable;
			postList.Config = config;
			postList.SiteRoot = SiteRoot;
			postList.ImageSiteRoot = ImageSiteRoot;

		}

		protected  void PopulateLabels()
		{

      if (this.ModuleConfiguration != null)
      {
        this.Title = GetModuleTitle(ModuleConfiguration.ModuleTitle); 
      }


      
      var pageSettings = CacheHelper.GetCurrentPage();
      if (pageSettings != null)
      {
        this.Page.Title = GetPageTitle(pageSettings.PageName, pageSettings.PageTitle);

      }

      //string  = string.Format("?pageid={0}&mid={1}", this.ModuleId, this.PageId);


      this.litTitle.Text = this.Title;

      if (IsEditable)
      {
        this.pnlAdminLinks.Visible = true;

        this.linkAddPosts.NavigateUrl += string.Format("?mid={0}&pageid={1}", this.ModuleId, this.PageId); ;
        this.linkAddPosts.Visible = true;

        this.linkSettings.NavigateUrl += string.Format("?mid={0}&pageid={1}", this.ModuleId, this.PageId); ;
        this.linkSettings.Visible = true;


        if (countOfDrafts > 0)
        {
          this.linkEditDrafts.NavigateUrl += string.Format("?mid={0}&pageid={1}", this.ModuleId, this.PageId); ;
          this.linkEditDrafts.Visible = true;
        }
      }

			//Title1.EditUrl = SiteRoot + "/Quantifi/Controls/BlogsModule/EditPost.aspx";
			//Title1.EditText = QuantifiBlogResources.BlogAddPostLabel;

      //if ((IsEditable) && (countOfDrafts > 0))
      //{
	
      //  Title1.LiteralExtraMarkup =
      //      "&nbsp;<a href='" + SiteRoot 
      //      + "/Quantifi/Controls/BlogsModule/EditCategory.aspx?pageid=" + PageId.ToInvariantString() 
      //      + "&amp;mid=" + ModuleId.ToInvariantString()
      //      + "' class='ModuleEditLink' title='" + QuantifiBlogResources.BlogEditCategoriesLabel 
      //      + "' >" + QuantifiBlogResources.BlogEditCategoriesLabel + "</a>"
      //      + "&nbsp;<a href='" + SiteRoot 
      //      + "/Quantifi/Controls/BlogsModule/Drafts.aspx?pageid=" + PageId.ToInvariantString() 
      //      + "&amp;mid=" + ModuleId.ToInvariantString()
      //      + "' class='ModuleEditLink' title='" + QuantifiBlogResources.BlogDraftsLink + "' >" 
      //      + QuantifiBlogResources.BlogDraftsLink + "</a>";
      //}
      //else if (IsEditable)
      //{
      //  Title1.LiteralExtraMarkup =
      //      "&nbsp;<a href='" + SiteRoot 
      //      + "/Quantifi/Controls/BlogsModule/EditCategory.aspx?pageid=" + PageId.ToInvariantString() 
      //      + "&amp;mid=" + ModuleId.ToInvariantString()
      //      + "' class='ModuleEditLink' title='" + QuantifiBlogResources.BlogEditCategoriesLabel + "'>" 
      //      + QuantifiBlogResources.BlogEditCategoriesLabel + "</a>";
      //}


		}

		protected virtual void SetupCss()
		{

			// older skins have this
			StyleSheet stylesheet = (StyleSheet)Page.Master.FindControl("StyleSheet");
			if (stylesheet != null)
			{
				if (stylesheet.FindControl("blogcss") == null)
				{
					Literal cssLink = new Literal();
					cssLink.ID = "blogcss";
					cssLink.Text = "\n<link href='"
					+ SiteUtils.GetSkinBaseUrl()
					+ "blogmodule.css' type='text/css' rel='stylesheet' media='screen' />";

					stylesheet.Controls.Add(cssLink);
				}

				if (stylesheet.FindControl("aspcalendar") == null)
				{
					Literal cssLink = new Literal();
					cssLink.ID = "aspcalendar";
					cssLink.Text = "\n<link href='"
					+ SiteUtils.GetSkinBaseUrl()
					+ "aspcalendar.css' type='text/css' rel='stylesheet' media='screen' />";

					stylesheet.Controls.Add(cssLink);
				}

			}

		}



    private string GetPageTitle(string pageName,string pageTitle)
    {
      var title = (String.IsNullOrWhiteSpace(pageTitle)) ? pageName : pageTitle;

      if (year > 2000 && year <= DateTime.Now.Year)
        title = title + " in " + year.ToInvariantString();

      if (pageNumber > 1)
        title = title + " - Page " + pageNumber.ToInvariantString();

      return title + " - " + siteSettings.SiteName;
    }

    private string GetModuleTitle(string moduleTitle)
    {
      if (year > 2000 && year <= DateTime.Now.Year)
        return moduleTitle + " in " + year.ToInvariantString();

      return moduleTitle;
    }
	}
}
