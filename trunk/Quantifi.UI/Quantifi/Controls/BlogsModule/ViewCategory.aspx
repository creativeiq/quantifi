<%@ Page Language="c#" CodeBehind="ViewCategory.aspx.cs" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="false" Inherits="Quantifi.UI.Blogs.BlogCategoryView" %>

<%@ Register TagPrefix="blog" TagName="TagList" Src="~/Quantifi/Controls/BlogsModule/Controls/CategoryListControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="Archives" Src="~/Quantifi/Controls/BlogsModule/Controls/ArchiveListControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="FeedLinks" Src="~/Quantifi/Controls/BlogsModule/Controls/FeedLinksControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="StatsControl" Src="~/Quantifi/Controls/BlogsModule/Controls/StatsControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="SideBar" Src="~/Quantifi/Controls/BlogsModule/Controls/BlogSideBarControl.ascx" %>


<asp:Content ContentPlaceHolderID="leftContent" ID="MPLeftPane" runat="server" >

</asp:Content>


<asp:Content ContentPlaceHolderID="mainContent" ID="MPContent" runat="server">
	<portal:ModulePanel ID="pnlContainer" runat="server">
		<mp:CornerRounderTop ID="ctop1" runat="server" />
		<asp:Panel ID="pnlBlog" runat="server" CssClass="blogwrapper">
			<div class="modulecontent">
				<asp:Panel ID="divblog" runat="server" CssClass="" SkinID="plain">
					<h2 class="blogtitle moduletitle">
						<asp:Literal ID="litHeader" runat="server" Visible="True" /></h2>
					<asp:Repeater ID="dlArchives" runat="server" EnableViewState="False">
						<ItemTemplate>
							<h3 class="blogtitle">
								<asp:HyperLink ID="Title" runat="server" SkinID="plain" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'
									Visible='True' NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
								</asp:HyperLink>&#160;</h3>
							<div class="blogcontent">
								<asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# FormatBlogDate(Convert.ToDateTime(Eval("StartDate"))) %>'
									Visible='True' NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
								</asp:HyperLink>&#160;
								<asp:HyperLink ID="Hyperlink2" runat="server" Text='<%# feedBackLabel + "(" + DataBinder.Eval(Container.DataItem,"CommentCount") + ")" %>'
									Visible='<%# config.AllowComments %>' NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
								</asp:HyperLink>&#160;
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</asp:Panel>
				<asp:Label ID="lblCopyright" runat="server" CssClass="txtcopyright"></asp:Label>
			</div>
		</asp:Panel>
		<mp:CornerRounderBottom ID="cbottom1" runat="server" />
	</portal:ModulePanel>
</asp:Content>
<asp:Content ContentPlaceHolderID="rightContent" ID="MPRightPane" runat="server" />

<asp:Content ContentPlaceHolderID="pageEditContent" ID="MPPageEdit" runat="server" />
