﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/layout.Master"
	AutoEventWireup="true" CodeBehind="ViewCategoryArchive.aspx.cs" Inherits="Quantifi.UI.Controls.BlogsModule.ViewCategoryArchive" %>


<asp:Content ContentPlaceHolderID="leftContent" ID="MPLeftPane" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
	<div>
		<h2 class="art-PostHeader moduletitle">
			<asp:Literal ID="litTitle" runat="server"></asp:Literal></h2>
		<asp:Repeater ID="rptPostList" runat="server">
			<HeaderTemplate>
				<div class="blogRoll ">
			</HeaderTemplate>
			<ItemTemplate>
				<div class="blogitem ">
					<h3 class="blogtitle">
						<asp:HyperLink ID="Title" CssClass="title" runat="server" SkinID="plain" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'
							Visible='True' NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
						</asp:HyperLink>&#160;
						<%# GetEditLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ItemID")), "ModuleEditLink")%>
					</h3>
					<div class="blogdate">
						<%# FormatBlogDate(Convert.ToDateTime(Eval("StartDate"))) %>
					</div>
					<div class="blogtext">
						<%# FormatBlogEntry(DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Abstract").ToString(), DataBinder.Eval(Container.DataItem, "ItemUrl").ToString(), Convert.ToInt32(Eval("ItemID")))%>
					</div>
				</div>
			</ItemTemplate>
			<FooterTemplate>
				</div>
			</FooterTemplate>
		</asp:Repeater>
		<%-- -------------------------------------------------- --%>
		<asp:Repeater ID="rptArchiveItems" runat="server">
			<HeaderTemplate>
				<div class="blogArchiveRoll ">
			</HeaderTemplate>
			<ItemTemplate>
				<div class="item ">
					<h3 class="blogtitle">
						<asp:HyperLink ID="Title" CssClass="title" runat="server" SkinID="plain" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'
							Visible='True' NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
						</asp:HyperLink>&#160;<%# GetEditLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ItemID")), "ModuleEditLink")%></h3>
					<div class="blogdate">
						<%# FormatBlogDate(Convert.ToDateTime(Eval("StartDate"))) %>
					</div>
				</div>
			</ItemTemplate>
			<FooterTemplate>
				</div>
			</FooterTemplate>
		</asp:Repeater>
		<portal:mojoCutePager ID="pgr" runat="server" CssClass="" WrapInDiv="true" />
	</div>
</asp:Content>




<asp:Content ContentPlaceHolderID="rightContent" ID="MPRightPane" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="pageEditContent" ID="MPPageEdit" runat="server">
</asp:Content>
