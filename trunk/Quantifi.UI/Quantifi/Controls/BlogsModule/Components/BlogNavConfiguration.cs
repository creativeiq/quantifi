﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using log4net;
using mojoPortal.Web.Framework;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.BlogsModule.Components
{
  public class BlogNavConfiguration
  {
    private bool showAggregateBlogView = true;
    private int aggregateBlogViewPageId = 131;
    private int aggregateBlogViewId = 262;
    private List<int> blogModuleIds = new List<int> { 78, 213, 214 };


    private static readonly ILog log = LogManager.GetLogger(typeof(BlogNavConfiguration));

    public BlogNavConfiguration()
    { }

    public BlogNavConfiguration(Hashtable settings)
    {
      LoadSettings(settings);
    }

    private void LoadSettings(Hashtable settings)
    {

      if (settings == null) { throw new ArgumentException("must pass in a hashtable of settings"); }

      showAggregateBlogView = WebUtils.ParseBoolFromHashtable(settings, "ShowAggregateBlogViewSetting", showAggregateBlogView);
      aggregateBlogViewPageId = WebUtils.ParseInt32FromHashtable(settings, "AggregateBlogViewPageIdSetting", aggregateBlogViewPageId);
      aggregateBlogViewId = WebUtils.ParseInt32FromHashtable(settings, "AggregateBlogViewIdSetting", aggregateBlogViewId);

      if (settings.Contains("BlogModuleIdsCSVSetting"))
      {
        blogModuleIds = settings["BlogModuleIdsCSVSetting"].ToString().SplitStringIntoInts().ToList();
      }


    }

    public bool ShowAggregateBlogView
    {
      get 
      { 
        return showAggregateBlogView; 
      }
    }
    
    public int AggregateBlogViewId
    {
      get
      {
        return aggregateBlogViewId;
      }

    }
    
    public int AggregateBlogViewPageId
    {
      get
      {
        return aggregateBlogViewPageId; 
      }

    }
    
    public List<int> BlogModuleIds
    {
      get 
      { 
        return blogModuleIds; 
      }

    }

  }
}