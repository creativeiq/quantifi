﻿// Author:				    Joe Audette
// Created:			        2010-05-11
// Last Modified:		    2010-05-26
// 
// The use and distribution terms for this software are covered by the 
// Common Public License 1.0 (http://opensource.org/licenses/cpl.php)
// which can be found in the file CPL.TXT at the root of this distribution.
// By using this software in any fashion, you are agreeing to be bound by 
// the terms of this license.
//
// You must not remove this notice, or any other, from this software.

using System;
using System.Linq;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using mojoPortal.Web.Controls.google;
using mojoPortal.Web.Framework;
using System.Collections.Generic;
using log4net;

namespace Quantifi.UI.Blogs
{
  /// <summary>
  /// encapsulates the feature instance configuration loaded from module settings into a more friendly object
  /// </summary>
  public class BlogConfiguration
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(BlogConfiguration));

    public BlogConfiguration()
    { }

    public BlogConfiguration(Hashtable settings)
    {
      LoadSettings(settings);

    }

    private void LoadSettings(Hashtable settings)
    {
      if (settings == null) { throw new ArgumentException("must pass in a hashtable of settings"); }

      useExcerpt = WebUtils.ParseBoolFromHashtable(settings, "BlogUseExcerptSetting", useExcerpt);

      titleOnly = WebUtils.ParseBoolFromHashtable(settings, "BlogShowTitleOnlySetting", titleOnly);

      showPager = WebUtils.ParseBoolFromHashtable(settings, "BlogShowPagerInListSetting", showPager);

      googleMapIncludeWithExcerpt = WebUtils.ParseBoolFromHashtable(settings, "GoogleMapIncludeWithExcerptSetting", googleMapIncludeWithExcerpt);

      enableContentRating = WebUtils.ParseBoolFromHashtable(settings, "EnableContentRatingSetting", enableContentRating);

      enableRatingComments = WebUtils.ParseBoolFromHashtable(settings, "EnableRatingCommentsSetting", enableRatingComments);

      if (UseExcerpt) { enableContentRating = false; }

      excerptLength = WebUtils.ParseInt32FromHashtable(settings, "BlogExcerptLengthSetting", excerptLength);

      if (settings.Contains("BlogExcerptSuffixSetting"))
      {
        excerptSuffix = settings["BlogExcerptSuffixSetting"].ToString();
      }

      if (settings.Contains("BlogMoreLinkText"))
      {
        moreLinkText = settings["BlogMoreLinkText"].ToString();
      }

      if (settings.Contains("BlogAuthorSetting"))
      {
        blogAuthor = settings["BlogAuthorSetting"].ToString();
      }

      if (settings.Contains("CustomCssClassSetting"))
      {
        instanceCssClass = settings["CustomCssClassSetting"].ToString();
      }

      if (settings.Contains("BlogDateTimeFormat"))
      {
        string format = settings["BlogDateTimeFormat"].ToString().Trim();
        if (format.Length > 0)
        {
          try
          {
            string d = DateTime.Now.ToString(format, CultureInfo.CurrentCulture);
            dateTimeFormat = format;
          }
          catch (FormatException) { }
        }

      }

      useTagCloudForCategories = WebUtils.ParseBoolFromHashtable(settings, "BlogUseTagCloudForCategoriesSetting", useTagCloudForCategories);

      showCalendar = WebUtils.ParseBoolFromHashtable(settings, "BlogShowCalendarSetting", showCalendar);

      showCategories = WebUtils.ParseBoolFromHashtable(settings, "BlogShowCategoriesSetting", showCategories);

      showArchives = WebUtils.ParseBoolFromHashtable(settings, "BlogShowArchiveSetting", showArchives);

      showStatistics = WebUtils.ParseBoolFromHashtable(settings, "BlogShowStatisticsSetting", showStatistics);


      archivesPageSize = WebUtils.ParseInt32FromHashtable(settings, "BlogArchivesPageSizeSetting", archivesPageSize);

      showFeedLinks = WebUtils.ParseBoolFromHashtable(settings, "BlogShowFeedLinksSetting", showFeedLinks);

      showAddFeedLinks = WebUtils.ParseBoolFromHashtable(settings, "BlogShowAddFeedLinksSetting", showAddFeedLinks);

      navigationOnRight = WebUtils.ParseBoolFromHashtable(settings, "BlogNavigationOnRightSetting", navigationOnRight);

      allowComments = WebUtils.ParseBoolFromHashtable(settings, "BlogAllowComments", allowComments);

      useLinkForHeading = WebUtils.ParseBoolFromHashtable(settings, "BlogUseLinkForHeading", useLinkForHeading);

      showPostAuthor = WebUtils.ParseBoolFromHashtable(settings, "ShowPostAuthorSetting", showPostAuthor);

      if (settings.Contains("GoogleMapInitialMapTypeSetting"))
      {
        string gmType = settings["GoogleMapInitialMapTypeSetting"].ToString();
        try
        {
          mapType = (MapType)Enum.Parse(typeof(MapType), gmType);
        }
        catch (ArgumentException) { }
      }

      googleMapHeight = WebUtils.ParseInt32FromHashtable(settings, "GoogleMapHeightSetting", googleMapHeight);

      googleMapWidth = WebUtils.ParseInt32FromHashtable(settings, "GoogleMapWidthSetting", googleMapWidth);


      googleMapEnableMapType = WebUtils.ParseBoolFromHashtable(settings, "GoogleMapEnableMapTypeSetting", googleMapEnableMapType);

      googleMapEnableZoom = WebUtils.ParseBoolFromHashtable(settings, "GoogleMapEnableZoomSetting", googleMapEnableZoom);

      googleMapShowInfoWindow = WebUtils.ParseBoolFromHashtable(settings, "GoogleMapShowInfoWindowSetting", googleMapShowInfoWindow);

      googleMapEnableLocalSearch = WebUtils.ParseBoolFromHashtable(settings, "GoogleMapEnableLocalSearchSetting", googleMapEnableLocalSearch);

      googleMapEnableDirections = WebUtils.ParseBoolFromHashtable(settings, "GoogleMapEnableDirectionsSetting", googleMapEnableDirections);

      googleMapInitialZoom = WebUtils.ParseInt32FromHashtable(settings, "GoogleMapInitialZoomSetting", googleMapInitialZoom);

      pageSize = WebUtils.ParseInt32FromHashtable(settings, "BlogEntriesToShowSetting", pageSize);

      if (settings.Contains("OdiogoFeedIDSetting"))
      {
        odiogoFeedId = settings["OdiogoFeedIDSetting"].ToString();
      }

      if (settings.Contains("OdiogoPodcastUrlSetting"))
        odiogoPodcastUrl = settings["OdiogoPodcastUrlSetting"].ToString();

      hideAddThisButton = WebUtils.ParseBoolFromHashtable(settings, "BlogHideAddThisButtonSetting", hideAddThisButton);

      if (settings.Contains("BlogAddThisDotComUsernameSetting"))
      {
        addThisAccountId = settings["BlogAddThisDotComUsernameSetting"].ToString().Trim();
      }

      useAddThisMouseOverWidget = WebUtils.ParseBoolFromHashtable(settings, "BlogAddThisDotComUseMouseOverWidgetSetting", useAddThisMouseOverWidget);


      if (settings.Contains("BlogAddThisButtonImageUrlSetting"))
      {
        string buttonImage = settings["BlogAddThisButtonImageUrlSetting"].ToString().Trim();
        if (buttonImage.Length > 0)
        {
          addThisButtonImageUrl = buttonImage;
        }
      }

      if (settings.Contains("BlogAddThisRssButtonImageUrlSetting"))
        addThisRssButtonImageUrl = settings["BlogAddThisRssButtonImageUrlSetting"].ToString().Trim();

      if (settings.Contains("BlogAddThisCustomBrandSetting"))
      {
        addThisCustomBrand = settings["BlogAddThisCustomBrandSetting"].ToString().Trim();
      }

      if (settings.Contains("BlogAddThisCustomOptionsSetting"))
      {
        addThisCustomOptions = settings["BlogAddThisCustomOptionsSetting"].ToString().Trim();
      }

      if (settings.Contains("BlogAddThisCustomLogoUrlSetting"))
      {
        addThisCustomLogoUrl = settings["BlogAddThisCustomLogoUrlSetting"].ToString().Trim();
      }

      if (settings.Contains("BlogAddThisCustomLogoBackColorSetting"))
      {
        addThisCustomLogoBackColor = settings["BlogAddThisCustomLogoBackColorSetting"].ToString().Trim();
      }

      if (settings.Contains("BlogAddThisCustomLogoForeColorSetting"))
      {
        addThisCustomLogoForeColor = settings["BlogAddThisCustomLogoForeColorSetting"].ToString().Trim();
      }

      if (settings.Contains("BlogFeedburnerFeedUrl"))
      {
        feedburnerFeedUrl = settings["BlogFeedburnerFeedUrl"].ToString().Trim();
      }

      if (settings.Contains("DisqusSiteShortName"))
      {
        disqusSiteShortName = settings["DisqusSiteShortName"].ToString();
      }

      if (settings.Contains("CommentSystemSetting"))
      {
        commentSystem = settings["CommentSystemSetting"].ToString();
      }

      if (settings.Contains("IntenseDebateAccountId"))
      {
        intenseDebateAccountId = settings["IntenseDebateAccountId"].ToString();
      }

      allowWebSiteUrlForComments = WebUtils.ParseBoolFromHashtable(settings, "AllowWebSiteUrlForComments", allowWebSiteUrlForComments);

      hideDetailsFromUnauthencticated = WebUtils.ParseBoolFromHashtable(settings, "HideDetailsFromUnauthencticated", hideDetailsFromUnauthencticated);

      if (settings.Contains("BlogCopyrightSetting"))
      {
        copyright = settings["BlogCopyrightSetting"].ToString();
      }

      showLeftContent = WebUtils.ParseBoolFromHashtable(settings, "ShowPageLeftContentSetting", showLeftContent);

      showRightContent = WebUtils.ParseBoolFromHashtable(settings, "ShowPageRightContentSetting", showRightContent);

      enableContentVersioning = WebUtils.ParseBoolFromHashtable(settings, "BlogEnableVersioningSetting", enableContentVersioning);

      if (settings.Contains("BlogCommentForDaysDefault"))
      {
        defaultCommentDaysAllowed = settings["BlogCommentForDaysDefault"].ToString();
      }

      if (settings.Contains("BlogEditorHeightSetting"))
      {
        editorHeight = Unit.Parse(settings["BlogEditorHeightSetting"].ToString());

      }

      useCaptcha = WebUtils.ParseBoolFromHashtable(settings, "BlogUseCommentSpamBlocker", useCaptcha);

      requireAuthenticationForComments = WebUtils.ParseBoolFromHashtable(settings, "RequireAuthenticationForComments", requireAuthenticationForComments);

      notifyOnComment = WebUtils.ParseBoolFromHashtable(settings, "ContentNotifyOnComment", notifyOnComment);

      if (settings.Contains("BlogAuthorEmailSetting"))
      {
        notifyEmail = settings["BlogAuthorEmailSetting"].ToString();
      }

      useFacebookLikeButton = WebUtils.ParseBoolFromHashtable(settings, "UseFacebookLikeButton", useFacebookLikeButton);



      sidebarNumYearAchivesShown = WebUtils.ParseInt32FromHashtable(settings, "SidebarNumYearAchivesShownSetting", sidebarNumYearAchivesShown);

      showTagsOnBlogRoll = WebUtils.ParseBoolFromHashtable(settings, "ShowTagsOnBlogRollSetting", false);
      showTagsOnBlogPost = WebUtils.ParseBoolFromHashtable(settings, "ShowTagsOnBlogPostSetting", false);
      tagsViewPageId = WebUtils.ParseInt32FromHashtable(settings, "TagsViewPageIdSetting", tagsViewPageId);



      showTimeStampOnBlogPost = WebUtils.ParseBoolFromHashtable(settings, "ShowTimeStampOnBlogPostSetting", false);
      showTimeStampOnBlogRoll = WebUtils.ParseBoolFromHashtable(settings, "ShowTimeStampOnBlogRollSetting", false);
      showTimeStampOnBlogArchive = WebUtils.ParseBoolFromHashtable(settings, "ShowTimeStampOnBlogArchiveSetting", false);


      daysPostAreConsideredRecent = WebUtils.ParseInt32FromHashtable(settings, "DaysPostAreConsideredRecentSetting", daysPostAreConsideredRecent); ;


      if (settings.Contains("RecentDateTimeFormatSetting"))
      {
        recentDateTimeFormat = settings["RecentDateTimeFormatSetting"].ToString();
      }


      useArchiveViewForArchive = WebUtils.ParseBoolFromHashtable(settings, "UseArchiveViewForArchiveSetting", false);

    }


    private bool useArchiveViewForArchive = true;
    public bool UseArchiveViewForArchive { get { return useArchiveViewForArchive; } }

    private int sidebarNumYearAchivesShown = 10;
    public int SidebarNumYearAchivesShown { get { return sidebarNumYearAchivesShown; } }


    private bool showTagsOnBlogRoll = false;
    public bool ShowTagsOnBlogRoll { get { return showTagsOnBlogRoll; } }


    private bool showTagsOnBlogPost = false;
    public bool ShowTagsOnBlogPost { get { return showTagsOnBlogPost; } }


    private int tagsViewPageId = 10;
    public int TagsViewPageId { get { return tagsViewPageId; } }



    private bool showTimeStampOnBlogPost = false;
    public bool ShowTimeStampOnBlogPost { get { return showTimeStampOnBlogPost; } }


    private bool showTimeStampOnBlogRoll = false;
    public bool ShowTimeStampOnBlogRoll { get { return showTimeStampOnBlogRoll; } }

    private bool showTimeStampOnBlogArchive = false;
    public bool ShowTimeStampOnBlogArchive { get { return showTimeStampOnBlogArchive; } }

    private bool useFacebookLikeButton = false;

    public bool UseFacebookLikeButton
    {
      get { return useFacebookLikeButton; }
    }

    private string notifyEmail = string.Empty;

    public string NotifyEmail
    {
      get { return notifyEmail; }
    }

    private bool notifyOnComment = false;

    public bool NotifyOnComment
    {
      get { return notifyOnComment; }
    }

    private bool requireAuthenticationForComments = false;

    public bool RequireAuthenticationForComments
    {
      get { return requireAuthenticationForComments; }
    }

    private bool useCaptcha = true;

    public bool UseCaptcha
    {
      get { return useCaptcha; }
    }

    private Unit editorHeight = Unit.Parse("350");

    public Unit EditorHeight
    {
      get { return editorHeight; }
    }

    private bool enableContentVersioning = false;

    public bool EnableContentVersioning
    {
      get { return enableContentVersioning; }
    }

    private string defaultCommentDaysAllowed = "90";

    public string DefaultCommentDaysAllowed
    {
      get { return defaultCommentDaysAllowed; }
    }

    private bool showLeftContent = false;

    public bool ShowLeftContent
    {
      get { return showLeftContent; }
    }


    private bool showRightContent = false;

    public bool ShowRightContent
    {
      get { return showRightContent; }
    }


    private string copyright = string.Empty;

    public string Copyright
    {
      get { return copyright; }
    }

    private bool hideDetailsFromUnauthencticated = false;

    public bool HideDetailsFromUnauthencticated
    {
      get { return hideDetailsFromUnauthencticated; }
    }

    private bool allowWebSiteUrlForComments = true;

    public bool AllowWebSiteUrlForComments
    {
      get { return allowWebSiteUrlForComments; }
    }

    private string commentSystem = "internal";

    public string CommentSystem
    {
      get { return commentSystem; }
    }

    private string intenseDebateAccountId = string.Empty;

    public string IntenseDebateAccountId
    {
      get { return intenseDebateAccountId; }
    }

    private string disqusSiteShortName = string.Empty;

    public string DisqusSiteShortName
    {
      get { return disqusSiteShortName; }
    }

    private string feedburnerFeedUrl = string.Empty;

    public string FeedburnerFeedUrl
    {
      get { return feedburnerFeedUrl; }
    }

    private string addThisCustomLogoForeColor = string.Empty;

    public string AddThisCustomLogoForeColor
    {
      get { return addThisCustomLogoForeColor; }
    }

    private string addThisCustomLogoBackColor = string.Empty;

    public string AddThisCustomLogoBackColor
    {
      get { return addThisCustomLogoBackColor; }
    }

    private string addThisCustomLogoUrl = string.Empty;

    public string AddThisCustomLogoUrl
    {
      get { return addThisCustomLogoUrl; }
    }

    private string addThisCustomOptions = string.Empty;

    public string AddThisCustomOptions
    {
      get { return addThisCustomOptions; }
    }

    private string addThisCustomBrand = string.Empty;

    public string AddThisCustomBrand
    {
      get { return addThisCustomBrand; }
    }

    private string addThisButtonImageUrl = "~/Data/SiteImages/addthissharebutton.gif";

    public string AddThisButtonImageUrl
    {
      get { return addThisButtonImageUrl; }
    }

    private string addThisRssButtonImageUrl = "~/Data/SiteImages/addthisrss.gif";

    public string AddThisRssButtonImageUrl
    {
      get { return addThisRssButtonImageUrl; }
    }

    private bool useAddThisMouseOverWidget = true;

    public bool UseAddThisMouseOverWidget
    {
      get { return useAddThisMouseOverWidget; }
    }

    private string addThisAccountId = string.Empty;

    public string AddThisAccountId
    {
      get { return addThisAccountId; }
    }

    private string odiogoFeedId = string.Empty;

    public string OdiogoFeedId
    {
      get { return odiogoFeedId; }
    }

    private string odiogoPodcastUrl = string.Empty;

    public string OdiogoPodcastUrl
    {
      get { return odiogoPodcastUrl; }
    }

    private int pageSize = 10;

    public int PageSize
    {
      get { return pageSize; }
    }

    private int googleMapInitialZoom = 13;

    public int GoogleMapInitialZoom
    {
      get { return googleMapInitialZoom; }
    }

    private bool googleMapEnableDirections = false;

    public bool GoogleMapEnableDirections
    {
      get { return googleMapEnableDirections; }
    }

    private bool googleMapEnableLocalSearch = false;

    public bool GoogleMapEnableLocalSearch
    {
      get { return googleMapEnableLocalSearch; }
    }

    private bool googleMapShowInfoWindow = false;

    public bool GoogleMapShowInfoWindow
    {
      get { return googleMapShowInfoWindow; }
    }

    private bool googleMapEnableZoom = false;

    public bool GoogleMapEnableZoom
    {
      get { return googleMapEnableZoom; }
    }

    private bool googleMapEnableMapType = false;

    public bool GoogleMapEnableMapType
    {
      get { return googleMapEnableMapType; }
    }

    private int googleMapWidth = 500;

    public int GoogleMapWidth
    {
      get { return googleMapWidth; }
    }

    private int googleMapHeight = 300;

    public int GoogleMapHeight
    {
      get { return googleMapHeight; }
    }

    private MapType mapType = MapType.G_SATELLITE_MAP;

    public MapType GoogleMapType
    {
      get { return mapType; }
    }

    private bool showPostAuthor = false;

    public bool ShowPostAuthor
    {
      get { return showPostAuthor; }
    }

    private bool useLinkForHeading = true;

    public bool UseLinkForHeading
    {
      get { return useLinkForHeading; }
    }

    private bool allowComments = true;

    public bool AllowComments
    {
      get { return allowComments; }
    }

    private bool navigationOnRight = false;

    public bool NavigationOnRight
    {
      get { return navigationOnRight; }
    }

    private bool showAddFeedLinks = true;

    public bool ShowAddFeedLinks
    {
      get { return showAddFeedLinks; }
    }

    private bool showFeedLinks = true;

    public bool ShowFeedLinks
    {
      get { return showFeedLinks; }
    }

    private bool showStatistics = true;

    public bool ShowStatistics
    {
      get { return showStatistics; }
    }

    private bool showArchives = false;

    public bool ShowArchives
    {
      get { return showArchives; }
    }


    private int archivesPageSize = 15;
    public int ArchivesPageSize
    {
      get { return archivesPageSize; }
    }


    private bool showCategories = false;

    public bool ShowCategories
    {
      get { return showCategories; }
    }

    private bool showCalendar = false;

    public bool ShowCalendar
    {
      get { return showCalendar; }
    }

    private bool useTagCloudForCategories = false;

    public bool UseTagCloudForCategories
    {
      get { return useTagCloudForCategories; }
    }


    private string dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat.LongDatePattern;
    public string DateTimeFormat
    {
      get { return dateTimeFormat; }
    }

    private int daysPostAreConsideredRecent = 7;
    public int DaysPostAreConsideredRecent
    {
      get { return daysPostAreConsideredRecent; }
    }

    private string recentDateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat.LongDatePattern;
    public string RecentDateTimeFormat
    {
      get { return recentDateTimeFormat; }
    }




    private bool useExcerpt = false;

    public bool UseExcerpt
    {
      get { return useExcerpt; }
    }

    private bool titleOnly = false;

    public bool TitleOnly
    {
      get { return titleOnly; }
    }

    private bool showPager = true;

    public bool ShowPager
    {
      get { return showPager; }
    }

    private bool googleMapIncludeWithExcerpt = false;

    public bool GoogleMapIncludeWithExcerpt
    {
      get { return googleMapIncludeWithExcerpt; }
    }

    private bool enableContentRating = false;

    public bool EnableContentRating
    {
      get { return enableContentRating; }
    }

    private bool enableRatingComments = false;

    public bool EnableRatingComments
    {
      get { return enableRatingComments; }
    }

    private bool hideAddThisButton = false;

    public bool HideAddThisButton
    {
      get { return hideAddThisButton; }
    }

    private int excerptLength = 250;

    public int ExcerptLength
    {
      get { return excerptLength; }
    }

    private string excerptSuffix = "...";

    public string ExcerptSuffix
    {
      get { return excerptSuffix; }
    }

    private string moreLinkText = "read more";

    public string MoreLinkText
    {
      get { return moreLinkText; }
    }

    private string blogAuthor = string.Empty;

    public string BlogAuthor
    {
      get { return blogAuthor; }
    }

    private string instanceCssClass = string.Empty;

    public string InstanceCssClass
    {
      get { return instanceCssClass; }
    }


    private bool showPostTitlesInNextPrevLinksInPostView = true;
    public bool ShowPostTitlesInNextPrevLinksInPostView
    {
      get { return showPostTitlesInNextPrevLinksInPostView; }

    }


    private int showPostTitlesInNextPrevLinksInPostViewLength = 80;
    public int ShowPostTitlesInNextPrevLinksInPostViewLength
    {
      get { return showPostTitlesInNextPrevLinksInPostViewLength; }

    }


    private bool addTagsToBlogViewBodyContent = true;
    public bool AddTagsToBlogViewBodyContent
    {
      get { return addTagsToBlogViewBodyContent; }
    }

    private bool addTagsToBlogViewBodyExerptContent = true;
    public bool AddTagsToBlogViewBodyExerptContent
    {
      get { return addTagsToBlogViewBodyExerptContent; }
    }


  }
}