﻿
using System;
using System.Collections.Generic;
using System.Text;
using Quantifi.Business;
using mojoPortal.Business.WebHelpers;




namespace Quantifi.UI.Blogs
{
    public class QuantifiBlogContentDeleteHandler : ContentDeleteHandlerProvider
    {
        public QuantifiBlogContentDeleteHandler()
        { }

	 

        public override void DeleteContent(int moduleId, Guid moduleGuid)
        {

          Blog.DeleteByModule(moduleId);


          mojoPortal.Business.ContentMetaRespository metaRepository = new mojoPortal.Business.ContentMetaRespository();
            metaRepository.DeleteByModule(moduleGuid);

            mojoPortal.Business.FriendlyUrl.DeleteByPageGuid(moduleGuid);
            
        }

    }
}
