﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using System.Collections;
using System.Globalization;
using mojoPortal.Web.Framework;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.BlogsModule.Components
{
  public class AggregateBlogsConfiguration
  {

    private static readonly ILog log = LogManager.GetLogger(typeof(AggregateBlogsConfiguration));


    public AggregateBlogsConfiguration()
    { }

    public AggregateBlogsConfiguration(Hashtable settings)
    {
      LoadSettings(settings);

    }

    private void LoadSettings(Hashtable settings)
    {
      if (settings == null) { throw new ArgumentException("must pass in a hashtable of settings"); }


      if (settings.Contains("IncludedBlogIdsSetting"))
      {
        includedBlogIds = settings["IncludedBlogIdsSetting"].ToString().SplitStringIntoInts().ToList();
      }

  
      showTagsOnBlogRoll = WebUtils.ParseBoolFromHashtable(settings, "ShowTagsOnBlogRollSetting", showTagsOnBlogRoll);

      showTimeStampOnBlogRoll = WebUtils.ParseBoolFromHashtable(settings, "ShowTimeStampOnBlogRollSetting", showTimeStampOnBlogRoll);

      pageSize = WebUtils.ParseInt32FromHashtable(settings, "BlogEntriesToShowSetting", pageSize);

      useExcerpt = WebUtils.ParseBoolFromHashtable(settings, "BlogUseExcerptSetting", useExcerpt);

      excerptLength = WebUtils.ParseInt32FromHashtable(settings, "BlogExcerptLengthSetting", excerptLength);


      if (settings.Contains("BlogMoreLinkText"))
      {
        moreLinkText = settings["BlogMoreLinkText"].ToString();
      }

      if (settings.Contains("BlogDateTimeFormat"))
      {
        string format = settings["BlogDateTimeFormat"].ToString().Trim();
        if (format.Length > 0)
        {
          try
          {
            string d = DateTime.Now.ToString(format, CultureInfo.CurrentCulture);
            dateTimeFormat = format;
          }
          catch (FormatException) { }
        }

      }


      showPostAuthor = WebUtils.ParseBoolFromHashtable(settings, "ShowPostAuthorSetting", showPostAuthor);

      if (settings.Contains("BlogAuthorSetting"))
      {
        blogAuthor = settings["BlogAuthorSetting"].ToString();
      }


      if (settings.Contains("BlogFeedburnerFeedUrl"))
      {
        feedburnerFeedUrl = settings["BlogFeedburnerFeedUrl"].ToString();
      }

      addTweetThisToFeed = WebUtils.ParseBoolFromHashtable(settings, "ShowTweetThisLink", addTweetThisToFeed);

      daysPostAreConsideredRecent = WebUtils.ParseInt32FromHashtable(settings, "DaysPostAreConsideredRecentSetting", daysPostAreConsideredRecent); ;


      if (settings.Contains("RecentDateTimeFormatSetting"))
      {
        recentDateTimeFormat = settings["RecentDateTimeFormatSetting"].ToString();
      }

    }

    private List<int> includedBlogIds = new List<int>();
    public List<int> IncludedBlogIds { get { return includedBlogIds; } }

    private bool showTagsOnBlogRoll = true;
    public bool ShowTagsOnBlogRoll { get { return showTagsOnBlogRoll; } }

    private bool showTimeStampOnBlogRoll = true;
    public bool ShowTimeStampOnBlogRoll { get { return showTimeStampOnBlogRoll; } }

    private int pageSize = 10;
    public int PageSize
    {
      get { return pageSize; }
    }

    private bool useExcerpt = true;
    public bool UseExcerpt
    {
      get { return useExcerpt; }
    }

    private int excerptLength = 250;
    public int ExcerptLength
    {
      get { return excerptLength; }
    }

    private string excerptSuffix = "...";
    public string ExcerptSuffix
    {
      get { return excerptSuffix; }
    }

    private string moreLinkText = "read more";
    public string MoreLinkText
    {
      get { return moreLinkText; }
    }

    private string dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat.LongDatePattern;
    public string DateTimeFormat
    {
      get { return dateTimeFormat; }
    }


    private int daysPostAreConsideredRecent = 7;
    public int DaysPostAreConsideredRecent
    {
      get { return daysPostAreConsideredRecent; }
    }

    private string recentDateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat.LongDatePattern;
    public string RecentDateTimeFormat
    {
      get { return recentDateTimeFormat; }
    }


    private bool showPostAuthor = false;
    public bool ShowPostAuthor
    {
      get { return showPostAuthor; }
    }


    private string blogAuthor = string.Empty;
    public string BlogAuthor
    {
      get { return blogAuthor; }
    }

    private string feedburnerFeedUrl = string.Empty;
    public string FeedburnerFeedUrl
    {
      get { return feedburnerFeedUrl; }
    }


    private bool addTweetThisToFeed = false;
    public bool AddTweetThisToFeed
    {
      get { return addTweetThisToFeed; }
    }



  }
}