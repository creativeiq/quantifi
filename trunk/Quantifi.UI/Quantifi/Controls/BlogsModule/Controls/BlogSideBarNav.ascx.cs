﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Quantifi.UI.Utils;
using mojoPortal.Web;
using Quantifi.Business.Utils;
using Quantifi.UI.Controls.BlogsModule.Components;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Business;

namespace Quantifi.UI.Controls.BlogsModule.Controls
{
  public partial class BlogSideBarNav : SiteModuleControl
	{

    
    protected int ActiveIndex { get; set; }

    protected BlogNavConfiguration Config = null;



		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			LoadParams();


      Config = new BlogNavConfiguration(this.Settings);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			LoadDisplayedModules();

		}


		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			var basePage = this.Page as mojoBasePage;


			basePage.SuppressPageMenu();
			//basePage.ForceSideContent(true, null);
		}

		private void LoadDisplayedModules()
		{

      this.AggregateBlogNavView1.Visible = Config.ShowAggregateBlogView;
      this.AggregateBlogNavView1.PageId = Config.AggregateBlogViewPageId;
      this.AggregateBlogNavView1.ModuleId = Config.AggregateBlogViewId;

				
			this.ActiveIndex = GetActiveIndex();

			this.rptBlogs.DataSource = Config.BlogModuleIds;
			this.rptBlogs.DataBind();
		}


    private int GetActiveIndex()
    {

 
      var currentPage = CacheHelper.GetCurrentPage();
      bool showAggregate = (Config.ShowAggregateBlogView && Config.AggregateBlogViewId > 0);

      var modules = currentPage.Modules;
      if (modules == null)
        return -1;

      //var pages = CacheUtils.GetPagesByModule(Config.AggregateBlogViewId);
      for (int i = 0; i < modules.Count; i++)
      {
        var module = currentPage.Modules[i] as Module;
        if (module == null)
           return -1;

        if (showAggregate && module.ModuleId == Config.AggregateBlogViewId)
        {
          this.AggregateBlogNavView1.isSelected = true;
          return -1;
        }
        for (int j = 0; j < Config.BlogModuleIds.Count; j++)
        {
          if (module.ModuleId == Config.BlogModuleIds[j])
            return  j;

        }
      }


      return -1;
    }

		private void LoadParams()
		{

      //pageId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pageid", pageId);
      //moduleId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("mid", moduleId);


		}

	}
}