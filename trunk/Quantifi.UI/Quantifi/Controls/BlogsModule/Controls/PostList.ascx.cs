﻿//	Author:				Joe Audette
//	Created:			2004-08-15
//	Last Modified:		2010-05-26
//		
// The use and distribution terms for this software are covered by the 
// Common Public License 1.0 (http://opensource.org/licenses/cpl.php)
// which can be found in the file CPL.TXT at the root of this distribution.
// By using this software in any fashion, you are agreeing to be bound by 
// the terms of this license.
//
// You must not remove this notice, or any other, from this software.

using System;
using System.Data;
using System.Linq;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using mojoPortal;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using Quantifi.Data.EF;
using Resources;
using Quantifi.Shared;

using QB = Quantifi.Business;

namespace Quantifi.UI.Blogs
{
  public partial class PostList : UserControl
  {
    #region Properties

    private static readonly ILog log = LogManager.GetLogger(typeof(PostList));

    private const string PagingParam = "pg";

    private int countOfDrafts = 0;
    private int pageNumber = 1;
    private int totalPages = 1;
    private int year = 0;
    private int pageSize = 0;


    private bool isArchiveMode = false;

    protected string EditContentImage = WebConfigSettings.EditContentImage;
    protected string EditBlogAltText = "Edit";
    protected Double TimeOffset = 0;
    private TimeZoneInfo timeZone = null;
    protected DateTime CalendarDate;
    protected string addThisAccountId = string.Empty;
    protected bool ShowGoogleMap = true;
    protected string addThisCustomBrand = string.Empty;
    protected string FeedBackLabel = string.Empty;
    protected string GmapApiKey = string.Empty;
    protected bool EnableContentRating = false;
    private string DisqusSiteShortName = string.Empty;
    protected string disqusFlag = string.Empty;
    protected string IntenseDebateAccountId = string.Empty;
    protected bool ShowCommentCounts = true;
    protected string EditLinkText = QuantifiBlogResources.BlogEditEntryLink;
    protected string EditLinkTooltip = QuantifiBlogResources.BlogEditEntryLink;
    protected string EditLinkImageUrl = string.Empty;
    private Module module = null;
    private BlogConfiguration config = new BlogConfiguration();


    private DateTime RecentlyPostedDate;


    private int pageId = -1;
    private int moduleId = -1;
    private bool isEditable = false;
    private string siteRoot = string.Empty;
    private string imageSiteRoot = string.Empty;
    private SiteSettings siteSettings = null;


    public int PageId
    {
      get { return pageId; }
      set { pageId = value; }
    }

    public int ModuleId
    {
      get { return moduleId; }
      set { moduleId = value; }
    }

    public string SiteRoot
    {
      get { return siteRoot; }
      set { siteRoot = value; }
    }

    public string ImageSiteRoot
    {
      get { return imageSiteRoot; }
      set { imageSiteRoot = value; }
    }

    public BlogConfiguration Config
    {
      get { return config; }
      set { config = value; }
    }

    public bool IsEditable
    {
      get { return isEditable; }
      set { isEditable = value; }
    }



    #endregion


    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      this.Load += new EventHandler(Page_Load);

      this.EnableViewState = false;

    }


    protected virtual void Page_Load(object sender, EventArgs e)
    {

      LoadSettings();
      PopulateLabels();

      if (!Page.IsPostBack)
      {
        PopulateControls();
      }

    }

    private void PopulateControls()
    {
      BindBlogs();
      //PopulateNavigation();

    }

    private void BindBlogs()
    {
      using (var dc = new CorpWebEntities())
      {

        var blogPostData = dc.BlogPosts
                            .Include("Tags")
                            .Where(b => b.ModuleID == this.ModuleId && b.StartDate.HasValue && b.IsPublished.HasValue == true && b.IsPublished.Value == true);

        if (isArchiveMode)
        {

          blogPostData = blogPostData.Where(b => b.StartDate.Value.Year == year);

          pageSize = config.ArchivesPageSize;

        }
        else
        {
          pageSize = config.PageSize;
        }



        //totalPages = //DataUtils.GetTotalPages(blogPostData.Count(), pageSize);
        totalPages = blogPostData.GetTotalPages(pageSize);

        if (isArchiveMode && year != DateTime.Now.Year && config.UseArchiveViewForArchive)
        {
          rptArchive.DataSource = blogPostData.OrderByDescending(b => b.StartDate)
                                               .Page(pageNumber, pageSize);

          rptArchive.DataBind();
        }
        else
        {
          rptBlogs.DataSource = blogPostData.OrderByDescending(b => b.StartDate)
                                            .Page(pageNumber, pageSize);
          rptBlogs.DataBind();
        }

        string pageUrl = string.Empty;

        var pageSettings = CacheHelper.GetCurrentPage();


        if (pageSettings != null)
        {
          pageUrl = SiteRoot
            + pageSettings.Url.Replace("~/", "/")
            + ((pageSettings.Url.Contains("?") ? "&" : "?"))
            + PagingParam
            + "={0}" + ((isArchiveMode) ? ("&amp;year=" + year.ToInvariantString()) : string.Empty);
        }
        else
        {
          pageUrl = SiteRoot + "/Quantifi/Controls/BlogsModule/ViewList.aspx"
               + "?pageid=" + pageId.ToInvariantString()
               + "&amp;mid=" + moduleId.ToInvariantString()
               + ((isArchiveMode) ? ("&amp;year=" + year.ToInvariantString()) : string.Empty)
               + "&amp;" + PagingParam + "={0}";


        }


        pgr.PageURLFormat = pageUrl;
        pgr.ShowFirstLast = true;
        pgr.PageSize = config.PageSize;
        pgr.PageCount = totalPages;
        pgr.CurrentIndex = pageNumber;
        pgr.Visible = (totalPages > 1) && config.ShowPager;

      }

    }



    protected virtual void PopulateLabels()
    {

      //calBlogNav.UseAccessibleHeader = true;

      EditBlogAltText = QuantifiBlogResources.EditImageAltText;
      FeedBackLabel = QuantifiBlogResources.BlogFeedbackLabel;

      mojoBasePage basePage = Page as mojoBasePage;
      if (basePage != null)
      {
        if (!basePage.UseTextLinksForFeatureSettings)
        {
          EditLinkImageUrl = ImageSiteRoot + "/Data/SiteImages/" + EditContentImage;
        }

      }

    }

    protected string FormatPostAuthor(string authorName)
    {
      if (config.ShowPostAuthor)
      {
        if (config.BlogAuthor.Length > 0)
        {
          return string.Format(CultureInfo.InvariantCulture, QuantifiBlogResources.PostAuthorFormat, config.BlogAuthor);
        }

        return string.Format(CultureInfo.InvariantCulture, QuantifiBlogResources.PostAuthorFormat, authorName);
      }

      return string.Empty;

    }

    protected string FormatBlogEntry(string blogHtml, string excerpt, string url, int itemId)
    {

      var MoreLink = " <a href='" + FormatBlogUrl(url, itemId) + "' class='blogMoreLink' >" + config.MoreLinkText + "</a>";


      if (config.UseExcerpt)
      {

        if ((excerpt.Length > 0) && (excerpt != "<p>&#160;</p>"))
        {
          var trimedExcerpt = excerpt.Trim();

          if (trimedExcerpt.EndsWith("</p>"))
          {
            return trimedExcerpt.Substring(0, trimedExcerpt.Length - 4) + config.ExcerptSuffix + MoreLink + "</p>";
          }
          else if (trimedExcerpt.EndsWith("</div>"))
          {
            return trimedExcerpt.Substring(0, trimedExcerpt.Length - 6) + config.ExcerptSuffix + MoreLink + "</div>";
          }
          else
          {
            return excerpt + config.ExcerptSuffix + MoreLink;
          }
        }

        string result = string.Empty;
        if ((blogHtml.Length > config.ExcerptLength) && (config.MoreLinkText.Length > 0))
        {

          result = UIHelper.CreateExcerpt(blogHtml, config.ExcerptLength, config.ExcerptSuffix);
          result += MoreLink;
          return result;
        }

      }

      return blogHtml;
    }

    protected string FormatBlogDate(DateTime startDate)
    {
      if (!isArchiveMode && !Config.ShowTimeStampOnBlogRoll)
        return string.Empty;

      if (isArchiveMode && !config.ShowTimeStampOnBlogArchive)
        return string.Empty;


      string formatStr;

      if (DateTime.Compare(RecentlyPostedDate, startDate) < 0)
        formatStr = config.DateTimeFormat;
      else
        formatStr = config.RecentDateTimeFormat;


      DateTime time;
      if (timeZone != null)
      {
        time = TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone);
      }
      else
      {
        time=startDate.AddHours(TimeOffset);
      }

      return @"<div class='blogdate'><span class='bdate'>" + time.ToString(formatStr) + @"</span></div>";

    }

    protected string FormatBlogUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty) + disqusFlag;

      return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
          + "&ItemID=" + itemId.ToInvariantString()
          + "&mid=" + ModuleId.ToInvariantString()
          + disqusFlag;

    }

    protected string FormatBlogTitleUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
          + "&ItemID=" + itemId.ToInvariantString()
          + "&mid=" + ModuleId.ToInvariantString();

    }

    private string GetRssUrl()
    {
      if (config.FeedburnerFeedUrl.Length > 0) return config.FeedburnerFeedUrl;

      return SiteRoot + "/blog" + ModuleId.ToInvariantString() + "rss.aspx";

    }




    protected virtual void LoadSettings()
    {
      siteSettings = CacheHelper.GetCurrentSiteSettings();
      TimeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();
      GmapApiKey = SiteUtils.GetGmapApiKey();
      addThisAccountId = siteSettings.AddThisDotComUsername;
      pageNumber = WebUtils.ParseInt32FromQueryString(PagingParam, pageNumber);

      module = new Module(moduleId);

      year = WebUtils.ParseInt32FromQueryString("year", 0);

      if (year > 2000)
      {
        isArchiveMode = true;
      }


      //CalendarDate = WebUtils.ParseDateFromQueryString("blogdate", DateTime.UtcNow).Date;

      //if (CalendarDate > DateTime.UtcNow.Date)
      //{
      //  CalendarDate = DateTime.UtcNow.Date;
      //}

      if ((config.UseExcerpt) && (!config.GoogleMapIncludeWithExcerpt)) { ShowGoogleMap = false; }

      EnableContentRating = config.EnableContentRating;
      if (config.UseExcerpt) { EnableContentRating = false; }

      if (config.AddThisAccountId.Length > 0)
        addThisAccountId = config.AddThisAccountId;

      if (config.AddThisCustomBrand.Length > 0)
      {
        addThisCustomBrand = config.AddThisCustomBrand;
      }
      else
      {
        addThisCustomBrand = siteSettings.SiteName;
      }

      if (config.DisqusSiteShortName.Length > 0)
      {
        DisqusSiteShortName = config.DisqusSiteShortName;
      }
      else
      {
        DisqusSiteShortName = siteSettings.DisqusSiteShortName;
      }

      if (config.IntenseDebateAccountId.Length > 0)
      {
        IntenseDebateAccountId = config.IntenseDebateAccountId;
      }
      else
      {
        IntenseDebateAccountId = siteSettings.IntenseDebateAccountId;
      }




      if (config.Copyright.Length > 0)
      {
        lblCopyright.Text = config.Copyright;
      }


      if (IsEditable)
      {
        countOfDrafts = QB.Blog.CountOfDrafts(ModuleId);
      }

      RecentlyPostedDate = DateTime.UtcNow.AddDays(-(config.DaysPostAreConsideredRecent));
    }


    protected virtual void SetupRssLink()
    {
      if (module != null)
      {
        if (Page.Master != null)
        {
          Control head = Page.Master.FindControl("Head1");
          if (head != null)
          {

            Literal rssLink = new Literal();
            rssLink.Text = "<link rel=\"alternate\" type=\"application/rss+xml\" title=\""
                    + module.ModuleTitle + "\" href=\""
                    + GetRssUrl() + "\" />";

            head.Controls.Add(rssLink);

          }
        }
      }

    }



  }



}