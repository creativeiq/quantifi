﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.UI.Blogs;
using System.Diagnostics;
using Quantifi.Business;
using Quantifi.Business.Utils;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web;


namespace Quantifi.UI.Controls.BlogsModule.Controls
{
	public partial class ModuleArchiveView : System.Web.UI.UserControl
	{

		public int ModuleId { get; set; }

		private int CurrentPageId = -1;
		private int CurrentPageModuleId = -1;
		private int CurrentYear = -1;


		protected void Page_Load(object sender, EventArgs e)
		{			
      
      LoadParams();

			var blogModule = new mojoPortal.Business.Module(ModuleId);
			var settings =  new BlogConfiguration(mojoPortal.Business.ModuleSettings.GetModuleSettings(ModuleId));

 
			this.blogLink.Text = blogModule.ModuleTitle;


      var pages = CacheUtils.GetPagesByModule(ModuleId);

      if (pages.Any())
      {
        var page = pages.First();
        this.blogLink.NavigateUrl = GetSiteRootUrl(page.Url);
      }
 
			if (settings.SidebarNumYearAchivesShown < 1)
				return;

			rptYears.DataSource = BlogCacheUtil.GetArchiveInfo(ModuleId).Take(settings.SidebarNumYearAchivesShown);
			rptYears.DataBind();

		}

		public string GetCssClassForYear(object obj)
		{
			if (CurrentYear < 2000)
				return string.Empty;

			var yearInfo = obj as YearArchiveInfo;
			if (yearInfo == null) 
        return string.Empty;

      if (this.CurrentPageModuleId > 0 && yearInfo.ModuleId == this.CurrentPageModuleId && yearInfo.Year == this.CurrentYear)
				return "AspNet-TreeView-Selected";

			return string.Empty;
		}

		public string GetLinkForYear(object obj)
		{
			var yearInfo = obj as YearArchiveInfo;

			if (yearInfo == null)
				return string.Empty;

      var url =  
        this.blogLink.NavigateUrl +
        ((this.blogLink.NavigateUrl.Contains("?")) ? "&year=" : "?year=") + yearInfo.Year.ToString();

      //var url = "/Quantifi/Controls/BlogsModule/ViewArchive.aspx?year=" + yearInfo.Year.ToString()
      //          + "&mid=" + this.ModuleId.ToString()
      //          + "&pageid=" + this.CurrentPageId.ToString();
			return url;
		}

		private void LoadParams()
		{
        var pageSettings = CacheHelper.GetCurrentPage();

        CurrentPageId = pageSettings.PageId;


        foreach (mojoPortal.Business.Module m in pageSettings.Modules)
        {
          if (m.ModuleId == this.ModuleId)
          {
            this.CurrentPageModuleId = this.ModuleId;
          }
        }
			//CurrentPageId = mojoPortal.Web.Framework.WebUtils.LoadOptionalRequestParam
        
        //mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pageid", CurrentPageId);

			//CurrentPageModuleId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("mid", CurrentPageModuleId);

			CurrentYear = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("year", CurrentYear);
			//pageNumber = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pagenumber", -1);
		}


    protected string GetSiteRootUrl(string url)
    {
      if (url.StartsWith("~/"))
        return SiteUtils.GetNavigationSiteRoot() + url.Replace("~/", "/");

      if (url.StartsWith("/"))
        return SiteUtils.GetNavigationSiteRoot() + url;

      return url;
    }
	}
}