﻿
using System;
using System.Linq;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Net;
using mojoPortal.Web.Editor;
using mojoPortal.Web.Framework;
using mojoPortal.Web.UI;
using mojoPortal.Web.BlogUI;
using Resources;
using mojoPortal.Web;
using Quantifi.Data.EF;
using Quantifi.Shared;
using Quantifi.UI.Utils;
using System.Collections.Generic;

namespace Quantifi.UI.Blogs
{
  public partial class BlogViewControl : UserControl
  {
    #region Properties

    private static readonly ILog log = LogManager.GetLogger(typeof(BlogViewControl));
    private Hashtable moduleSettings;
    public BlogConfiguration config { get; protected set; }
    private string virtualRoot;
    private string addThisAccountId = string.Empty;
    //private Quantifi.Data.EF.BlogPosts post = null;

    private Module module;
    protected string DeleteLinkImage = "~/Data/SiteImages/" + WebConfigSettings.DeleteLinkImage;

    protected int PageId = -1;
    protected int ModuleId = -1;
    protected int ItemId = -1;
    protected bool AllowComments = false;

    protected string CommentDateTimeFormat;
    protected bool parametersAreInvalid = false;
    protected Double TimeOffset = 0;
    private TimeZoneInfo timeZone = null;

    protected bool IsEditable = false;
    protected string addThisCustomBrand = string.Empty;

    protected string EditContentImage = ConfigurationManager.AppSettings["EditContentImage"];
    protected string GmapApiKey = string.Empty;

    protected string blogAuthor = string.Empty;
    protected string SiteRoot = string.Empty;
    protected string ImageSiteRoot = string.Empty;
    private mojoBasePage basePage;

    private string DisqusSiteShortName = string.Empty;
    private string IntenseDebateAccountId = string.Empty;

    protected string RegexRelativeImageUrlPatern = @"^/.*[_a-zA-Z0-9]+\.(png|jpg|jpeg|gif|PNG|JPG|JPEG|GIF)$";

    #endregion

    #region OnInit

    override protected void OnInit(EventArgs e)
    {
      this.Load += new EventHandler(this.Page_Load);
      this.btnPostComment.Click += new EventHandler(this.btnPostComment_Click);
      this.dlComments.ItemCommand += new RepeaterCommandEventHandler(dlComments_ItemCommand);
      this.dlComments.ItemDataBound += new RepeaterItemEventHandler(dlComments_ItemDataBound);
      base.OnInit(e);
      //this.EnableViewState = this.UserCanEditPage();
      basePage = Page as mojoBasePage;
      SiteRoot = basePage.SiteRoot;
      ImageSiteRoot = basePage.ImageSiteRoot;

      SiteUtils.SetupEditor(this.edComment);

    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);
    }


    #endregion

    private void Page_Load(object sender, EventArgs e)
    {

      LoadParams();

      if ((basePage == null) || (!basePage.UserCanViewPage(ModuleId)))
      {
        SiteUtils.RedirectToAccessDeniedPage();
        return;
      }

        if (parametersAreInvalid)
        {
          AllowComments = false;
          this.pnlBlog.Visible = false;
          return;
        }


      using (var dc = new Data.EF.CorpWebEntities())
      {
        var allBlogPosts = dc.BlogPosts.Where(br => br.ModuleID == this.ModuleId && br.StartDate.HasValue);


        var currentPost = allBlogPosts.SingleOrDefault(b => b.ItemID == this.ItemId);

        var previousPost = allBlogPosts
                            .Where(b => b.StartDate < currentPost.StartDate)
                            .OrderByDescending(b => b.StartDate)
                            .FirstOrDefault();


        var nextPost = allBlogPosts
                              .Where(b => b.StartDate > currentPost.StartDate)
                              .OrderBy(br => br.StartDate)
                              .FirstOrDefault();



        LoadSettings(currentPost);
        SetupCss();
        PopulateLabels();

        if (ModuleId > 0 && ItemId > 0)
        {

          if (Context.User.Identity.IsAuthenticated && WebUser.HasEditPermissions(basePage.SiteInfo.SiteId, ModuleId, basePage.CurrentPage.PageId))
          {
            IsEditable = true;
          }


          PopulateControls(currentPost, previousPost, nextPost);
        }


      //  SetupTagsScripts();
        AddConnoicalUrl(currentPost);
        AddRssUrl();

      }

    }

    protected void PopulateControls(BlogPosts currentPost, BlogPosts prevPost, BlogPosts nextPost)
    {
      if (parametersAreInvalid)
      {
        AllowComments = false;
        pnlBlog.Visible = false;
        return;
      }

      // if not published only the editor can see it
      if (((!currentPost.IsPublished.HasValue) || (!currentPost.IsPublished.Value) || (!currentPost.StartDate.HasValue) || (currentPost.StartDate.Value > DateTime.UtcNow)) && (!basePage.UserCanEditModule(ModuleId)))
      {
        AllowComments = false;
        pnlBlog.Visible = false;
        WebUtils.SetupRedirect(this, SiteUtils.GetCurrentPageUrl());
        return;
      }

      #region IsEditable

      if (this.IsEditable)
      {
        this.lnkEdit.Visible = true;

        if (!basePage.UseTextLinksForFeatureSettings)
        {
          lnkEdit.ImageUrl = basePage.ImageSiteRoot + "/Data/SiteImages/" + EditContentImage;
          lnkEdit.Text = QuantifiBlogResources.EditImageAltText;
        }
        else
        {
          lnkEdit.Text = QuantifiBlogResources.BlogEditEntryLink;
        }


        this.lnkEdit.NavigateUrl = basePage.SiteRoot
                                    + "/Quantifi/Controls/BlogsModule/EditPost.aspx?pageid=" + PageId.ToInvariantString()
                                    + "&ItemID=" + ItemId.ToInvariantString()
                                    + "&mid=" + ModuleId.ToInvariantString();


      }

      #endregion


      litTitle.Text = Server.HtmlEncode(currentPost.Heading);


      basePage.Title = SiteUtils.FormatPageTitle(basePage.SiteInfo, currentPost.Heading);
      basePage.MetaDescription = GetMetaDescription(currentPost);
      basePage.MetaKeywordCsv = GetMetaKeywordCSV(currentPost);
      basePage.AdditionalMetaMarkup = currentPost.CompiledMeta;





      txtCommentTitle.Text = "re: " + currentPost.Heading;


      if (config.ShowTimeStampOnBlogPost && currentPost.StartDate.HasValue)
      {
        var startDate = currentPost.StartDate.Value;

        litStartDate.Visible = true;
        if (timeZone != null)
        {
          litStartDate.Text = TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone).ToString(config.DateTimeFormat);
        }
        else
        {
          litStartDate.Text = startDate.AddHours(TimeOffset).ToString(config.DateTimeFormat);
        }
      }
      else
      {
        litStartDate.Visible = false;
      }

      odiogoPlayer.OdiogoFeedId = config.OdiogoFeedId;
      odiogoPlayer.ItemId = currentPost.ItemID.ToString(CultureInfo.InvariantCulture);
      odiogoPlayer.ItemTitle = currentPost.Heading;

      //if (blogAuthor.Length == 0) 
      //{ 
      //  blogAuthor = blogpost.//user;    
      //}

      litAuthor.Text = string.Format(CultureInfo.InvariantCulture, QuantifiBlogResources.PostAuthorFormat, blogAuthor);


      // parse content for tags
      if (config.AddTagsToBlogViewBodyContent)
      {
        litDescription.Text = GetHtmlContentWithTagsLinks(currentPost.Description, currentPost.Tags);
      }
      else
      {
        litDescription.Text = currentPost.Description;
      }

      if (config.AddTagsToBlogViewBodyExerptContent)
      {
        litExcerpt.Text = GetHtmlContentWithTagsLinks(GetExcerpt(currentPost), currentPost.Tags);
      }
      else
      {
        litExcerpt.Text = GetExcerpt(currentPost);
      }






      if (config.ShowTagsOnBlogPost)
      {
        this.rptTags.DataSource = currentPost.Tags;
        this.rptTags.DataBind();
      }

      #region Next Previous Links

      if (prevPost != null && !string.IsNullOrWhiteSpace(prevPost.ItemUrl))
      {
        var previousPostUrl = basePage.SiteRoot + prevPost.ItemUrl.Replace("~", string.Empty);

        lnkPreviousPost.Visible = true;
        lnkPreviousPost.NavigateUrl = previousPostUrl;
        lnkPreviousPost.ToolTip = prevPost.Heading;

        if (config.ShowPostTitlesInNextPrevLinksInPostView)
        {
          //"&lt;&lt;&nbsp;" +
          lnkPreviousPost.Text = prevPost.Heading.TrimToLength(config.ShowPostTitlesInNextPrevLinksInPostViewLength);
        }

      }

      //if (blogpost.NextPostUrl.Length > 0)
      if (nextPost != null && !string.IsNullOrWhiteSpace(nextPost.ItemUrl))
      {

        lnkNextPost.Visible = true;
        lnkNextPost.NavigateUrl = basePage.SiteRoot + nextPost.ItemUrl.Replace("~", string.Empty);
        lnkNextPost.ToolTip = nextPost.Heading;

        if (config.ShowPostTitlesInNextPrevLinksInPostView)
        {
          //+ "&nbsp;&gt;&gt;"
          lnkNextPost.Text = nextPost.Heading.TrimToLength(config.ShowPostTitlesInNextPrevLinksInPostViewLength);
        }

      }
      #endregion

      #region Post Location

      if (currentPost.Location.Length > 0)
      {
        gmap.Visible = true;
        gmap.GMapApiKey = GmapApiKey;
        gmap.Location = currentPost.Location;
        gmap.EnableMapType = config.GoogleMapEnableMapType;
        gmap.EnableZoom = config.GoogleMapEnableZoom;
        gmap.ShowInfoWindow = config.GoogleMapShowInfoWindow;
        gmap.EnableLocalSearch = config.GoogleMapEnableLocalSearch;
        gmap.MapHeight = config.GoogleMapHeight;
        gmap.MapWidth = config.GoogleMapWidth;
        gmap.EnableDrivingDirections = config.GoogleMapEnableDirections;
        gmap.GmapType = config.GoogleMapType;
        gmap.ZoomLevel = config.GoogleMapInitialZoom;
        gmap.DirectionsButtonText = QuantifiBlogResources.MapGetDirectionsButton;
        gmap.DirectionsButtonToolTip = QuantifiBlogResources.MapGetDirectionsButton;
      }



      #endregion


    }

    private void AddConnoicalUrl(BlogPosts blogpost)
    {
      if (Page.Header == null) { return; }

      Literal link = new Literal();
      link.ID = "blogurl";
      link.Text = "\n<link rel='canonical' href='"
          + SiteRoot
          + blogpost.ItemUrl.Replace("~/", "/")
          + "' />";

      Page.Header.Controls.Add(link);

    }

    private void AddRssUrl()
    {
      if (Page.Header == null) { return; }


      Literal link = new Literal();
      link.ID = "blogrssurl";
      link.Text = "\n<link rel='alternate' type='application/rss+xml' title='" + module.ModuleTitle
        + "' href='" + GetRssUrl()
        + "' />";

      Page.Header.Controls.Add(link);

    }

    private string GetRssUrl()
    {
      if (config.FeedburnerFeedUrl.Length > 0)
        return config.FeedburnerFeedUrl;

      return SiteRoot + "/blog" + ModuleId.ToString(CultureInfo.InvariantCulture) + "rss.aspx";

    }

    private void btnPostComment_Click(object sender, EventArgs e)
    {
      using (var dc = new CorpWebEntities())
      {

        var allBlogPosts = dc.BlogPosts.Where(br => br.ModuleID == this.ModuleId && br.StartDate.HasValue);


        var currentPost = allBlogPosts.SingleOrDefault(b => b.ItemID == this.ItemId);

        var previousPost = allBlogPosts
                            .Where(b => b.StartDate < currentPost.StartDate)
                            .OrderByDescending(b => b.StartDate)
                            .FirstOrDefault();


        var nextPost = allBlogPosts
                              .Where(b => b.StartDate > currentPost.StartDate)
                              .OrderBy(br => br.StartDate)
                              .FirstOrDefault();



        if (currentPost == null)
        {
          return;
        }

        if (!AllowComments)
        {
          WebUtils.SetupRedirect(this, Request.RawUrl);
          return;
        }

        if (!IsValidComment(currentPost))
        {
          PopulateControls(currentPost, previousPost, nextPost);
          return;
        }



        if (currentPost.AllowCommentsForDays < 0)
        {
          WebUtils.SetupRedirect(this, Request.RawUrl);
          return;
        }


        if (!currentPost.StartDate.HasValue)
          return;


        DateTime endDate = currentPost.StartDate.Value.AddDays((double)currentPost.AllowCommentsForDays);

        if ((endDate < DateTime.UtcNow) && (currentPost.AllowCommentsForDays > 0)) return;

        if (this.chkRememberMe.Checked)
        {
          SetCookies();
        }

        //Blog.AddBlogComment(
        //        ModuleId,
        //        ItemId,
        //        this.txtName.Text,
        //        this.txtCommentTitle.Text,
        //        this.txtURL.Text,
        //        edComment.Text,
        //        DateTime.UtcNow);

        if ((config.NotifyOnComment) && (Email.IsValidEmailAddressSyntax(config.NotifyEmail)))
        {
          //added this 2008-08-07 due to blog comment spam and need to be able to ban the ip of the spammer
          StringBuilder message = new StringBuilder();
          message.Append(basePage.SiteRoot + currentPost.ItemUrl.Replace("~", string.Empty));

          message.Append("\n\nHTTP_USER_AGENT: " + Page.Request.ServerVariables["HTTP_USER_AGENT"] + "\n");
          message.Append("HTTP_HOST: " + Page.Request.ServerVariables["HTTP_HOST"] + "\n");
          message.Append("REMOTE_HOST: " + Page.Request.ServerVariables["REMOTE_HOST"] + "\n");
          message.Append("REMOTE_ADDR: " + SiteUtils.GetIP4Address() + "\n");
          message.Append("LOCAL_ADDR: " + Page.Request.ServerVariables["LOCAL_ADDR"] + "\n");
          message.Append("HTTP_REFERER: " + Page.Request.ServerVariables["HTTP_REFERER"] + "\n");

          BlogNotification.SendBlogCommentNotificationEmail(
              SiteUtils.GetSmtpSettings(),
              ResourceHelper.GetMessageTemplate(ResourceHelper.GetDefaultCulture(), "BlogCommentNotificationEmail.config"),
              basePage.SiteInfo.DefaultEmailFromAddress,
              basePage.SiteRoot,
              config.NotifyEmail,
              message.ToString());


        }
      }

      WebUtils.SetupRedirect(this, Request.RawUrl);


    }

    private void PopulateLabels()
    {

      lnkEdit.ToolTip = QuantifiBlogResources.BlogEditEntryLink;

      edComment.WebEditor.ToolBar = ToolBar.AnonymousUser;
      edComment.WebEditor.Height = Unit.Pixel(170);

      captcha.ProviderName = basePage.SiteInfo.CaptchaProvider;
      captcha.Captcha.ControlID = "captcha" + ModuleId.ToInvariantString();
      captcha.RecaptchaPrivateKey = basePage.SiteInfo.RecaptchaPrivateKey;
      captcha.RecaptchaPublicKey = basePage.SiteInfo.RecaptchaPublicKey;

      regexUrl.ErrorMessage = QuantifiBlogResources.WebSiteUrlRegexWarning;

      btnPostComment.Text = QuantifiBlogResources.BlogCommentPostCommentButton;
      SiteUtils.SetButtonAccessKey(btnPostComment, QuantifiBlogResources.BlogCommentPostCommentButtonAccessKey);


      litCommentsClosed.Text = QuantifiBlogResources.BlogCommentsClosedMessage;
      litCommentsRequireAuthentication.Text = QuantifiBlogResources.CommentsRequireAuthenticationMessage;

      //addThis1.Text = QuantifiBlogResources.AddThisButtonAltText;

      //lnkPreviousPostTop.Text = Server.HtmlEncode(QuantifiBlogResources.BlogPreviousPostLink);
      //lnkNextPostTop.Text = Server.HtmlEncode(QuantifiBlogResources.BlogNextPostLink);

      lnkPreviousPost.Text = Server.HtmlEncode(QuantifiBlogResources.BlogPreviousPostLink);
      lnkNextPost.Text = Server.HtmlEncode(QuantifiBlogResources.BlogNextPostLink);

    }

    private void LoadSettings(BlogPosts blogpost)
    {



      //blog = new QB.Blog(ItemId);
      module = new Module(ModuleId, basePage.CurrentPage.PageId);

      if ((module.ModuleId == -1) || (blogpost.ModuleID == -1) || (blogpost.ModuleID != module.ModuleId) || (basePage.SiteInfo == null))
      {
        // query string params have been manipulated
        this.pnlBlog.Visible = false;
        AllowComments = false;
        parametersAreInvalid = true;
        return;
      }

      RegexRelativeImageUrlPatern = SecurityHelper.RegexRelativeImageUrlPatern;

      moduleSettings = ModuleSettings.GetModuleSettings(ModuleId);

      config = new BlogConfiguration(moduleSettings);

      GmapApiKey = SiteUtils.GetGmapApiKey();

      if (config.DisqusSiteShortName.Length > 0)
      {
        DisqusSiteShortName = config.DisqusSiteShortName;
      }
      else
      {
        DisqusSiteShortName = basePage.SiteInfo.DisqusSiteShortName;
      }

      if (config.IntenseDebateAccountId.Length > 0)
      {
        IntenseDebateAccountId = config.IntenseDebateAccountId;
      }
      else
      {
        IntenseDebateAccountId = basePage.SiteInfo.IntenseDebateAccountId;
      }


      if (config.InstanceCssClass.Length > 0) { pnlBlog.CssClass += " " + config.InstanceCssClass; }

      CommentDateTimeFormat = config.DateTimeFormat;

      divCommentUrl.Visible = config.AllowWebSiteUrlForComments;

      ((mojoRating)Rating).Enabled = config.EnableContentRating;
      ((mojoRating)Rating).AllowFeedback = config.EnableRatingComments;
      ((mojoRating)Rating).ContentGuid = blogpost.BlogGuid.Value;



      if (config.AddThisAccountId.Length > 0)
      {
        addThisAccountId = config.AddThisAccountId;
      }
      else
      {
        addThisAccountId = basePage.SiteInfo.AddThisDotComUsername;
      }


      if (config.AddThisCustomBrand.Length > 0)
      {
        addThisCustomBrand = config.AddThisCustomBrand;
      }
      else
      {
        addThisCustomBrand = basePage.SiteInfo.SiteName;
      }

      lblCopyright.Text = config.Copyright;


      AllowComments = config.AllowComments;

      litAuthor.Visible = config.ShowPostAuthor;


      pnlFeedback.Visible = AllowComments;





      if (blogpost.AllowCommentsForDays < 0)
      {
        pnlNewComment.Visible = false;
        pnlCommentsClosed.Visible = true;
        AllowComments = false;
      }

      if (blogpost.AllowCommentsForDays == 0)
      {
        pnlNewComment.Visible = true;
        pnlCommentsClosed.Visible = false;
        AllowComments = true;
      }

      if (blogpost.AllowCommentsForDays > 0)
      {
        DateTime endDate = blogpost.StartDate.Value.AddDays((double)blogpost.AllowCommentsForDays);

        if (endDate > DateTime.UtcNow)
        {
          pnlNewComment.Visible = true;
          pnlCommentsClosed.Visible = false;
          AllowComments = true;
        }
        else
        {
          pnlNewComment.Visible = false;
          pnlCommentsClosed.Visible = true;
          AllowComments = false;
        }
      }

      if (AllowComments)
      {
        if ((config.RequireAuthenticationForComments) && (!Request.IsAuthenticated))
        {
          AllowComments = false;
          pnlNewComment.Visible = false;
          pnlCommentsRequireAuthentication.Visible = true;
        }

      }

      if (!config.UseCaptcha)
      {
        pnlAntiSpam.Visible = false;
        captcha.Visible = false;
        captcha.Enabled = false;
        //pnlNewComment.Controls.Remove(captcha);
      }



      if (AllowComments)
      {
        if ((config.CommentSystem == "disqus") && (DisqusSiteShortName.Length > 0))
        {

          // don't use new external comment system for existing posts that already have comments
          if (blogpost.CommentCount == 0)
          {
            disqus.SiteShortName = DisqusSiteShortName;
            disqus.RenderWidget = true;
            disqus.WidgetPageUrl = FormatBlogUrl(blogpost.ItemUrl, blogpost.ItemID);
            //disqus.WidgetPageId = blog.BlogGuid.ToString();
            pnlFeedback.Visible = false;
            if (config.UseCaptcha)
            {
              pnlAntiSpam.Visible = false;
              captcha.Visible = false;
              captcha.Enabled = false;
              //this.Controls.Remove(pnlAntiSpam); 
            }
          }

          //stats.ShowCommentCount = false;

        }

        if ((config.CommentSystem == "intensedebate") && (IntenseDebateAccountId.Length > 0))
        {
          if (blogpost.CommentCount == 0)
          {
            intenseDebate.AccountId = IntenseDebateAccountId;
            intenseDebate.PostUrl = FormatBlogUrl(blogpost.ItemUrl, blogpost.ItemID);
            pnlFeedback.Visible = false;
            if (config.UseCaptcha)
            {
              pnlAntiSpam.Visible = false;
              captcha.Visible = false;
              captcha.Enabled = false;
              //this.Controls.Remove(pnlAntiSpam); 
            }
          }
          //stats.ShowCommentCount = false;
        }
      }




      if (Request.IsAuthenticated)
      {
        SiteUser currentUser = SiteUtils.GetCurrentSiteUser();
        this.txtName.Text = currentUser.Name;
        txtURL.Text = currentUser.WebSiteUrl;

      }
      else
      {
        if ((config.HideDetailsFromUnauthencticated) && (blogpost.Description.Length > config.ExcerptLength))
        {
          pnlDetails.Visible = false;
          pnlExcerpt.Visible = true;
        }

        if (CookieHelper.CookieExists("blogUser"))
        {
          this.txtName.Text = CookieHelper.GetCookieValue("blogUser");
        }
        if (CookieHelper.CookieExists("blogUrl"))
        {
          this.txtURL.Text = CookieHelper.GetCookieValue("blogUrl");
        }
      }

    }

    private string GetExcerpt(BlogPosts blogpost)
    {
      string exerpt;
      if ((blogpost.Excerpt.Length > 0) && (blogpost.Excerpt != "<p>&#160;</p>"))
      {
        exerpt = blogpost.Excerpt;
      }
      else if ((blogpost.Description.Length > config.ExcerptLength))
      {
        exerpt = UIHelper.CreateExcerpt(blogpost.Description, config.ExcerptLength, config.ExcerptSuffix);
      }
      else
      {
        exerpt = blogpost.Description;
      }

      return exerpt;
    }




    protected string FormatCommentDate(DateTime startDate)
    {
      if (timeZone != null)
      {
        return TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone).ToString(CommentDateTimeFormat);
      }

      return startDate.AddHours(TimeOffset).ToString(config.DateTimeFormat);
    }

    protected string FormatBlogUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
          + "&ItemID=" + itemId.ToInvariantString()
          + "&mid=" + ModuleId.ToInvariantString();

    }


    private bool IsValidComment(BlogPosts blogpost)
    {
      if (parametersAreInvalid) { return false; }

      if (!AllowComments) { return false; }

      if ((config.CommentSystem != "internal") && (blogpost.CommentCount == 0)) { return false; }

      if (edComment.Text.Length == 0) { return false; }
      if (edComment.Text == "<p>&#160;</p>") { return false; }

      bool result = true;

      try
      {
        Page.Validate();
        result = Page.IsValid;

      }
      catch (NullReferenceException)
      {
        //Recaptcha throws nullReference here if it is not visible/disabled
      }
      catch (ArgumentNullException)
      {
        //manipulation can make the Challenge null on recaptcha
      }


      try
      {
        if ((result) && (config.UseCaptcha))
        {
          result = captcha.IsValid;
        }
      }
      catch (NullReferenceException)
      {
        return false;
      }
      catch (ArgumentNullException)
      {
        //manipulation can make the Challenge null on recaptcha
        return false;
      }


      return result;
    }

    protected void dlComments_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
      if (e.CommandName == "DeleteComment")
      {
        Blog.DeleteBlogComment(int.Parse(e.CommandArgument.ToString()));
        WebUtils.SetupRedirect(this, Request.RawUrl);

      }
    }

    protected void dlComments_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      ImageButton btnDelete = e.Item.FindControl("btnDelete") as ImageButton;
      UIHelper.AddConfirmationDialog(btnDelete, QuantifiBlogResources.BlogDeleteCommentWarning);
    }


    private void SetCookies()
    {
      HttpCookie blogUserCookie = new HttpCookie("blogUser", this.txtName.Text);
      blogUserCookie.Expires = DateTime.Now.AddMonths(1);
      Response.Cookies.Add(blogUserCookie);

      HttpCookie blogUrlCookie = new HttpCookie("LinkUrl", this.txtURL.Text);
      blogUrlCookie.Expires = DateTime.Now.AddMonths(1);
      Response.Cookies.Add(blogUrlCookie);
    }

    private void SetupCss()
    {
      // older skins have this
      StyleSheet stylesheet = (StyleSheet)Page.Master.FindControl("StyleSheet");
      if (stylesheet != null)
      {
        if (stylesheet.FindControl("blogcss") == null)
        {
          Literal cssLink = new Literal();
          cssLink.ID = "blogcss";
          cssLink.Text = "\n<link href='"
          + SiteUtils.GetSkinBaseUrl()
          + "blogmodule.css' type='text/css' rel='stylesheet' media='screen' />";

          stylesheet.Controls.Add(cssLink);
        }
      }

    }

    private void LoadParams()
    {
      virtualRoot = WebUtils.GetApplicationRoot();
      TimeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();
      PageId = WebUtils.ParseInt32FromQueryString("pageid", -1);
      ModuleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      ItemId = WebUtils.ParseInt32FromQueryString("ItemID", -1);

      if (PageId == -1) parametersAreInvalid = true;
      if (ModuleId == -1) parametersAreInvalid = true;
      if (ItemId == -1) parametersAreInvalid = true;
      if (!basePage.UserCanViewPage(ModuleId)) { parametersAreInvalid = true; }

      addThisAccountId = basePage.SiteInfo.AddThisDotComUsername;
      addThisCustomBrand = basePage.SiteInfo.SiteName;


    }



    private string GetHtmlContentWithTagsLinks(string html, IEnumerable<Tags> tags)
    {
      string tagLink;


      foreach (var tag in tags)
      {

        tagLink = "<a class='body-inline-tag' href='" + SiteRoot
                            + "/tags.aspx?tag=" + tag.Text.UrlEncode()
                            + "&tid=" + tag.Id.ToString() + "'>"
                            + tag.Text.HtmlEncode() + "</a>";

        html = html.ReplaceFirstInHtml(tag.Text, tagLink, StringComparison.InvariantCultureIgnoreCase);

      }

      return html;
    }

    private string GetMetaKeywordCSV(BlogPosts blogpost)
    {
      var tags = blogpost.Tags.Select(t => t.Text);
      var meta = blogpost.MetaKeywords.FromDelimitedString();


      return meta.Union(tags).ToDelimitedString();
      //return string.Empty;
    }

    private string GetMetaDescription(BlogPosts blogpost)
    {
      if (string.IsNullOrWhiteSpace(blogpost.MetaDescription))
      {
        return UIHelper.CreateExcerpt(blogpost.Description, 150, string.Empty).Trim();
      }


      return blogpost.MetaDescription;
    }

//    private void SetupTagsScripts()
//    {
//      if (Page.ClientScript.IsStartupScriptRegistered(typeof(BlogViewControl), "QTagsInit"))
//      {
//        var script = @"
//var quantifiTages = new QTagsManager(); 
//quantifiTages.init();";


//        Page.ClientScript.RegisterStartupScript(typeof(BlogViewControl), "QTagsInit", script);

//      }

//    }


    protected override void Render(HtmlTextWriter writer)
    {
      if ((Page.IsPostBack) && (!pnlFeedback.Visible))
      {
        WebUtils.SetupRedirect(this, Request.RawUrl);
        return;
      }

      base.Render(writer);
    }

  }
}