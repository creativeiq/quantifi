﻿// Author:				        Joe Audette
// Created:			            2009-05-04
//	Last Modified:              2010-05-23
// 
// The use and distribution terms for this software are covered by the 
// Common Public License 1.0 (http://opensource.org/licenses/cpl.php)  
// which can be found in the file CPL.TXT at the root of this distribution.
// By using this software in any fashion, you are agreeing to be bound by 
// the terms of this license.
//
// You must not remove this notice, or any other, from this software.

using System;
using System.Globalization;
using System.Web.UI;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;
using Resources;
using mojoPortal.Web;

namespace Quantifi.UI.Blogs
{
  public partial class FeedLinksControl : UserControl
  {
    private int pageId = -1;
    private int moduleId = -1;
    private string siteRoot = string.Empty;
    private BlogConfiguration config = new BlogConfiguration();
    private string imageSiteRoot = string.Empty;
    private SiteSettings siteSettings = null;
    protected string addThisAccountId = string.Empty;
    protected string RssImageFile = WebConfigSettings.RSSImageFileName;


    public int PageId
    {
      get { return pageId; }
      set { pageId = value; }
    }

    public int ModuleId
    {
      get { return moduleId; }
      set { moduleId = value; }
    }

    public string SiteRoot
    {
      get { return siteRoot; }
      set { siteRoot = value; }
    }

    public string ImageSiteRoot
    {
      get { return imageSiteRoot; }
      set { imageSiteRoot = value; }
    }

    public BlogConfiguration Config
    {
      get { return config; }
      set { config = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected override void OnPreRender(EventArgs e)
    {
      if (this.Visible)
      {
        if (pageId == -1) { return; }
        if (moduleId == -1) { return; }

        LoadSettings();
        PopulateLabels();
        SetupLinks();
      }


      base.OnPreRender(e);

    }

    private void SetupLinks()
    {
      if (siteSettings == null) { return; }

      lnkRSS.HRef = GetRssUrl();
      imgRSS.Src = ImageSiteRoot + "/Data/SiteImages/" + RssImageFile;

      lnkAddThisRss.HRef = "http://www.addthis.com/feed.php?pub="
          + addThisAccountId + "&amp;h1=" + Server.UrlEncode(GetRssUrl())
          + "&amp;t1=";

      imgAddThisRss.Src = config.AddThisRssButtonImageUrl;

      lnkAddMSN.HRef = "http://my.msn.com/addtomymsn.armx?id=rss&amp;ut=" + GetRssUrl();

      imgMSNRSS.Src = ImageSiteRoot + "/Data/SiteImages/rss_mymsn.gif";

      lnkAddToLive.HRef = "http://www.live.com/?add=" + Server.UrlEncode(GetRssUrl());

      imgAddToLive.Src = ImageSiteRoot + "/Data/SiteImages/addtolive.gif";

      lnkAddYahoo.HRef = "http://e.my.yahoo.com/config/promo_content?.module=ycontent&amp;.url="
          + GetRssUrl();

      imgYahooRSS.Src = ImageSiteRoot + "/Data/SiteImages/addtomyyahoo2.gif";

      lnkAddGoogle.HRef = "http://fusion.google.com/add?feedurl="
          + GetRssUrl();

      imgGoogleRSS.Src = ImageSiteRoot + "/Data/SiteImages/googleaddrss.gif";

      liOdiogoPodcast.Visible = (config.OdiogoPodcastUrl.Length > 0);
      lnkOdiogoPodcast.HRef = config.OdiogoPodcastUrl;
      lnkOdiogoPodcastTextLink.NavigateUrl = config.OdiogoPodcastUrl;
      imgOdiogoPodcast.Src = ImageSiteRoot + "/Data/SiteImages/podcast.png";




    }

    private string GetRssUrl()
    {
      if (config.FeedburnerFeedUrl.Length > 0) return config.FeedburnerFeedUrl;

      return SiteRoot + "/blog" + ModuleId.ToString(CultureInfo.InvariantCulture) + "rss.aspx";

    }

    private void PopulateLabels()
    {
      lnkRSS.Title = QuantifiBlogResources.BlogRSSLinkTitle;
      lnkAddThisRss.Title = QuantifiBlogResources.BlogAddThisSubscribeAltText;
      lnkAddMSN.Title = QuantifiBlogResources.BlogModuleAddToMyMSNLink;
      lnkAddYahoo.Title = QuantifiBlogResources.BlogModuleAddToMyYahooLink;
      lnkAddGoogle.Title = QuantifiBlogResources.BlogModuleAddToGoogleLink;
      lnkAddToLive.Title = QuantifiBlogResources.BlogModuleAddToWindowsLiveLink;
      lnkOdiogoPodcast.Title = QuantifiBlogResources.PodcastLink;
      lnkOdiogoPodcastTextLink.Text = QuantifiBlogResources.PodcastLink;

    }

    private void LoadSettings()
    {
      siteSettings = CacheHelper.GetCurrentSiteSettings();
      if (siteSettings == null) { return; }

      siteRoot = SiteUtils.GetNavigationSiteRoot(siteSettings);

      if (config.AddThisAccountId.Length > 0)
      {
        addThisAccountId = config.AddThisAccountId;
      }
      else
      {
        addThisAccountId = siteSettings.AddThisDotComUsername;
      }

      liAddThisRss.Visible = (addThisAccountId.Length > 0);

      liAddThisRss.Visible = (config.ShowAddFeedLinks && (addThisAccountId.Length > 0));
      liAddGoogle.Visible = config.ShowAddFeedLinks;
      liAddMSN.Visible = config.ShowAddFeedLinks;
      liAddYahoo.Visible = config.ShowAddFeedLinks;
      liAddToLive.Visible = config.ShowAddFeedLinks;

      if (liAddThisRss.Visible)
      {
        liAddGoogle.Visible = false;
        liAddMSN.Visible = false;
        liAddYahoo.Visible = false;
        liAddToLive.Visible = false;

      }

      if (imageSiteRoot.Length == 0) { imageSiteRoot = WebUtils.GetSiteRoot(); }

    }


  }
}