﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogSideBarNav.ascx.cs"
  ViewStateMode="Disabled" EnableViewState="false" Inherits="Quantifi.UI.Controls.BlogsModule.Controls.BlogSideBarNav" %>
<%@ Register Src="~/Quantifi/Controls/BlogsModule/Controls/ModuleArchiveView.ascx"
  TagName="ModuleArchiveView" TagPrefix="quantifi" %>
<%@ Register Src="~/Quantifi/Controls/BlogsModule/Controls/AggregateBlogNavView.ascx"
  TagName="AggregateBlogNavView" TagPrefix="quantifi" %>
<asp:Panel ID="wrapper" runat="server">
  <portal:ModuleTitleControl ID="Title1" runat="server" />
  <div class="AccordianLeftNavPanel">
    <div class=" QLightTheme ui-accordion ui-widget ui-helper-reset ui-accordion-icons   ">
      <quantifi:AggregateBlogNavView ID="AggregateBlogNavView1" runat="server" />
    </div>
    <div class=" QLightTheme  accordion " style="display: none;">
      <asp:Repeater ID="rptBlogs" runat="server">
        <ItemTemplate>
          <quantifi:ModuleArchiveView runat="server" ModuleId="<%# int.Parse(Container.DataItem.ToString()) %>" />
        </ItemTemplate>
      </asp:Repeater>
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      var activeIndex = (<%=this.ActiveIndex.ToString() %> == -1) ? false : <%=this.ActiveIndex.ToString() %>;

      var nav = $(".accordion");

      nav.accordion({
        active: activeIndex,
        autoHeight: false,
        collapsible: true
      });

      var panel = $(".AccordianLeftNavPanel");


      panel.find(".blogCatYearLink").hover(function() { $(this).addClass("Menu-Hover"); }, function() { $(this).removeClass("Menu-Hover"); });

      

      panel.find("h3 a").click(function(e) {
        window.location.href = this.href;
        return false;
      });

      nav.show();

     
    });
  </script>
</asp:Panel>
