﻿<%@ Control Language="C#" AutoEventWireup="false" 
	CodeBehind="ArchiveViewControl.ascx.cs"
	Inherits="Quantifi.UI.Blogs.ArchiveViewControl" %>

<%@ Register TagPrefix="blog" TagName="TagList" Src="~/Quantifi/Controls/BlogsModule/Controls/CategoryListControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="Archives" Src="~/Quantifi/Controls/BlogsModule/Controls/ArchiveListControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="FeedLinks" Src="~/Quantifi/Controls/BlogsModule/Controls/FeedLinksControl.ascx" %>
<%@ Register TagPrefix="blog" TagName="StatsControl" Src="~/Quantifi/Controls/BlogsModule/Controls/StatsControl.ascx" %>

<asp:Panel ID="pnlBlog" runat="server" CssClass="blogwrapper">
	<div class="modulecontent">

		<asp:Panel ID="divblog" runat="server" CssClass="" SkinID="plain">
			<h2 class="  moduletitle">
				<asp:Label ID="litHeader" runat="server" Visible="True" /></h2>
			<asp:Repeater ID="dlArchives" runat="server" EnableViewState="False">
				<ItemTemplate>
					<h3 class="blogtitle">
						<asp:HyperLink ID="Title" runat="server" SkinID="plain" Text='<%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem,"Heading").ToString()) %>'
							NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
						</asp:HyperLink>&#160;</h3>
					<div class="blogcontent">
						<asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# FormatBlogDate(Convert.ToDateTime(Eval("StartDate"))) %>'
							NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
						</asp:HyperLink>&#160;
						<asp:HyperLink ID="Hyperlink2" runat="server" Text='<%# FeedBackLabel + "(" + DataBinder.Eval(Container.DataItem,"CommentCount") + ")" %>'
							Visible='<%# config.AllowComments %>' NavigateUrl='<%# FormatBlogUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID"))) %>'>
						</asp:HyperLink>&#160;
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</asp:Panel>
		<asp:Label ID="lblCopyright" runat="server" CssClass="txtcopyright"></asp:Label>
	</div>
</asp:Panel>