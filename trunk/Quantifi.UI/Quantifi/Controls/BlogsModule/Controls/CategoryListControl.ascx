﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryListControl.ascx.cs"
  Inherits="Quantifi.UI.Blogs.BlogCategories" %>
<h3>
  <mp:SiteLabel ID="Sitelabel4" runat="server" ConfigKey="BlogCategoriesLabel" ResourceFile="QuantifiBlogResources"
    UseLabelTag="false"></mp:SiteLabel>
</h3>
<asp:Repeater ID="dlCategories" runat="server" EnableViewState="False" SkinID="plain">
  <HeaderTemplate>
    <ul class="blognav">
  </HeaderTemplate>
  <ItemTemplate>
    <li>
      <asp:HyperLink ID="Hyperlink5" runat="server" EnableViewState="false" Text='<%# ResourceHelper.FormatCategoryLinkText(DataBinder.Eval(Container.DataItem,"Category").ToString(),Convert.ToInt32(DataBinder.Eval(Container.DataItem,"PostCount"))) %>'
        NavigateUrl='<%# this.SiteRoot + "/Quantifi/Controls/BlogsModule/ViewCategory.aspx?cat=" + DataBinder.Eval(Container.DataItem,"CategoryID") + "&amp;mid=" + ModuleId.ToString() + "&amp;pageid=" + PageId.ToString() %>'>
      </asp:HyperLink></li>
  </ItemTemplate>
  <FooterTemplate>
    </ul></FooterTemplate>
</asp:Repeater>
<portal:TagCloudControl ID="cloud" runat="server" />
