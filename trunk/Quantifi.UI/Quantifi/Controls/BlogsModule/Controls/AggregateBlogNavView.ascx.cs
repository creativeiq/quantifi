﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Business;
using Quantifi.Business.Utils;
using Quantifi.UI.Controls.BlogsModule.Components;

namespace Quantifi.UI.Controls.BlogsModule.Controls
{
  public partial class AggregateBlogNavView : System.Web.UI.UserControl
  {
    private int pageId = 0;
    private int moduleId = 0;

    public int PageId
    {
      get { return pageId; }
      set { pageId = value;  }
    }

    public int ModuleId
    {
      get { return moduleId; }
      set { moduleId = value; }
    }

    public bool isSelected { get; set; }

    protected string SelectedCss { get { return (this.isSelected) ? "q-selected" : string.Empty; } }

    protected void Page_Load(object sender, EventArgs e)
    {

      var page = CacheUtils.GetPage(pageId);
      if (page == null)
      {

        this.Visible = false;
        return;
      }

      Module module = null;

      foreach (Module m in page.Modules)
      {
        if (m.ModuleId == this.ModuleId)
        {
          module = m;
          break;
        }
      }
      
      if (module == null)
      {

        this.Visible = false;
        return;
      }


      //this.Visible = true;
      link.NavigateUrl = page.Url;
      link.Text = module.ModuleTitle;


      //AggregateBlogsConfiguration config = new AggregateBlogsConfiguration(ModuleSettings.GetModuleSettings(ModuleId));
      

      //if (config != null && !String.IsNullOrWhiteSpace(config.FeedburnerFeedUrl))
      //{
      //  linkRss.NavigateUrl = config.FeedburnerFeedUrl;
      //}
      //else
      //{
      //  linkRss.NavigateUrl = "~/blog" + this.moduleId.ToString() + "rss.aspx";
      //}

    }


  }
}