﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleArchiveView.ascx.cs"
  Inherits="Quantifi.UI.Controls.BlogsModule.Controls.ModuleArchiveView" %>
<h3>
  <asp:HyperLink ID="blogLink" runat="server">Blog Title Link</asp:HyperLink>
</h3>
<asp:Repeater ID="rptYears" runat="server">
  <HeaderTemplate>
    <div class="AspNet-TreeView" style="overflow: hidden">
      <ul>
  </HeaderTemplate>
  <ItemTemplate>
    <li class="AspNet-TreeView-Root AspNet-TreeView-Leaf <%# GetCssClassForYear(Container.DataItem) %>">
      <a class="blogCatYearLink" href="<%# GetLinkForYear(Container.DataItem) %>">
        <%# DataBinder.Eval(Container.DataItem, "Year").ToString()%></a></li>
  </ItemTemplate>
  <FooterTemplate>
    </ul></div>
  </FooterTemplate>
</asp:Repeater>
