﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Business;
using mojoPortal.Web.Framework;


namespace Quantifi.UI.Controls.BlogsModule.Controls
{
	public partial class CategoryArchiveControl : UserControl
	{
		#region Properties

		public int ModuleId { get; set; }
		public int CatId { get; set; }
		public int Year { get; set; }
		public int PageId { get; set; }

		public List<int> CategoryIds { get; set; }

		public int ActiveCategoryIndex { get; set; }


		#endregion

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			LoadParams();

		}

		private void LoadParams()
		{
			PageId = WebUtils.ParseInt32FromQueryString("pageid", -1);
			CatId = WebUtils.ParseInt32FromQueryString("cat", -1);
			Year = WebUtils.ParseInt32FromQueryString("year", -1);
			ModuleId = WebUtils.ParseInt32FromQueryString("mid", -1);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected override void OnPreRender(EventArgs e)
		{
			//if (ModuleId > 0)
			//  LoadControls();

			base.OnPreRender(e);
		}

		//private void LoadControls()
		//{
		//  var data = BlogCacheUtil.GetCategoryArchiveInfo(ModuleId);
		//  if (data == null || !data.Any())
		//    return;

		//  if (CategoryIds != null || data.Any())
		//  {
		//    var filtered = data.Where(d => this.CategoryIds.Contains(d.CategoryId));
				
		//    this.rptCategories.DataSource = filtered;
		//    this.rptCategories.DataBind();
		//  }
		//  else
		//  {
		//    this.rptCategories.DataSource = data;
		//    this.rptCategories.DataBind();

		//  }
		//}

		public string GetCssClassForCategoryAndYear(object obj)
		{
			if (Year < 2000 && CatId < 1)
				return string.Empty;
			
			var yearInfo = obj as YearInfo;
			if (yearInfo == null) return string.Empty;

			if (yearInfo.CategoryId == this.CatId && yearInfo.Year == this.Year)
			{
				ActiveCategoryIndex = CategoryIds.IndexOf(yearInfo.CategoryId);


				return "AspNet-TreeView-Selected";


			}
			return string.Empty;
		}

		public string GetLinkForCategoryAndYear(object obj) 
		{
			var yearInfo = obj as YearInfo;
			
			if (yearInfo == null) 
				return string.Empty;

			var url = "/Quantifi/Controls/BlogsModule/ViewCategoryArchive.aspx?cat=" + yearInfo.CategoryId.ToString()
								+ "&year=" + yearInfo.Year.ToString() 
								+ "&mid=" + this.ModuleId.ToString()
								+ "&pageid=" + this.PageId.ToString();
			return url;
		}

		public string GetHeaderLink(int catId, string catName, string cssClass)
		{

			var url = "/Quantifi/Controls/BlogsModule/ViewCategoryArchive.aspx?cat=" + catId.ToInvariantString()
					+ "&year=" + DateTime.Today.Year.ToString()
					+ "&mid=" + this.ModuleId.ToString()
					+ "&pageid=" + this.PageId.ToString();


			return "<a href=\"" + url + "\"  class=\"" + cssClass + "\">" + catName + "</a>"; ;
		}
	}
}