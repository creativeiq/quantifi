﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BlogViewControl.ascx.cs"
  EnableViewState="false" Inherits="Quantifi.UI.Blogs.BlogViewControl" %>

<%@ Register Src="~/Quantifi/Controls/LinkedInShareControl.ascx" TagName="LinkedInShare" TagPrefix="quantifi" %>

<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>



<asp:Panel ID="pnlBlog" runat="server" CssClass="panelwrapper blogwrapper blogview">
  <portal:mojoPanel ID="MojoPanel1" runat="server" ArtisteerCssClass="art-PostContent">
    <div class="modulecontent">
      <asp:Panel ID="divblog" CssClass="blogitem" runat="server" SkinID="plain" DefaultButton="btnPostComment">
        <h1 class="blogtitle">
          <asp:Literal ID="litTitle" runat="server" EnableViewState="false" />
          <asp:HyperLink ID="lnkEdit" runat="server" EnableViewState="false" CssClass="editlink"
            Visible="false">Edit</asp:HyperLink>
        </h1>
        <div class="blogdate">
          <asp:Literal ID="litAuthor" runat="server" EnableViewState="false" Visible="false" />
          <asp:Literal ID="litStartDate" runat="server" EnableViewState="false" />
        </div>
        <asp:Panel ID="pnlDetails" runat="server">
          <portal:mojoRating runat="server" ID="Rating" Enabled="false" />
          <mp:OdiogoItem ID="odiogoPlayer" runat="server" EnableViewState="false" />
          <%-- Blog Text --%>
          <div class="blogtext">
            <asp:Literal ID="litDescription" runat="server" EnableViewState="false" />
          </div>
          <%-- Location Map --%>
          <goog:LocationMap ID="gmap" runat="server" EnableViewState="false" Visible="false">
          </goog:LocationMap>
          <%-- Tags --%>
          <quantifi:ExRepeater ID="rptTags" runat="server" ViewStateMode="Disabled">
            <EmptyTemplate>
              <!-- No Tags -->
            </EmptyTemplate>
            <HeaderTemplate>
              <div class="tags">
                <span class="tags-toggle">Tags:</span>
            </HeaderTemplate>
            <ItemTemplate>
              <a class="taglink " href='/tags.aspx?tag=<%# Server.UrlEncode( Eval("Text").ToString()) %>&amp;tid=<%# Server.UrlEncode( Eval("Id").ToString()) %>'
                title='<%# string.Format("View {0} tagged content", Server.HtmlEncode( Eval("Text").ToString()))%>'>
                <%# Server.HtmlEncode(Eval("Text").ToString())%></a>
            </ItemTemplate>
            <FooterTemplate>
              </div>
            </FooterTemplate>
          </quantifi:ExRepeater>
          <%-- Copyright --%>
          <div class="blogcopyright">
            <asp:Label ID="lblCopyright" runat="server" />
          </div>
          <%-- Social --%>
          <div class="social">
            <portal:TweetThisLink ID="tweetThis1" runat="server" EnableViewState="false" />
            <portal:PlusOneButton ID="btnPlusOne" runat="server" SkinID="BlogDetail" EnableViewState="false" />
            <quantifi:LinkedInShare ID="linkedInShare" runat="server" EnableViewState="false" />
            <portal:FacebookLikeButton ID="FacebookLikeButton1" runat="server" EnableViewState="false"
              Visible="false" />
          </div>
          <%-- Pager --%>
          <div class="blogpager">
            <asp:HyperLink ID="lnkPreviousPost" runat="server" Visible="false" CssClass="prevPost"
              EnableViewState="false"></asp:HyperLink>
            <asp:HyperLink ID="lnkNextPost" runat="server" Visible="false" CssClass="nextPost"
              EnableViewState="false"></asp:HyperLink>
          </div>
          <%-- comment service --%>
          <div class="blogcommentservice">
            <portal:IntenseDebateDiscussion ID="intenseDebate" runat="server" />
            <portal:DisqusWidget ID="disqus" runat="server" RenderPoweredBy="false" />
          </div>
          <%-- internal comment --%>
          <asp:Panel ID="pnlFeedback" runat="server">
            <fieldset>
              <legend>
                <mp:SiteLabel ID="lblFeedback" runat="server" ConfigKey="BlogFeedbackLabel" ResourceFile="QuantifiBlogResources"
                  EnableViewState="false"></mp:SiteLabel>
              </legend>
              <div class="blogcomments">
                <asp:Repeater ID="dlComments" runat="server" EnableViewState="true" OnItemCommand="dlComments_ItemCommand">
                  <ItemTemplate>
                    <h3 class="blogtitle">
                      <asp:ImageButton ID="btnDelete" runat="server" AlternateText="<%# Resources.QuantifiBlogResources.DeleteImageAltText %>"
                        ToolTip="<%# Resources.QuantifiBlogResources.DeleteImageAltText %>" ImageUrl='<%# DeleteLinkImage %>'
                        CommandName="DeleteComment" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"BlogCommentID")%>'
                        Visible="<%# IsEditable%>" />
                      <asp:Literal ID="litTitle" runat="server" EnableViewState="false" Text='<%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem,"Title").ToString()) %>' />
                    </h3>
                    <div>
                      <asp:Label ID="Label2" Visible="True" runat="server" EnableViewState="false" CssClass="blogdate"
                        Text='<%# FormatCommentDate(Convert.ToDateTime(Eval("DateCreated"))) %>' />
                      <asp:Label ID="Label3" runat="server" EnableViewState="false" Visible='<%# (bool) (DataBinder.Eval(Container.DataItem, "URL").ToString().Length == 0) %>'
                        CssClass="blogcommentposter">
					        <%#  Server.HtmlEncode(DataBinder.Eval(Container.DataItem,"Name").ToString()) %>
                      </asp:Label>
                      <asp:HyperLink ID="Hyperlink2" runat="server" EnableViewState="false" Visible='<%# (bool) (DataBinder.Eval(Container.DataItem, "URL").ToString().Length != 0) %>'
                        Text='<%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem,"Name").ToString()) %>'
                        NavigateUrl='<%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem,"URL").ToString())%>'
                        CssClass="blogcommentposter">
                      </asp:HyperLink>
                    </div>
                    <div class="blogcommenttext">
                      <NeatHtml:UntrustedContent ID="UntrustedContent1" runat="server" EnableViewState="false"
                        TrustedImageUrlPattern='<%# RegexRelativeImageUrlPatern %>' ClientScriptUrl="~/ClientScript/NeatHtml.js">
                        <asp:Literal ID="litComment" runat="server" EnableViewState="false" Text='<%# DataBinder.Eval(Container.DataItem, "Comment").ToString() %>' />
                      </NeatHtml:UntrustedContent>
                      <br />
                    </div>
                  </ItemTemplate>
                </asp:Repeater>
                <asp:Panel ID="pnlNewComment" runat="server">
                  <div class="settingrow">
                    <mp:SiteLabel ID="lblCommentTitle" runat="server" ForControl="txtCommentTitle" CssClass="settinglabel"
                      ConfigKey="BlogCommentTitleLabel" ResourceFile="QuantifiBlogResources" EnableViewState="false">
                    </mp:SiteLabel>
                    <asp:TextBox ID="txtCommentTitle" runat="server" Width="300" MaxLength="100" EnableViewState="false"
                      CssClass="forminput"></asp:TextBox>
                  </div>
                  <div class="settingrow">
                    <mp:SiteLabel ID="lblCommentUserName" runat="server" ForControl="txtName" CssClass="settinglabel"
                      ConfigKey="BlogCommentUserNameLabel" ResourceFile="QuantifiBlogResources" EnableViewState="false">
                    </mp:SiteLabel>
                    <asp:TextBox ID="txtName" runat="server" Width="300" MaxLength="100" EnableViewState="false"
                      CssClass="forminput"></asp:TextBox>
                  </div>
                  <div id="divCommentUrl" runat="server" class="settingrow">
                    <mp:SiteLabel ID="lblCommentURL" runat="server" ForControl="txtURL" CssClass="settinglabel"
                      ConfigKey="BlogCommentUrlLabel" ResourceFile="QuantifiBlogResources" EnableViewState="false">
                    </mp:SiteLabel>
                    <asp:TextBox ID="txtURL" runat="server" Width="300" MaxLength="200" EnableViewState="false"
                      CssClass="forminput"></asp:TextBox>
                  </div>
                  <div class="settingrow">
                    <mp:SiteLabel ID="lblRememberMe" runat="server" ForControl="chkRememberMe" CssClass="settinglabel"
                      ConfigKey="BlogCommentRemeberMeLabel" ResourceFile="QuantifiBlogResources" EnableViewState="false">
                    </mp:SiteLabel>
                    <asp:CheckBox ID="chkRememberMe" runat="server" EnableViewState="false" CssClass="forminput">
                    </asp:CheckBox>
                  </div>
                  <div class="settingrow">
                    <mp:SiteLabel ID="SiteLabel1" runat="server" CssClass="settinglabel" ConfigKey="BlogCommentCommentLabel"
                      ResourceFile="QuantifiBlogResources" EnableViewState="false"></mp:SiteLabel>
                  </div>
                  <div class="settingrow">
                    <mpe:EditorControl ID="edComment" runat="server">
                    </mpe:EditorControl>
                  </div>
                  <asp:Panel ID="pnlAntiSpam" runat="server">
                    <mp:CaptchaControl ID="captcha" runat="server" />
                  </asp:Panel>
                  <div class="settingrow">
                    <asp:ValidationSummary ID="vSummary" runat="server" />
                    <asp:RegularExpressionValidator ID="regexUrl" runat="server" ControlToValidate="txtURL"
                      Display="Dynamic" ValidationExpression="(((http(s?))\://){1}\S+)"></asp:RegularExpressionValidator>
                  </div>
                  <div class="modulebuttonrow">
                    <portal:mojoButton ID="btnPostComment" runat="server" Text="Submit" />
                  </div>
                </asp:Panel>
                <asp:Panel ID="pnlCommentsClosed" runat="server" EnableViewState="false">
                  <asp:Literal ID="litCommentsClosed" runat="server" EnableViewState="false" />
                </asp:Panel>
                <asp:Panel ID="pnlCommentsRequireAuthentication" runat="server" Visible="false" EnableViewState="false">
                  <asp:Literal ID="litCommentsRequireAuthentication" runat="server" EnableViewState="false" />
                </asp:Panel>
              </div>
            </fieldset>
          </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlExcerpt" runat="server" Visible="false">
          <div class="blogtext">
            <asp:Literal ID="litExcerpt" runat="server" EnableViewState="false" />
          </div>
          <mp:SiteLabel ID="SiteLabel2" runat="server" CssClass="settinglabel" ConfigKey="MustSignInToViewFullPost"
            ResourceFile="QuantifiBlogResources" EnableViewState="false"></mp:SiteLabel>
        </asp:Panel>
      </asp:Panel>
    </div>
  </portal:mojoPanel>
  <div class="cleared" ></div >
</asp:Panel>

 
<script type="text/javascript">
  $(document).ready(function () {
//    $('.blogtitle').css("font-size", "10px");
//    $('.blogtitle').jFitText(1, { minFontSize: '10px', maxFontSize: '20px' });
    Q.C.TagsManager(".blogview");
  });

</script>
