﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PostList.ascx.cs" Inherits="Quantifi.UI.Blogs.PostList"
  ViewStateMode="Disabled" %>
<%@ Register TagPrefix="quantifi" Namespace="Quantifi.Shared.Controls" Assembly="Quantifi.Shared" %>
<asp:Panel ID="divblog" runat="server" CssClass="blog-container" EnableTheming="false">
  <%-- Normal Blog View --%>
  <quantifi:ExRepeater ID="rptBlogs" runat="server" EnableViewState="False">
    <EmptyTemplate>
    </EmptyTemplate>
    <HeaderTemplate>
      <div class="blogRoll">
    </HeaderTemplate>
    <ItemTemplate>
      <div class="blogitem">
        <h3 class="blogtitle">
          <asp:HyperLink ID="lnkTitle" runat="server" EnableViewState="false" NavigateUrl='<%# FormatBlogTitleUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID")))  %>'
            SkinID="BlogTitle" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'
            Visible="<%# Config.UseLinkForHeading %>">
          </asp:HyperLink>
          <asp:Literal ID="litTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'
            Visible="<%#(!Config.UseLinkForHeading) %>" />
          &nbsp;
          <asp:HyperLink ID="editLink" runat="server" CssClass="ModuleEditLink" EnableViewState="false"
            ImageUrl="<%# EditLinkImageUrl %>" NavigateUrl='<%# this.SiteRoot + "/Quantifi/Controls/BlogsModule/EditPost.aspx?pageid=" + PageId.ToString() + "&amp;ItemID=" + DataBinder.Eval(Container.DataItem,"ItemID") + "&amp;mid=" + ModuleId.ToString() %>'
            Text="<%# EditLinkText %>" ToolTip="<%# EditLinkTooltip %>" Visible="<%# IsEditable %>" />
        </h3>
        <%-- BLOG DATE --%>
        <%#  FormatBlogDate(Convert.ToDateTime(Eval("StartDate")))    %>
        <%-- BLOG POST --%>
        <asp:Panel ID="pnlPost" runat="server" CssClass="postlistcontent" Visible="<%# !Config.TitleOnly %>">
          <div class="blogtext">
            <%# FormatBlogEntry(DataBinder.Eval(Container.DataItem, "Description").ToString(), DataBinder.Eval(Container.DataItem, "Excerpt").ToString(), DataBinder.Eval(Container.DataItem, "ItemUrl").ToString(), Convert.ToInt32(Eval("ItemID")))%>
          </div>
          <quantifi:ExRepeater ID="rptTags" runat="server" ViewStateMode="Disabled" DataSource='<%#DataBinder.Eval(Container.DataItem, "Tags") %>'>
            <EmptyTemplate>
              <!-- No Tags -->
            </EmptyTemplate>
            <HeaderTemplate>
              <div class="tags tags-hidden">
                <span class="tags-toggle" title="Show Tags">Tags:</span>
            </HeaderTemplate>
            <ItemTemplate>
              <a class="taglink " href='<%= SiteRoot %>/tags.aspx?tag=<%# Server.UrlEncode( Eval("Text").ToString()) %>&amp;tid=<%# Server.UrlEncode( Eval("Id").ToString()) %>'
                title='<%# string.Format("View {0} tagged content", Server.HtmlEncode( Eval("Text").ToString()))%>'>
                <%# Server.HtmlEncode(Eval("Text").ToString())%></a>
            </ItemTemplate>
            <FooterTemplate>
              </div>
            </FooterTemplate>
          </quantifi:ExRepeater>
        </asp:Panel>
      </div>
    </ItemTemplate>
    <FooterTemplate>
      </div>
    </FooterTemplate>
  </quantifi:ExRepeater>
  <%-- Archive Blog View --%>
  <quantifi:ExRepeater ID="rptArchive" runat="server" EnableViewState="False">
    <EmptyTemplate>
    </EmptyTemplate>
    <HeaderTemplate>
      <div class="blogArchive">
    </HeaderTemplate>
    <ItemTemplate>
      <div class="blogitem">
        <h3 class="blogtitle">
          <asp:HyperLink ID="lnkTitle" runat="server" EnableViewState="false" NavigateUrl='<%# FormatBlogTitleUrl(DataBinder.Eval(Container.DataItem,"ItemUrl").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"ItemID")))  %>'
            SkinID="BlogTitle" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'
            Visible="<%# Config.UseLinkForHeading %>">
          </asp:HyperLink>
          <asp:Literal ID="litTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Heading") %>'
            Visible="<%#(!Config.UseLinkForHeading) %>" />
          &nbsp;
          <asp:HyperLink ID="editLink" runat="server" CssClass="ModuleEditLink" EnableViewState="false"
            ImageUrl="<%# EditLinkImageUrl %>" NavigateUrl='<%# this.SiteRoot + "/Quantifi/Controls/BlogsModule/EditPost.aspx?pageid=" + PageId.ToString() + "&amp;ItemID=" + DataBinder.Eval(Container.DataItem,"ItemID") + "&amp;mid=" + ModuleId.ToString() %>'
            Text="<%# EditLinkText %>" ToolTip="<%# EditLinkTooltip %>" Visible="<%# IsEditable %>" />
        </h3>
        <%-- BLOG DATE --%>
        <%# FormatBlogDate(Convert.ToDateTime(Eval("StartDate"))) %>
      </div>
    </ItemTemplate>
    <FooterTemplate>
      </div>
    </FooterTemplate>
  </quantifi:ExRepeater>
  <%-- Pager --%>
  <div class="pager">
    <portal:mojoCutePager ID="pgr" runat="server" />
  </div>
  <div class="copyright">
    <asp:Label ID="lblCopyright" runat="server" />
  </div>
  <script type="text/javascript">
    $(document).ready(function () {

      Q.C.TagsManager(".blog-container");

    });
  </script>
</asp:Panel>
