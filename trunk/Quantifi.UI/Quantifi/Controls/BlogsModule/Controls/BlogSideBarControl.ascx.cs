﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.UI.Blogs;
using mojoPortal.Web;
using mojoPortal.Web.Controls;
using System.Globalization;
using mojoPortal.Web.Framework;
using mojoPortal.Business;

namespace Quantifi.UI.Blogs.Controls
{
	public partial class BlogSideBarControl : System.Web.UI.UserControl
	{

		#region Properties

		protected int pageId = -1;

		protected int moduleId = -1;
		private bool userCanEdit = false;
		private int countOfDrafts = 0;
		private int pageNumber = 1;
		private Module blogModule = null;



		public BlogConfiguration BlogConfiguration { get; set; }
		public DateTime CalendarDate { get; set; }
		public int ModuleId { get { return moduleId; } set { moduleId = value; } }


		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			LoadParams();
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (BlogConfiguration != null)
				PopulateNavigation(BlogConfiguration);
		}

		protected virtual void PopulateNavigation(BlogConfiguration config)
		{

			//this.CategoryArchive.ModuleId = this.ModuleId;
			//this.CategoryArchive.CategoryIds = config.VisibleSidebarCategoryIds;


			if (config.ShowCalendar)
			{
				this.pnlCalendar.Visible = true;
				this.calBlogNav.Visible = true;
				try
				{
					calBlogNav.FirstDayOfWeek = (FirstDayOfWeek)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
				}
				catch (ArgumentNullException) { }
				catch (ArgumentOutOfRangeException) { }
				catch (InvalidOperationException) { }
				catch (InvalidCastException) { }

				if (!Page.IsPostBack)
				{
					this.calBlogNav.SelectedDate = CalendarDate;
					this.calBlogNav.VisibleDate = CalendarDate;

				}
			}
			else
			{
				this.pnlCalendar.Visible = false;
				this.calBlogNav.Visible = false;
			}

			if (config.ShowFeedLinks)
			{
				Feeds.Config = config;
				Feeds.PageId = pageId;
				Feeds.ModuleId = moduleId;
				Feeds.Visible = true;
				pnlFeeds.Visible = true;
			}
			else
			{
				Feeds.Visible = false;
				pnlFeeds.Visible = false;
			}


			var page = this.Page as mojoBasePage;

			if (config.ShowCategories)
			{
				tags.CanEdit = userCanEdit;
				tags.PageId = pageId;
				tags.ModuleId = moduleId;
				tags.SiteRoot = page.SiteRoot;
				tags.RenderAsTagCloud = config.UseTagCloudForCategories;
				this.pnlCategories.Visible = true;
			}
			else
			{
				tags.Visible = false;
				this.pnlCategories.Visible = false;
			}

			if (config.ShowArchives)
			{
				archive.PageId = pageId;
				archive.ModuleId = moduleId;
				archive.SiteRoot = page.SiteRoot;
			}
			else
			{
				archive.Visible = false;
				this.pnlArchives.Visible = false;
			}

			if (config.ShowStatistics)
			{
				stats.PageId = pageId;
				stats.ModuleId = moduleId;
				stats.CountOfDrafts = countOfDrafts;
				stats.Visible = true;
				pnlStatistics.Visible = true;
			}
			else
			{
				stats.Visible = false;
				pnlStatistics.Visible = false;
			}


		}

		private void calBlogNav_SelectionChanged(object sender, EventArgs e)
		{
			System.Web.UI.WebControls.Calendar cal = (System.Web.UI.WebControls.Calendar)sender;

			CalendarDate = cal.SelectedDate;
			//calBlogNav.VisibleDate = CalendarDate;
			//calBlogNav.SelectedDate = CalendarDate;
			//PopulateControls();

			RedirectFromCalendar();

		}

		private void CalBlogNavVisibleMonthChanged(object sender, MonthChangedEventArgs e)
		{
			CalendarDate = e.NewDate;
			//calBlogNav.VisibleDate = CalendarDate;
			//calBlogNav.SelectedDate = CalendarDate;
			//PopulateControls();

			RedirectFromCalendar();

		}

		private void RedirectFromCalendar()
		{
			var page = this.Page as mojoBasePage;

			string pageUrl = page.SiteRoot + "/Quantifi/Controls/BlogsModule/ViewList.aspx"
							 + "?pageid=" + pageId.ToInvariantString()
							 + "&mid=" + moduleId.ToInvariantString()
							 + "&blogdate=" + CalendarDate.Date.ToString("s");

			WebUtils.SetupRedirect(this, pageUrl);

		}

		private void LoadParams()
		{
			pageId = WebUtils.ParseInt32FromQueryString("pageid", pageId);

			moduleId = WebUtils.ParseInt32FromQueryString("mid", moduleId);

			pageNumber = WebUtils.ParseInt32FromQueryString("pagenumber", pageNumber);

			blogModule = new Module(moduleId);
		}


	}
}