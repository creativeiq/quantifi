﻿// Author:				Joe Audette
// Created:			    2004-08-14
// Last Modified:	    2010-05-23
// 
// The use and distribution terms for this software are covered by the 
// Common Public License 1.0 (http://opensource.org/licenses/cpl.php)
// which can be found in the file CPL.TXT at the root of this distribution.
// By using this software in any fashion, you are agreeing to be bound by 
// the terms of this license.
//
// You must not remove this notice, or any other, from this software.

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;
using mojoPortal.Web.UI;
using Resources;
using mojoPortal.Web;
using QB = Quantifi.Business;

namespace Quantifi.UI.Blogs
{

  public partial class ArchiveViewControl : UserControl
  {
		#region Properties

		protected int PageId = -1;
		protected int ModuleId = 0;
		protected int ItemId = 0;
		protected int Month = -1;
		protected int Year = DateTime.UtcNow.Year;
		private int countOfDrafts = 0;
		private Hashtable moduleSettings;
		private mojoBasePage basePage;
		private Module blogModule = null;
		protected string FeedBackLabel = ConfigurationManager.AppSettings["BlogFeedbackLabel"];
		protected Double TimeOffset = 0;
		private TimeZoneInfo timeZone = null;
		private string DisqusSiteShortName = string.Empty;
		protected string SiteRoot = string.Empty;
		protected string ImageSiteRoot = string.Empty;
		private SiteSettings siteSettings = null;
		protected BlogConfiguration config = new BlogConfiguration();

		#endregion

    #region OnInit
    override protected void OnInit(EventArgs e)
    {
      this.Load += new EventHandler(this.Page_Load);
      base.OnInit(e);

      basePage = Page as mojoBasePage;
      SiteRoot = basePage.SiteRoot;
      ImageSiteRoot = basePage.ImageSiteRoot;
    }
    #endregion

    private void Page_Load(object sender, EventArgs e)
    {
      LoadParams();

      if (
          (basePage == null)
          || (!basePage.UserCanViewPage(ModuleId))
      )
      {
        SiteUtils.RedirectToAccessDeniedPage();
        return;
      }

      SetupCss();
      AddConnoicalUrl();
      LoadSettings();


      PopulateLabels();

      if (!IsPostBack)
      {
        IDataReader reader;
        if ((Month > -1) && (Year > -1))
        {
          //DateTime selectedMonth = DateTime.Now;
          DateTime selectedMonth = new DateTime(Year, Month, 1, CultureInfo.CurrentCulture.Calendar);
          try
          {
            // selectedMonth = new DateTime(year, month, 1);
            selectedMonth = new DateTime(Year, Month, 1, CultureInfo.CurrentCulture.Calendar);
          }
          catch (Exception)
          { }

          this.litHeader.Text = Page.Server.HtmlEncode(QuantifiBlogResources.BlogArchivesPrefixLabel
              + selectedMonth.ToString("MMMM, yyyy"));

          if (blogModule != null)
          {
            basePage.Title = SiteUtils.FormatPageTitle(basePage.SiteInfo, blogModule.ModuleTitle + " - " + QuantifiBlogResources.BlogArchivesPrefixLabel
              + selectedMonth.ToString("MMMM, yyyy"));

            basePage.MetaDescription = string.Format(CultureInfo.InvariantCulture,
                QuantifiBlogResources.ArchiveMetaDescriptionFormat,
                blogModule.ModuleTitle,
                selectedMonth.ToString("MMMM, yyyy"));


          }


          reader = QB.Blog.GetBlogEntriesByMonth(Month, Year, ModuleId);
        }
        else
        {
          reader = QB.Blog.GetBlogs(ModuleId, DateTime.UtcNow);
        }
        try
        {
          dlArchives.DataSource = reader;
          dlArchives.DataBind();
        }
        finally
        {
          reader.Close();
        }
 

      }

      basePage.LoadSideContent(config.ShowLeftContent, config.ShowRightContent);

    }


    protected string FormatBlogUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + PageId.ToInvariantString()
          + "&ItemID=" + itemId.ToInvariantString()
          + "&mid=" + ModuleId.ToInvariantString();

    }



    private void LoadSettings()
    {
      siteSettings = CacheHelper.GetCurrentSiteSettings();
      moduleSettings = ModuleSettings.GetModuleSettings(ModuleId);
      config = new BlogConfiguration(moduleSettings);

      if (config.DisqusSiteShortName.Length > 0)
      {
        DisqusSiteShortName = config.DisqusSiteShortName;
      }
      else
      {
        DisqusSiteShortName = siteSettings.DisqusSiteShortName;
      }

      countOfDrafts = QB.Blog.CountOfDrafts(ModuleId);

    }

    protected string FormatBlogDate(DateTime startDate)
    {
      if (timeZone != null)
      {
        return TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone).ToString(config.DateTimeFormat);

      }

      return startDate.AddHours(TimeOffset).ToString(config.DateTimeFormat);

    }


    private void PopulateLabels()
    {

      FeedBackLabel = QuantifiBlogResources.BlogFeedbackLabel;

    }


    private void SetupCss()
    {

      // older skins have this
      StyleSheet stylesheet = (StyleSheet)Page.Master.FindControl("StyleSheet");
      if (stylesheet != null)
      {
        if (stylesheet.FindControl("blogcss") == null)
        {
          Literal cssLink = new Literal();
          cssLink.ID = "blogcss";
          cssLink.Text = "\n<link href='"
          + SiteUtils.GetSkinBaseUrl()
          + "blogmodule.css' type='text/css' rel='stylesheet' media='screen' />";

          stylesheet.Controls.Add(cssLink);
        }
      }

    }

    private void AddConnoicalUrl()
    {
      if (Page.Header == null) { return; }

      Literal link = new Literal();
      link.ID = "blogarchiveurl";
      link.Text = "\n<link rel='canonical' href='"
          + SiteRoot
          + "/Quantifi/Controls/BlogsModule/ViewArchive.aspx?month="
          + Month.ToInvariantString()
          + "&amp;year=" + Year.ToInvariantString()
          + "&amp;mid=" + ModuleId.ToInvariantString()
          + "&amp;pageid=" + PageId.ToInvariantString()
          + "' />";

      Page.Header.Controls.Add(link);

    }

    private void LoadParams()
    {
      TimeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();
      PageId = WebUtils.ParseInt32FromQueryString("pageid", -1);
      ModuleId = WebUtils.ParseInt32FromQueryString("mid", -1);
      ItemId = WebUtils.ParseInt32FromQueryString("ItemID", -1);
      Month = WebUtils.ParseInt32FromQueryString("month", Month);
      Year = WebUtils.ParseInt32FromQueryString("year", Year);

      // don't let the archive be used to see unpublished future posts
      try
      {
        //This line commentted by Asad Samarian 2009-01-08
        //DateTime d = new DateTime(year, month, 1,ResourceHelper.GetDefaultCulture().Calendar);
        //This line added by Asad Samarian 2009-01-08
        DateTime d = new DateTime(Year, Month, 1, CultureInfo.CurrentCulture.Calendar);

        if (d > DateTime.UtcNow)
        {
          Month = DateTime.UtcNow.Month;
          Year = DateTime.UtcNow.Year;

        }
      }
      catch (ArgumentOutOfRangeException)
      {
        Month = DateTime.UtcNow.Month;
        Year = DateTime.UtcNow.Year;
      }

      blogModule = basePage.GetModule(ModuleId);

    }

  }
}
