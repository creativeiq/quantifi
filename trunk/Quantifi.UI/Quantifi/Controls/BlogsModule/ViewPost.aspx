<%@ Page Language="c#" CodeBehind="ViewPost.aspx.cs" MasterPageFile="~/App_MasterPages/layout.Master"   EnableViewState="false"
	AutoEventWireup="false" Inherits="Quantifi.UI.Blogs.BlogView" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="blog" TagName="BlogView" Src="~/Quantifi/Controls/BlogsModule/Controls/BlogViewControl.ascx" %>


<asp:Content ContentPlaceHolderID="leftContent" ID="MPLeftPane" runat="server">
</asp:Content>



<asp:Content ContentPlaceHolderID="mainContent" ID="MPContent" runat="server">
	<portal:ModulePanel ID="pnlContainer" runat="server">
		<portal:mojoPanel ID="mp1" runat="server" ArtisteerCssClass="art-Post" RenderArtisteerBlockContentDivs="true">
			<mp:CornerRounderTop ID="ctop1" runat="server" />
			<blog:BlogView id="BlogView1" runat="server" />
			<mp:CornerRounderBottom ID="cbottom1" runat="server" />
		</portal:mojoPanel>
	</portal:ModulePanel>


  <script type="text/javascript">


    $().ready(function () {
      $('#divRight').jScroll({ top: 20, speed: 2000 });
    });

  </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="rightContent" ID="MPRightPane" runat="server">
</asp:Content>


<asp:Content ContentPlaceHolderID="pageEditContent" ID="MPPageEdit" runat="server" />
