using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Quantifi.UI.Blogs;
using mojoPortal.Business;
using mojoPortal.Web;
using System.Data;
using QB = Quantifi.Business;
using mojoPortal.Web.Framework;
using mojoPortal.Web.UI;
using System.Globalization;
using System.Text;

namespace Quantifi.UI.Blogs
{
	public partial class BlogArchiveView : mojoBasePage
	{


		//#region OnInit

		//protected override void OnPreInit(EventArgs e)
		//{
		//  AllowSkinOverride = true;
		//  base.OnPreInit(e);
		//}

		//override protected void OnInit(EventArgs e)
		//{
		//  this.Load += new EventHandler(this.Page_Load);
		//  base.OnInit(e);
		//}
		//#endregion

		//private int moduleId = -1;

		//private Hashtable moduleSettings;
		//private mojoBasePage basePage;
		//private BlogConfiguration config;

		//private void Page_Load(object sender, EventArgs e)
		//{
		//  LoadSettings();

		//  pnlContainer.ModuleId = moduleId;

		//}

		//protected override void OnPreRender(EventArgs e)
		//{
		//  base.ForceSideContent(config.ShowLeftContent, config.ShowRightContent);
		//  base.OnPreRender(e);
		//}


		//private void LoadSettings()
		//{
		//  moduleId = WebUtils.ParseInt32FromQueryString("mid", -1);

		//  if (moduleId == -1)
		//    return;

		//  moduleSettings = ModuleSettings.GetModuleSettings(moduleId);

		//  config = new BlogConfiguration(moduleSettings);

		//  //sideBar.BlogConfiguration = config;
		//}


		#region Properties

		protected int pageId = -1;
		protected int moduleId = -1;

		protected int year = DateTime.Now.Year;
		private int pageNumber = 1;
		private int totalPages = 1;
		private int pageSize = 15;

		protected string category = string.Empty;

		private Hashtable moduleSettings;
		protected BlogConfiguration config = new BlogConfiguration();
		protected Double timeOffset = 0;
		protected TimeZoneInfo timeZone = null;
		private Module blogModule = null;
		private string DisqusSiteShortName = string.Empty;

		#endregion

		#region OnInit

		protected override void OnPreInit(EventArgs e)
		{
			AllowSkinOverride = true;
			base.OnPreInit(e);
		}

		override protected void OnInit(EventArgs e)
		{
			this.Load += new EventHandler(this.Page_Load);
			base.OnInit(e);
		}

		#endregion

		private void Page_Load(object sender, EventArgs e)
		{
			LoadParams();

			if (!UserCanViewPage(moduleId))
			{
				SiteUtils.RedirectToAccessDeniedPage();
				return;
			}

			LoadSettings();
			SetupCss();

			if (!IsPostBack)
			{
				LoadData();
			}

			PopulateLabels();
			AddConnoicalUrl();
			AddRssUrl();

			LoadSideContent(config.ShowLeftContent, config.ShowRightContent);
			base.ForceSideContent(true, null);
		}

		private void PopulateLabels()
		{


			this.litTitle.Text = string.Format("{0} in {1}", this.blogModule.ModuleTitle, year);

			if (pageNumber > 1)
			{
				this.Page.Title = this.litTitle.Text + string.Format(" - Page {0} - Quantifi", pageNumber);
			}
			else
			{
				this.Page.Title = this.litTitle.Text + " - Quantifi";
			}

		}

		private void LoadSettings()
		{
			moduleSettings = ModuleSettings.GetModuleSettings(moduleId);
			config = new BlogConfiguration(moduleSettings);

			if (config.DisqusSiteShortName.Length > 0)
			{
				DisqusSiteShortName = config.DisqusSiteShortName;
			}
			else
			{
				DisqusSiteShortName = siteSettings.DisqusSiteShortName;
			}


			pageSize = (year == DateTime.Today.Year) ? config.PageSize : config.ArchivesPageSize;


		}

		private void SetupCss()
		{
			// older skins have this
			StyleSheet stylesheet = (StyleSheet)Page.Master.FindControl("StyleSheet");
			if (stylesheet != null)
			{
				if (stylesheet.FindControl("blogcss") == null)
				{
					Literal cssLink = new Literal();
					cssLink.ID = "blogcss";
					cssLink.Text = "\n<link href='"
					+ SiteUtils.GetSkinBaseUrl()
					+ "blogmodule.css' type='text/css' rel='stylesheet' media='screen' />";

					stylesheet.Controls.Add(cssLink);
				}
			}

		}

		private void LoadData()
		{

			if ((moduleId > -1))
			{


				var items = QB.BlogCacheUtil.GetArchivePage(moduleId, year, pageNumber, pageSize, out totalPages);

				if (year == DateTime.Today.Year)
				{
					rptPostList.DataSource = items;
					rptPostList.DataBind();
				}
				else
				{
					rptArchiveItems.DataSource = items;
					rptArchiveItems.DataBind();
				}


				string pageUrlFormat = SiteRoot
																+ "/Quantifi/Controls/BlogsModule/ViewArchive.aspx?year=" + year.ToInvariantString()
																+ "&amp;mid=" + moduleId.ToInvariantString()
																+ "&amp;pageid=" + pageId.ToInvariantString()
																+ "&amp;pagenumber={0}";

				pgr.PageURLFormat = pageUrlFormat;
				pgr.ShowFirstLast = true;
				pgr.PageSize = pageSize;
				pgr.PageCount = totalPages;
				pgr.CurrentIndex = pageNumber;
				pgr.Visible = (totalPages > 1) && config.ShowPager;

				if (year != DateTime.Today.Year)
					pgr.CssClass = "blogpager";



			}

		}

		private void LoadParams()
		{
			timeOffset = SiteUtils.GetUserTimeOffset();
			timeZone = SiteUtils.GetUserTimeZone();
			pageId = WebUtils.ParseInt32FromQueryString("pageid", pageId);
			moduleId = WebUtils.ParseInt32FromQueryString("mid", moduleId);
			pageNumber = WebUtils.ParseInt32FromQueryString("pagenumber", pageNumber);

			year = WebUtils.ParseInt32FromQueryString("year", year);

			//pnlContainer.ModuleId = moduleId;
			blogModule = GetModule(moduleId);
		}

		private void AddConnoicalUrl()
		{
			if (Page.Header == null) { return; }

			Literal link = new Literal();
			link.ID = "blogcaturl";
			link.Text = "\n<link rel='canonical' href='"
					+ SiteRoot
					+ "/Quantifi/Controls/BlogsModule/ViewArchive.aspx?year=" + year.ToInvariantString()
					+ "&amp;mid=" + moduleId.ToInvariantString()
					+ "&amp;pageid=" + pageId.ToInvariantString()
					+ "' />";




			Page.Header.Controls.Add(link);

		}

		private void AddRssUrl()
		{
			if (Page.Header == null) { return; }


			Literal link = new Literal();
			link.ID = "blogrssurl";
			link.Text = "\n<link rel='alternate' type='application/rss+xml' title='" + blogModule.ModuleTitle
				+ "' href='" + GetRssUrl()
				+ "' />";

			Page.Header.Controls.Add(link);

		}

		private string GetRssUrl()
		{
			if (config.FeedburnerFeedUrl.Length > 0)
				return config.FeedburnerFeedUrl;

			return SiteRoot + "/blog" + moduleId.ToString(CultureInfo.InvariantCulture) + "rss.aspx";

		}

		protected string GetBlogEntryHtml(string cssClass, string blogHtml, string excerpt, string url, int itemId)
		{

			var MoreLink = " <a href='" + FormatBlogUrl(url, itemId) + "' class='blogMoreLink' >" + config.MoreLinkText + "</a>";


			if (config.UseExcerpt)
			{
				if ((excerpt.Length > 0) && (excerpt != "<p>&#160;</p>"))
				{
					if (excerpt.EndsWith("</p>"))
					{
						return excerpt.Substring(0, excerpt.Length - 4) + config.ExcerptSuffix + MoreLink + "</p>";
					}
					else if (excerpt.EndsWith("</div>"))
					{
						return excerpt.Substring(0, excerpt.Length - 6) + config.ExcerptSuffix + MoreLink + "</div>";
					}
					else
					{

						return excerpt + config.ExcerptSuffix + MoreLink;

					}
				}

				string result = string.Empty;
				if ((blogHtml.Length > config.ExcerptLength) && (config.MoreLinkText.Length > 0))
				{

					result = UIHelper.CreateExcerpt(blogHtml, config.ExcerptLength, config.ExcerptSuffix);
					result += MoreLink;
					return result;
				}

			}

			return blogHtml;
		}

		protected string FormatBlogEntry(string blogHtml, string excerpt, string url, int itemId)
		{

			var MoreLink = " <a href='" + FormatBlogUrl(url, itemId) + "' class='blogMoreLink' >" + config.MoreLinkText + "</a>";


			if (config.UseExcerpt)
			{
				if ((excerpt.Length > 0) && (excerpt != "<p>&#160;</p>"))
				{
					if (excerpt.EndsWith("</p>"))
					{
						return excerpt.Substring(0, excerpt.Length - 4) + config.ExcerptSuffix + MoreLink + "</p>";
					}
					else if (excerpt.EndsWith("</div>"))
					{
						return excerpt.Substring(0, excerpt.Length - 6) + config.ExcerptSuffix + MoreLink + "</div>";
					}
					else
					{

						return excerpt + config.ExcerptSuffix + MoreLink;

					}
				}

				string result = string.Empty;
				if ((blogHtml.Length > config.ExcerptLength) && (config.MoreLinkText.Length > 0))
				{

					result = UIHelper.CreateExcerpt(blogHtml, config.ExcerptLength, config.ExcerptSuffix);
					result += MoreLink;
					return result;
				}

			}

			return blogHtml;
		}
		
		protected string GetEditLink(int itemId, string cssClass)
		{
			if (!Request.IsAuthenticated || !UserCanEditModule(moduleId))
				return string.Empty;



			var url = "/Quantifi/Controls/BlogsModule/EditPost.aspx?pageid=" + this.pageId.ToInvariantString() + "&amp;ItemID=" + itemId.ToInvariantString() + "&amp;mid=" + this.moduleId.ToInvariantString();

			return "<a class=\"" + cssClass + "\" href=\"" + url + "\"    >Edit</a>";
		}

		protected string FormatBlogUrl(string itemUrl, int itemId)
		{
			if (itemUrl.Length > 0)
				return SiteRoot + itemUrl.Replace("~", string.Empty);

			return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + pageId.ToInvariantString()
					+ "&ItemID=" + itemId.ToInvariantString()
					+ "&mid=" + moduleId.ToInvariantString();

		}

		protected string FormatBlogDate(DateTime startDate)
		{
      if (!config.ShowTimeStampOnBlogArchive)
        return string.Empty;

			string dateFormat = "d";
			if (year == DateTime.Today.Year)
				dateFormat = config.DateTimeFormat;

			if (timeZone != null)
			{
				return TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone).ToString(dateFormat);
			}

			return startDate.AddHours(timeOffset).ToString(dateFormat);

		}
	
	}
}
