<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="BlogModule.ascx.cs"
  Inherits="Quantifi.UI.Blogs.BlogModule" %>
<%@ Register TagPrefix="blog" TagName="PostList" Src="~/Quantifi/Controls/BlogsModule/Controls/PostList.ascx" %>
<portal:ModulePanel ID="pnlContainer" runat="server">
  <portal:mojoPanel ID="mp1" runat="server">
    <asp:Panel ID="pnlWrapper" runat="server" CssClass="panelwrapper blogmodule">
      <%-- <portal:ModuleTitleControl ID="Title1" runat="server" /> --%>
      <h2 class="art-PostHeader moduletitle">
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
        <asp:Panel ID="pnlAdminLinks" runat="server" CssClass="adminWrapper " EnableViewState="false"
          Visible="false">
          <div class="adminContainer">
            <asp:HyperLink ID="linkSettings" runat="server" Visible="false" EnableViewState="false" ToolTip="Settings"
              CssClass="ModuleEditLink" NavigateUrl="~/Admin/ModuleSettings.aspx"><span class="edit-settings">Settings</span></asp:HyperLink>
            <asp:HyperLink ID="linkAddPosts" runat="server" Visible="false" EnableViewState="false" ToolTip="Add New Post"
              CssClass="ModuleEditLink" NavigateUrl="~/Quantifi/Controls/BlogsModule/EditPost.aspx"><span class="add-post">Add Post</span></asp:HyperLink>
            <asp:HyperLink ID="linkEditDrafts" runat="server" Visible="false" EnableViewState="false" ToolTip="Edit Drafts"
              CssClass="ModuleEditLink" NavigateUrl="~/Quantifi/Controls/BlogsModule/Drafts.aspx"><span class="edit-drafts">Edit Drafts</span></asp:HyperLink>
          </div>
        </asp:Panel>
      </h2>
      <portal:mojoPanel ID="MojoPanel1" runat="server">
        <blog:PostList ID="postList" runat="server" />
      </portal:mojoPanel>
      <div class="clear">
      </div>


    </asp:Panel>
  </portal:mojoPanel>
</portal:ModulePanel>
