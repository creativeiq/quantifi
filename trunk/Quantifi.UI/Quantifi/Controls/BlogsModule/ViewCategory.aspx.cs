// Author:				    Joe Audette
// Created:			        2005-06-05
// Last Modified:		    2010-05-13
// 
// The use and distribution terms for this software are covered by the 
// Common Public License 1.0 (http://opensource.org/licenses/cpl.php)
// which can be found in the file CPL.TXT at the root of this distribution.
// By using this software in any fashion, you are agreeing to be bound by 
// the terms of this license.
//
// You must not remove this notice, or any other, from this software.

using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Business;
using mojoPortal.Web.Framework;
using mojoPortal.Web.UI;
using Resources;
using mojoPortal.Web;

using QB = Quantifi.Business;

namespace Quantifi.UI.Blogs
{

  public partial class BlogCategoryView : mojoBasePage
  {
    #region Properties

    private int countOfDrafts = 0;
    protected string feedBackLabel;
    protected int pageId = -1;
    protected int moduleId = -1;
    protected int categoryId = -1;
    protected string category = string.Empty;
    private Hashtable moduleSettings;
    protected BlogConfiguration config = new BlogConfiguration();
    protected Double timeOffset = 0;
    protected TimeZoneInfo timeZone = null;
    private Module blogModule = null;
    private string DisqusSiteShortName = string.Empty;


    #endregion

    #region OnInit

    protected override void OnPreInit(EventArgs e)
    {
      AllowSkinOverride = true;
      base.OnPreInit(e);
    }

    override protected void OnInit(EventArgs e)
    {
      this.Load += new EventHandler(this.Page_Load);
      base.OnInit(e);
    }

    #endregion

    private void Page_Load(object sender, EventArgs e)
    {
      LoadParams();

      if (!UserCanViewPage(moduleId))
      {
        SiteUtils.RedirectToAccessDeniedPage();
        return;
      }

      LoadSettings();
      SetupCss();
      AddConnoicalUrl();
      PopulateLabels();


      if (!IsPostBack)
      {
        if ((moduleId > -1) && (categoryId > -1))
        {
          using (IDataReader reader = QB.Blog.GetCategory(categoryId))
          {
            if (reader.Read())
            {
              this.category = reader["Category"].ToString();
            }
          }

          this.litHeader.Text = Page.Server.HtmlEncode(QuantifiBlogResources.BlogCategoriesPrefixLabel + category);

          if (blogModule != null)
          {
            Title = SiteUtils.FormatPageTitle(siteSettings,
                blogModule.ModuleTitle + " - " + QuantifiBlogResources.BlogCategoriesPrefixLabel + category);

            MetaDescription = string.Format(CultureInfo.InvariantCulture,
                QuantifiBlogResources.CategoryMetaDescriptionFormat,
                blogModule.ModuleTitle, category);
          }

          using (IDataReader reader = QB.Blog.GetEntriesByCategory(moduleId, categoryId))
          {
            dlArchives.DataSource = reader;
            dlArchives.DataBind();
          }

        }
      }

      LoadSideContent(config.ShowLeftContent, config.ShowRightContent);

    }

    protected string FormatBlogUrl(string itemUrl, int itemId)
    {
      if (itemUrl.Length > 0)
        return SiteRoot + itemUrl.Replace("~", string.Empty);

      return SiteRoot + "/Quantifi/Controls/BlogsModule/ViewPost.aspx?pageid=" + pageId.ToInvariantString()
          + "&ItemID=" + itemId.ToInvariantString()
          + "&mid=" + moduleId.ToInvariantString();

    }

		protected override void OnPreRender(EventArgs e)
		{

			base.ForceSideContent(config.ShowLeftContent, config.ShowRightContent);

			base.OnPreRender(e);
		}

    private void PopulateLabels()
    {

      feedBackLabel = QuantifiBlogResources.BlogFeedbackLabel;

    }


		//private void PopulateNavigation()
		//{
		//  Feeds.Config = config;
		//  Feeds.PageId = pageId;
		//  Feeds.ModuleId = moduleId;
		//  Feeds.Visible = config.ShowFeedLinks;

		//  if (config.ShowCategories)
		//  {
		//    //tags.CanEdit = IsEditable;
		//    tags.PageId = pageId;
		//    tags.ModuleId = moduleId;
		//    tags.SiteRoot = SiteRoot;
		//    tags.RenderAsTagCloud = config.UseTagCloudForCategories;
		//  }
		//  else
		//  {
		//    tags.Visible = false;
		//    this.pnlCategories.Visible = false;
		//  }

		//  if (config.ShowArchives)
		//  {
		//    archive.PageId = pageId;
		//    archive.ModuleId = moduleId;
		//    archive.SiteRoot = SiteRoot;
		//  }
		//  else
		//  {
		//    archive.Visible = false;
		//    this.pnlArchives.Visible = false;
		//  }

		//  stats.PageId = pageId;
		//  stats.ModuleId = moduleId;
		//  stats.CountOfDrafts = countOfDrafts;
		//  stats.Visible = config.ShowStatistics;

		//}

    protected string FormatBlogDate(DateTime startDate)
    {
      if (timeZone != null)
      {
        return TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZone).ToString(config.DateTimeFormat);

      }

      return startDate.AddHours(timeOffset).ToString(config.DateTimeFormat);

    }

    private void LoadSettings()
    {
      moduleSettings = ModuleSettings.GetModuleSettings(moduleId);
      config = new BlogConfiguration(moduleSettings);

      if (config.DisqusSiteShortName.Length > 0)
      {
        DisqusSiteShortName = config.DisqusSiteShortName;
      }
      else
      {
        DisqusSiteShortName = siteSettings.DisqusSiteShortName;
      }


      countOfDrafts = QB.Blog.CountOfDrafts(moduleId);

			//sideBar.BlogConfiguration = config;
    }

    private void SetupCss()
    {
      // older skins have this
      StyleSheet stylesheet = (StyleSheet)Page.Master.FindControl("StyleSheet");
      if (stylesheet != null)
      {
        if (stylesheet.FindControl("blogcss") == null)
        {
          Literal cssLink = new Literal();
          cssLink.ID = "blogcss";
          cssLink.Text = "\n<link href='"
          + SiteUtils.GetSkinBaseUrl()
          + "blogmodule.css' type='text/css' rel='stylesheet' media='screen' />";

          stylesheet.Controls.Add(cssLink);
        }
      }

    }

    private void AddConnoicalUrl()
    {
      if (Page.Header == null) { return; }

      Literal link = new Literal();
      link.ID = "blogcaturl";
      link.Text = "\n<link rel='canonical' href='"
          + SiteRoot
          + "/Quantifi/Controls/BlogsModule/ViewCategory.aspx?cat="
          + categoryId.ToInvariantString()
          + "&amp;mid=" + moduleId.ToInvariantString()
          + "&amp;pageid=" + pageId.ToInvariantString()
          + "' />";

      Page.Header.Controls.Add(link);

    }

    private void LoadParams()
    {
      timeOffset = SiteUtils.GetUserTimeOffset();
      timeZone = SiteUtils.GetUserTimeZone();
      pageId = WebUtils.ParseInt32FromQueryString("pageid", pageId);
      moduleId = WebUtils.ParseInt32FromQueryString("mid", moduleId);
      categoryId = WebUtils.ParseInt32FromQueryString("cat", categoryId);
      pnlContainer.ModuleId = moduleId;
      blogModule = GetModule(moduleId);
    }

  }
}
