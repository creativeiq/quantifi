﻿/// <reference path="../../Data/scripts/quantifi.js" />
/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.js" />



Q.MC = (function ($, Q) {
  var my = {};

  my.ImgCache = new Object();


  my.isChanging = false;

  my.pageId = 0;
  my.cId = 0; // SET SERVER SIDE
  my.nId = 0; // Next
  my.pId = 0; // Prev

  my.timeout = 10000;
  my.data = new Object();
  my.uiClicked = false;
  my.intervalId = null;
  my.transSpeed = 1500;
  my.transCount = 0;
  my.transMaxCount = 20;

  // ELEMENTS
  my.Navigation = null;
  my.WebSiteWrap = null;
  my.WebSiteNav = null;
  my.LogoImg = null;
  my.DemoImg = null;
  my.MainLink = null;

  //  function privateMethod() {
  //    // ...
  //  }


  my.initControl = function (pageId, currentId, nextId, prevId) {


    my.pageId = pageId;
    my.cId = currentId;
    my.nId = nextId;
    my.pId = prevId;

    my.Navigation = $("#mcNav");
    my.WebSiteNav = $("#topnav");
    my.WebSiteWrap = $("#wrapwebsite");
    my.LogoImg = $("#qlogo");
    my.DemoImg = $("#qdemo");
    my.MainLink = $(".mainContent .mainContentLink");


    my.ImgCache.isLoading = true;
    my.ImgCache.mnBgImg = new Image();
    my.ImgCache.mainImg = new Image();
    my.ImgCache.logoImg = new Image();
    my.ImgCache.demoImg = new Image();


    $(my.ImgCache.mnBgImg).load(function () {
      Q.logInfo("Main BackgroundImage Loaded: " + my.ImgCache.mnBgImg.src);
      my.ImgCache.isLoading = false;
    });

    $(my.ImgCache.mnBgImg).error(function () {
      Q.logError("Main BackgroundImage Error Loading: " + my.ImgCache.mnBgImg.src);
      my.ImgCache.isLoading = false;
    });


    $(my.MainLink).click(function (e) {
      e.preventDefault();

      var campaignName = $(this).attr("gac");
      var url = $(this).attr("href");



      _gaq.push(['_trackEvent', "HP-MainContent", campaignName, url]);
      setTimeout('document.location = "' + url + '"', 100);
    });

    getData();
  }



  function getData() {
    jQuery.getJSON("/Quantifi/Controls/MainContentModule/MainContentHandler.ashx",
          {
            pageId: my.pageId,
            itemId: my.cId
          },
          function (returnData) {

            if (returnData == null || returnData.length == 0) {
              Q.logError("No Data");
              my.Navigation.hide();
              return;
            }

            if (returnData.length == 1) {
              my.Navigation.hide();
              return;
            }

            my.Navigation.show();

            my.data = returnData;

            my.transMaxCount = my.transMaxCount + (my.transMaxCount % returnData.length);

            InitLinks();

            try {
              if (my.data.length > 1) {
                var matched = false;
                var index = 0;
                for (var i = 0; i < my.data.length; i++) {
                  if (my.data[i].Id == my.cId) {
                    index = i;
                    matched = true;
                    break;
                  }
                }
                InitItem(my.data[1], my.timeout);
              }
            }
            catch (e) {
              Q.logError(e);
            }

          });
  }

  function InitLinks() {

    my.Navigation.find("a.prev").click(function (e) {
      if (my.isChanging == true)
        return;

      clearInterval(my.intervalId);
      my.intervalId = null;
      my.uiClicked = true;

      var item = null;
      for (var i = 0; i < my.data.length; i++) {
        if (my.data[i].Id == pId) {
          item = my.data[i];
          break;
        }
      }
      if (item == null)
        return;

      InitItem(item, 0);
    });

    my.Navigation.find("a.next").click(function (e) {
      if (my.isChanging == true)
        return;

      clearInterval(my.intervalId);
      my.intervalId = null;
      my.uiClicked = true;

      var item = null;
      for (var i = 0; i < my.data.length; i++) {
        if (my.data[i].Id == my.nId) {
          item = my.data[i];
          break;
        }
      }
      if (item == null)
        return;

      InitItem(item, 0);
    });

    var linksContainer = my.Navigation.find("span.links");

    for (var i = 0; i < my.data.length; i++) {
      var css = "";
      if (my.data[i].Id == my.cId) {
        css = " selected";
      }
      var link = " <a class='item op " + css + "' href='javascript:void(0);' itemid='" + my.data[i].Id + "' >" + my.data[i].Id + "</a> ";
      linksContainer.append(link);
    }

    linksContainer.find("a.item").click(function (e) {
      if (my.isChanging == true) {
        return;
      }

      my.uiClicked = true;
      clearInterval(my.intervalId);
      my.intervalId = null;

      var itemClicked = parseInt($(this).attr("itemid"));
      if (itemClicked == my.cId)
        return;

      var item = null;
      for (var i = 0; i < my.data.length; i++) {
        if (my.data[i].Id == itemClicked) {
          item = my.data[i];
          break;
        }
      }

      if (item == null) {
        return;
      }

      InitItem(item, 0);

    });


  };


  function InitItem(item, timeOut) {
    if (my.intervalId != null)
      return;

    my.ImgCache.isLoading = true;


    my.ImgCache.mnBgImg.src = item.BackgroundImageUrl;
    my.ImgCache.mainImg.src = item.MainImageUrl;
    my.ImgCache.logoImg.src = item.RequestADemoImageUrl;
    my.ImgCache.demoImg.src = item.QuantifiLogoImageUrl;

    var timeCount = 0;

    my.intervalId = setInterval(function () {
      timeCount += 100;

      if (my.ImgCache.isLoading || timeOut > timeCount)
        return;

      clearInterval(my.intervalId);
      my.intervalId = null;
      ChangeContent(item);
    }
    , 100);
  }


  function ChangeContent(item) {
    if (my.isChanging == true) {
      return;
    }

    my.isChanging = true;

    var nextItem = SetCurrentItem(item.Id);
    var hpMod = $("#content  div.hpModule");
    var bg1, bg2;

    // flip between 2 bg layers so animation are smooth
    if ($("#wrapwebsite #bg1").data("isActive") === true) {
      bg2 = $("#wrapwebsite #bg1");
      bg1 = $("#wrapwebsite #bg2");
    }
    else {
      bg1 = $("#wrapwebsite #bg1");
      bg2 = $("#wrapwebsite #bg2");
    }

    // STOP any animations that have not completed
    bg1.stop(true, true);
    bg2.stop(true, true);

    bg1.animate({ opacity: 0 }, my.transSpeed);
    bg2.css({ 'opacity': 0, 'background-image': "url(" + item.BackgroundImageUrl + ")" })
                              .animate({ opacity: 1 }, my.transSpeed, function () {
                                bg1.data("isActive", true);
                                bg2.data("isActive", false);
                              });


    my.MainLink.css("background-image", "url(" + item.MainImageUrl + ")")
                .attr("href", item.MainImageLink)
                .attr("gac", item.CampaignName);


    if (item.QuantifiLogoImageUrl.length > 1)
      my.LogoImg.attr("src", item.QuantifiLogoImageUrl);

    if (item.RequestADemoImageUrl.length > 1)
      my.DemoImg.attr("src", item.RequestADemoImageUrl);

    if (item.HeaderTextColor.length == 0) {
      hpMod.find("div.content h2 a").css("color", "");
    }
    else {
      hpMod.find("div.content h2 a").animate({ color: "#" + item.HeaderTextColor });
    }

    if (item.MaskColor.length == 0) {
      hpMod.find("div.mask").css("background-color", "");
    }
    else {
      hpMod.find("div.mask").animate({ backgroundColor: "#" + item.MaskColor });
    }

    if (item.TopSubNavColorCss.length > 0) {
      $(my.WebSiteWrap).attr("class", "").addClass(item.TopSubNavColorCss);
    }

    if (item.TopNavColorCss.length > 0) {
      $(my.WebSiteNav).attr("class", "").addClass(item.TopNavColorCss);
    }

    my.transCount++;
    SetAdminLink(item);


    my.isChanging = false;

    if (nextItem != null) {
      if (my.transCount >= my.transMaxCount)
        return;

      if (my.uiClicked == false) {
        InitItem(nextItem, my.timeout);
      }
      else {
        // if user interacted... triple time out before changing again.
        InitItem(nextItem, (my.timeout * 3));
      }
    }
  }

  function SetAdminLink(item) {
    var link = $("#MainContentControl1_linkEdit");

    if (link == null || link.length == 0) {
      return;
    }

    var url = link.attr("href");

    if (url == null || url.length == 0) {
      return;
    }

    var index = url.indexOf("&id=");
    if (index < 0) {
      return;
    }

    try {
      var subUrl = url.substr(0, (index + 4));
      $("#MainContentControl1_linkEdit").attr("href", subUrl + item.Id);
    }
    catch (e) {
      Q.logError(e);
    }

  }

  function SetCurrentItem(cItemId) {
    $("#mcNav span.links a.item").removeClass("selected");

    my.cId = cItemId;

    var itemIndex = 0;
    var maxIndex = my.data.length - 1;
    for (var i = 0; i < my.data.length; i++) {
      if (my.data[i].Id == cItemId) {
        itemIndex = i;
        break;
      }
    }

    $("#mcNav span.links a.item:nth-child(" + (itemIndex + 1) + " )").addClass("selected");

    if (itemIndex == 0) {
      my.pId = my.data[maxIndex].Id;
      my.nId = my.data[itemIndex + 1].Id;
      return my.data[itemIndex + 1];
    }
    else if (itemIndex == maxIndex) {
      my.pId = my.data[itemIndex - 1].Id;
      my.nId = my.data[0].Id;
      return my.data[0];
    }
    else {
      my.pId = my.data[itemIndex - 1].Id;
      my.nId = my.data[itemIndex + 1].Id;
      return my.data[itemIndex + 1];
    }

  }


  return my;

} (jQuery, Quantifi));











