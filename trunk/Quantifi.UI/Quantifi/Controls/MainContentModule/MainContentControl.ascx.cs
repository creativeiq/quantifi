﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using System.Web.UI.HtmlControls;
using Quantifi.Business.MainContentModule;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using mojoPortal.Web.Framework;
using System.Collections.Specialized;
using Quantifi.Shared;
using System.Text;

namespace Quantifi.UI.Controls.MainContentModule
{
  public partial class MainContentControl : SiteModuleControl, IWorkflow
  {
    #region Properties

    private MainContent mainContent = null;
    private bool isAdmin = false;
    new private PageSettings currentPage = null;
    private int pageId = -1;


    private int previewItemId = -1;



    #endregion

    protected int currentId { get; private set; }
    protected int nextId { get; private set; }
    protected int prevId { get; private set; }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      previewItemId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("previewMC", -1);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (this.Page is NonCmsBasePage)
      {
        this.Visible = false;
        return;
      }

      this.Page.EnableViewState = false;

      LoadSettings();

      if (previewItemId > 0)
      {
        LoadPreviewContent();
      }
      else
      {
        LoadContent();
      }

    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      AddMainContentScripts();

    }


    private void LoadSettings()
    {
      if ((WebUser.IsAdminOrContentAdmin) || (SiteUtils.UserIsSiteEditor()))
        isAdmin = true;



      var mojoPage = this.Page as mojoBasePage;

      //showActiveMainContent = !Request.IsAuthenticated || (mojoPage.ViewMode == PageViewMode.Live);

      if (currentPage == null)
        currentPage = CacheHelper.GetCurrentPage();

      pageId = currentPage.PageId;

    }


    private void LoadContent()
    {


      var items = MainContentCacheUtil.GetByPageId(pageId);

      if (items.IsNullOrEmpty())
        return;

      var mainContent = items.First();

      currentId = mainContent.Id;

      if (items.Count() > 1)
      {
        nextId = items[1].Id;
        prevId = items.Last().Id;

        // if more than 1 item active, set the rotation active

      }
      else
      {
        nextId = 0;
        prevId = 0;
      }

      LoadBackgroundImage(mainContent);

      LoadMainImage(mainContent);

      LoadLogoAndRequestADemoImages(mainContent);

      LoadHeadlineAndMaskColors(mainContent);

      LoadTopNavAndSubTopNavCss(mainContent);

      if (isAdmin)
      {
        this.pnlAdmin.Visible = true;
        this.linkView.NavigateUrl = SiteRoot +  "/Quantifi/Controls/MainContentModule/View.aspx?pageId=" + pageId.ToString();
        this.linkEdit.NavigateUrl = SiteRoot +  "/Quantifi/Controls/MainContentModule/Edit.aspx?pageId=" + pageId.ToString() + "&id=" + currentId.ToString();
      }
      else
      {
        this.pnlAdmin.Visible = false;
      }



    }

    private void LoadPreviewContent()
    {

      var mainContent = MainContent.Get(previewItemId);
      if (mainContent == null)
        return;

      this.mainContent = mainContent;

      LoadBackgroundImage(mainContent);

      LoadMainImage(mainContent);

      LoadLogoAndRequestADemoImages(mainContent);

      LoadHeadlineAndMaskColors(mainContent);

      LoadTopNavAndSubTopNavCss(mainContent);

      this.pnlAdmin.Visible = false;
    }



    private void LoadLogoAndRequestADemoImages(MainContent mainContent)
    {

  


      if (!string.IsNullOrWhiteSpace(mainContent.RequestADemoImageUrl))
      {
        var demoImage = this.Page.Master.FindControl("qdemo") as Image;
        demoImage.ImageUrl = GetSiteUrl(mainContent.RequestADemoImageUrl);
      }

      if (!string.IsNullOrWhiteSpace(mainContent.QuantifiLogoImageUrl))
      {
        var logoImage = this.Page.Master.FindControl("qlogo") as Image;
        logoImage.ImageUrl = GetSiteUrl(mainContent.QuantifiLogoImageUrl);
      }

    }

    private void LoadMainImage(MainContent content)
    {

      if (!string.IsNullOrWhiteSpace(content.MainImageUrl))
      {
        if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion < 7)
        {
          this.linkMain.Style.Add(HtmlTextWriterStyle.Filter, "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + GetSiteUrl(content.MainImageUrl) + "')");
        }
        else
        {
          this.linkMain.Style.Add(HtmlTextWriterStyle.BackgroundImage, GetSiteUrl( content.MainImageUrl));
        }
      }


      if (!string.IsNullOrWhiteSpace(content.MainImageLink))
      {
        var linkUrl = content.MainImageLink.ToLowerInvariant();
 
        this.linkMain.NavigateUrl = GetSiteUrl(linkUrl);
        this.linkMain.Attributes.Add("gac", content.CampaignName);
      }


      if (!string.IsNullOrWhiteSpace(content.MainImageAltText))
        this.linkMain.Text = content.MainImageAltText;

    }

    private void LoadBackgroundImage(MainContent content)
    {
      Panel wrapWebsitePanel;
      if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion < 7)
      {
        wrapWebsitePanel = this.Page.Master.FindControl("wrapwebsite") as Panel;
      }
      else
      {
        wrapWebsitePanel = this.Page.Master.FindControl("bg1") as Panel;
      }

      if (wrapWebsitePanel == null)
        return;

      if (!string.IsNullOrWhiteSpace(content.BackgroundImageUrl))
        wrapWebsitePanel.Style.Add(HtmlTextWriterStyle.BackgroundImage, GetSiteUrl(content.BackgroundImageUrl));

    }

    private void LoadHeadlineAndMaskColors(MainContent mainContent)
    {
      if (string.IsNullOrWhiteSpace(mainContent.HeaderTextColor) && string.IsNullOrWhiteSpace(mainContent.MaskColor))
        return;

      var sb = new StringBuilder();

      sb.AppendLine(@"$(""#content .wrapcenter"").ready(function () { ");
      sb.AppendLine(@"var hpMod = $(""#content  div.hpModule"");");


      if (!string.IsNullOrWhiteSpace(mainContent.MaskColor))
      {
        sb.AppendFormat(@"hpMod.find(""div.content h2 a"").css(""color"", ""#{0}""); {1}", mainContent.HeaderTextColor, Environment.NewLine);
      }
      if (!string.IsNullOrWhiteSpace(mainContent.MaskColor))
      {
        sb.AppendFormat(@"hpMod.find(""div.mask"").css(""background-color"", ""#{0}""); {1}", mainContent.MaskColor, Environment.NewLine);
      }

      sb.AppendLine(@"});");

      this.Page.ClientScript.RegisterClientScriptBlock(typeof(MainContentControl), "HeadlineMaskColors", sb.ToString(), true);

    }

    private void LoadTopNavAndSubTopNavCss(MainContent mainContent)
    {
      if (!string.IsNullOrWhiteSpace(mainContent.TopNavColorCss))
      {
        var topNav = this.Page.Master.FindControl("topnav") as Panel;
        topNav.CssClass += " " + mainContent.TopNavColorCss;
      }

      if (!string.IsNullOrWhiteSpace(mainContent.TopSubNavColorCss))
      {
        var wrapheader = this.Page.Master.FindControl("wrapheader") as Panel;
        wrapheader.CssClass += " " + mainContent.TopSubNavColorCss;
      }

    }

    private void AddMainContentScripts()
    {

      //// Unable to support in IE6 at this time
      //if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion < 7)
      //  return;


      //var sb = new StringBuilder();

      //sb.AppendLine(@"// Main Content Setup");

      //sb.AppendFormat("Q.MC.pageId = {0};{1}", this.PageId, Environment.NewLine);
      //sb.AppendFormat("Q.MC.cId = {0};{1}", this.currentId, Environment.NewLine);
      //sb.AppendFormat("Q.MC.nId = {0};{1}", this.nextId, Environment.NewLine);
      //sb.AppendFormat("Q.MC.pId = {0};{1}{1}", this.prevId, Environment.NewLine);

      //sb.AppendLine(@"$(document).ready(function () { Q.MC.GetData(); });");


      //Page.ClientScript.RegisterStartupScript(typeof(MainContentControl), "MainContentSetup", sb.ToString(), true);


      //Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "MainContent", "/Quantifi/Controls/MainContentModule/Quantifi.MainContent.js");



      var sb = new StringBuilder();
  
      sb.AppendLine("<script type='text/javascript'>");
      sb.AppendLine(@"// Main Content Setup"); 

      //sb.AppendFormat("$(document).ready(function () {{  Q.MainContent = new Q.MC({0}, {1}, {2}, {3}); }});", this.PageId, this.currentId, this.nextId, this.prevId);
      //sb.AppendFormat("$(document).ready(function () {{  Q.MC.initControl({0}, {1}, {2}, {3}); }});", this.PageId, this.currentId, this.nextId, this.prevId);
      sb.AppendFormat("Q.MC.initControl({0}, {1}, {2}, {3}, '{4}');", 
                                                      this.PageId, 
                                                      this.currentId, 
                                                      this.nextId, 
                                                      this.prevId, 
                                                      GetSiteUrl("~/Quantifi/Controls/MainContentModule/MainContentHandler.ashx"));

      //sb.AppendLine("$.ajax({ url: '/ClientScript/quantifi.mainContent.min.js', cache: true, dataType: 'script', success: ");
 

      //sb.AppendFormat("    function( ){{ Q.MC.initControl({0}, {1}, {2}, {3}); }}", this.PageId, this.currentId, this.nextId, this.prevId);
      //sb.AppendLine("});");
      sb.AppendLine("</script >");
      sb.AppendLine("");

      //Page.ClientScript.RegisterStartupScript(typeof(MainContentControl), "MainContentSetup", sb.ToString(), true);


      litScripts.Text = sb.ToString();

    }


    protected string GetSiteUrl(string url)
    {

      if (url.StartsWith("~/"))
        return SiteRoot + url.Replace("~/", "/");


      if (url.StartsWith("/"))
        return SiteRoot + url;

      return url;
    }


    #region IWorkflow Members

    public void SubmitForApproval()
    {
      //throw new NotImplementedException();
    }

    public void CancelChanges()
    {
      //throw new NotImplementedException();
    }

    public void Approve()
    {
      //throw new NotImplementedException();
    }

    #endregion
  }
}