﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mojoPortal.Web;
using mojoPortal.Web.Framework;
using Quantifi.Business.MainContentModule;
//using Quantifi.UI.Utils;
using Quantifi.Shared;
using mojoPortal.Business;
using mojoPortal.Business.WebHelpers;
using log4net;

namespace Quantifi.UI.Controls.MainContentModule
{
	public partial class Edit : NonCmsBasePage
	{
    private static readonly ILog log = LogManager.GetLogger(typeof(Edit));

    #region Properties

    private int itemId = -1;
    private int pageId = -1;
    private PageSettings Settings = null;
    private MainContent mainContent = null;

    #endregion

    protected override void OnPreInit(EventArgs e)
    {

      AllowSkinOverride = true;
      base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      this.btnSave.Click += new EventHandler(btnSave_Click);
      this.btnDelete.Click += new EventHandler(btnDelete_Click);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      this.ScriptConfig.IncludeColorBox = true;
 

      if (!Request.IsAuthenticated)
      {
        SiteUtils.RedirectToLoginPage(this);
        return;
      }

      LoadParams();


      SecurityHelper.DisableBrowserCache();
      SuppressPageMenu();

      LoadData();



      if (!Page.IsPostBack && !Page.IsCallback)
        LoadControls();

    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);


      //this.Page.Header.Controls.Add(new LiteralControl(this.litStyleInject.Text));

    }

    private void LoadParams()
    {
      itemId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("id", -1);
      pageId = mojoPortal.Web.Framework.WebUtils.ParseInt32FromQueryString("pageid", -1);


    }

    private void LoadData()
    {
      Settings = new PageSettings(CacheHelper.GetCurrentSiteSettings().SiteId, pageId);

      mainContent = MainContent.Get(itemId);

    }

    private void LoadControls()
    {

      if (this.mainContent == null)
      {

        this.txtPriority.Text = (MainContent.GetHighestPriority(pageId) + 1).ToString();
        this.phPreview.Visible = false;
        this.btnDelete.Enabled = false;

      }
      else
      {
        // only allow InActive content to be deleted;

        this.btnDelete.Enabled = !mainContent.isActive;
        this.btnDelete.ConfirmThenDisableButtonOnClick("Are you sure you want to delete this?", string.Empty);

        this.headline.Text = "Edit Main Content";

        this.phPreview.Visible = true;
        this.linkPreview.NavigateUrl = Settings.Url + "?previewMC=" + mainContent.Id;
 


        this.txtPriority.Text = mainContent.Priority.ToString();
        this.checkboxIsActive.Checked = mainContent.isActive;

        this.txtCampaign.Text = mainContent.CampaignName;

        this.fileSelector_BackgroundImg.Value = mainContent.BackgroundImageUrl;
        this.fileSelector_MainImg.Value = mainContent.MainImageUrl;
        this.fileSelector_LogoImg.Value = mainContent.QuantifiLogoImageUrl;
        this.fileSelector_DemoImg.Value = mainContent.RequestADemoImageUrl;


        this.txtMainImageAltText.Text = mainContent.MainImageAltText;
        this.txtMainLink.Text = mainContent.MainImageLink;



        foreach (ListItem listItem in this.ddlTopNavCssClass.Items)
        {
          if (string.Equals(listItem.Value, mainContent.TopNavColorCss, StringComparison.InvariantCultureIgnoreCase))
          {
            this.ddlTopNavCssClass.SelectedIndex = this.ddlTopNavCssClass.Items.IndexOf(listItem);

            break;
          }
        }

        foreach (ListItem listItem in this.ddlTopSubNavCssClass.Items)
        {
          if (string.Equals(listItem.Value, mainContent.TopSubNavColorCss, StringComparison.InvariantCultureIgnoreCase))
          {
            this.ddlTopSubNavCssClass.SelectedIndex = this.ddlTopSubNavCssClass.Items.IndexOf(listItem);

            break;
          }
        }



        this.colorFeatureHeaderColor.Text = mainContent.HeaderTextColor;
        this.colorFeatureMaskColor.Text = mainContent.MaskColor;

      }

    }


    private void btnDelete_Click(object sender, EventArgs e)
    {

      try
      {

        if (mainContent == null)
          return;


        mainContent.Delete();

        

      }
      catch (Exception ex)
      {
        var msg = "An Error occured while Deleting : " + ex.Message;
        pnlWrapper.ShowErrorMessageBanner(msg);
        log.Error(msg, ex);
        return;
      }

      Response.Redirect("~/Quantifi/Controls/MainContentModule/View.aspx");

    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      bool redirectOnSave = false;
      if (this.mainContent == null)
      {
        redirectOnSave = true;
        this.mainContent = new MainContent();

      }

      mainContent.Priority = int.Parse(this.txtPriority.Text);
      mainContent.isActive = this.checkboxIsActive.Checked;


      mainContent.BackgroundImageUrl = this.fileSelector_BackgroundImg.Value;
      mainContent.MainImageUrl = this.fileSelector_MainImg.Value;
      mainContent.QuantifiLogoImageUrl = this.fileSelector_LogoImg.Value;
      mainContent.RequestADemoImageUrl = this.fileSelector_DemoImg.Value;

      mainContent.CampaignName = this.txtCampaign.Text.RemoveInvalidPathChars().RemoveLineBreaks().Replace("'", string.Empty).Replace("\"", string.Empty).Replace(" ", "-");

      mainContent.MainImageAltText = this.txtMainImageAltText.Text;
      mainContent.MainImageLink = this.txtMainLink.Text;

      mainContent.TopNavColorCss = this.ddlTopNavCssClass.SelectedValue;
      mainContent.TopSubNavColorCss = this.ddlTopSubNavCssClass.SelectedValue;

      mainContent.HeaderTextColor = this.colorFeatureHeaderColor.Text;
      mainContent.MaskColor = this.colorFeatureMaskColor.Text;

      mainContent.Save();

      if (redirectOnSave)
        Response.Redirect("~/Quantifi/Controls/MainContentModule/Edit.aspx?pageId=" + pageId.ToString() + "&id=" + mainContent.Id.ToString());
      else
        LoadControls();
    }

	}
}