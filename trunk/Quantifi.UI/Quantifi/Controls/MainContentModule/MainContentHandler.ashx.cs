﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quantifi.Business.MainContentModule;
using Quantifi.Shared;


namespace Quantifi.UI.Controls.MainContentModule
{
  /// <summary>
  /// Summary description for MainContentHandler
  /// </summary>
  public class MainContentHandler : IHttpHandler
  {

    public void ProcessRequest(HttpContext context)
    {

      if (context.Request != null)
      {
        if (context.Request.IsAuthenticated)
        {
          //add preview items?



        }

      }

      var items = MainContentCacheUtil.GetByPageId(0);


      context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
      context.Response.ContentType = "text/plain";
      context.Response.Write(items.ToJson());
      context.Response.End();
    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }
  }
}