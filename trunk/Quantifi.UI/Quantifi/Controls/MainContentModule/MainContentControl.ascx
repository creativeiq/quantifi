﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainContentControl.ascx.cs"
  Inherits="Quantifi.UI.Controls.MainContentModule.MainContentControl" %>


<asp:Panel ID="pnlContent" runat="server" CssClass="mainContentControl">
  <asp:Panel ID="pnlAdmin" runat="server" CssClass="mainContentAdmin" Visible="false">
    <asp:HyperLink ID="linkView" runat="server" ToolTip="Manage Homepages"><img src="<%= SiteRoot %>/Data/SiteImages/page_gear.png" alt="Manage" /> Manage</asp:HyperLink>
    &nbsp;
    <asp:HyperLink ID="linkEdit" runat="server"  ToolTip="Edit this Homepage"><img src="<%= SiteRoot %>/Data/SiteImages/page_edit.png" alt="Edit" /> EDIT</asp:HyperLink>
  </asp:Panel>
  <h1 class="mainContent">
    <asp:HyperLink ID="linkMain" runat="server" NavigateUrl="<%= SiteRoot %>/request-a-demo.aspx" CssClass="mainContentLink" >Simplify, Clarify, Quantifi: Transparent
		Valuation and risk management, learn how</asp:HyperLink>
  </h1>
  <div id="mcNav" style="display: none;">
    <div class="mcNavCont" style="">
      <a class="prev op" title="PREV" href="javascript:void(0);">&lt;&lt;</a> <span class="links">
      </span><a class="next op" title="NEXT" href="javascript:void(0);">&gt;&gt;</a>
    </div>
  </div>
</asp:Panel>
<asp:Literal ID="litScripts" runat="server" Mode="PassThrough"></asp:Literal>
