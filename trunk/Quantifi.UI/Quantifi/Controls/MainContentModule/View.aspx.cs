﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Quantifi.Business.MainContentModule;
using mojoPortal.Web;
using Quantifi.Shared;

namespace Quantifi.UI.Controls.MainContentModule
{
  public partial class View : NonCmsBasePage
  {

    private int _pageId = 0;
    private int _pageNum = 1;
    private int _pageSize = 20;
    private int _totalItems = 0;

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);


      this.gvItems.RowCommand += new GridViewCommandEventHandler(gvItems_RowCommand);

    }


    protected void Page_Load(object sender, EventArgs e)
    {
      this.ScriptConfig.IncludeColorBox = true;


      if (IsPostBack)
        return;

      LoadData();

    }


    private void LoadData()
    {


      List<MainContent> items = MainContent.GetPage(_pageId, _pageNum, _pageSize, out _totalItems);


      this.gvItems.DataSource = items;
      this.gvItems.DataBind();

    }

    void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      if (e == null)
        return;

      

      int itemId;

      if (!int.TryParse(e.CommandArgument.ToString(), out itemId))
        return;

      switch (e.CommandName)
      {
        case "Down":
          MovePriorityDown(itemId);
          break;

        case "Up":
          MovePriorityUp(itemId);

          break;

        default:
          break;

      }
    }


    private void MovePriorityUp(int itemId)
    {
      var content = MainContent.Get(itemId);

      if (content != null)
        MainContent.MoveContentPriorityUp(content.Id, content.Priority);
      LoadData();
    }

    private void MovePriorityDown(int itemId)
    {
      var content = MainContent.Get(itemId);

      if (content != null)
        MainContent.MoveContentPriorityDown(content.Id, content.Priority);
      LoadData();
    }

  }
}