﻿using System;
using System.Data.Objects;
using System.Linq;


namespace Quantifi.Data.EF
{
  /// <summary>
  /// Collection of static Compiled Linq Queries
  /// </summary>
  public static class Queries
  {
    #region NewsFeedItems

    private static readonly Func<CorpWebEntities, int, int, DateTime, IQueryable<NewsFeedItems>>
      getTopActiveNewsFeedItemsByPubdate = CompiledQuery.Compile(
        (CorpWebEntities context, int moduleId, int topCount, DateTime utcTime) =>
        context.NewsFeedItems.Where(nfi => nfi.ModuleId == moduleId && nfi.IsActive == true && nfi.PubDate < utcTime)
          .OrderByDescending(nfi => nfi.PubDate)
          .Take(topCount));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="moduleId"></param>
    /// <param name="topCount"></param>
    /// <param name="utcTime"></param>
    /// <returns></returns>
    public static IQueryable<NewsFeedItems> GetTopActiveNewsFeedItemsByPubdate( CorpWebEntities context, int moduleId, int topCount, DateTime utcTime )
    {
      return getTopActiveNewsFeedItemsByPubdate.Invoke(context, moduleId, topCount, utcTime);
    }


    #endregion

    #region FeaturedItems

    private static readonly Func<CorpWebEntities, int, int, DateTime, IQueryable<FeaturedItems>>
      getTopActiveFeaturedItemsByPubdate = CompiledQuery.Compile(
        (CorpWebEntities context, int moduleId, int topCount, DateTime utcTime) =>
        context.FeaturedItems.Where(f => f.ModuleId == moduleId && f.IsActive == true && f.PubDate < utcTime)
          .OrderByDescending(f => f.PubDate)
          .Take(topCount));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="moduleId"></param>
    /// <param name="topCount"></param>
    /// <param name="utcTime"></param>
    /// <returns></returns>
    public static IQueryable<FeaturedItems> GetTopActiveFeaturedItemsByPubdate(CorpWebEntities context, int moduleId, int topCount, DateTime utcTime)
    {
      return getTopActiveFeaturedItemsByPubdate.Invoke(context, moduleId, topCount, utcTime);
    }


    #endregion
  
  
  }
}
