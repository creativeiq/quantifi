﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using mojoPortal.Data;
using System.Configuration;

namespace Quantifi.Data
{

	public static class DownloadFileDB
	{

		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];
		}

		/// <summary>
		/// Insert
		/// </summary>
		/// <param name="guid"></param>
		/// <param name="displayName"></param>
		/// <param name="displayDescription"></param>
		/// <param name="originalFileName"></param>
		/// <param name="serverFileName"></param>
		/// <param name="isActive"></param>
		/// <param name="currentUserName"></param>
		/// <returns></returns>
		public static int Insert(Guid guid, string displayName, string displayDescription, string originalFileName, 
      string serverFileName, bool isActive, int sizeInKB, string currentUserName,
      bool requireForm, bool emailUserActivity, bool emailFilesToUser, bool emailFilesToRestrictedEmails, string adminEmailAddress)
		{
			var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_Insert", 15);

			sph.DefineSqlParameter("@Guid", SqlDbType.UniqueIdentifier, ParameterDirection.Input, guid);
			sph.DefineSqlParameter("@DisplayName", SqlDbType.NVarChar, 255, ParameterDirection.Input, displayName);
			sph.DefineSqlParameter("@DisplayDescription", SqlDbType.NText, ParameterDirection.Input, displayDescription);
			sph.DefineSqlParameter("@OriginalFileName", SqlDbType.NVarChar, 255, ParameterDirection.Input, originalFileName);
			sph.DefineSqlParameter("@ServerFileName", SqlDbType.NVarChar, 255, ParameterDirection.Input, serverFileName);
			sph.DefineSqlParameter("@IsActive", SqlDbType.Bit, ParameterDirection.Input, isActive);

			sph.DefineSqlParameter("@SizeInKB", SqlDbType.Int, ParameterDirection.Input, sizeInKB);

			sph.DefineSqlParameter("@CreatedTime", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
			sph.DefineSqlParameter("@CreatedUser", SqlDbType.NVarChar, 255, ParameterDirection.Input, currentUserName);

			sph.DefineSqlParameter("@RequireForm", SqlDbType.Bit, ParameterDirection.Input, requireForm);
			sph.DefineSqlParameter("@EmailUserActivity", SqlDbType.Bit, ParameterDirection.Input, emailUserActivity);

      sph.DefineSqlParameter("@EmailFilesToUser", SqlDbType.Bit, ParameterDirection.Input, emailFilesToUser);
      sph.DefineSqlParameter("@EmailFilesToRestrictedEmails", SqlDbType.Bit, ParameterDirection.Input, emailFilesToRestrictedEmails);

			sph.DefineSqlParameter("@AdminEmailAddress", SqlDbType.NVarChar, 100, ParameterDirection.Input, adminEmailAddress);			 

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.InputOutput, null);

			sph.ExecuteNonQuery();
			int newID = Convert.ToInt32(sph.Parameters.Last().Value);
			return newID;

		}

		/// <summary>
		/// Update
		/// </summary>
		/// <param name="id"></param>
		/// <param name="displayName"></param>
		/// <param name="displayDescription"></param>
		/// <param name="originalFileName"></param>
		/// <param name="serverFileName"></param>
		/// <param name="isActive"></param>
		/// <param name="currentUserName"></param>
		public static void Update(int id, string displayName, string displayDescription, string originalFileName, 
      string serverFileName, bool isActive, int sizeInKB, string currentUserName,
      bool requireForm, bool emailUserActivity, bool emailFilesToUser, bool emailFilesToRestrictedEmails, string adminEmailAddress)
		{
			var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_Update", 14);

			sph.DefineSqlParameter("@Id", SqlDbType.Int, ParameterDirection.Input, id);
			sph.DefineSqlParameter("@DisplayName", SqlDbType.NVarChar, 255, ParameterDirection.Input, displayName);
			sph.DefineSqlParameter("@DisplayDescription", SqlDbType.NText, ParameterDirection.Input, displayDescription);
			sph.DefineSqlParameter("@OriginalFileName", SqlDbType.NVarChar, 255, ParameterDirection.Input, originalFileName);
			sph.DefineSqlParameter("@ServerFileName", SqlDbType.NVarChar, 255, ParameterDirection.Input, serverFileName);	
			sph.DefineSqlParameter("@SizeInKB", SqlDbType.Int, ParameterDirection.Input, sizeInKB);	
			sph.DefineSqlParameter("@IsActive", SqlDbType.Bit, ParameterDirection.Input, isActive);
			sph.DefineSqlParameter("@LastUpdatedTime", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
			sph.DefineSqlParameter("@LastUpdatedUser", SqlDbType.NVarChar, 255, ParameterDirection.Input, currentUserName);


			sph.DefineSqlParameter("@RequireForm", SqlDbType.Bit, ParameterDirection.Input, requireForm);
			sph.DefineSqlParameter("@EmailUserActivity", SqlDbType.Bit, ParameterDirection.Input, emailUserActivity);
      sph.DefineSqlParameter("@EmailFilesToUser", SqlDbType.Bit, ParameterDirection.Input, emailFilesToUser);
      sph.DefineSqlParameter("@EmailFilesToRestrictedEmails", SqlDbType.Bit, ParameterDirection.Input, emailFilesToRestrictedEmails);

			sph.DefineSqlParameter("@AdminEmailAddress", SqlDbType.NVarChar, 100, ParameterDirection.Input, adminEmailAddress);			 		 


			sph.ExecuteNonQuery();
		}




		public static void Delete(int id)
		{
			var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_Delete", 1);

			sph.DefineSqlParameter("@Id", SqlDbType.Int, ParameterDirection.Input, id);

			sph.ExecuteNonQuery();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static IDataReader GetDownloadFiles()
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_SelectAll", 0);

			return sph.ExecuteReader();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pageSize"></param>
		/// <param name="pageNum"></param>
		/// <returns></returns>
		public static IDataReader GetPage(int pageSize, int pageNum, out int totalPages)
		{

      totalPages = 1;

      int totalRows = GetCount();

      if (pageSize > 0) totalPages = totalRows / pageSize;

      if (totalRows <= pageSize)
      {
        totalPages = 1;
      }
      else
      {
        int remainder;
        Math.DivRem(totalRows, pageSize, out remainder);
        if (remainder > 0)
        {
          totalPages += 1;
        }
      }


			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_SelectPage", 2);

      sph.DefineSqlParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNum);
			sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);

			return sph.ExecuteReader();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="guid"></param>
		/// <returns></returns>
		public static IDataReader GetDownloadFile(Guid guid)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_SelectByGuid", 1);

			sph.DefineSqlParameter("@Guid", SqlDbType.UniqueIdentifier, ParameterDirection.Input, guid);

			return sph.ExecuteReader();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		public static IDataReader GetDownloadFile(int fileId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_SelectById", 1);

			sph.DefineSqlParameter("@Id", SqlDbType.Int, ParameterDirection.Input, fileId);

			return sph.ExecuteReader();
		}

    public static int GetCount()
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFiles_GetCount", 0);


      return Convert.ToInt32(sph.ExecuteScalar());
    }

	}


	public static class DownloadFileActivityDB
	{
		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="userHostAddress"></param>
		/// <param name="userHostName"></param>
		/// <param name="referrer"></param>
		/// <param name="timeStamp"></param>
		/// <returns></returns>
		public static long Insert(int fileId, string userHostAddress, string userHostName, string referrer, DateTime timeStamp)
		{

			var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_Insert", 6);


			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);
			sph.DefineSqlParameter("@UserHostAddress", SqlDbType.NVarChar, 50, ParameterDirection.Input, userHostAddress);
			sph.DefineSqlParameter("@UserHostName", SqlDbType.NVarChar, 255, ParameterDirection.Input, userHostName);
			sph.DefineSqlParameter("@Referrer", SqlDbType.NVarChar, 510, ParameterDirection.Input, referrer);
			sph.DefineSqlParameter("@TimeStamp", SqlDbType.DateTime, ParameterDirection.Input, timeStamp);

			sph.DefineSqlParameter("@ItemID", SqlDbType.BigInt, ParameterDirection.InputOutput, null);


			sph.ExecuteNonQuery();
			long newID = Convert.ToInt64(sph.Parameters.Last().Value);
			return newID;


		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		public static IDataReader GetActivity(int fileId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_Select", 1);

			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);

			return sph.ExecuteReader();

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <returns></returns>
		public static IDataReader GetActivityByDate(int fileId, DateTime startDate, DateTime endDate)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_SelectByDate", 3);

			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
			sph.DefineSqlParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);

			return sph.ExecuteReader();

		}


		public static int GetCountByFile(int fileId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_GetCount", 1);

			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);

			return Convert.ToInt32(sph.ExecuteScalar());
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="pageNum"></param>
		/// <param name="pageSize"></param>
		/// <param name="pageNum"></param>
		/// <returns></returns>
		public static IDataReader GetActivityByPage(int fileId, int pageSize, int pageNum, out int totalPages)
		{

			totalPages = 1;

			int totalRows = GetCountByFile(fileId);

			if (pageSize > 0) totalPages = totalRows / pageSize;

			if (totalRows <= pageSize)
			{
				totalPages = 1;
			}
			else
			{
				int remainder;
				Math.DivRem(totalRows, pageSize, out remainder);
				if (remainder > 0)
				{
					totalPages += 1;
				}
			}



			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_SelectPage", 3);

			sph.DefineSqlParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileId);
			sph.DefineSqlParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNum);
			sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);

			return sph.ExecuteReader();

		}



		public static IDataReader GetAllByFile(int fileId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_GetAllByFile", 1);
			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);
			return sph.ExecuteReader();
		}

	}


	public static class DownloadFileFormActivityDB
	{
		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="activityId"></param>
		/// <param name="fileId"></param>
		/// <param name="firstName"></param>
		/// <param name="lastName"></param>
		/// <param name="jobTitle"></param>
		/// <param name="companyName"></param>
		/// <param name="email"></param>
		/// <param name="phone"></param>
		/// <param name="marketingOptIn"></param>
		/// <returns></returns>
		public static int Insert(long activityId, int fileId,
															string firstName, string lastName,
															string jobTitle, string companyName,
															string email, string phone, int countryId,
															bool marketingOptIn)
		{

			var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_Form_Insert", 10);


			sph.DefineSqlParameter("@ActivityId", SqlDbType.BigInt, ParameterDirection.Input, activityId);
			sph.DefineSqlParameter("@FirstName", SqlDbType.NVarChar, 100, ParameterDirection.Input, firstName);
			sph.DefineSqlParameter("@LastName", SqlDbType.NVarChar, 100, ParameterDirection.Input, lastName);
			sph.DefineSqlParameter("@JobTitle", SqlDbType.NVarChar, 100, ParameterDirection.Input, jobTitle);
			sph.DefineSqlParameter("@CompanyName", SqlDbType.NVarChar, 100, ParameterDirection.Input, companyName);
			sph.DefineSqlParameter("@Email", SqlDbType.NVarChar, 100, ParameterDirection.Input, email);
			sph.DefineSqlParameter("@Phone", SqlDbType.NVarChar, 50, ParameterDirection.Input, phone);
			sph.DefineSqlParameter("@CountryId", SqlDbType.Int,  ParameterDirection.Input, countryId);
			sph.DefineSqlParameter("@MarketingOptIn", SqlDbType.Bit, ParameterDirection.Input, marketingOptIn);


			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.InputOutput, null);


			sph.ExecuteNonQuery();

			var newID = Convert.ToInt32(sph.Parameters.Last().Value);
			return newID;


		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		public static int GetCountByFile(int fileId)
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_Form_GetCount", 1);

			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);

			return Convert.ToInt32(sph.ExecuteScalar());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalPages"></param>
		/// <returns></returns>
		public static IDataReader GetPage(int fileId, int pageNumber, int pageSize, out int totalPages)
		{
			totalPages = 1;

			int totalRows = GetCountByFile(fileId);

			if (pageSize > 0) totalPages = totalRows / pageSize;

			if (totalRows <= pageSize)
			{
				totalPages = 1;
			}
			else
			{
				int remainder;
				Math.DivRem(totalRows, pageSize, out remainder);
				if (remainder > 0)
				{
					totalPages += 1;
				}
			}

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_Form_GetPageByFile", 3);
			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);
			sph.DefineSqlParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
			sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
			return sph.ExecuteReader();

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		public static IDataReader GetAllByFile(int fileId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_Form_GetAllByFile", 1);
			sph.DefineSqlParameter("@FileId", SqlDbType.Int, ParameterDirection.Input, fileId);
			return sph.ExecuteReader();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static IDataReader Get(int id)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DownloadFileActivity_Form_Get", 1);
			sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);
			return sph.ExecuteReader();
		}

	}

}
