﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using mojoPortal.Data;

namespace Quantifi.Data
{
  public static class LinksDB
  {

    /// <summary>
    /// Gets the connection string.
    /// </summary>
    /// <returns></returns>
    private static string GetConnectionString()
    {
      return ConfigurationManager.AppSettings["MSSQLConnectionString"];
    }


    public static int AddLink(Guid itemGuid, Guid moduleGuid,
        int moduleId,
        string title,
        string url,
        string imageUrl,
        string location,
        DateTime pubDate,
        int viewOrder,
        string description,
        DateTime createdDate,
        int createdBy,
        string target,
        Guid userGuid)
    {

      var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_Insert", 14);
      sph.DefineSqlParameter("@ItemGuid", SqlDbType.UniqueIdentifier, ParameterDirection.Input, itemGuid);
      sph.DefineSqlParameter("@ModuleGuid", SqlDbType.UniqueIdentifier, ParameterDirection.Input, moduleGuid);
      sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
      sph.DefineSqlParameter("@Title", SqlDbType.NVarChar, 255, ParameterDirection.Input, title);
      sph.DefineSqlParameter("@Url", SqlDbType.NVarChar, 255, ParameterDirection.Input, url);
      sph.DefineSqlParameter("@ImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, imageUrl);
      sph.DefineSqlParameter("@Location", SqlDbType.NVarChar, 255, ParameterDirection.Input, location);
      sph.DefineSqlParameter("@PubDate", SqlDbType.DateTime, ParameterDirection.Input, pubDate);
      sph.DefineSqlParameter("@ViewOrder", SqlDbType.Int, ParameterDirection.Input, viewOrder);
      sph.DefineSqlParameter("@Description", SqlDbType.NText, ParameterDirection.Input, description);
      sph.DefineSqlParameter("@CreatedDate", SqlDbType.DateTime, ParameterDirection.Input, createdDate);
      sph.DefineSqlParameter("@CreatedBy", SqlDbType.Int, ParameterDirection.Input, createdBy);
      sph.DefineSqlParameter("@Target", SqlDbType.NVarChar, 20, ParameterDirection.Input, target);
      sph.DefineSqlParameter("@UserGuid", SqlDbType.UniqueIdentifier, ParameterDirection.Input, userGuid);
      var newID = Convert.ToInt32(sph.ExecuteScalar());
      return newID;
    }

    public static bool UpdateLink(
      int itemId,
      int moduleId,
      string title,
      string url,
      string imageUrl,
      string location,
      DateTime pubDate,
      int viewOrder,
      string description,
      DateTime createdDate,
      string target,
      int createdBy)
    {

      var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_Update", 12);
      sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);
      sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
      sph.DefineSqlParameter("@Title", SqlDbType.NVarChar, 255, ParameterDirection.Input, title);
      sph.DefineSqlParameter("@Url", SqlDbType.NVarChar, 255, ParameterDirection.Input, url);
      sph.DefineSqlParameter("@ImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, imageUrl);
      sph.DefineSqlParameter("@Location", SqlDbType.NVarChar, 255, ParameterDirection.Input, location);
      sph.DefineSqlParameter("@PubDate", SqlDbType.DateTime, ParameterDirection.Input, pubDate);
      sph.DefineSqlParameter("@ViewOrder", SqlDbType.Int, ParameterDirection.Input, viewOrder);
      sph.DefineSqlParameter("@Description", SqlDbType.NText, ParameterDirection.Input, description);
      sph.DefineSqlParameter("@CreatedDate", SqlDbType.DateTime, ParameterDirection.Input, createdDate);
      sph.DefineSqlParameter("@CreatedBy", SqlDbType.Int, ParameterDirection.Input, createdBy);
      sph.DefineSqlParameter("@Target", SqlDbType.NVarChar, 20, ParameterDirection.Input, target);
      var rowsAffected = sph.ExecuteNonQuery();
      return (rowsAffected > -1);
    }

    public static bool DeleteLink(int itemId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_Delete", 1);
      sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);
      int rowsAffected = sph.ExecuteNonQuery();
      return (rowsAffected > -1);
    }

    public static bool DeleteByModule(int moduleId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_DeleteByModule", 1);
      sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
      int rowsAffected = sph.ExecuteNonQuery();
      return (rowsAffected > -1);
    }

    public static bool DeleteBySite(int siteId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_DeleteBySite", 1);
      sph.DefineSqlParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteId);
      int rowsAffected = sph.ExecuteNonQuery();
      return (rowsAffected > -1);
    }

    public static IDataReader GetLink(int itemId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_SelectOne", 1);
      sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);
      return sph.ExecuteReader();
    }

    public static IDataReader GetLinks(int moduleId, DateTime frombydate)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_Select", 2);
      sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
      sph.DefineSqlParameter("@FromDate", SqlDbType.DateTime, ParameterDirection.Input, frombydate);
      return sph.ExecuteReader();
    }

    public static IDataReader GetLinksByPage(int siteId, int pageId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_SelectByPage", 2);
      sph.DefineSqlParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteId);
      sph.DefineSqlParameter("@PageID", SqlDbType.Int, ParameterDirection.Input, pageId);
      return sph.ExecuteReader();
    }


    /// <summary>
    /// Gets a count of rows in the mp_Links table.
    /// </summary>
    public static int GetCount(int moduleId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_GetCount", 1);
      sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
      return Convert.ToInt32(sph.ExecuteScalar());

    }

    public static IDataReader GetPage(
        int moduleId,
        int pageNumber,
        int pageSize,
        out int totalPages)
    {
      totalPages = 1;
      int totalRows = GetCount(moduleId);

      if (pageSize > 0) totalPages = totalRows / pageSize;

      if (totalRows <= pageSize)
      {
        totalPages = 1;
      }
      else
      {
        int remainder;
        Math.DivRem(totalRows, pageSize, out remainder);
        if (remainder > 0)
        {
          totalPages += 1;
        }
      }

      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_SelectPage", 3);
      sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
      sph.DefineSqlParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
      sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
      return sph.ExecuteReader();

    }



  }



}
