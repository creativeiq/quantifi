﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using mojoPortal.Data;
using System.Configuration;

namespace Quantifi.Data
{
  public static class DemoRequestDB
  {
    private static string GetConnectionString()
    {
      return ConfigurationManager.AppSettings["MSSQLConnectionString"];
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static IDataReader GetDemoRequest(int itemId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DemoRequest_SelectOne", 1);
      sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);
      return sph.ExecuteReader();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fullName"></param>
    /// <param name="title"></param>
    /// <param name="phone"></param>
    /// <param name="email"></param>
    /// <param name="companyName"></param>
    /// <param name="contactNotes"></param>
    /// <param name="questions"></param>
    /// <param name="clientIP"></param>
    /// <param name="urlReferrer"></param>
    /// <param name="timestamp"></param>
    /// <returns></returns>
    public static int Insert(string fullName, string title, string phone, string email, string companyName, string contactNotes, string questions, string clientIP, string urlReferrer, DateTime timestamp )
    {
      var sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DemoRequests_Insert", 10);
 
 
      sph.DefineSqlParameter("@FullName", SqlDbType.NVarChar, 250, ParameterDirection.Input, fullName);
      sph.DefineSqlParameter("@Title", SqlDbType.NVarChar, 100, ParameterDirection.Input, title);
      sph.DefineSqlParameter("@Phone", SqlDbType.NVarChar, 50, ParameterDirection.Input, phone);
      sph.DefineSqlParameter("@Email", SqlDbType.NVarChar, 250, ParameterDirection.Input, email);
      sph.DefineSqlParameter("@CompanyName", SqlDbType.NVarChar, 250, ParameterDirection.Input, companyName);
      sph.DefineSqlParameter("@ContactNotes", SqlDbType.NVarChar, 250, ParameterDirection.Input, contactNotes);
      sph.DefineSqlParameter("@Questions", SqlDbType.NVarChar, 500, ParameterDirection.Input, questions);
      sph.DefineSqlParameter("@ClientIP", SqlDbType.NVarChar, 50, ParameterDirection.Input, clientIP);
      sph.DefineSqlParameter("@UrlReferrer", SqlDbType.NVarChar, 500, ParameterDirection.Input, urlReferrer);
      sph.DefineSqlParameter("@TimeStamp", SqlDbType.DateTime, ParameterDirection.Input, timestamp);
    

      var newID = Convert.ToInt32(sph.ExecuteScalar());
      return newID;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageNum"></param>
    /// <param name="pageSize"></param>
    /// <returns></returns>
    public static IDataReader GetDemoRequests(int pageNum, int pageSize)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DemoRequests_SelectPage", 2);


      sph.DefineSqlParameter("@PageNum", SqlDbType.Int, ParameterDirection.Input, pageNum);
      sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);

      return sph.ExecuteReader();

    }

    /// <summary>
    /// 
    /// </summary>
    public static IDataReader GetDemoRequests()
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_DemoRequests_Select",0);

      return sph.ExecuteReader();

    }


  }
}
