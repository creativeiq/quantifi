﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using mojoPortal.Data;
using System.Data;

namespace Quantifi.Data.BannerModule
{
	public static class BannerDB
	{

		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];
		}

		public static int Insert(int pageId, bool isActive, string imageUrl)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Banner_Insert", 4);

			sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
			sph.DefineSqlParameter("@isActive", SqlDbType.Bit, ParameterDirection.Input, isActive);
			sph.DefineSqlParameter("@ImageUrl", SqlDbType.NVarChar, 255 ,ParameterDirection.Input, imageUrl);
			sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.InputOutput, null);

			sph.ExecuteNonQuery();

			int newID = Convert.ToInt32(sph.Parameters[3].Value);
			return newID;
		}

		public static void Update(int id, int pageId, bool isActive, string imageUrl)
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Banner_Update", 4);
			
			sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);
			sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
			sph.DefineSqlParameter("@isActive", SqlDbType.Bit, ParameterDirection.Input, isActive);
			sph.DefineSqlParameter("@ImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, imageUrl);
 
			sph.ExecuteNonQuery();

		}

		/// <summary>
		/// Delete By ModuleId
		/// </summary>
		/// <param name="mId"></param>
		public static void Delete(int id)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Banner_Delete", 1);

			sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);

			sph.ExecuteNonQuery();
		}

		public static IDataReader Get(int id)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Banner_Select", 1);
			sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);
			return sph.ExecuteReader();
		}

		public static IDataReader GetByPage(int pageId, bool isActive)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Banner_SelectByPage", 2);
			sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
			sph.DefineSqlParameter("@isActive", SqlDbType.Bit, ParameterDirection.Input, isActive);
			return sph.ExecuteReader();
		}


     
	}
}
