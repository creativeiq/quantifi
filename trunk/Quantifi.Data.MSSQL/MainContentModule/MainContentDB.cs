﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using mojoPortal.Data;

namespace Quantifi.Data.MainContentModule
{
  public static class MainContentDB
  {

    private static string GetConnectionString()
    {
      return ConfigurationManager.AppSettings["MSSQLConnectionString"];
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageId"></param>
    /// <param name="priority"></param>
    /// <param name="isActive"></param>
    /// <param name="backgroundImageUrl"></param>
    /// <param name="mainImageUrl"></param>
    /// <param name="mainImageAltText"></param>
    /// <param name="mainImageLink"></param>
    /// <param name="headerTextColor"></param>
    /// <param name="maskColor"></param>
    /// <param name="requestADemoImageUrl"></param>
    /// <param name="quantifiLogoImageUrl"></param>
    /// <param name="topNavColorCss"></param>
    /// <param name="topSubNavColorCss"></param>
    /// <returns></returns>
    public static int Insert(int pageId, int priority, bool isActive, string campaignName,
                              string backgroundImageUrl, string mainImageUrl,
                              string mainImageAltText, string mainImageLink,
                              string headerTextColor, string maskColor,
                              string requestADemoImageUrl, string quantifiLogoImageUrl,
                              string topNavColorCss, string topSubNavColorCss)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_Insert", 16);

      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
      sph.DefineSqlParameter("@Priority", SqlDbType.Int, ParameterDirection.Input, priority);
      sph.DefineSqlParameter("@isActive", SqlDbType.Bit, ParameterDirection.Input, isActive);

      sph.DefineSqlParameter("@CampaignName", SqlDbType.NVarChar, 50, ParameterDirection.Input, campaignName);

      sph.DefineSqlParameter("@BackgroundImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, backgroundImageUrl);
      sph.DefineSqlParameter("@MainImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, mainImageUrl);
      sph.DefineSqlParameter("@MainImageAltText", SqlDbType.NVarChar, 255, ParameterDirection.Input, mainImageAltText);
      sph.DefineSqlParameter("@MainImageLink", SqlDbType.NVarChar, 255, ParameterDirection.Input, mainImageLink);

      sph.DefineSqlParameter("@HeaderTextColor", SqlDbType.NVarChar, 10, ParameterDirection.Input, headerTextColor);
      sph.DefineSqlParameter("@MaskColor", SqlDbType.NVarChar, 10, ParameterDirection.Input, maskColor);

      sph.DefineSqlParameter("@RequestADemoImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, requestADemoImageUrl);
      sph.DefineSqlParameter("@QuantifiLogoImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, quantifiLogoImageUrl);


      sph.DefineSqlParameter("@TopNavColorCss", SqlDbType.NVarChar, 255, ParameterDirection.Input, topNavColorCss);
      sph.DefineSqlParameter("@TopSubNavColorCss", SqlDbType.NVarChar, 255, ParameterDirection.Input, topSubNavColorCss);

      sph.DefineSqlParameter("@CreatedDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.UtcNow);

      sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.InputOutput, null);

      sph.ExecuteNonQuery();

      int newID = Convert.ToInt32(sph.Parameters.Last().Value);
      return newID;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="pageId"></param>
    /// <param name="priority"></param>
    /// <param name="isActive"></param>
    /// <param name="backgroundImageUrl"></param>
    /// <param name="mainImageUrl"></param>
    /// <param name="mainImageAltText"></param>
    /// <param name="mainImageLink"></param>
    /// <param name="headerTextColor"></param>
    /// <param name="maskColor"></param>
    /// <param name="requestADemoImageUrl"></param>
    /// <param name="quantifiLogoImageUrl"></param>
    /// <param name="topNavColorCss"></param>
    /// <param name="topSubNavColorCss"></param>
    public static void Update(int id, int pageId, int priority, bool isActive, string campaignName,
                              string backgroundImageUrl, string mainImageUrl,
                              string mainImageAltText, string mainImageLink,
                              string headerTextColor, string maskColor,
                              string requestADemoImageUrl, string quantifiLogoImageUrl,
                              string topNavColorCss, string topSubNavColorCss)
    {

      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_Update", 15);

      sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
      sph.DefineSqlParameter("@Priority", SqlDbType.Int, ParameterDirection.Input, priority);
      sph.DefineSqlParameter("@isActive", SqlDbType.Bit, ParameterDirection.Input, isActive);

      sph.DefineSqlParameter("@CampaignName", SqlDbType.NVarChar, 50, ParameterDirection.Input, campaignName);

      sph.DefineSqlParameter("@BackgroundImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, backgroundImageUrl);
      sph.DefineSqlParameter("@MainImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, mainImageUrl);
      sph.DefineSqlParameter("@MainImageAltText", SqlDbType.NVarChar, 255, ParameterDirection.Input, mainImageAltText);
      sph.DefineSqlParameter("@MainImageLink", SqlDbType.NVarChar, 255, ParameterDirection.Input, mainImageLink);

      sph.DefineSqlParameter("@HeaderTextColor", SqlDbType.NVarChar, 10, ParameterDirection.Input, headerTextColor);
      sph.DefineSqlParameter("@MaskColor", SqlDbType.NVarChar, 10, ParameterDirection.Input, maskColor);

      sph.DefineSqlParameter("@RequestADemoImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, requestADemoImageUrl);
      sph.DefineSqlParameter("@QuantifiLogoImageUrl", SqlDbType.NVarChar, 255, ParameterDirection.Input, quantifiLogoImageUrl);


      sph.DefineSqlParameter("@TopNavColorCss", SqlDbType.NVarChar, 255, ParameterDirection.Input, topNavColorCss);
      sph.DefineSqlParameter("@TopSubNavColorCss", SqlDbType.NVarChar, 255, ParameterDirection.Input, topSubNavColorCss);

      sph.ExecuteNonQuery();

    }

    /// <summary>
    /// Delete By ModuleId
    /// </summary>
    /// <param name="mId"></param>
    public static void Delete(int id)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_Delete", 1);

      sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);

      sph.ExecuteNonQuery();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static IDataReader Get(int id)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_Select", 1);
      sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);
      return sph.ExecuteReader();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageId"></param>
    /// <param name="isActive"></param>
    /// <returns></returns>
    public static IDataReader GetByPage(int pageId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_SelectByPage", 1);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);

      return sph.ExecuteReader();
    }



    public static IDataReader GetByPriority(int pageId, int priority)
    {

      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_SelectByPriority", 2);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
      sph.DefineSqlParameter("@Priority", SqlDbType.Int, ParameterDirection.Input, priority);

      return sph.ExecuteReader();
    }

    public static int GetNextPriority(int pageId, int currentPriority)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_GetNextPriority", 2);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
      sph.DefineSqlParameter("@CurrentPriority", SqlDbType.Int, ParameterDirection.Input, currentPriority);

      object obj = sph.ExecuteScalar();

      if (obj == null)
        return currentPriority + 1;

      return Convert.ToInt32(obj);

    }

    public static int GetPreviousPriority(int pageId, int currentPriority)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_GetPreviousPriority", 2);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
      sph.DefineSqlParameter("@CurrentPriority", SqlDbType.Int, ParameterDirection.Input, currentPriority);

      object obj = sph.ExecuteScalar();

      if (obj == null)
        return currentPriority - 1;

      return Convert.ToInt32(obj);
    }

    public static int GetHighestPriority(int pageId)
    {

      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_GetHighestPriority", 1);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);


      object obj = sph.ExecuteScalar();

      if (obj == null)
        return 1;

      return Convert.ToInt32(obj);
    }

    public static IDataReader GetPage(int pageId, int pageNumber, int pageSize)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_SelectPage", 3);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);
      sph.DefineSqlParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
      sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
      return sph.ExecuteReader();

    }

    public static int GetCount(int pageId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_MainContent_GetCount", 1);
      sph.DefineSqlParameter("@PageId", SqlDbType.Int, ParameterDirection.Input, pageId);

      return Convert.ToInt32(sph.ExecuteScalar());

    }



  }
}
