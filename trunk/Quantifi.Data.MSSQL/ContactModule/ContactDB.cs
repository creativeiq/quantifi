﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using mojoPortal.Data;
using System.Configuration;

namespace Quantifi.Data
{
  public static class ContactDB
  {
    private static string GetConnectionString()
    {
      return ConfigurationManager.AppSettings["MSSQLConnectionString"];
    }


    public static int Insert(int mId, string topContent, string thankYouContent)
    {


      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Contact_Insert", 4);
      sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, mId);
      sph.DefineSqlParameter("@TopContent", SqlDbType.NText, ParameterDirection.Input, topContent);
      sph.DefineSqlParameter("@ThankYouContent", SqlDbType.NText, ParameterDirection.Input, thankYouContent);
      sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.InputOutput, null);
      sph.ExecuteNonQuery();

      int newID = Convert.ToInt32(sph.Parameters[3].Value);
      return newID;
    }

    public static void Update(int id, int mId, string topContent, string thankYouContent)
    {

      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Contact_Update", 4);
      sph.DefineSqlParameter("@ItemId", SqlDbType.Int, ParameterDirection.Input, id);
      sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, mId);
      sph.DefineSqlParameter("@TopContent", SqlDbType.NText, ParameterDirection.Input, topContent);
      sph.DefineSqlParameter("@ThankYouContent", SqlDbType.NText, ParameterDirection.Input, thankYouContent);

      
      sph.ExecuteNonQuery();

    }

    /// <summary>
    /// Delete By ModuleId
    /// </summary>
    /// <param name="mId"></param>
    public static void DeleteByModule(int mId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Contact_Delete", 1);

      sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, mId);


      sph.ExecuteNonQuery();
    }



    public static IDataReader Get(int mId)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Contact_SelectByModuleId", 1);
      sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, mId);
 
      return sph.ExecuteReader();
    }


     

  }
}
