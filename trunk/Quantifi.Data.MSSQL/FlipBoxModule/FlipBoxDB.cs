﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using mojoPortal.Data;
using System.Configuration;

namespace Quantifi.Data.FlipBoxModule
{

	public static class FlipBoxDB
	{

		/// <summary>
		/// Gets the connection string.
		/// </summary>
		/// <returns></returns>
		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static IDataReader Get(int id)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_FlipBox_Get", 1);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);
			return sph.ExecuteReader();

		}
 


		public static int Insert(int moduleId, int priority, string imageUrl, string imageAltText, string description, string linkText, string linkUrl, bool isExtLink)
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_FlipBox_Insert", 9);

			sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, moduleId);
			sph.DefineSqlParameter("@Priority", SqlDbType.Int, ParameterDirection.Input, priority);
			sph.DefineSqlParameter("@ImageUrl", SqlDbType.NVarChar, 500, ParameterDirection.Input, imageUrl);
			sph.DefineSqlParameter("@ImageAltText", SqlDbType.NVarChar, 500, ParameterDirection.Input, imageAltText);
			sph.DefineSqlParameter("@Description", SqlDbType.NText, ParameterDirection.Input, description);
			sph.DefineSqlParameter("@LinkText", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkText);
			sph.DefineSqlParameter("@LinkUrl", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkUrl);
			sph.DefineSqlParameter("@isExtLink", SqlDbType.Bit, ParameterDirection.Input, isExtLink);
 

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.InputOutput, null);


			sph.ExecuteNonQuery();
			int newID = Convert.ToInt32(sph.Parameters.Last().Value);
			return newID;
		}


		public static bool Update(int id, int moduleId, int priority, string imageUrl, string imageAltText, string description, string linkText, string linkUrl, bool isExtLink)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_FlipBox_Update", 9);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);
			sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, moduleId);
			sph.DefineSqlParameter("@Priority", SqlDbType.Int, ParameterDirection.Input, priority);
			sph.DefineSqlParameter("@ImageUrl", SqlDbType.NVarChar, 500, ParameterDirection.Input, imageUrl);
			sph.DefineSqlParameter("@ImageAltText", SqlDbType.NVarChar, 500, ParameterDirection.Input, imageAltText);
			sph.DefineSqlParameter("@Description", SqlDbType.NText, ParameterDirection.Input, description);
			sph.DefineSqlParameter("@LinkText", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkText);
			sph.DefineSqlParameter("@LinkUrl", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkUrl);
			sph.DefineSqlParameter("@isExtLink", SqlDbType.Bit, ParameterDirection.Input, isExtLink);



			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static bool Delete(int id)
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_FlipBox_Delete", 1);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="moduleId"></param>
		/// <returns></returns>
		public static bool DeleteByModule(int moduleId)
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_FlipBox_DeleteByModule", 1);

			sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}



		public static IDataReader GetByModule(int moduleId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_FlipBox_GetByModuleId", 1);

			sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, moduleId);

			return sph.ExecuteReader();

		}




	}


}
