﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using mojoPortal.Data;
using System.Configuration;

namespace Quantifi.Data.TagsModule
{

	public static class ContentTagsDB
	{
		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];

		}

		public static int InsertTag(string text)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_Insert", 2);
			

			sph.DefineSqlParameter("@Text", SqlDbType.NVarChar, 255, ParameterDirection.Input, text);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.InputOutput, null);

			sph.ExecuteNonQuery();
			int newID = Convert.ToInt32(sph.Parameters.Last().Value);
			return newID;
		}

		public static bool UpdateTag(int id, string text)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_Update", 2);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);
			sph.DefineSqlParameter("@Text", SqlDbType.NVarChar, 255, ParameterDirection.Input, text);


			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}

		public static bool DeleleTag(int id)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_Delete", 1);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);
 
			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}

		public static IDataReader GetTag(int id)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetById", 1);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);

			return sph.ExecuteReader();

		}

		public static IDataReader GetAll()
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetAll", 0);

			return sph.ExecuteReader();
		}

		public static IDataReader GetByTaggedCount()
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetByTaggedCount", 0);

			return sph.ExecuteReader();
		}

		public static IDataReader GetTagsByText(string searchText)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetByText", 1);
			sph.DefineSqlParameter("@SearchText", SqlDbType.NVarChar, 255, ParameterDirection.Input, searchText);
			return sph.ExecuteReader();
		}

    public static IDataReader GetTopTagsByDate(DateTime date, int count)
    {
      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetTopTagsByDate", 2);

      sph.DefineSqlParameter("@Expired", SqlDbType.DateTime, ParameterDirection.Input, date);
      sph.DefineSqlParameter("@Count", SqlDbType.Int, ParameterDirection.Input, count);

      return sph.ExecuteReader();

    }


		#region ContentItem

		public static IDataReader GetContentItemTags(int itemId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetContentItemTags", 1);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);
			return sph.ExecuteReader();
		}

		public static bool DeleteAllTagsForContentItem(int itemId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_DeleteAllTagsForContentItem", 1);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);
		}

		public static bool AddTagForContentItem(int itemId, int tagId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_AddTagForContentItem", 2);

			sph.DefineSqlParameter("@TagID", SqlDbType.Int, ParameterDirection.Input, tagId);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}

		#endregion

		#region BlogPost

		public static IDataReader GetBlogPostTags(int blogId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetBlogPostTags", 1);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, blogId);
			return sph.ExecuteReader();
		}

		public static bool DeleteAllTagsForBlogPost(int itemId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_DeleteAllTagsForBlogPost", 1);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);
		}

		public static bool AddTagForBlogPost(int itemId, int tagId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_AddTagForBlogPost", 2);

			sph.DefineSqlParameter("@TagID", SqlDbType.Int, ParameterDirection.Input, tagId);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}


		#endregion

	}
}
