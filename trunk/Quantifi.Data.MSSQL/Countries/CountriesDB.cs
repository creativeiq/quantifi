﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using mojoPortal.Data;
using System.Configuration;

namespace Quantifi.Data.Countries
{
	public static class CountriesDB
	{
		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];
		}


		public static IDataReader GetAll()
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Countries_SelectAll", 0);

			return sph.ExecuteReader();
		}


	}
}
