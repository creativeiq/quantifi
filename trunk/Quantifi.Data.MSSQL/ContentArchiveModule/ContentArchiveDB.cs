﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using mojoPortal.Data;
using System.Configuration;

namespace Quantifi.Data.ContentArchiveModule
{

	public static class ContentArchiveDB
	{

		/// <summary>
		/// Gets the connection string.
		/// </summary>
		/// <returns></returns>
		private static string GetConnectionString()
		{
			return ConfigurationManager.AppSettings["MSSQLConnectionString"];

		}


		public static IDataReader Get(int id)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_Get", 1);
			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);
			return sph.ExecuteReader();

		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static int Insert(int moduleId, DateTime startDate,
															string title, string subTitle, string desc,
															string linkDesc, string linkText, string linkUrl,
															bool isExtLink, string tags)
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_Insert", 11);

			sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, moduleId);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
			sph.DefineSqlParameter("@Title", SqlDbType.NVarChar, 500, ParameterDirection.Input, title);
			sph.DefineSqlParameter("@SubTitle", SqlDbType.NVarChar, 500, ParameterDirection.Input, subTitle);
			sph.DefineSqlParameter("@Desc", SqlDbType.NText, ParameterDirection.Input, desc);
			sph.DefineSqlParameter("@LinkDesc", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkDesc);
			sph.DefineSqlParameter("@LinkText", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkText);
			sph.DefineSqlParameter("@LinkUrl", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkUrl);
			sph.DefineSqlParameter("@isExtLink", SqlDbType.Bit, ParameterDirection.Input, isExtLink);
			sph.DefineSqlParameter("@Tags", SqlDbType.NVarChar, 500, ParameterDirection.Input, tags);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.InputOutput, null);


			sph.ExecuteNonQuery();
			int newID = Convert.ToInt32(sph.Parameters.Last().Value);
			return newID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="text"></param>
		/// <returns></returns>
		public static bool Update(int id, int moduleId, DateTime startDate,
															string title, string subTitle, string desc,
															string linkDesc, string linkText, string linkUrl,
															bool isExtLink, string tags)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_Update", 11);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);
			sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, moduleId);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
			sph.DefineSqlParameter("@Title", SqlDbType.NVarChar, 500, ParameterDirection.Input, title);
			sph.DefineSqlParameter("@SubTitle", SqlDbType.NVarChar, 500, ParameterDirection.Input, subTitle);
			sph.DefineSqlParameter("@Desc", SqlDbType.NText, ParameterDirection.Input, desc);
			sph.DefineSqlParameter("@LinkDesc", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkDesc);
			sph.DefineSqlParameter("@LinkText", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkText);
			sph.DefineSqlParameter("@LinkUrl", SqlDbType.NVarChar, 500, ParameterDirection.Input, linkUrl);
			sph.DefineSqlParameter("@isExtLink", SqlDbType.Bit, ParameterDirection.Input, isExtLink);
			sph.DefineSqlParameter("@Tags", SqlDbType.NVarChar, 500, ParameterDirection.Input, tags);



			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}


		public static bool Delete(int id, int moduleId)
		{

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_Delete", 1);

			sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}

		public static IDataReader GetAll(int moduleId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_GetByModuleId", 1);

			sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, moduleId);

			return sph.ExecuteReader();

		}

		public static IDataReader GetLatest(int moduleId, int count, DateTime currentTime)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_GetLatest", 3);

			sph.DefineSqlParameter("@ModuleId", SqlDbType.Int, ParameterDirection.Input, moduleId);
			sph.DefineSqlParameter("@Count", SqlDbType.Int, ParameterDirection.Input, count);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, currentTime);

			return sph.ExecuteReader();

		}

		public static IDataReader GetPageByTag(
															int tagId,
															DateTime currentTime,
															int pageNumber,
															int pageSize,
															out int totalPages)
		{
			totalPages = 1;

			int totalRows = GetCountByTag(tagId, currentTime);

			if (pageSize > 0) totalPages = totalRows / pageSize;

			if (totalRows <= pageSize)
			{
				totalPages = 1;
			}
			else
			{
				int remainder;
				Math.DivRem(totalRows, pageSize, out remainder);
				if (remainder > 0)
				{
					totalPages += 1;
				}
			}

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_GetPageByTag", 4);
			sph.DefineSqlParameter("@TagID", SqlDbType.Int, ParameterDirection.Input, tagId);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, currentTime);
			sph.DefineSqlParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
			sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
			return sph.ExecuteReader();

		}

		public static int GetCountByTag(int tagId, DateTime currentTime)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_GetCountByTag", 2);

			sph.DefineSqlParameter("@TagID", SqlDbType.Int, ParameterDirection.Input, tagId);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, currentTime);

			return Convert.ToInt32(sph.ExecuteScalar());

		}

		public static IDataReader GetPage(
													int moduleId,
													DateTime currentTime,
													int pageNumber,
													int pageSize,
													out int totalPages)
		{
			totalPages = 1;

			int totalRows = GetCountByModule(moduleId, currentTime);

			if (pageSize > 0) totalPages = totalRows / pageSize;

			if (totalRows <= pageSize)
			{
				totalPages = 1;
			}
			else
			{
				int remainder;
				Math.DivRem(totalRows, pageSize, out remainder);
				if (remainder > 0)
				{
					totalPages += 1;
				}
			}

			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_GetPageByModule", 4);
			sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, currentTime);
			sph.DefineSqlParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
			sph.DefineSqlParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
			return sph.ExecuteReader();

		}


		public static int GetCountByModule(int moduleId, DateTime currentTime)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_GetCountByModule", 2);

			sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
			sph.DefineSqlParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, currentTime);

			return Convert.ToInt32(sph.ExecuteScalar());

		}


		public static IDataReader GetByTag(
													int tagId,
													DateTime currentTime)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_GetByTag", 2);
			sph.DefineSqlParameter("@TagID", SqlDbType.Int, ParameterDirection.Input, tagId);
			sph.DefineSqlParameter("@CurrentTime", SqlDbType.DateTime, ParameterDirection.Input, currentTime);
			return sph.ExecuteReader();
		}


		public static bool DeleteByModule (int moduleId)
		{
			SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ContentItem_DeleteByModule", 1);
			sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);

			int rowsAffected = sph.ExecuteNonQuery();
			return (rowsAffected > -1);

		}






	}

	//public static class TagsDB
	//{
	//  private static string GetConnectionString()
	//  {
	//    return ConfigurationManager.AppSettings["MSSQLConnectionString"];

	//  }

	//  public static int InsertTag(string text)
	//  {
	//    SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_Insert", 2);

	//    sph.DefineSqlParameter("@Text", SqlDbType.NVarChar, 255, ParameterDirection.Input, text);
	//    sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.InputOutput, null);


	//    sph.ExecuteNonQuery();
	//    int newID = Convert.ToInt32(sph.Parameters.Last().Value);
	//    return newID;
	//  }

	//  public static bool UpdateTag(int id, string text, int taggedCount)
	//  {
	//    SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_Update", 3);

	//    sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, id);
	//    sph.DefineSqlParameter("@Text", SqlDbType.NVarChar, 255, ParameterDirection.Input, text);
	//    sph.DefineSqlParameter("@TaggedCount", SqlDbType.Int, ParameterDirection.Input, taggedCount);


	//    int rowsAffected = sph.ExecuteNonQuery();
	//    return (rowsAffected > -1);

	//  }

	//  public static IDataReader GetAll()
	//  {
	//    SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetAll", 0);

	//    return sph.ExecuteReader();
	//  }

	//  public static IDataReader GetTagsByText(string searchText)
	//  {
	//    SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetByText", 1);
	//    sph.DefineSqlParameter("@SearchText", SqlDbType.NVarChar, 255, ParameterDirection.Input, searchText);
	//    return sph.ExecuteReader();
	//  }

	//  public static IDataReader GetContentItemTags(int itemId)
	//  {
	//    SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_GetContentItemTags", 1);
	//    sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);
	//    return sph.ExecuteReader();
	//  }

	//  public static bool DeleteAllTagsForContentItem(int itemId)
	//  {
	//    SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_DeleteAllTagsForContentItem", 1);
	//    sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);

	//    int rowsAffected = sph.ExecuteNonQuery();
	//    return (rowsAffected > -1);
	//  }

	//  public static bool AddTagForContentItem(int tagId, int itemId)
	//  {
	//    SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_Tags_AddTagForContentItem", 2);

	//    sph.DefineSqlParameter("@TagID", SqlDbType.Int, ParameterDirection.Input, tagId);
	//    sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);

	//    int rowsAffected = sph.ExecuteNonQuery();
	//    return (rowsAffected > -1);

	//  }




	//}

}
