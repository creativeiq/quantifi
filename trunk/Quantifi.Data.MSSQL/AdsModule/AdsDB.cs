﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using mojoPortal.Data;

namespace Quantifi.Data
{
  public static class AdsDB 
  {
    /// <summary>
    /// Gets the connection string.
    /// </summary>
    /// <returns></returns>
    private static string GetConnectionString()
    {
      return ConfigurationManager.AppSettings["MSSQLConnectionString"];
    }



    public static IDataReader GetAd(int itemId)
    {
      throw new NotImplementedException();

      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_SelectOne", 1);
      sph.DefineSqlParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemId);
      return sph.ExecuteReader();
    }

    public static IDataReader GetAds(int moduleId)
    {
      throw new NotImplementedException();

      SqlParameterHelper sph = new SqlParameterHelper(GetConnectionString(), "quantifi_ImageLinks_Select", 1);
      sph.DefineSqlParameter("@ModuleID", SqlDbType.Int, ParameterDirection.Input, moduleId);
      return sph.ExecuteReader();
    }

    public static int CreateAd()
    {
      return 0;
    }

    public static int UpdateAd()
    {
      return 0;
    }
    
    public static void DeleteAd(int itemId)
    {
      

    }

  }
}
