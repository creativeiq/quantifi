﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quantifi.Shared.FormatProviders;


namespace Quantifi.Shared
{

  public static class FileSizeExtensions
  {
    public static string ToFileSize(this long l)
    {
      return String.Format(new FileSizeFormatProvider(), "{0:fs}", l);
    }
  }

}
