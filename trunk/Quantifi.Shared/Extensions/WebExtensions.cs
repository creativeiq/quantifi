﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;

namespace Quantifi.Shared
{
	public static class WebExtensions
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string HtmlEncode(this string text)
		{
			return HttpUtility.HtmlEncode(text);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string HtmlDecode(this string text)
		{
			return HttpUtility.HtmlDecode(text);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string UrlEncode(this string text)
		{
			return HttpUtility.UrlEncode(text);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string UrlDecode(this string text)
		{
			return HttpUtility.UrlDecode(text);
		}




		/// <summary>
		/// Strips HTML, CSS, and Script tags from string 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string StripHTML(this string text)
		{
			return StripHTML(text, false);
		}

		/// <summary>
		/// Strips HTML, CSS, and Script tags from string  
		/// </summary>
		/// <param name="text"></param>
		/// <param name="leaveSafeHtml">leave some of the safer html in the string if true
		/// Not Safe Elements Removed: script|embed|object|frameset|frame|iframe|meta|link|style
		/// </param>
		/// <returns></returns>
		public static string StripHTML(this string text, bool leaveSafeHtml)
		{
			if (leaveSafeHtml)
				return Regex.Replace(text, @"</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>", "");
			//
			return Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
		}

    /// <summary>
    /// Strips Script tags from string  
    /// </summary>
    /// <param name="html"></param>
    /// <returns></returns>
		public static string StripJSTags(this string html)
		{
      return Regex.Replace(html, @"<script[\d\D]*?>[\d\D]*?</script>", "", RegexOptions.Singleline);
		}

    /// <summary>
    /// Strips CDATA from string  
    /// </summary>
    /// <param name="html"></param>
    /// <returns></returns>
		public static string StripCDATA(this string html)
		{
      return Regex.Replace(html, @"\<\!\[CDATA\[(?<text>[^\]]*)\]\]\>", "", RegexOptions.Singleline);
		}
	}
}
