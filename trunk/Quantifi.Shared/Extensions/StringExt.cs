﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Web;
using log4net;


namespace Quantifi.Shared
{
  public static class StringExtensions
  {
    private static readonly ILog log = LogManager.GetLogger(typeof(StringExtensions));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="toEncode"></param>
    /// <returns></returns>
    static public string EncodeTo64(this string toEncode)
    {
      byte[] toEncodeAsBytes
            = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
      string returnValue
            = System.Convert.ToBase64String(toEncodeAsBytes);
      return returnValue;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="encodedData"></param>
    /// <returns></returns>
    static public string DecodeFrom64(this string encodedData)
    {
      byte[] encodedDataAsBytes
          = System.Convert.FromBase64String(encodedData);
      string returnValue =
         System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
      return returnValue;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="nvc"></param>
    /// <returns></returns>
    static public string ToQueryString(this NameValueCollection nvc)
    {
      if (nvc.Count == 0)
        return string.Empty;

      return "?" + string.Join("&", Array.ConvertAll(nvc.AllKeys, key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(nvc[key]))));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="s">string</param>
    /// <param name="length">length value the string must be grater than</param>
    /// <returns></returns>
    static public bool IsNullEmptyWhiteSpaceOfLength(this string s, int atleastlength)
    {
      return (string.IsNullOrWhiteSpace(s) || s.Length < atleastlength);

    }


    /// <summary>
    /// Trim To Length
    /// </summary>
    /// <param name="s"></param>
    /// <param name="maxLength"></param>
    /// <param name="tail"></param>
    /// <returns></returns>
    static public string TrimToLength(this string s, int maxLength = 75, string tail = "...")
    {
      if (string.IsNullOrWhiteSpace(s))
        return string.Empty;

      var maxChars = (maxLength - tail.Length);
      if (s.Length > maxChars)
      {
        return s.Substring(0, maxChars).TrimEnd() + tail;
      }

      return s;

    }



    /// <summary>
    /// Remap International Chars To Ascii
    /// http://meta.stackoverflow.com/questions/7435/non-us-ascii-characters-dropped-from-full-profile-url/7696#7696
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    public static string RemapInternationalCharToAscii(this char c)
    {
      string s = c.ToString().ToLowerInvariant();
      if ("àåáâäãåą".Contains(s))
      {
        return "a";
      }
      else if ("èéêëę".Contains(s))
      {
        return "e";
      }
      else if ("ìíîïı".Contains(s))
      {
        return "i";
      }
      else if ("òóôõöøőð".Contains(s))
      {
        return "o";
      }
      else if ("ùúûüŭů".Contains(s))
      {
        return "u";
      }
      else if ("çćčĉ".Contains(s))
      {
        return "c";
      }
      else if ("żźž".Contains(s))
      {
        return "z";
      }
      else if ("śşšŝ".Contains(s))
      {
        return "s";
      }
      else if ("ñń".Contains(s))
      {
        return "n";
      }
      else if ("ýÿ".Contains(s))
      {
        return "y";
      }
      else if ("ğĝ".Contains(s))
      {
        return "g";
      }
      else if (c == 'ř')
      {
        return "r";
      }
      else if (c == 'ł')
      {
        return "l";
      }
      else if (c == 'đ')
      {
        return "d";
      }
      else if (c == 'ß')
      {
        return "ss";
      }
      else if (c == 'Þ')
      {
        return "th";
      }
      else if (c == 'ĥ')
      {
        return "h";
      }
      else if (c == 'ĵ')
      {
        return "j";
      }
      else
      {
        return "";
      }
    }


    /// <summary>
    /// Produces optional, URL-friendly version of a title, "like-this-one". 
    /// hand-tuned for speed, reflects performance refactoring contributed by John Gietzen (user otac0n) 
    /// http://stackoverflow.com/questions/25259/how-do-you-include-a-webpage-title-as-part-of-a-webpage-url/25486#25486
    /// </summary>
    public static string URLFriendly(this string title)
    {
      if (title == null) return "";

      const int maxlen = 80;
      int len = title.Length;
      bool prevdash = false;
      var sb = new StringBuilder(len);
      char c;

      for (int i = 0; i < len; i++)
      {
        c = title[i];
        if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
        {
          sb.Append(c);
          prevdash = false;
        }
        else if (c >= 'A' && c <= 'Z')
        {
          // tricky way to convert to lowercase
          sb.Append((char)(c | 32));
          prevdash = false;
        }
        else if (c == ' ' || c == ',' || c == '.' || c == '/' || c == '\\' || c == '-' || c == '_' || c == '=')
        {
          if (!prevdash && sb.Length > 0)
          {
            sb.Append('-');
            prevdash = true;
          }
        }
        else if ((int)c >= 128)
        {
          int prevlen = sb.Length;
          sb.Append(RemapInternationalCharToAscii(c));
          if (prevlen != sb.Length) prevdash = false;
        }
        if (i == maxlen) break;
      }

      if (prevdash)
        return sb.ToString().Substring(0, sb.Length - 1);
      else
        return sb.ToString();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="pageName"></param>
    /// <param name="siteSettings"></param>
    /// <returns></returns>
    //public static String SuggestFriendlyUrl(String pageName, mojoPortal.Business.SiteSettings siteSettings)
    //{
    //  String friendlyUrl = pageName.URLFriendly();
    //  if (mojoPortal.Web.WebConfigSettings.AlwaysUrlEncode)
    //  {
    //    friendlyUrl = HttpUtility.UrlEncode(friendlyUrl);
    //  }


    //  string urlTail = string.Empty;
    //  switch (siteSettings.DefaultFriendlyUrlPattern)
    //  {
    //    case mojoPortal.Business.SiteSettings.FriendlyUrlPattern.PageNameWithDotASPX:
    //      urlTail = ".aspx";
    //      break;

    //  }



    //  var tempFriendlyUrl = friendlyUrl + urlTail;
    //  int i = 1;
    //  while (mojoPortal.Business.FriendlyUrl.Exists(siteSettings.SiteId, tempFriendlyUrl))
    //  {
    //    tempFriendlyUrl = friendlyUrl + "-" + i.ToString() + urlTail;
    //    i++;
    //  }
    //  friendlyUrl = tempFriendlyUrl;


    //  if (mojoPortal.Web.WebConfigSettings.ForceFriendlyUrlsToLowerCase) { return friendlyUrl.ToLower(); }

    //  return friendlyUrl;
    //}


    /// <summary>
    /// 
    /// </summary>
    /// <param name="text"></param>
    /// <param name="search"></param>
    /// <param name="replace"></param>
    /// <param name="comparisonType"></param>
    /// <returns></returns>
    public static string ReplaceFirst(this string text, string search, string replace, StringComparison comparisonType = StringComparison.InvariantCultureIgnoreCase)
    {
      int pos = text.IndexOf(search, comparisonType);
      if (pos < 0)
      {
        return text;
      }
      return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="text"></param>
    /// <param name="search"></param>
    /// <param name="replace"></param>
    /// <param name="comparisonType"></param>
    /// <returns></returns>
    public static string ReplaceFirstInHtml(this string text, string search, string replace, StringComparison comparisonType = StringComparison.InvariantCultureIgnoreCase)
    {
      if (string.IsNullOrWhiteSpace(text))
        return string.Empty;

      if (string.IsNullOrWhiteSpace(search))
        return string.Empty;

      var inHtmlNode = true;
      var inHref = true;
      var length = text.Length;
      var matched = 0;
      var matchedIndex = 0;

      try
      {
        for (var i = 0; i < length; i++)
        {
          var c = text[i];
          var charactor = c.ToString();
          if (string.Equals(charactor, "<", comparisonType))
          {
            inHtmlNode = false;
            matched = 0;
            matchedIndex = 0;

            var htmlElementStart = text.Substring(i, 3);
            inHref = htmlElementStart.Equals("<a ", comparisonType);
            if (inHref)
            {
              var endIndex = text.IndexOf("</a>", i, comparisonType);

              i = endIndex + 4;

              inHtmlNode = true;
              matched = 0;
              matchedIndex = 0;
              continue;
            }

          }
          else if (string.Equals(charactor, ">", comparisonType))
          {
            inHtmlNode = true;
            matched = 0;
            matchedIndex = 0;
          }

          // keep moving...
          if (!inHtmlNode)
          {
            continue;
          }


          var toMatch = search[matched].ToString();
          if (!string.Equals(charactor, toMatch, comparisonType))
          {
            matched = 0;
            matchedIndex = 0;
            continue;
          }

          if (matched == 0)
          {
            matchedIndex = i;
          }

          matched++;

          // hurray!
          if (matched == search.Length)
          {
            // check if next char is not a letter
            // VAR should not replace vary
            if (i + 1 < length)
            {
              var nextChar = text[i + 1].ToString().ToUpperInvariant();

              if (SimpleAlphabet.Contains(nextChar))
              {
                matched = 0;
                matchedIndex = 0;
                continue;
              }
            }


            return text.Substring(0, matchedIndex) + replace + text.Substring(matchedIndex + search.Length);
          }
        }


      }
      catch (Exception ex)
      {
        log.Error(ex);

      }

      return text;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="html"></param>
    /// <param name="search"></param>
    /// <param name="replace"></param>
    /// <returns></returns>
    public static string AddTagToHtmlContent(string html, string search, string replace)
    {
      var excludeParentNodesList = new List<string>() {"a", "img"};
      var hd = new HtmlAgilityPack.HtmlDocument();

      try
      {
        hd.LoadHtml(html);

        var doc = hd.DocumentNode;

        // Search all nodes for the text content, this is case-sensitive :(
        var subNodes = doc.SelectNodes("//text()[contains(., '" + search + "')]");

        foreach (var node in subNodes)
        {

          var parentNode = node.ParentNode;
          var exitNode = false;
          while (parentNode != null)
          { 
            if (excludeParentNodesList.Contains(parentNode.Name))
            {
              exitNode = true;
              break;
            }

            parentNode = parentNode.ParentNode;
          }

          if (exitNode)
            continue;

          var rgx = new Regex("(\\W)?" + search.ToLowerInvariant() + "(\\W)?",
            RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant);


          node.InnerHtml = rgx.Replace(node.InnerHtml, replace, 1);

          break;
        }

        return hd.DocumentNode.InnerHtml;
      }
      catch (Exception ex)
      {
        log.Error(ex);
      }

      return html;
    }


    public static string SimpleAlphabet
    {
      get { return "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; }
    }


    public static string RemoveScriptTags(this string text)
    {

      var options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
      var regex = new Regex(@"<script.*?>.*?</script>", options);

      return regex.Replace(text, "<!-- removed script tags -->");
      //return text;

    }
  }
}
