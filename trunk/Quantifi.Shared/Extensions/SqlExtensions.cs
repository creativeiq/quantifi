﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlTypes;


namespace Quantifi.Shared
{
  public static class SqlExtensions
  {
    public static DateTime ToDateTime(this SqlDateTime time)
    {
      return DateTime.Parse(time.ToString());
    }

    /// <summary>
    /// Protect Against DateTime Min Max
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public static SqlDateTime ToSqlDateTime(this DateTime time)
    {

      if (DateTime.Equals(DateTime.MinValue, time))
        return SqlDateTime.MinValue;


      if (DateTime.Equals(DateTime.MaxValue, time))
        return SqlDateTime.MaxValue;

      return SqlDateTime.Parse(time.ToString());
    }

  }
}
