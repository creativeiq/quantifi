﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Diagnostics;
using System.Web;

namespace Quantifi.Shared
{
  public static class WebUIExtensions
  {

    #region Button Extensions

    /// <summary>
    /// Disable button with a JavaScript function call.
    /// </summary>
    /// <param name="buttonControl"></param>
    /// <param name="clientFunction">Name of JS function to call</param>
    public static void DisableButtonOnClick(this Button buttonControl, string clientFunction = "")
    {
      DisableButtonOnClick(buttonControl as WebControl, buttonControl.CausesValidation, buttonControl.ValidationGroup, clientFunction);
    }

    /// <summary>
    /// Disable button with a JavaScript function call.
    /// </summary>
    /// <param name="buttonControl"></param>
    /// <param name="clientFunction">Name of JS function to call</param>
    public static void DisableButtonOnClick(this LinkButton buttonControl, string clientFunction = "")
    {
      DisableButtonOnClick(buttonControl as WebControl, buttonControl.CausesValidation, buttonControl.ValidationGroup, clientFunction);
    }

    /// <summary>
    /// Disable button with a JavaScript function call.
    /// </summary>
    /// <param name="buttonControl"></param>
    /// <param name="clientFunction">Name of JS function to call</param>
    public static void DisableButtonOnClick(this ImageButton buttonControl, string clientFunction = "")
    {
      DisableButtonOnClick(buttonControl as WebControl, buttonControl.CausesValidation, buttonControl.ValidationGroup, clientFunction);
    }

    private static void DisableButtonOnClick(this WebControl buttonControl, bool causesValidation, string validationGroup, string clientFunction)
    {
      StringBuilder sb = new StringBuilder(128);

      // If the page has ASP.NET validators on it, this code ensures the
      // page validates before continuing.
      if (causesValidation && !String.IsNullOrEmpty(validationGroup))
      {
        sb.Append("if ( typeof( Page_ClientValidate ) == 'function' ) { ");
        sb.AppendFormat(@"if ( ! Page_ClientValidate('{0}') ) {{ return false; }} }} ", validationGroup);
      }
      // Disable this button.
      sb.Append("this.disabled = true; this.value='Please Wait'; ");

      // If a secondary JavaScript function has been provided, and if it can be found,
      // call it. Note the name of the JavaScript function to call should be passed without
      // parens.
      if (!String.IsNullOrEmpty(clientFunction))
      {
        sb.AppendFormat("if ( typeof( {0} ) == 'function') {{ {0}() }};", clientFunction);
      }

      // GetPostBackEventReference() obtains a reference to a client-side script function 
      // that causes the server to post back to the page (ie this causes the server-side part 
      // of the "click" to be performed).

      sb.Append(buttonControl.Page.ClientScript.GetPostBackEventReference(buttonControl, string.Empty).Replace("\"", "'") + ";");
      //sb.Append( ButtonControl.Page.GetPostBackEventReference(ButtonControl ) + ";");

      // Add the JavaScript created a code to be executed when the button is clicked.
      buttonControl.Attributes.Add("onclick", sb.ToString());
    }



    /// <summary>
    /// Adds ClientSide confirm and if true: disable button, run clientfunction ( if provided ), then postback
    /// </summary>
    /// <param name="buttonControl">Button to apply client side logic to</param>
    /// <param name="confirmMessage"> Message in Confirm Pop up
    /// </param>
    /// <param name="clientFunction">
    /// If a secondary JavaScript function has been provided, and if it can be found,
    /// call it. Note the name of the JavaScript function to call should be passed without parens.
    /// Use String.Empty to call no function
    /// </param>
    public static void ConfirmThenDisableButtonOnClick(this Button buttonControl, string confirmMessage, string clientFunction = "")
    {
      ConfirmThenDisableButtonOnClick(buttonControl as WebControl, buttonControl.CausesValidation, buttonControl.ValidationGroup, confirmMessage, clientFunction);
    }
    /// <summary>
    /// Adds ClientSide confirm and if true: disable button, run clientfunction ( if provided ), then postback
    /// </summary>
    /// <param name="buttonControl">Button to apply client side logic to</param>
    /// <param name="confirmMessage"> Message in Confirm Pop up
    /// </param>
    /// <param name="clientFunction">
    /// If a secondary JavaScript function has been provided, and if it can be found,
    /// call it. Note the name of the JavaScript function to call should be passed without parens.
    /// Use String.Empty to call no function
    /// </param>
    public static void ConfirmThenDisableButtonOnClick(this LinkButton buttonControl, string confirmMessage, string clientFunction = "")
    {
      ConfirmThenDisableButtonOnClick(buttonControl as WebControl, buttonControl.CausesValidation, buttonControl.ValidationGroup, confirmMessage, clientFunction);
    }
    /// <summary>
    /// Adds ClientSide confirm and if true: disable button, run clientfunction ( if provided ), then postback
    /// </summary>
    /// <param name="buttonControl">Button to apply client side logic to</param>
    /// <param name="confirmMessage"> Message in Confirm Pop up
    /// </param>
    /// <param name="clientFunction">
    /// If a secondary JavaScript function has been provided, and if it can be found,
    /// call it. Note the name of the JavaScript function to call should be passed without parens.
    /// Use String.Empty to call no function
    /// </param>
    public static void ConfirmThenDisableButtonOnClick(this ImageButton buttonControl, string confirmMessage, string clientFunction = "")
    {
      ConfirmThenDisableButtonOnClick(buttonControl as WebControl, buttonControl.CausesValidation, buttonControl.ValidationGroup, confirmMessage, clientFunction);
    }


    private static void ConfirmThenDisableButtonOnClick(this WebControl buttonControl, bool causesValidation, string validationGroup, string confirmMessage, string clientFunction)
    {



      StringBuilder sb = new StringBuilder();

      // If the page has ASP.NET validators on it, this code ensures the
      // page validates before continuing.
      if (causesValidation && !String.IsNullOrEmpty(validationGroup))
      {
        sb.Append("if ( typeof( Page_ClientValidate ) == 'function' ) { ");
        sb.AppendFormat(@"if ( ! Page_ClientValidate('{0}') ) {{ return false; }} }} ", validationGroup);
      }
      string msg = HttpContext.Current.Server.HtmlEncode(confirmMessage);
      //pop confirm, if confirm==true, disable button
      sb.AppendFormat("if(confirm('{0}'))", msg);
      sb.Append("{this.disabled=true; this.value='Please Wait'; ");
      // If a secondary JavaScript function has been provided, and if it can be found,
      // call it. Note the name of the JavaScript function to call should be passed without
      // parens.
      if (!String.IsNullOrEmpty(clientFunction))
      {
        sb.AppendFormat("if ( typeof( {0} ) == 'function' ) {{ {0}() }};", clientFunction);
      }

      // GetPostBackEventReference() obtains a reference to a client-side script function 
      // that causes the server to post back to the page (ie this causes the server-side part 
      // of the "click" to be performed).


      sb.Append(buttonControl.Page.ClientScript.GetPostBackEventReference(buttonControl, string.Empty).Replace("\"", "'"));

      // if confirm==false do nothing
      sb.Append(";}else{return false;}");


      Debug.WriteLineIf(!string.IsNullOrEmpty(buttonControl.Attributes["onclick"]),
                          string.Format("ButtonOnClick Already Set: ", buttonControl.Attributes["onclick"]),
                          "fun?");


      buttonControl.Attributes.Add("onclick", sb.ToString());
    }


    #endregion

    public static string jQueryId (this Control c)
    {
      return "#" + c.ClientID;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="message"></param>
    public static void ShowInfoMessageBanner(this WebControl parent, string message)
    {
      ShowMessageBanner(parent, "info", "#819FF7", message);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="message"></param>
    public static void ShowSuccessMessageBanner(this WebControl parent, string message)
    {
      ShowMessageBanner(parent, "success", "#64FE2E", message);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="message"></param>
    public static void ShowErrorMessageBanner(this WebControl parent, string message)
    {
      ShowMessageBanner(parent, "error", "#FE2E2E", message);
    }


    private static void ShowMessageBanner(this WebControl parent, string cssclass, string borderColor, string message)
    {

      var script = "$(document).ready(function () {"
     + "$('#" + parent.ClientID + "')"
     + ".prepend(\"<div class='" + cssclass + "Msg' >" +  message.HtmlEncode() + "<a class='closebutton' href='#' role='button'><span >close</span></a></div>\")"
     + ".find('." + cssclass + "Msg').animate({ backgroundColor: '#fff', color: '#000', borderBottomColor: '" + borderColor + "', borderLeftColor: '" + borderColor + "', borderRightColor: '" + borderColor + "', borderTopColor: '" + borderColor + "'}, 1500)"
     + ".find('.closebutton').click(function () { $(this).parent().hide('slow'); });});";

      parent.Page.ClientScript.RegisterClientScriptBlock(parent.GetType(), cssclass + "Message", script, true);
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    public static void ScrollTo(this Control control)
    {
      if (control == null)
        return;

      var script = string.Format(@"

        <script type='text/javascript'> 

                $(document).ready(function() {{
                        var element = document.getElementById('{0}');
                        element.scrollIntoView();
                        element.focus();
                }});

        </script>

    ", control.ClientID);

      control.Page.ClientScript.RegisterClientScriptBlock(
        control.GetType(),
            "ScrollTo",
            script,
            false);

    }
  }
}
