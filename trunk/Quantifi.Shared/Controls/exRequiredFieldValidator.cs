﻿using System;
using System.Web.UI;

namespace Quantifi.Shared.Controls
{
  public class ExRequiredFieldValidator : System.Web.UI.WebControls.RequiredFieldValidator
  {
    protected override void OnPreRender(EventArgs e)
    {

      if (ErrorContent != null && string.IsNullOrWhiteSpace(Text))
      {
        this.Controls.Clear();

        ErrorContent.InstantiateIn(this);

      }
      
      base.OnPreRender(e);
    
    
    }

 
    [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
    public ITemplate ErrorContent
    {
      get;
      set;
    }

  }
}
