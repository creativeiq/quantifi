﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Quantifi.Shared.Controls
{
  

  /// <summary>
  /// Advance ASP.NET Repeater with EmptyTemplate, ShowHeaderWhenEmpty, ShowFooterWhenEmpty
  /// http://abhijitjana.net/2011/03/09/asp-net-custom-repeater-control-with-emptytemplate-showheaderwhenemptyshowfooterwhenemptyshowcount-properties/
  /// </summary>
  [ToolboxData("<{0}:ExRepeater runat=server>")]
  public class ExRepeater : Repeater
  {
    #region Private Properties
    
    private bool _ShowHeaderWhenEmpty = false;
    private bool _ShowFooterWhenEmpty = false;

    #endregion
    
    #region Repeater Properties

    /// <summary>
    /// Gets or sets a value indicating whether [show header when empty].
    /// </summary>
    /// <value>
    /// 	<c>true</c> if [show header when empty]; otherwise, <c>false</c>.
    /// </value>
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowHeaderWhenEmpty
    {
      get { return _ShowHeaderWhenEmpty; }
      set { _ShowHeaderWhenEmpty = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowFooterWhenEmpty
    {
      get { return _ShowFooterWhenEmpty; }
      set { _ShowFooterWhenEmpty = value; }
    }

    /// <summary>
    /// Gets or sets the empty template.
    /// </summary>
    /// <value>The empty template.</value>
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate EmptyTemplate
    {
      get;
      set;
    }



    #endregion

    #region Repeater Methods

    /// <summary>
    /// Creates the child controls.
    /// </summary>
    protected override void CreateChildControls()
    {
      base.CreateChildControls();
      EnabledEmptyTemplate();
    }

    /// <summary>
    /// Raises the DataBinding event.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
    protected override void OnDataBinding(EventArgs e)
    {
      base.OnDataBinding(e);
      EnabledEmptyTemplate();

    }

    /// <summary>
    /// Enables the empty template.
    /// </summary>
    private void EnabledEmptyTemplate()
    {
      if (this.Items.Count <= 0 && EmptyTemplate != null)
      {
        this.Controls.Clear();

        // Instantiate Header Template When Item count 0
        if (ShowHeaderWhenEmpty == true)
        {
          HeaderTemplate.InstantiateIn(this);
        }

        EmptyTemplate.InstantiateIn(this);
      }
    }

    #endregion
  }
}
