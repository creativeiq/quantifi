﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Linq.Expressions;
using System.Reflection;

namespace Quantifi.Shared
{
  public static class CollectionsUtils
  {

    public static bool IsNullOrEmpty<T>(this IEnumerable<T> items)
    {
      return items == null || !items.Any();
    }

    /// <summary>
    /// Takes all strings, calls Trim then appends the delimiter
    /// </summary>
    /// <param name="items">list of string</param>
    /// <param name="delimiter">Default ", "</param>
    /// <returns>string</returns>
    public static string ToDelimitedString(this IEnumerable<string> items, string delimiter = ", ")
    {
      if (items.IsNullOrEmpty())
        return string.Empty;


      StringBuilder sb = new StringBuilder();

      foreach (string s in items)
      {
        sb.Append(s.Trim() + delimiter);
      }

      return sb.ToString(0, sb.Length - delimiter.Length);
    }



    public static string ToDelimitedString<T>(this IEnumerable<T> items, string delimiter = ", ")
    {
      if (items.IsNullOrEmpty())
        return string.Empty;


      StringBuilder sb = new StringBuilder();

      foreach (T obj in items)
      {
        sb.Append(obj.ToString() + delimiter);
      }

      return sb.ToString(0, sb.Length - delimiter.Length);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="s"></param>
    /// <param name="delimiter"></param>
    /// <returns></returns>
    public static IEnumerable<string> FromDelimitedString(this string s, string delimiter = ",")
    {
      if (string.IsNullOrWhiteSpace(s))
        return Enumerable.Empty<string>();


      return s.Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries).Select(str => str.Trim());

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="s"></param>
    /// <param name="delimiter"></param>
    /// <returns></returns>
    public static IEnumerable<int> SplitStringIntoInts(this string s, string delimiter = ",")
    {


      var split = s.FromDelimitedString(delimiter);

      if (split.IsNullOrEmpty())
        return Enumerable.Empty<int>();

      var numbers = new List<int>();
      int parsed;

      foreach (string n in split)
      {
        if (int.TryParse(n.Trim(), out parsed))
          numbers.Add(parsed);
      }

      return numbers;


    }

    /// <summary>
    /// Shuffle List ( Base off predicatable Random() )
    /// http://stackoverflow.com/questions/273313/randomize-a-listt-in-c
    /// http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    public static void QuickShuffle<T>(this IList<T> list)
    {
      Random rng = new Random();
      int n = list.Count;
      while (n > 1)
      {
        n--;
        int k = rng.Next(n + 1);
        T value = list[k];
        list[k] = list[n];
        list[n] = value;
      }
    }


    /// <summary>
    /// Shuffle List ( Better randomization using RNGCryptoServiceProvider  )
    /// 
    /// http://blog.thijssen.ch/2010/02/when-random-is-too-consistent.html
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    public static void Shuffle<T>(this IList<T> list)
    {
      RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
      int n = list.Count;
      while (n > 1)
      {
        byte[] box = new byte[1];
        do provider.GetBytes(box);
        while (!(box[0] < n * (Byte.MaxValue / n)));
        int k = (box[0] % n);
        n--;
        T value = list[k];
        list[k] = list[n];
        list[n] = value;
      }
    }


    //http://stackoverflow.com/questions/41244/dynamic-linq-orderby
    public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
    {
      return ApplyOrder<T>(source, property, "OrderBy");
    }

    public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
    {
      return ApplyOrder<T>(source, property, "OrderByDescending");
    }

    public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
    {
      return ApplyOrder<T>(source, property, "ThenBy");
    }

    public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string property)
    {
      return ApplyOrder<T>(source, property, "ThenByDescending");
    }

    static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
    {
      string[] props = property.Split('.');
      Type type = typeof(T);
      ParameterExpression arg = Expression.Parameter(type, "x");
      Expression expr = arg;
      foreach (string prop in props)
      {
        // use reflection (not ComponentModel) to mirror LINQ
        PropertyInfo pi = type.GetProperty(prop);
        expr = Expression.Property(expr, pi);
        type = pi.PropertyType;
      }
      Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
      LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

      object result = typeof(Queryable).GetMethods().Single(
              method => method.Name == methodName
                      && method.IsGenericMethodDefinition
                      && method.GetGenericArguments().Length == 2
                      && method.GetParameters().Length == 2)
              .MakeGenericMethod(typeof(T), type)
              .Invoke(null, new object[] { source, lambda });
      return (IOrderedQueryable<T>)result;
    }


    //used by LINQ to SQL
    public static IQueryable<TSource> Page<TSource>(this IQueryable<TSource> source, int page, int pageSize)
    {
      return source.Skip((page - 1) * pageSize).Take(pageSize);
    }

    //used by LINQ
    public static IEnumerable<TSource> Page<TSource>(this IEnumerable<TSource> source, int page, int pageSize)
    {
      return source.Skip((page - 1) * pageSize).Take(pageSize);
    }


    //used by LINQ to SQL
    public static int GetTotalPages<TSource>(this IQueryable<TSource> source, int pageSize)
    {
      return GetTotalPages( source.Count(), pageSize);
    }

    //used by LINQ
    public static int GetTotalPages<TSource>(this IEnumerable<TSource> source, int pageSize)
    {
      return GetTotalPages(source.Count(), pageSize);
    }


    public static int GetTotalPages(int totalRows, int pageSize)
    {
      var totalPages = 1;

      if (pageSize > 0) totalPages = totalRows / pageSize;

      if (totalRows <= pageSize)
      {
        totalPages = 1;
      }
      else
      {
        int remainder;
        Math.DivRem(totalRows, pageSize, out remainder);
        if (remainder > 0)
        {
          totalPages += 1;
        }
      }

      return totalPages;
    }


  }
}