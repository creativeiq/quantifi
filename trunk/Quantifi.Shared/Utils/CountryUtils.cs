﻿using System;
using System.Globalization;
using System.Web;

namespace Quantifi.Shared
{

  /// <summary>
  /// Country Utils
  /// </summary>
  public static class CountryUtils
  {
    /// <summary>
    /// Resolves the culture.
    /// </summary>
    /// <returns></returns>
    public static CultureInfo ResolveCulture()
    {
      string[] languages = HttpContext.Current.Request.UserLanguages;
      if (languages == null || languages.Length == 0)
        return null;

      try
      {
        string language = languages[0].ToLowerInvariant().Trim();

        return CultureInfo.CreateSpecificCulture(language);
      }
      catch (ArgumentException)
      {
        return null;
      }
    }

    /// <summary>
    /// Resolves the country.
    /// </summary>
    /// <returns></returns>
    public static RegionInfo ResolveCountry()
    {
      CultureInfo culture = ResolveCulture();

      if (culture != null)
        return new RegionInfo(culture.LCID);

      return null;
    }
  }
}