﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;

namespace Quantifi.Shared
{
  public class SimpleSessionMessageUtil
  {
    private static string SessionKey = "QuickMsg";
    private static string MsgTypeInfo = "INFO";
    private static string MsgTypeSuccess = "SUCCESS";
    private static string MsgTypeError = "ERROR";
    private static string MsgDelimiter = ":";

    public static void AddInfoMessage(string msg)
    {

      HttpContext.Current.Session.Add(SessionKey, MsgTypeInfo + MsgDelimiter + msg);
    }
    public static void AddSuccessMessage(string msg)
    {
      HttpContext.Current.Session.Add(SessionKey, MsgTypeSuccess + MsgDelimiter + msg);
    }
    public static void AddErrorMessage(string msg)
    {
      HttpContext.Current.Session.Add(SessionKey, MsgTypeError + MsgDelimiter + msg);
    }

    public static void ShowMessageIfAny(Panel parentPanel)
    {
      object obj = HttpContext.Current.Session[SessionKey];

      if (obj == null)
        return;

      HttpContext.Current.Session.Remove(SessionKey);

      var data = obj.ToString();

      var msgType = data.Substring(0, (data.IndexOf(':')));
      var msg = data.Substring((data.IndexOf(':') + 1));

      if (string.Equals(msgType, MsgTypeInfo, StringComparison.InvariantCultureIgnoreCase))
      {
        parentPanel.ShowInfoMessageBanner(msg);
        return;
      }

      if (string.Equals(msgType, MsgTypeSuccess, StringComparison.InvariantCultureIgnoreCase))
      {
        parentPanel.ShowSuccessMessageBanner(msg);
        return;
      }

      if (string.Equals(msgType, MsgTypeError, StringComparison.InvariantCultureIgnoreCase))
      {
        parentPanel.ShowErrorMessageBanner(msg);
        return;
      }



    }
  }
}
