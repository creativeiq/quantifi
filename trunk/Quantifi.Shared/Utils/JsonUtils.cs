﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Quantifi.Shared
{
	public static class JsonUtils
	{

		public static string ToJson(this object obj)
		{
			JavaScriptSerializer serializer = new JavaScriptSerializer();
			return serializer.Serialize(obj);
		}

		public static string ToJson(this object obj, int recursionDepth)
		{
			JavaScriptSerializer serializer = new JavaScriptSerializer();
			serializer.RecursionLimit = recursionDepth;
			return serializer.Serialize(obj);
		}


	}
}