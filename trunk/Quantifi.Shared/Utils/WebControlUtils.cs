﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Quantifi.Shared
{
  public static class WebControlUtils
  {

    public static bool SetActiveListItem(this DropDownList dropDownList, string value )
    {
      var selectedListItem = dropDownList.Items.FindByValue(value);

      if (selectedListItem != null)
      {
        selectedListItem.Selected = true;
        return true;
      }
      return false;
    }


  }
}
