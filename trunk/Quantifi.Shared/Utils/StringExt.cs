﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web;

namespace Quantifi.Shared.Utils
{
	public static class StringExt
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="toEncode"></param>
		/// <returns></returns>
		static public string EncodeTo64(this string toEncode)
		{
			byte[] toEncodeAsBytes
						= System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
			string returnValue
						= System.Convert.ToBase64String(toEncodeAsBytes);
			return returnValue;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="encodedData"></param>
		/// <returns></returns>
		static public string DecodeFrom64(this string encodedData)
		{
			byte[] encodedDataAsBytes
					= System.Convert.FromBase64String(encodedData);
			string returnValue =
				 System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
			return returnValue;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="nvc"></param>
		/// <returns></returns>
		static public string ToQueryString(this NameValueCollection nvc)
		{
			if (nvc.Count == 0)
				return string.Empty;

			return "?" + string.Join("&", Array.ConvertAll(nvc.AllKeys, key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(nvc[key]))));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="s">string</param>
		/// <param name="length">length value the string must be grater than</param>
		/// <returns></returns>
		static public bool IsNullEmptyWhiteSpaceOfLength(this string s, int atleastlength)
		{
			return (string.IsNullOrWhiteSpace(s) || s.Length < atleastlength);
 
		}



    static public string TrimToLength(this string s, int maxLength = 75, string tail = "...")
    {
      if (string.IsNullOrWhiteSpace(s))
        return string.Empty;

      var maxChars = (maxLength - tail.Length);
      if (s.Length > maxChars)
      {
        return s.Substring(maxChars).Trim() + tail;
      }

      return s;

    }
	}
}
