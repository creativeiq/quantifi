﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Quantifi.Shared
{
  public static class EncryptedCookieUtils
  {

    public static void SetBase64Cookie(string name, string value)
    {
      if (HttpContext.Current == null)
        throw new NullReferenceException("HttpContext.Current is null");

      var qname =  HttpUtility.UrlEncode(Convert.ToBase64String(Encoding.Unicode.GetBytes(name)));
      
      HttpContext.Current.Response.Cookies[qname].Value = HttpUtility.UrlEncode(Convert.ToBase64String(Encoding.Unicode.GetBytes(value)));
    }

    public static string GetBase64Cookie(string name)
    {
      if (HttpContext.Current == null)
        throw new NullReferenceException("HttpContext.Current is null");

      var qname = HttpUtility.UrlEncode(Convert.ToBase64String(Encoding.Unicode.GetBytes(name)));

      return Encoding.Unicode.GetString(
                                Convert.FromBase64String(
                                  HttpUtility.UrlDecode(
                                      HttpContext.Current.Response.Cookies[qname].Value)));

    }

    //public static void SetEncryptedCookie(string name, string value)
    //{
    //  if (HttpContext.Current == null)
    //    throw new NullReferenceException("HttpContext.Current is null");

    //  var encryptName = SomeEncryptionMethod(name);
    //  Response.Cookies[encryptName].Value = SomeEncryptionMethod(value);
    //  //set other cookie properties here, expiry &c.
    //  //Response.Cookies[encryptName].Expires = ...
    //}

    //public static string GetEncryptedCookie(string name)
    //{
    //  if (HttpContext.Current == null)
    //    throw new NullReferenceException("HttpContext.Current is null");

    //  //you'll want some checks/exception handling around this
    //  return SomeDecryptionMethod(
    //             Response.Cookies[SomeDecryptionMethod(name)].Value);
    //}
  }
}
